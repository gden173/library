geostack.core package
=====================

Subpackages
-----------

.. toctree::
   :maxdepth: 2


Submodules
----------

geostack.core.json11 module
---------------------------

.. automodule:: geostack.core.json11
   :members:
   :undoc-members:
   :show-inheritance:

geostack.core.projection module
-------------------------------

.. automodule:: geostack.core.projection
   :members:
   :undoc-members:
   :show-inheritance:

geostack.core.property module
-----------------------------

.. automodule:: geostack.core.property
   :members: PropertyType, PropertyMap
   :undoc-members:
   :show-inheritance:

geostack.core.solver module
---------------------------

.. automodule:: geostack.core.solver
   :members:
   :undoc-members:
   :show-inheritance:

geostack.core.variables module
------------------------------

.. automodule:: geostack.core.variables
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: geostack.core
   :members:
   :undoc-members:
   :show-inheritance:
