# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

#cython: nonecheck=False
#cython: embedsignature=True
#cython: language_level=3
#cython: auto_pickle=False
#cython: c_string_encoding=utf8
#cython: c_string_type=unicode

from cython.operator cimport dereference as deref
from libcpp.memory cimport unique_ptr, shared_ptr
from libcpp.string cimport string
from libcpp.vector cimport vector
from libcpp cimport nullptr_t, nullptr
from libc.stdint cimport uint32_t, uint64_t
from libcpp cimport bool
import numpy as np
cimport numpy as np
from ..raster._cy_raster cimport _cyRaster_d, _cyRaster_f, Raster, RasterBase
from ..raster._cy_raster cimport _RasterPtrList_d, _RasterPtrList_f
from ..core._cy_property cimport PropertyMap

np.import_array()

cdef extern from "gs_multigrid.h" namespace "Geostack::MultigridLayers":
    cdef enum MultigridLayersType "Type":
        b
        l0
        l1
        bm
        lm0
        lm1
        MultigridLayers_END

cdef extern from "gs_multigrid.h" namespace "Geostack":
    cdef cppclass Multigrid[T]:
        Multigrid() except +
        bool init(string jsonConfig,
                  vector[shared_ptr[RasterBase[T]]] input_layers) except + nogil
        bool step() except + nogil
        Raster[T, T]& getForcing() except +
        Raster[T, T]& getForcingLevel(int level) except +
        Raster[T, T]& getSolution() except +
        bool pyramids() except +

include "_cy_multigrid.pxi"