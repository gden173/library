/* This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <iostream>
#include <functional>
#include "gs_utilities.h"



namespace Geostack
{
    // function called from c++
    std::string Proj4FromEPSG(proj4getter func, std::size_t epsgCode) {
        std::string out;
        out = func(epsgCode);
        return out;
    }

    // function called from python
    DLL00_EXPORT void callback_Proj4FromEPSG(void* pyfunction,
                                std::function<std::string(void*, std::size_t)> eval,
                                std::size_t epsgCode) {

        Proj4FromEPSG([&](std::size_t x) -> std::string {
            return eval(pyfunction, x);
            }, epsgCode);
    }
} // namespace Geostack
