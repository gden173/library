"""
Template for each `dtype` helper function for series

WARNING: DO NOT edit .pxi FILE directly, .pxi is generated from .pxi.in
"""

{{py:

# name, x_name, y_name

xtypes = [('dbl', '"float64"', 'double'),
         ('int', '"int64"', 'int64_t'),]
ytypes = [('dbl', '"float64"', 'double'),
         ('flt', '"float32"', 'float'),]
}}

{{for x_name, x_np, x_type in xtypes}}
{{for y_name, y_np, y_type in ytypes}}

cdef class Series_{{x_name}}_{{y_name}}:
    def __cinit__(self):
        self.thisptr.reset(new Series[{{x_type}}, {{y_type}}]())

    cdef void c_copy(self, Series[{{x_type}}, {{y_type}}]& c_ptr) except *:
        self.clear()
        self.thisptr.reset(new Series[{{x_type}}, {{y_type}}](c_ptr))

    cpdef void clear(self):
        if self.thisptr != nullptr:
            deref(self.thisptr).clear()

    cpdef void from_file(self, string fileName) except *:
        self.thisptr.reset(new Series[{{x_type}}, {{y_type}}](fileName))

    cpdef {{x_type}}[:] getAbscissas(self):
        cdef vector[{{x_type}}] vec
        cdef size_t vec_size
        cdef {{x_type}}[:] out
        if self.thisptr != nullptr:
            vec = deref(self.thisptr).getAbscissas()

            vec_size = vec.size()

            {{if x_name == "dbl"}}
            out = np.zeros(shape=vec_size, dtype=np.float64)
            {{elif x_name == "int"}}
            out = np.zeros(shape=vec_size, dtype=np.int64)
            {{endif}}

            memcpy(&out[0], vec.data(), sizeof({{x_type}})*vec_size)
            return out
        else:
            raise RuntimeError("Object is not initialised")

    cpdef {{x_type}} get_xMax(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).get_xMax()

    cpdef {{x_type}} get_xMin(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).get_xMin()

    cpdef {{y_type}}[:] getOrdinates(self):
        cdef vector[{{y_type}}] vec
        cdef size_t vec_size
        cdef {{y_type}}[:] out
        if self.thisptr != nullptr:
            vec = deref(self.thisptr).getOrdinates()
            vec_size = vec.size()

            {{if y_name == "dbl"}}
            out = np.zeros(shape=vec_size, dtype=np.float64)
            {{elif y_name == "flt"}}
            out = np.zeros(shape=vec_size, dtype=np.float32)
            {{endif}}

            memcpy(&out[0], vec.data(), sizeof({{y_type}})*vec_size)
            return out
        else:
            raise RuntimeError("Object is not initialised")

    cpdef {{y_type}} get_yMax(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).get_yMax()

    cpdef {{y_type}} get_yMin(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).get_yMin()

    cpdef bool isInitialised(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).isInitialised()

    cpdef bool isConstant(self):
        if self.thisptr != nullptr:
            return deref(self.thisptr).isConstant()

    cpdef string getName(self):
        cdef string out
        if self.thisptr != nullptr:
            out = deref(self.thisptr).getName()
            return out
        else:
            raise RuntimeError("Series is not yet initialised")

    cpdef void setName(self, string name):
        if self.thisptr != nullptr:
            deref(self.thisptr).setName(name)

    cpdef bool inRange(self, {{x_type}} x):
        if self.thisptr != nullptr:
            return deref(self.thisptr).inRange(x)

    cpdef void update(self, bool isSorted=False):
        if self.thisptr != nullptr:
            deref(self.thisptr).update(isSorted)

    cpdef void updateLimits(self):
        if self.thisptr != nullptr:
            deref(self.thisptr).updateLimits()

    cpdef void setBounds(self, {{y_type}} lowerLimit, {{y_type}} upperLimit):
        if self.thisptr != nullptr:
            deref(self.thisptr).setBounds(lowerLimit, upperLimit)

    cdef {{y_type}} get_value(self, {{x_type}} x):
        if self.thisptr != nullptr:
            return deref(self.thisptr).get_value(x)

    cdef {{y_type}}[:] get_values(self, {{x_type}}[:] x):
        cdef size_t vec_size
        cdef vector[{{x_type}}] vec_x
        cdef vector[{{y_type}}] vec_y
        cdef {{y_type}}[:] out

        if self.thisptr != nullptr:
            # copy x data to c++ vector
            vec_size = x.shape[0]
            vec_x.reserve(vec_size)
            vec_x.assign(&x[0], &x[0]+vec_size)

            # get y values
            vec_y = deref(self.thisptr).get_values(vec_x)

            # now copy y values to numpy array
            {{if y_name == "dbl"}}
            out = np.zeros(shape=vec_size, dtype=np.float64)
            {{elif y_name == "flt"}}
            out = np.zeros(shape=vec_size, dtype=np.float32)
            {{endif}}

            memcpy(&out[0], vec_y.data(), sizeof({{y_type}})*vec_size)
            return out
        else:
            raise RuntimeError("Object is not initialised")

    cpdef void from_series(self, Series_{{x_name}}_{{y_name}} other):
        self.thisptr.reset(new Series[{{x_type}}, {{y_type}}](deref(other.thisptr)))

    cpdef void add_string_value(self, string xval, {{y_type}} yval, bool isSorted=False):
        deref(self.thisptr).addValue(xval, yval, isSorted)

    cpdef void add_value(self, {{x_type}} xval, {{y_type}} yval, bool isSorted=False):
        deref(self.thisptr).addValue(xval, yval, isSorted)

    cpdef void add_values(self, {{x_type}}[:] xvals, {{y_type}}[:] yvals, bool isSorted=False):
        cdef size_t nx, i
        nx = <size_t> xvals.shape[0]
        for i in range(nx):
            deref(self.thisptr).addValue(xvals[i], yvals[i], isSorted)

    cpdef void setInterpolation(self, int interp_type):
        if self.thisptr != nullptr:
            if interp_type == SeriesInterpolationType.Linear:
                deref(self.thisptr).setInterpolation(SeriesInterpolationType.Linear)
            elif interp_type == SeriesInterpolationType.MonotoneCubic:
                deref(self.thisptr).setInterpolation(SeriesInterpolationType.MonotoneCubic)
            elif interp_type == SeriesInterpolationType.BoundedLinear:
                deref(self.thisptr).setInterpolation(SeriesInterpolationType.BoundedLinear)
            else:
                raise ValueError("interp_type should be 0,1,2")

    cpdef int getInterpolation(self):
        return deref(self.thisptr).getInterpolation()

    cpdef void setCapping(self, int capping_type):
        if self.thisptr != nullptr:
            if capping_type == SeriesCappingType.Uncapped:
                deref(self.thisptr).setCapping(SeriesCappingType.Uncapped)
            elif capping_type == SeriesCappingType.Capped:
                deref(self.thisptr).setCapping(SeriesCappingType.Capped)
            else:
                raise ValueError("capping_type should be 0,1")

    cpdef int getCapping(self):
        return deref(self.thisptr).getCapping()

    cpdef bool isSorted(self) except *:
        return deref(self.thisptr).isSorted()

    cpdef {{y_type}} total(self) except *:
        return deref(self.thisptr).total()

    cpdef {{y_type}} mean(self) except *:
        return deref(self.thisptr).mean()

    cpdef size_t getSize(self):
        return deref(self.thisptr).getSize()

    def __call__(self, x):
        if np.isscalar(x):
            return self.get_value(x)
        else:
            return self.get_values(x)

    def __repr__(self):
        return self.__class__.__name__

    def __dealloc__(self):
        if self.thisptr != nullptr:
            self.thisptr.reset()

    def __del__(self):
        try:
            self.__dealloc__()
        except Exception:
            pass

{{endfor}}
{{endfor}}
