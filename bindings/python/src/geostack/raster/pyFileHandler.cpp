/* This Source Code Form is subject to the terms of the Mozilla Public
   License, v. 2.0. If a copy of the MPL was not distributed with this
   file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <iostream>
#include <exception>
#include <memory>
#include <string>
#include "pyFileHandler.h"
#include "gs_raster.h"
#include "gs_vector.h"

namespace Geostack
{

namespace pyGeostack
{

template <typename RTYPE, typename CTYPE>
pyFileHandler<RTYPE, CTYPE>::pyFileHandler(data_reader py_f,
                    void *py_class, std::string filename) :
                    RasterFileHandler<RTYPE, CTYPE>(), py_function(py_f),
                    py_class_obj(py_class), fileName(filename)
{
    RasterFileHandler<RTYPE, CTYPE>::registerReadDataHandler(
        std::bind(&pyFileHandler<RTYPE, CTYPE>::dataFunction,
                  this, std::placeholders::_1,
                  std::placeholders::_2));
};

template <typename RTYPE, typename CTYPE>
void pyFileHandler<RTYPE, CTYPE>::read(std::string fileName,
                                       Raster<RTYPE, CTYPE> &r_,
                                       std::string jsonConfig)
{
}

template <typename RTYPE, typename CTYPE>
void pyFileHandler<RTYPE, CTYPE>::write(std::string fileName,
                                        Raster<RTYPE, CTYPE> &r_,
                                        std::string jsonConfig)
{
}

template <typename RTYPE, typename CTYPE>
std::string pyFileHandler<RTYPE, CTYPE>::getFileName()
{
    return fileName;
}

template <typename RTYPE, typename CTYPE>
void pyFileHandler<RTYPE, CTYPE>::dataFunction(TileDimensions<CTYPE> tdim,
                                               std::vector<RTYPE> &vec)
{
    pyException outException;
    outException = py_function(py_class_obj, tdim, vec);
    if (outException.rc == -1)
    {
        throw std::runtime_error(outException.errMessage);
    }
    else if (outException.rc == -2)
    {
        throw std::out_of_range(outException.errMessage);
    }
    else if (outException.rc == -3)
    {
        throw std::invalid_argument(outException.errMessage);
    }
    else if (outException.rc == -4)
    {
        std::cout << outException.errMessage << std::endl;
        throw std::bad_cast();
    }
    else if (outException.rc == -5)
    {
        throw std::invalid_argument(outException.errMessage);
    }
    else if (outException.rc == -6)
    {
        throw std::invalid_argument(outException.errMessage);
    };
}

template <typename CTYPE>
void add_ref_to_vec(std::vector<RasterBaseRef<CTYPE>> &v,
                    RasterBase<CTYPE> &rf)
{
    // std::string name = rf.template getProperty<std::string>("name");
    // std::cout << "@Cython:: Address of " << name << " when adding to RasterBaseList: " << &rf << std::endl;
    v.emplace_back(std::ref(rf));
}

template <typename CTYPE>
RasterBase<CTYPE>& get_raster_ref_from_vec(std::vector<RasterBaseRef<CTYPE>> &v,
                                          std::size_t i)
{
    return v[i].get();
}

template <typename RTYPE, typename CTYPE>
void add_raster_ptr_to_vec(std::vector<std::shared_ptr<RasterBase<CTYPE>>> &v,
                           std::shared_ptr<Raster<RTYPE, CTYPE>> &rf)
{
    v.emplace_back(rf);
}

template <typename CTYPE>
std::shared_ptr<RasterBase<CTYPE>> get_raster_ptr_from_vec(
    std::vector<std::shared_ptr<RasterBase<CTYPE>>> &v, std::size_t i)
{
    return v[i];
}

template <typename CTYPE>
void add_vector_ptr_to_vec(std::vector<std::shared_ptr<Vector<CTYPE>>> &v,
                           std::shared_ptr<Vector<CTYPE>> &vec)
{
    v.emplace_back(vec);
}

template <typename CTYPE>
std::shared_ptr<Vector<CTYPE>> get_vector_ptr_from_vec(
    std::vector<std::shared_ptr<Vector<CTYPE>>> &v, std::size_t i)
{
    return v[i];
}

//RTYPE float, CTYPE float
template class pyFileHandler<float, float>;

//RTYPE uint32_t, CTYPE float
template class pyFileHandler<uint32_t, float>;

//RTYPE uint8_t, CTYPE float
template class pyFileHandler<uint8_t, float>;

//RTYPE double, CTYPE double
template class pyFileHandler<double, double>;

//RTYPE uint32_t, CTYPE double
template class pyFileHandler<uint32_t, double>;

//RTYPE uint8_t, CTYPE double
template class pyFileHandler<uint8_t, double>;


} // namespace pyGeostack

template void pyGeostack::add_ref_to_vec(std::vector<RasterBaseRef<double>> &v,
                                         RasterBase<double> &rf);
template void pyGeostack::add_ref_to_vec(std::vector<RasterBaseRef<float>> &v,
                                         RasterBase<float> &rf);

template void pyGeostack::add_raster_ptr_to_vec(std::vector<std::shared_ptr<RasterBase<double>>> &v,
                                    std::shared_ptr<Raster<double, double>> &rf);
template void pyGeostack::add_raster_ptr_to_vec(std::vector<std::shared_ptr<RasterBase<float>>> &v,
                                    std::shared_ptr<Raster<float, float>> &rf);
template void pyGeostack::add_raster_ptr_to_vec(std::vector<std::shared_ptr<RasterBase<float>>> &v,
                                    std::shared_ptr<Raster<uint32_t, float>> &rf);
template void pyGeostack::add_raster_ptr_to_vec(std::vector<std::shared_ptr<RasterBase<double>>> &v,
                                    std::shared_ptr<Raster<uint32_t, double>> &rf);
template void pyGeostack::add_raster_ptr_to_vec(std::vector<std::shared_ptr<RasterBase<float>>> &v,
                                    std::shared_ptr<Raster<uint8_t, float>> &rf);
template void pyGeostack::add_raster_ptr_to_vec(std::vector<std::shared_ptr<RasterBase<double>>> &v,
                                    std::shared_ptr<Raster<uint8_t, double>> &rf);

template void pyGeostack::add_vector_ptr_to_vec(std::vector<std::shared_ptr<Vector<double>>> &v,
                                    std::shared_ptr<Vector<double>> &vec);
template void pyGeostack::add_vector_ptr_to_vec(std::vector<std::shared_ptr<Vector<float>>> &v,
                                    std::shared_ptr<Vector<float>> &vec);

template std::shared_ptr<RasterBase<double>> pyGeostack::get_raster_ptr_from_vec(
    std::vector<std::shared_ptr<RasterBase<double>>> &v, std::size_t i);
template std::shared_ptr<RasterBase<float>> pyGeostack::get_raster_ptr_from_vec(
    std::vector<std::shared_ptr<RasterBase<float>>> &v, std::size_t i);

template std::shared_ptr<Vector<double>> pyGeostack::get_vector_ptr_from_vec(
    std::vector<std::shared_ptr<Vector<double>>> &v, std::size_t i);
template std::shared_ptr<Vector<float>> pyGeostack::get_vector_ptr_from_vec(
    std::vector<std::shared_ptr<Vector<float>>> &v, std::size_t i);

template RasterBase<double>& pyGeostack::get_raster_ref_from_vec(std::vector<RasterBaseRef<double>> &v,
                                                                 std::size_t i);
template RasterBase<float>& pyGeostack::get_raster_ref_from_vec(std::vector<RasterBaseRef<float>> &v,
                                                                std::size_t i);
} // namespace Geostack
