# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
 
import json
import numpy as np

from geostack.raster import Raster, RasterPtrList
from geostack.vector import Vector
from geostack.solvers import LevelSet, Multigrid
from geostack.io import vectorToGeoJson

# Create start points
v = Vector()
point_id = v.addPoint( [-20, 0] )
v.setProperty(point_id, "radius", 5)
point_id = v.addPoint( [20, 0] )
v.setProperty(point_id, "radius", 5)

#line_id = v.addLineString( [[-20, 0], [20, 0]] )
#v.setProperty(line_id, "radius", 5)

# Create layers
layers = RasterPtrList()
source = Raster(name = 'source')
solution = Raster(name = 'solution')
layers.append(source)
layers.append(solution)

# Create configurations
config_level_set = {
    "resolution": 1.0,
    "initialisationScript": '''
        source = 0;
    ''',
    "advectionScript": '''
    
        // Gradient of field
        REALVEC2 g = -0.1*grad(solution);
        
        // Update advection vectors
        advect_x = g.x;
        advect_y = 0.5+g.y;
    ''',
    "buildScript": '''
    
        // Set rate-of-spread
        speed = 0.25+advect_dot_normal;
    ''',
    "updateScript": '''
    
        // Set source to be area burnt in the last 10 s
        source = 0;
        if (time - arrival < 10.0) {
            source = time_step*speed;
        }
    '''
}

config_multigrid = {
    "initialisationScript": '''
    
        // Set new source
        b = source;
    ''',
    "updateScript": '''
    
        // Copy solution from internal variable
        solution = l0;
    '''
}

# Create solvers
solver_level_set = LevelSet()
solver_multigrid = Multigrid()

# Initialise solvers
solver_level_set.init(json.dumps(config_level_set), v, outputLayers = layers)
solver_multigrid.init(json.dumps(config_multigrid), inputLayers = layers)
    
# Run solvers
while solver_level_set.getParameters()['time'] <= 100.0:
    solver_level_set.step()
    solver_multigrid.step()

# Write isochrone file
isochroneVector = solver_level_set.getArrival().vectorise(
    np.arange(0, 100, 10), 100)
with open('./_out_level_set_multigrid_iso.json', 'w') as outfile:
    outfile.write(vectorToGeoJson(isochroneVector, enforceProjection = False))
    
# Write source
source.write('./_out_level_set_multigrid_source.tiff')
solution.write('./_out_level_set_multigrid_solution.tiff')
          
print("Done")