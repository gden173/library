# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
 
import sys
import json
import numpy as np

from geostack.raster import Raster, RasterPtrList
from geostack.vector import Vector
from geostack.runner import runScript
from geostack.solvers import LevelSet
from geostack.core import Variables
from geostack.io import vectorToGeoJson

# Create vector layer
v = Vector()
point_id = v.addPoint( [0, 0] )
v.setProperty(point_id, "radius", 10)

# Create solver
solver = LevelSet()

# Level set configuration
config = {
    "resolution": 1.0,
    "advectionScript": '''
        advect_x = 1.0;
        advect_y = 0.0;
    ''',
    "initialisationScript": '''
        if (x > 50 && x < 55 && (y < -5 || y > 5)) {
            class = 0;
        } else {
            class = 1;
        }
    ''',
    "buildScript": '''
        if (class == 1) { 
            speed = 1.0;
            
            REAL ndot = dot(normalize(advect_vector), advect_normal_vector);

            // Defining an arbitrary length to breadth ratio
            REAL LBR = 3;

            // Determine coefficient for backing and flanking rank of spread using elliptical equations
            // Where R_backing = cb * R_head, R_flanking = cf * R_head,
            REAL cc = sqrt(1.0-pow(LBR, -2.0));
            REAL cb = (1.0-cc)/(1.0+cc);
            REAL a_LBR = 0.5*(cb+1.0);
            REAL cf = a_LBR/LBR;

            // Determine shape parameters
            REAL f = 0.5*(1.0+cb);
            REAL g = 0.5*(1.0-cb);
            REAL h = cf;

            // Now calculate the speed coefficient using normal flow formula
            speed *= (g*ndot+sqrt(h*h+(f*f-h*h)*ndot*ndot));
        }
    '''
}

solver.init(json.dumps(config), v)
    
while solver.getParameters()['time'] <= 100.0:
    solver.step()
        
solver.getClassification().write('./_out_level_set_gap_ellipse_class.tiff')
        
# Generate isochrones
isochroneVector = solver.getArrival().vectorise(
    np.arange(0, 100, 10), 100)

# Write isochrone file
with open('./_out_level_set_gap_ellipse_iso.json', 'w') as outfile:
    outfile.write(vectorToGeoJson(isochroneVector, enforceProjection = False))
            
print("Done")