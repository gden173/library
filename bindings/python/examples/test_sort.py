# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import numpy as np

from geostack.raster import Raster, sortColumns
from geostack.runner import runScript

def printRaster(r):
    v = []
    for k in range(0, 10):
        v.append(r.getCellValue(50, 50, k))
    print(v)

# Float sort
print("Float sort:")
test_float = Raster(name="test_float")
test_float.init(nx = 100, ny = 100, nz = 10, hx = 1, hy = 1, hz = 1)
runScript("test_float = 10-z;", [test_float])
printRaster(test_float)
sortColumns(test_float, inplace = True)
printRaster(test_float)
print()

# Int sort
print("Int sort:")
test_int = Raster(name="test_int", base_type=np.float32, data_type=np.uint32)
test_int.init(nx = 100, ny = 100, nz = 10, hx = 1, hy = 1, hz = 1)
runScript("test_int = 10-kpos;", [test_int])
printRaster(test_int)
sortColumns(test_int, inplace = True)
printRaster(test_int)
print()

# Byte sort
print("Byte sort:")
test_byte = Raster(name="test_byte", base_type=np.float32, data_type=np.uint8)
test_byte.init(nx = 100, ny = 100, nz = 10, hx = 1, hy = 1, hz = 1)
runScript("test_byte = 10-kpos;", [test_byte])
printRaster(test_byte)
sortColumns(test_byte, inplace = True)
printRaster(test_byte)