# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

import json
import numpy as np

from geostack.vector import Vector
from geostack.solvers import Particle
from geostack.io import vectorToGeoJson

# Create initial conditions
particles = Vector()
particles.addPoint( [0, 0, 0] )

# Create solver
config = {
    "dt": 0.01,
    "initialisationScript" : '''
        radius = 0.1; 
        velocity = (REALVEC3)(10.0, 10.0, 10.0);
    ''',
    "advectionScript" : '''
        advect = (REALVEC3)(0.0, 0.0, 0.0);
        time += dt;
        if (position.z < 0.0) {
            position.z = 0.0;
            velocity.z = -0.75*velocity.z;
        }
    ''',
    "updateScript" : '''
        // Drag coefficient
        const REAL cd = 0.42;
        
        // Ratio of air to particle densities
        const REAL rho_diff = 1.2/250.0;
        
        // Relative velocity, wind vector must be in m/s
        REALVEC3 rel_velocity = advect-velocity;
        
        // Set particle acceleration:
        // a = F = 0.5*rho_gas*cd*area*u^2
        //     -   -----------------------
        //     m     rho_particle*volume
        acceleration = (0.375*rho_diff*cd/radius)*length(rel_velocity)*rel_velocity;
        
        // Apply gravity
        acceleration.z -= 9.8;
    '''
}
solver = Particle()
solver.init(json.dumps(config), particles)

# Run solver
c = [particles.getPointCoordinate(0).getCoordinate()]
for iter in range(0, 1000):
    solver.step()    
    c.append(particles.getPointCoordinate(0).getCoordinate())
      
# Unpack lists
x, y, z, t = zip(*c)

# Plot
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
fig = plt.figure()
ax = plt.axes(projection = "3d")
#ax.plot(x, y, z)
ax.scatter3D(x, y, z, c=t, cmap="viridis")
plt.show()

print("Done")