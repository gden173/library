# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

from geostack.raster import Raster
from geostack.vector import Vector
from geostack.runner import runScript, runVectorScript
from geostack.io import vectorToGeoJson
from geostack.gs_enums import ReductionType

# Create raster layer
A = Raster(name = "A")

# Initialize Raster
A.init(nx = 10, ny = 10, nz = 10, hx = 10.0, hy = 10.0, hz = 1.0)

runScript("A = z;", [A])

# Create vector
v = Vector()
v.addPoint( [50, 50] )
v.addProperty("max")
v.addProperty("min")
v.addProperty("all")

# Run script
runVectorScript("max = A;", v, [A], ReductionType.Maximum)
runVectorScript("min = A;", v, [A], ReductionType.Minimum)
runVectorScript("all = A;", v, [A])

# Write vector
with open('./_out_vector_script.json', 'w') as outfile:
    outfile.write(vectorToGeoJson(v, enforceProjection = False))
    
print("Done")

