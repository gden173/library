{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "96b8b6eb-be07-46bf-be87-4d08bfcdf988",
   "metadata": {},
   "source": [
    "## 2. Processing raster data\n",
    "\n",
    "A general-purpose function called _runScript_ can be used for intensive processing of geospatial raster data. This function takes a _script_ along with a set of Rasters and runs the script on each cell of the raster. A simple example using the Raster layer in the previous example is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "5b49576f-f445-455b-ac9a-9a2d1077865e",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1007.5\n",
      "nan\n"
     ]
    }
   ],
   "source": [
    "from geostack.raster import Raster\n",
    "from geostack.runner import runScript\n",
    "\n",
    "# Create raster\n",
    "r = Raster(name = \"r\")\n",
    "r.read(\"./data/01_example.tif\")\n",
    "print(r.getCellValue(1, 1))\n",
    "\n",
    "# Run script\n",
    "s = runScript(\"output = noData_REAL\", [r])\n",
    "print(s.getCellValue(1, 1))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cb97fcce-4b0c-4f16-a292-ada130d17fac",
   "metadata": {},
   "source": [
    "The script is applied in parallel to each cell in the raster, the centres of which are shown as grey dots in the figure below. In each cell the current value of the cell in Raster _r_ is read, multiplied by two and written to the output Raster _s_. The formula in the script is automatically applied in parallel using whatever multi-core CPU or GPU hardware is available to Geostack.\n",
    "\n",
    "![image](https://gitlab.com/geostack/library/-/wikis/uploads/037a1c2cd41afd8b81e1c2ed908d0c6b/Fig_cell_points.png)\n",
    "\n",
    "The special variable _output_ refers to the value of the output data written to _s_. This script sets every cell of _s_ to 2_r_, where _r_ in the above script means \"the value of r in the current cell\".\n",
    "\n",
    "Scripts must be written in C, as Geostack uses OpenCL for the computation which requires C code as input.\n",
    "\n",
    "A more interesting application is to calculate a new Raster based on a formula. The example file _01_example.tif_ is a 6-layer multi-layer raster from Sentinel 2 data, with bands B2, B3, B4, B8, B8A and B12 in 6 layers.\n",
    "\n",
    "![image](https://gitlab.com/geostack/library/-/wikis/uploads/94a3319adf5a4fcc19293aaf26f48f81/Fig_raster_6_layer_Sentinel.png)\n",
    "\n",
    "The normalized difference vegetation index (NDVI) can be calculated from this using the formula (B8-B4)/(B8+B4). To implement this in Geostack the script is:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "0988f194-12db-4bc5-afb0-4d94ed354096",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Calculate NDVI\n",
    "NDVI = runScript(\"output = (r[3]-r[2])/(r[3]+r[2])\", [r]);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b27e7353-3785-4fec-9b95-fd2c2043e895",
   "metadata": {},
   "source": [
    "Again, _output_ is used to define the cell value of the new NDVI Raster created. Cells in different layers of a multi-level raster can be indexed (from zero) using square brackets, so here r[2] is B4 and r[3] is B8. Importing into QGIS:\n",
    "\n",
    "![image](https://gitlab.com/geostack/library/-/wikis/uploads/5bdfea57f5afde5ebf5d6c3078129ac0/image.png)\n",
    "\n",
    "The ability is process a set of stacked geospatial layers (or a stack of layers!) is where the name _Geostack_ comes from.\n",
    "\n",
    "The script is not entirely efficient as the resulting raster is, by default, run over _all_ cells of the input Raster _r_. This means the output NDVI raster has six identical layers. To create only one layer the NDVI Raster must be manually created with the same dimensions as the r Raster, but only one layer.\n",
    "\n",
    "![image](https://gitlab.com/geostack/library/-/wikis/uploads/61833bcccfddd31ee8865aef7e72bef4/Fig_raster_6_layer_Sentinel_out.png)\n",
    "\n",
    "This can be implemented by getting the dimensions of _r_ and creating _NDVI_ as a 2D Raster. One additional step is to set the projection parameters of _NDVI_ manually from _r_ using the _getProjectionParameters_ and _setProjectionParameters_ functions:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "81a7a29a-9612-45f8-b23d-ca1ba2361411",
   "metadata": {},
   "outputs": [],
   "source": [
    "NDVI = Raster(name = \"NDVI\")\n",
    "dims = r.getDimensions()\n",
    "NDVI.init(\n",
    "    nx = dims.nx, ny = dims.ny,\n",
    "    hx = dims.hx, hy = dims.hy,\n",
    "    ox = dims.ox, oy = dims.oy,\n",
    ")\n",
    "NDVI.setProjectionParameters(r.getProjectionParameters())\n",
    "\n",
    "runScript(\"NDVI = (r[3]-r[2])/(r[3]+r[2])\", [NDVI, r]);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8fbbeae3-8994-4b02-bb99-4e927256d467",
   "metadata": {},
   "source": [
    "The NDVI Raster now only has one layer. The _runScript_ function runs over the cells of the _first Raster specified_, called the anchor layer. Here the anchor is _NDVI_, whereas in the previous example it was _r_. Note that the script no longer has an output layer, instead the NDVI Raster is populated directly in the script.\n",
    "\n",
    "There are various rules for how these Rasters can be referenced:\n",
    "- If a Raster has the same origin and _two-dimensional_ spacing and number of cells as the anchor it's _aligned_. Here _NDVI_ and _r_ are aligned, even though _NDVI_ is three-dimensional and _r_ is two-dimensional.\n",
    "- Different levels from aligned Raster can be referenced using square brackets. Here _NDVI_ is using different levels from _r_.\n",
    "- If Rasters are _not_ aligned the position of the cell centre is transformed into the unaligned Raster projection and the value is interpolated from the unaligned Raster.\n",
    "\n",
    "The third point allows rasters with different projections to be used in the script. For example, a coded land use layer is shown below.\n",
    "\n",
    "![image](https://gitlab.com/geostack/library/-/wikis/uploads/5feceefc3ab72bf7d1f2e14004d87f23/image.png)\n",
    "\n",
    "The data is in a projected coordinate system, whereas the Sentinel data is in a geospatial coordinate system. We'd like to only calculate the NDVI within conservation or forested regions, which are classes lower than 300 in the land use dataset. These are coloured green and purple above. This simply requires the data to be read and a threshold applied:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "31bfa60e-49af-4bc2-9573-9fab8fa1dce1",
   "metadata": {},
   "outputs": [],
   "source": [
    "land = Raster(name = \"land\")\n",
    "land.read(\"./data/02_example.tif\")\n",
    "\n",
    "NDVI = Raster(name = \"NDVI\")\n",
    "dims = r.getDimensions()\n",
    "NDVI.init(\n",
    "    nx = dims.nx, ny = dims.ny,\n",
    "    hx = dims.hx, hy = dims.hy,\n",
    "    ox = dims.ox, oy = dims.oy,\n",
    ")\n",
    "NDVI.setProjectionParameters(r.getProjectionParameters())\n",
    "\n",
    "runScript(\"if (land < 300) NDVI = (r[3]-r[2])/(r[3]+r[2])\", [NDVI, r, land]);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20213676-59ac-4a37-ab0f-705b9380389d",
   "metadata": {},
   "source": [
    "The results are:\n",
    "\n",
    "![image](https://gitlab.com/geostack/library/-/wikis/uploads/1054c8f5e6a46b4d2dfa940850280388/image.png)\n",
    "\n",
    "The script is run over each cell of the _NDVI_ raster layer. When a _land_ value is needed the centroid of the cell is projected into the coordinate system and sampled from the _land_ Raster.\n",
    "\n",
    "![image](https://gitlab.com/geostack/library/-/wikis/uploads/11150ce230a153a51355479f4a421d94/Fig_raster_6_layer_Sentinel_land_out.png)\n",
    "\n",
    "This is one of the major benefits of using Geostack when manipulating Raster data. All layers geospatial resampling and projection is handled on-the-fly without user intervention. As long as both layers have valid projection information Geostack can transparently handle any number of layers in the script allowing concise code without the user worring about re-projection. Of course, there are a wide range of options to specify how this sampling is carried out and how to handle nodata values.\n",
    "\n",
    "In the above case regions where the NDVI is not calculated are marked as nodata. The existence of nodata can be checked using the isValid_REAL(v) or isInvalid_REAL(v) functions for floating-point data and isValid_INT(v) or isInvalid_INT(v) for integer data. For example, to change all nodata values to -1, the following script could be used:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "d98aed03-8c3d-424b-974b-1d499f32ed14",
   "metadata": {},
   "outputs": [],
   "source": [
    "runScript(\"if (isInvalid_REAL(NDVI)) NDVI = -1;\", [NDVI]);"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "55ac8679-c0ef-4d0c-a82e-ff0b8272e963",
   "metadata": {},
   "source": [
    "### Reductions\n",
    "\n",
    "An reduction, or aggregation, over a raster is often needed to find the maximum, minimum or average value over all values. Each Raster has an optional flag that can be set specifying a reduction to perform. For example to find the maximum NDVI value the _setReductionType_ method can be used with _ReductionType.Maximum_. The reduction is calculated whenever a script is run, with the result being available in the _reduceVal_ variable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "39dc2bbe-54a7-4b48-9832-f2f18c412d50",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Max: 0.7437499761581421\n"
     ]
    }
   ],
   "source": [
    "from geostack.definitions import ReductionType\n",
    "\n",
    "NDVI.setReductionType(ReductionType.Maximum)\n",
    "\n",
    "runScript(\"if (land < 300) NDVI = (r[3]-r[2])/(r[3]+r[2])\", \n",
    "    [NDVI, r, land])\n",
    "\n",
    "print(f\"Max: {NDVI.reduceVal}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1be3985e-dfbe-4f2a-9f0a-6be2293642f0",
   "metadata": {},
   "source": [
    "### Sorting data\n",
    "\n",
    "Another common operation is to find quartiles over a Raster data set. If the data is arranged such that the third dimension holds a column of data values (for example this is a time or frequency dimension) Geostack provides an efficient in-place column-wise sorting operation though the _sortColumns_ function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "9e70e434-9107-4935-86f8-f2d27d6ef7da",
   "metadata": {},
   "outputs": [],
   "source": [
    "from geostack.raster import sortColumns\n",
    "\n",
    "r.read(\"./data/01_example.tif\")\n",
    "\n",
    "sortColumns(r, inplace = True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "82397531-a41f-40e1-b322-067f9c127093",
   "metadata": {},
   "source": [
    "The data is arranged from minimum to maximum in each column after sorting:\n",
    "\n",
    "![image](https://gitlab.com/geostack/library/-/wikis/uploads/a40f026b7824b5617464b4c608dbcb1d/Fig_raster_6_layer_column.png)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "dd1275e9-d545-4f9e-939c-e9c248a77a62",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Min: 751.0\n",
      "Median: 1142.0\n",
      "Max: 1255.0\n"
     ]
    }
   ],
   "source": [
    "print(f\"Min: {r.getCellValue(0, 0, 0)}\")\n",
    "print(f\"Median: {r.getCellValue(0, 0, 3)}\")\n",
    "print(f\"Max: {r.getCellValue(0, 0, 5)}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd700889-efe2-4d6e-8f58-094aa5287e90",
   "metadata": {},
   "source": [
    "(Note that the sort over this data is only used for illustration here as these are different Sentinel bands.)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c8102c9b",
   "metadata": {},
   "source": [
    "### Area scripts\n",
    "Some spatial algorithms require reduction or aggregation over a window of neighbouring cell values. An efficient function for this type of processing is the _runAreaScript_ operation. This allows a two-dimensional window width, in cell units, to be specified and runs over all cells within this window surrounding the current cell. For example the dilation operator can be applied using:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "148f3cd1",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[nan nan nan nan nan nan nan]\n",
      " [nan nan nan nan nan nan nan]\n",
      " [nan nan nan nan nan nan nan]\n",
      " [nan nan nan  1. nan nan nan]\n",
      " [nan nan nan nan nan nan nan]\n",
      " [nan nan nan nan nan nan nan]\n",
      " [nan nan nan nan nan nan nan]]\n",
      "\n",
      "[[nan nan nan nan nan nan nan]\n",
      " [nan  1.  1.  1.  1.  1. nan]\n",
      " [nan  1.  1.  1.  1.  1. nan]\n",
      " [nan  1.  1.  1.  1.  1. nan]\n",
      " [nan  1.  1.  1.  1.  1. nan]\n",
      " [nan  1.  1.  1.  1.  1. nan]\n",
      " [nan nan nan nan nan nan nan]]\n"
     ]
    }
   ],
   "source": [
    "from geostack.runner import runAreaScript\n",
    "\n",
    "# Create raster layer\n",
    "r = Raster(name = \"r\")\n",
    "\n",
    "# Initialize raster layer\n",
    "r.init(nx = 7, ny = 7, hx = 1.0, hy = 1.0)\n",
    "\n",
    "# Populate with example data\n",
    "r.setCellValue(1, 3, 3)\n",
    "print(r.data)\n",
    "print()\n",
    "\n",
    "# Run area script\n",
    "r = runAreaScript(\"output = max(output, r)\", r, 2)\n",
    "print(r.data)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "04be9b43",
   "metadata": {},
   "source": [
    "Or a convolution with a Gaussian kernel can be performed by using the internal variable _dist_sqr_, giving the squared distance in cell units from the central pixel and the final denominator applied to output _sum_:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "08736fe2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[[0. 0. 0. 0. 0. 0. 0.]\n",
      " [0. 0. 0. 0. 0. 0. 0.]\n",
      " [0. 0. 0. 0. 0. 0. 0.]\n",
      " [0. 0. 0. 1. 0. 0. 0.]\n",
      " [0. 0. 0. 0. 0. 0. 0.]\n",
      " [0. 0. 0. 0. 0. 0. 0.]\n",
      " [0. 0. 0. 0. 0. 0. 0.]]\n",
      "\n",
      "[[0.         0.         0.         0.         0.         0.\n",
      "  0.        ]\n",
      " [0.         0.         0.         0.         0.         0.\n",
      "  0.        ]\n",
      " [0.         0.         0.10740843 0.11291539 0.10740843 0.\n",
      "  0.        ]\n",
      " [0.         0.         0.11291539 0.11870468 0.11291539 0.\n",
      "  0.        ]\n",
      " [0.         0.         0.10740843 0.11291539 0.10740843 0.\n",
      "  0.        ]\n",
      " [0.         0.         0.         0.         0.         0.\n",
      "  0.        ]\n",
      " [0.         0.         0.         0.         0.         0.\n",
      "  0.        ]]\n"
     ]
    }
   ],
   "source": [
    "# Create raster layer\n",
    "r = Raster(name = \"r\")\n",
    "\n",
    "# Initialize raster layer\n",
    "r.init(nx = 7, ny = 7, hx = 1.0, hy = 1.0)\n",
    "\n",
    "# Populate with example data\n",
    "r.setAllCellValues(0)\n",
    "r.setCellValue(1, 3, 3)\n",
    "print(r.data)\n",
    "print()\n",
    "\n",
    "# Run area script\n",
    "r = runAreaScript('''\n",
    "    if (isValid_REAL(r)) {\n",
    "        REAL ssigma = 10.0;\n",
    "        REAL csigma = 1.0/(2.0*ssigma);\n",
    "        REAL G = exp(-csigma*dist_sqr)/(2.0*M_PI*ssigma);\n",
    "\n",
    "        if (isValid_REAL(output)) {\n",
    "            output += G*r; \n",
    "            sum += G;\n",
    "        } else {\n",
    "            output = G*r; \n",
    "            sum = G;\n",
    "        }\n",
    "    }\n",
    "    ''', r, 1)\n",
    "print(r.data)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
