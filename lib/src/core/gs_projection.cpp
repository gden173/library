/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <vector>
#include <map>
#include <set>
#include <algorithm>
#include <cmath>
#include <sstream>
#include <regex>
#include <iostream>

#include "gs_models.h"
#include "gs_opencl.h"
#include "gs_geometry.h"
#include "gs_string.h"
#include "gs_vector.h"
#include "gs_projection.h"
#include "gs_epsg.h"

// Include code for projection
#include "cl_projection_head.c"

using namespace json11;

namespace Geostack
{
    /**
    * %ProjectionParameters constructor
    */
    template <typename CTYPE>
    ProjectionParameters<CTYPE>::ProjectionParameters() {
        type = 0;
        cttype = 0;
        a = CTYPE();
        f = CTYPE();
        x0 = CTYPE();
        k0 = CTYPE();
        fe = CTYPE();
        fn = CTYPE();
        phi_0 = CTYPE();
        phi_1 = CTYPE();
        phi_2 = CTYPE();
    }

    /**
    * %ProjectionParameters copy constructor
    */
    template <typename CTYPE>
    template <typename DTYPE>
    ProjectionParameters<CTYPE>::ProjectionParameters(const ProjectionParameters<DTYPE> &r) {

        // Copy and convert data
        type = r.type;
        cttype = r.cttype;
        a = (CTYPE)r.a;
        f = (CTYPE)r.f;
        x0 = (CTYPE)r.x0;
        k0 = (CTYPE)r.k0;
        fe = (CTYPE)r.fe;
        fn = (CTYPE)r.fn;
        phi_0 = (CTYPE)r.phi_0;
        phi_1 = (CTYPE)r.phi_1;
        phi_2 = (CTYPE)r.phi_2;
    }

    /**
    * %ProjectionParameters assignment operator
    */
    template <typename CTYPE>
    ProjectionParameters<CTYPE> &ProjectionParameters<CTYPE>::operator=(ProjectionParameters<CTYPE> const &r) {

        if (this != &r) {

            // Copy data
            type = r.type;
            cttype = r.cttype;
            a = (CTYPE)r.a;
            f = (CTYPE)r.f;
            x0 = (CTYPE)r.x0;
            k0 = (CTYPE)r.k0;
            fe = (CTYPE)r.fe;
            fn = (CTYPE)r.fn;
            phi_0 = (CTYPE)r.phi_0;
            phi_1 = (CTYPE)r.phi_1;
            phi_2 = (CTYPE)r.phi_2;
        }

        return *this;
    }

    /**
    * Convert %ProjectionParameters to string
    */
    template <typename CTYPE>
    std::string ProjectionParameters<CTYPE>::str() {

        std::ostringstream ps;
        ps << (int)type << " "
           << (int)cttype << " "
           << a << " "
           << f << " "
           << x0 << " "
           << k0 << " "
           << fe << " "
           << fn << " "
           << phi_0 << " "
           << phi_1 << " "
           << phi_2;
        return ps.str();
    }

    /**
    * Singleton instance of %ProjectionTable.
    */
    ProjectionTable &ProjectionTable::instance() {

        static ProjectionTable projectionOperator;
        return projectionOperator;
    }

    /**
    * %ProjectionTable initialisation
    */
    void ProjectionTable::initialise() {

        if (!initialised) {

            // Build datums
            std::string datum;
            std::string datumCSV = std::string(R_PROJ4_datum_csv);
            std::istringstream datumCSVStream(datumCSV);

            // Skip header
            std::getline(datumCSVStream, datum);

            // Parse data
            while (std::getline(datumCSVStream, datum)) {

                // Clear whitespace
                datum = Strings::removeWhitespace(datum);

                // Get parts
                auto datumParts = Strings::split(datum, ',');

                // Check data
                if (datumParts.size() != 2) {
                    throw std::runtime_error("Invalid number of entries in PROJ4_datum csv file");
                } else {

                    // Add to table
                    datumTable[Strings::toUpper(datumParts[0])] = Strings::toUpper(datumParts[1]);
                }
            }

            // Build ellipsoids
            std::string ellipsoid;
            std::string ellipsoidCSV = std::string(R_PROJ4_ellipsoid_csv);
            std::istringstream ellipsoidCSVStream(ellipsoidCSV);

            // Skip header
            std::getline(ellipsoidCSVStream, ellipsoid);

            // Parse data
            while (std::getline(ellipsoidCSVStream, ellipsoid)) {

                // Clear whitespace
                ellipsoid = Strings::removeWhitespace(ellipsoid);

                // Get parts
                auto ellipsoidParts = Strings::split(ellipsoid, ',');

                // Check data
                if (ellipsoidParts.size() != 4) {
                    throw std::runtime_error("Invalid number of entries in PROJ4_ellipsoid csv file");
                } else {

                    // Check values
                    if (!Strings::isNumber(ellipsoidParts[1]) ||
                        !Strings::isNumber(ellipsoidParts[2]) ||
                        !Strings::isNumber(ellipsoidParts[3])) {
                        throw std::runtime_error("Invalid numeric data in PROJ4_ellipsoid csv file");
                    }

                    // Create values
                    std::vector<double> values = {
                        (double)Strings::toNumber<double>(ellipsoidParts[1]),   // Equatorial radius
                        (double)Strings::toNumber<double>(ellipsoidParts[2]) }; // Inverse flattening

                    // Add to tables
                    std::string ellipsoidName = Strings::toUpper(ellipsoidParts[0]);
                    ellipsoidTable[ellipsoidName] = values;
                    ellipsoidCode[ellipsoidName] = (uint32_t)Strings::toNumber<uint32_t>(ellipsoidParts[3]);
                }
            }

            // Set flag
            initialised = true;
        }
    }

    /**
    * Get datum ellipsoid from %ProjectionTable
    * @param datumName PROJ4 name of datum
    * @param ellipsoid refernce to pass ellipsoid name
    * @return true if datum is found, false otherwise
    */
    bool ProjectionTable::getDatum(std::string datumName, std::string &ellipsoidName) {

        // Ensure tables are initialised
        if (!initialised)
            initialise();

        // Get value
        auto itDatum = datumTable.find(Strings::toUpper(datumName));
        if (itDatum == datumTable.end())
            return false;

        // Populate value
        ellipsoidName = itDatum->second;

        return true;
    }

    /**
    * Get ellipsoid value from %ProjectionTable
    * @param ellipsoidName PROJ4 name of ellipsoid
    * @param equatorialRadius equatorial radius for ellipsoid
    * @param flattening flattening for ellipsoid
    * @return true if ellipsoid is found, false otherwise
    */
    bool ProjectionTable::getEllipsoid(std::string ellipsoidName, double &equatorialRadius, double &inverseFlattening) {

        // Ensure tables are initialised
        if (!initialised)
            initialise();

        // Get value
        auto itEllipsoid = ellipsoidTable.find(Strings::toUpper(ellipsoidName));
        if (itEllipsoid == ellipsoidTable.end())
            return false;

        // Populate values
        auto &values = itEllipsoid->second;
        equatorialRadius = values[0];
        inverseFlattening = values[1];

        return true;
    }

    /**
    * Get ellipsoid name from %ProjectionTable
    * @param equatorialRadius equatorial radius for ellipsoid
    * @param flattening flattening for ellipsoid
    * @return PROJ4 name of ellipsoid, blank if not found
    */
    std::string ProjectionTable::getEllipsoidName(double equatorialRadius, double flattening) {

        // Ensure tables are initialised
        if (!initialised)
            initialise();

        // Scan table
        for (auto eit : ellipsoidTable) {

            auto &values = eit.second;
            if (equatorialRadius == values[0] && flattening == (1.0/values[1]))
                return eit.first;
        }

        return std::string();
    }

    /**
    * Get ellipsoid code from %ProjectionTable
    * @param ellipsoidName PROJ4 name of ellipsoid
    * @return GeogEllipsoidGeoKey code for ellipsoid, 0 if not valid
    */
    uint32_t ProjectionTable::getEllipsoidCode(std::string ellipsoidName) {

        // Ensure tables are initialised
        if (!initialised)
            initialise();

        // Look up code
        auto eit = ellipsoidCode.find(ellipsoidName);
        if (eit != ellipsoidCode.end())
            return eit->second;
        return 0;
    }

    /**
    * Singleton instance of %ProjectionOperator.
    */
    ProjectionOperator &ProjectionOperator::instance() {

        static ProjectionOperator projectionOperator;
        return projectionOperator;
    }

    /**
    * %ProjectionOperator initialisation
    */
    void ProjectionOperator::initialise(Geostack::Solver &solver) {

        if (!initialised) {

            // Implement all projections
            auto projStr = Models::cl_projection_head_c;
            std::string projDef = "#define USE_PROJ_ALL\n";
            projStr = projDef + projStr;

            // Build program
            std::string script = projStr + Models::cl_projection_c;
            Solver::processScript(script);
            clProjectionHash = solver.buildProgram(script);
            if (clProjectionHash == solver.getNullHash()) {
                throw std::runtime_error("Cannot build projection program");
            }

            // Set flag
            initialised = true;
        }
    }

    /**
    * %ProjectionOperator kernels
    * @return %ProjectionOperator kernel
    */
    cl::Kernel ProjectionOperator::getKernel(std::string kernel) {

        // Get solver handle
        Geostack::Solver &solver = Geostack::Solver::getSolver();

        // Ensure program is built and kernel is initialised
        if (!initialised)
            initialise(solver);

        // Return kernel
        return solver.getKernel(clProjectionHash, kernel);
    }

    namespace ProjectionProcessing {

        // Ignored data
        const std::set<std::string> ignoreList = { "TOWGS84" };

        // Translations from OGC WKT to ESRI WKT style
        const std::map<std::string, std::string> translations = {
            {"LATITUDE_OF_CENTER", "LATITUDE_OF_ORIGIN"},
            {"LONGITUDE_OF_CENTER", "CENTRAL_MERIDIAN"}
        };

        // Translation of specific parameters in WKT1 (spheroid), WKT2 (ellipsoid)
        const std::map<std::pair<std::string, int>, std::string> parameterTranslations = {
            { { "UNIT", 0 }, "CONVERSION_FACTOR" },
            { { "PRIMEM", 0 }, "LONGITUDE" },
            { { "SPHEROID", 0 }, "EQUATORIAL_RADIUS" },
            { { "SPHEROID", 1 }, "INVERSE_FLATTENING"},
            { { "ELLIPSOID", 0 }, "EQUATORIAL_RADIUS" },
            { { "ELLIPSOID", 1 }, "INVERSE_FLATTENING"}
        };

        /*
        * Clean string and return uppercase substring
        */
        std::string getFormattedSubString(std::string s, std::size_t start, std::size_t length) {

            // Clip and clean string
            std::string r = Strings::removeCharacter(s.substr(start, length), '"');

            // Convert to uppercase
            r = Strings::toUpper(r);

            return r;
        }

        /*
        * Check and get value from map
        */
        bool getMapValue(std::string name, double &v, std::map<std::string, std::string> &projMap) {

            // Find value
            auto it = projMap.find(Strings::toUpper(name));
            if (it == projMap.end())
                return false;

            // Check value
            std::string &val = it->second;
            if (!Strings::isNumber(val))
                return false;

            // Set value
            v = Strings::toNumber<double>(val);

            return true;
        }

        /*
        * Recursive parsing for WKT string, expects form of Name["Value", ...]
        * @param string to parse
        * @return Json object
        */
        Json::object parseProjectionRecurse(std::string s) {

            // Skip blank strings
            if (!s.empty()) {

                // Break string
                unsigned level = 0;
                unsigned maxLevel = 0;
                std::vector<std::size_t> delPos;
                for (std::size_t pos = 0; pos < s.size(); pos++) {
                    if (s[pos] == '[') {
                        if (level == 0)
                            delPos.push_back(pos);
                        level++;
                    }
                    if (level == 1 && (s[pos] == ',' || s[pos] == ']'))
                        delPos.push_back(pos);
                    if (s[pos] == ']')
                        level--;
                }

                // Parse sub-strings
                if (delPos.size() == 2) {

                    // Get name and value
                    std::string name = getFormattedSubString(s, 0, delPos[0]);
                    std::string value = getFormattedSubString(s, delPos[0]+1, delPos[1]-(delPos[0]+1));

                    // Create object
                    if (Strings::isNumber(value)) {
                        return Json::object { { name, std::stod(value) } };
                    } else {
                        return Json::object { { name, value } };
                    }

                } else {

                    // Get name and value
                    std::string name = getFormattedSubString(s, 0, delPos[0]);
                    std::string value = getFormattedSubString(s, delPos[0]+1, delPos[1]-(delPos[0]+1));

                    // Ignore certain OGC strings
                    if (ignoreList.find(name) != ignoreList.end())
                        return Json::object();

                    // Check for parameters
                    if (name == "PARAMETER" && delPos.size() == 3) {
                        std::string paramValue = getFormattedSubString(s, delPos[1]+1, delPos[2]-(delPos[1]+1));

                        // Specialised translations
                        auto it = translations.find(value);
                        if (it != translations.end())
                            value = it->second;

                        // Create object
                        if (Strings::isNumber(paramValue)) {
                            return Json::object { { value, std::stod(paramValue) } };
                        } else {
                            return Json::object { { value, paramValue } };
                        }
                    }

                    // Create object and array
                    Json::object jsonObj { { "NAME", value } };

                    // Recursively parse
                    unsigned param = 0;
                    for (std::size_t pos = 1; pos < delPos.size()-1; pos++) {

                        // Get substring
                        std::string ss = getFormattedSubString(s, delPos[pos]+1, delPos[pos+1]-(delPos[pos]+1));

                        // Search for groups in substring
                        if (ss.find('[') != std::string::npos) {
                            Json::object p = parseProjectionRecurse(ss);
                            
                            for (auto const &x: p) {
                                auto it = jsonObj.find(x.first);
                                bool has_key = false;

                                // check if the key exist
                                if (it != jsonObj.end()) {
                                    has_key = true;
                                }

                                if (!has_key) {
                                    jsonObj[x.first] = x.second;
                                } else {
                                    // convert to Json::array when same key is repeated
                                    // create a new Json array
                                    Json::array json_array;
                                    auto json_value = Json(jsonObj[x.first]);

                                    if (json_value.is_array()) {
                                        // handle case when value is json array
                                        json_array = json_value.array_items();
                                        json_array.push_back(x.second);
                                    } else if (json_value.is_object()) {
                                        // handle case when value is json object
                                        json_array.push_back(json_value);
                                        json_array.push_back(x.second);                                    
                                    }
                                    // remove the earlier key
                                    jsonObj.erase(x.first);
                                    // now add the new array object
                                    jsonObj[x.first] = json_array;
                                }
                            }
                            // jsonObj.insert(p.begin(), p.end());
                        } else {

                            // Search for special parameter translations
                            std::string parameterName = "PARAMETER" + std::to_string(param);
                            auto it = parameterTranslations.find( {name, param} );
                            if (it != parameterTranslations.end()) {
                                parameterName = it->second;
                            }

                            // Create object
                            if (Strings::isNumber(ss)) {
                                jsonObj[parameterName] = std::stod(ss);
                            } else {
                                jsonObj[parameterName] = ss;
                            }

                            // Increment parameter counter
                            param++;
                        }
                    }

                    return Json::object { { name , jsonObj } };
                }
            }

            return Json::object();
        }
    }
    
    /*
    * handle parsing of Proj4 or EPSG code
    * @param projString Proj4. parameters or EPSG code string
    */
    ProjectionParameters<double> Projection::parseProjString(const std::string projString) {
        auto proj = ProjectionParameters<double>();
        try {
            std::regex searchProj4("^\\+", std::regex_constants::icase);
            std::regex searchEPSG("EPSG:(\\d+)", std::regex_constants::icase);
            std::smatch matchGroups;
            
            if (std::regex_search(projString, searchProj4)) {
                proj = Projection::parsePROJ4(projString);
            } else if (std::regex_search(projString, matchGroups, searchEPSG)) {
                auto epsgCode = matchGroups[1];
                proj = Projection::fromEPSG(epsgCode);
            } else {
                std::stringstream err;
                err << "Unable to deduce" << projString << " value of projection";
                throw std::runtime_error(err.str());                
            }                
        } catch (std::regex_error& e) {
            throw std::runtime_error(e.what());
        }
        return proj;
    }

    /*
    * Parse OGC WKT string into %Projection object
    * @param WKT well-known text string
    * @return %Projection object
    */
    ProjectionParameters<double> Projection::parseWKT(const std::string WKT) {
        if (WKT.length() > 0) {
            // Parse projection into searchable JSON object
            Json json;
            std::string cleanWKT = Geostack::Strings::removeWhitespace(WKT);
            json = ProjectionProcessing::parseProjectionRecurse(cleanWKT);
            if (json.object_items().size() == 0) {
                std::stringstream err;
                err << "Unknown projection type '" + json.string_value() + "'";
                throw std::runtime_error(err.str());
            }

            // Create projection
            auto jsonObj = json.object_items();
            std::string topLevelName = jsonObj.begin()->first;
            if (topLevelName == "GEOGCS" || topLevelName == "PROJCS") {
                return Projection::parseWKT1(jsonObj);
            } else if (topLevelName == "GEOGCRS" || topLevelName == "PROJCRS" ||
                       topLevelName == "GEOGRAPHICCRS" || topLevelName == "PROJECTEDCRS") {
                return Projection::parseWKT2(jsonObj);
            } else {
                std::stringstream err;
                err << "Unknown WKT specification '" + topLevelName + "'";
                throw std::runtime_error(err.str());                
            }
            
        } else {
                std::stringstream err;
                err << "WKT string is empty.'";
                throw std::runtime_error(err.str());
        }        
    }

    /*
    * Parse OGC WKT2 string into %Projection object
    * @param Json::object a parsed Json object
    * @return %Projection object
    */
    ProjectionParameters<double> Projection::parseWKT2(Json::object jsonObj) {
        auto p = ProjectionParameters<double>();
        std::string topLevelName = jsonObj.begin()->first;
        
        // std::cout << topLevelName << std::endl;
        // std::cout << Json(jsonObj).dump() << std::endl;
        
        if (topLevelName == "GEOGCRS" || topLevelName == "GEOGRAPHICCRS") {

            // Check for proj4 extension
            if (!jsonObj[topLevelName]["EXTENSION"].is_null()) {

                // Get and check extension
                auto &extension = jsonObj[topLevelName]["EXTENSION"];
                if (extension["NAME"].string_value() == "PROJ4") {
                    return parsePROJ4(extension["PARAMETER0"].string_value());
                }
            }

            // Check geographic coordinate system
            if (jsonObj[topLevelName]["DATUM"].is_null() && jsonObj[topLevelName]["ENSEMBLE"].is_null()) {
                throw std::runtime_error("Geographic coordinate system has no DATUM/ ENSEMBLE definition");
            }

            std::string datumKey;
            if (!jsonObj[topLevelName]["DATUM"].is_null()) {
                datumKey = "DATUM";
            } else {
                datumKey = "ENSEMBLE";
            }


            if (jsonObj[topLevelName][datumKey]["ELLIPSOID"].is_null()) {
                throw std::runtime_error("Geographic coordinate system has no ELLIPSOID definition");
            }

            // Get projection objects
            auto &ellipsoidObj = jsonObj[topLevelName][datumKey]["ELLIPSOID"];

            // Set projection parameters
            p.type = 2;
            p.cttype = 0;
            p.a = ellipsoidObj["EQUATORIAL_RADIUS"].number_value();
            p.f = (1.0/ellipsoidObj["INVERSE_FLATTENING"].number_value());

        } else if (topLevelName == "PROJCRS" || topLevelName == "PROJECTEDCRS") {
            // Check for proj4 extension
            if (!jsonObj[topLevelName]["EXTENSION"].is_null()) {

                // Get and check extension
                auto &extension = jsonObj[topLevelName]["EXTENSION"];
                if (extension["NAME"].string_value() == "PROJ4") {
                    return parsePROJ4(extension["PARAMETER0"].string_value());
                }
            }

            // Check for geographic coordinate system
            auto GEOGCRSjson = jsonObj[topLevelName]["BASEGEOGCRS"];
            if (GEOGCRSjson.is_null()) {
                throw std::runtime_error("Projected system  does not contain a geographic projection");
            }

            std::string projTypeName;
            // Check projected coordinate system
            if (jsonObj[topLevelName]["CONVERSION"]["METHOD"].is_object()) {
                projTypeName = jsonObj[topLevelName]["CONVERSION"]["METHOD"]["NAME"].string_value();
                
            } else if (jsonObj[topLevelName]["CONVERSION"]["METHOD"].is_string()) {
                projTypeName = jsonObj[topLevelName]["CONVERSION"]["METHOD"].string_value();
            }

            // Lambert conformal conic
            if (projTypeName== "LAMBERT_CONFORMAL_CONIC_2SP" || 
                projTypeName == "LAMBERT_CONFORMAL_CONIC" || 
                projTypeName == "LAMBERTCONICCONFORMAL(2SP)" ||
                projTypeName == "LAMBERTCONICCONFORMAL(2SPBELGIUM)") {

                auto conversionParameters = jsonObj[topLevelName]["CONVERSION"]["PARAMETER"];

                // Lambert conformal conic
                std::vector<std::string> requiredParameters {
                    "LONGITUDEOFFALSEORIGIN",
                    "EASTINGATFALSEORIGIN",
                    "NORTHINGATFALSEORIGIN",
                    "LATITUDEOFFALSEORIGIN",
                    "LATITUDEOF1STSTANDARDPARALLEL",
                    "LATITUDEOF2NDSTANDARDPARALLEL",
                };
                
                // extract values from the JSON object and store in the map
                std::map<std::string, double> projcsObj;
                for (auto &r : requiredParameters) {
                    if (conversionParameters.is_array()) {
                        for (auto &it: conversionParameters.array_items()) {
                            if (it["NAME"] == r) {
                                projcsObj[r] = it["PARAMETER0"].number_value();
                            } else if (it["ANGLEUNIT"]["NAME"] == r) {
                                projcsObj[r] = it["ANGLEUNIT"]["PARAMETER0"].number_value();
                            }
                        }
                    } else if (conversionParameters.is_object()) {
                        // retain the case where parameter is object
                        if (conversionParameters[r].is_null()) {
                            std::stringstream err;
                            err << "Lambert conformal conic projection does not contain '" << r << "' parameter";
                            throw std::runtime_error(err.str());
                        }   
                    }
                }
                
                // check if we got all the parameters
                if (projcsObj.size() != requiredParameters.size()) {
                    std::stringstream err;
                    err << "Lambert conformal conic projection does not contain '";
                    for (auto &t: requiredParameters) {
                        if (projcsObj.find(t) == projcsObj.end()) {
                            err << t << ", ";   
                        }
                    }
                    err << "' parameter";
                    throw std::runtime_error(err.str());
                }

                // Get projection objects
                // auto &projcsObj = jsonObj[topLevelName];
                json11::Json ellipsoidObj;
                if (!jsonObj[topLevelName]["BASEGEOGCRS"]["DATUM"].is_null()) {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["DATUM"]["ELLIPSOID"];
                } else if (!jsonObj[topLevelName]["BASEGEOGCRS"]["ENSEMBLE"]["ELLIPSOID"].is_null()) {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["ENSEMBLE"]["ELLIPSOID"];
                } else {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["ELLIPSOID"];
                }
                
                if (ellipsoidObj.is_null()) {
                    throw std::runtime_error("Unable to find elliposoid information.");
                }

                // Set projection parameters
                p.type = 1;
                p.cttype = 8;
                p.a = ellipsoidObj["EQUATORIAL_RADIUS"].number_value();
                p.f = (1.0/ellipsoidObj["INVERSE_FLATTENING"].number_value());
                p.x0 = projcsObj["LONGITUDEOFFALSEORIGIN"];
                p.k0 = 0.0;
                p.fe = projcsObj["EASTINGATFALSEORIGIN"];
                p.fn = projcsObj["NORTHINGATFALSEORIGIN"];
                p.phi_0 = projcsObj["LATITUDEOFFALSEORIGIN"];
                p.phi_1 = projcsObj["LATITUDEOF1STSTANDARDPARALLEL"];
                p.phi_2 = projcsObj["LATITUDEOF2NDSTANDARDPARALLEL"];
            } else if (projTypeName== "TRANSVERSE_MERCATOR" ||
                        projTypeName== "TRANSVERSEMERCATOR" ||
                        projTypeName== "TRANSVERSEMERCATOR(SOUTHORIENTATED)") {    
                // Transverse Mercator
                std::vector<std::string> requiredParameters {
                    "LONGITUDEOFNATURALORIGIN",
                    "FALSEEASTING",
                    "FALSENORTHING",
                    "LATITUDEOFNATURALORIGIN",
                    "SCALEFACTORATNATURALORIGIN",
                };
                
                auto conversionParameters = jsonObj[topLevelName]["CONVERSION"]["PARAMETER"];
                
                // extract values from the JSON object and store in the map
                std::map<std::string, double> projcsObj;
                for (auto &r : requiredParameters) {
                    if (conversionParameters.is_array()) {
                        for (auto &it: conversionParameters.array_items()) {
                            if (it["NAME"] == r) {
                                projcsObj[r] = it["PARAMETER0"].number_value();
                            } else if (it["ANGLEUNIT"]["NAME"] == r) {
                                projcsObj[r] = it["ANGLEUNIT"]["PARAMETER0"].number_value();
                            }
                        }
                    } else if (conversionParameters.is_object()) {
                        // retain the case where parameter is object
                        if (conversionParameters[r].is_null()) {
                            std::stringstream err;
                            err << "Transverse mercator projection does not contain '" << r << "' parameter";
                            throw std::runtime_error(err.str());
                        }   
                    }
                }
                
                // check if we got all the parameters
                if (projcsObj.size() != requiredParameters.size()) {
                    std::stringstream err;
                    err << "Transverse mercator projection does not contain '";
                    for (auto &t: requiredParameters) {
                        if (projcsObj.find(t) == projcsObj.end()) {
                            err << t << ", ";   
                        }
                    }
                    err << "' parameter";
                    throw std::runtime_error(err.str());
                }
                
                // Get projection objects
                json11::Json ellipsoidObj;
                if (!jsonObj[topLevelName]["BASEGEOGCRS"]["DATUM"].is_null()) {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["DATUM"]["ELLIPSOID"];
                } else if (!jsonObj[topLevelName]["BASEGEOGCRS"]["ENSEMBLE"]["ELLIPSOID"].is_null()) {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["ENSEMBLE"]["ELLIPSOID"];
                } else {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["ELLIPSOID"];
                }
                
                if (ellipsoidObj.is_null()) {
                    throw std::runtime_error("Unable to find elliposoid information.");
                }

                // Set projection parameters
                p.type = 1;
                p.cttype = 1;
                p.a = ellipsoidObj["EQUATORIAL_RADIUS"].number_value();
                p.f = (1.0/ellipsoidObj["INVERSE_FLATTENING"].number_value());
                p.x0 = projcsObj["LONGITUDEOFNATURALORIGIN"];
                p.k0 = projcsObj["SCALEFACTORATNATURALORIGIN"];
                p.fe = projcsObj["FALSEEASTING"];
                p.fn = projcsObj["FALSENORTHING"];
                p.phi_0 = projcsObj["LATITUDEOFNATURALORIGIN"];
            } else if (projTypeName == "POPULARVISUALISATIONPSEUDOMERCATOR") {
                // Pseudo Mercator
                std::vector<std::string> requiredParameters {
                    "LONGITUDEOFNATURALORIGIN",
                    "FALSEEASTING",
                    "FALSENORTHING",
                };
                
                auto conversionParameters = jsonObj[topLevelName]["CONVERSION"]["PARAMETER"];
                
                // extract values from the JSON object and store in the map
                std::map<std::string, double> projcsObj;
                for (auto &r : requiredParameters) {
                    if (conversionParameters.is_array()) {
                        for (auto &it: conversionParameters.array_items()) {
                            if (it["NAME"] == r) {
                                projcsObj[r] = it["PARAMETER0"].number_value();
                            } else if (it["ANGLEUNIT"]["NAME"] == r) {
                                projcsObj[r] = it["ANGLEUNIT"]["PARAMETER0"].number_value();
                            }
                        }
                    } else if (conversionParameters.is_object()) {
                        // retain the case where parameter is object
                        if (conversionParameters[r].is_null()) {
                            std::stringstream err;
                            err << "Mercator projection does not contain '" << r << "' parameter";
                            throw std::runtime_error(err.str());
                        }   
                    }
                }
                
                // check if we got all the parameters
                if (projcsObj.size() != requiredParameters.size()) {
                    std::stringstream err;
                    err << "Mercator projection does not contain '";
                    for (auto &t: requiredParameters) {
                        if (projcsObj.find(t) == projcsObj.end()) {
                            err << t << ", ";   
                        }
                    }
                    err << "' parameter";
                    throw std::runtime_error(err.str());
                }
                
                // Get projection objects
                json11::Json ellipsoidObj;
                if (!jsonObj[topLevelName]["BASEGEOGCRS"]["DATUM"].is_null()) {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["DATUM"]["ELLIPSOID"];
                } else if (!jsonObj[topLevelName]["BASEGEOGCRS"]["ENSEMBLE"]["ELLIPSOID"].is_null()) {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["ENSEMBLE"]["ELLIPSOID"];
                } else {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["ELLIPSOID"];
                }
                
                if (ellipsoidObj.is_null()) {
                    throw std::runtime_error("Unable to find elliposoid information.");
                }

                // Set projection parameters
                p.type = 1;
                p.cttype = 7;
                p.a = ellipsoidObj["EQUATORIAL_RADIUS"].number_value();
                p.x0 = projcsObj["LONGITUDEOFNATURALORIGIN"];
                p.k0 = 1.0;
                p.fe = projcsObj["FALSEEASTING"];
                p.fn = projcsObj["FALSENORTHING"];
            } else if (projTypeName == "MERCATOR_1SP" ||
                        projTypeName== "MERCATOR" ||
                        projTypeName == "MERCATOR(VARIANTA)" ||
                        projTypeName == "MERCATOR(1SP)(SPHERICAL)") {
                // Mercator 1SP
                std::vector<std::string> requiredParameters {
                    "LONGITUDEOFNATURALORIGIN",
                    "FALSEEASTING",
                    "FALSENORTHING",
                    "SCALEFACTORATNATURALORIGIN",
                };
                
                auto conversionParameters = jsonObj[topLevelName]["CONVERSION"]["PARAMETER"];
                
                // extract values from the JSON object and store in the map
                std::map<std::string, double> projcsObj;
                for (auto &r : requiredParameters) {
                    if (conversionParameters.is_array()) {
                        for (auto &it: conversionParameters.array_items()) {
                            if (it["NAME"] == r) {
                                projcsObj[r] = it["PARAMETER0"].number_value();
                            } else if (it["ANGLEUNIT"]["NAME"] == r) {
                                projcsObj[r] = it["ANGLEUNIT"]["PARAMETER0"].number_value();
                            }
                        }
                    } else if (conversionParameters.is_object()) {
                        // retain the case where parameter is object
                        if (conversionParameters[r].is_null()) {
                            std::stringstream err;
                            err << "Mercator projection does not contain '" << r << "' parameter";
                            throw std::runtime_error(err.str());
                        }   
                    }
                }
                
                // check if we got all the parameters
                if (projcsObj.size() != requiredParameters.size()) {
                    std::stringstream err;
                    err << "Mercator projection does not contain '";
                    for (auto &t: requiredParameters) {
                        if (projcsObj.find(t) == projcsObj.end()) {
                            err << t << ", ";   
                        }
                    }
                    err << "' parameter";
                    throw std::runtime_error(err.str());
                }
                
                // Get projection objects
                json11::Json ellipsoidObj;
                if (!jsonObj[topLevelName]["BASEGEOGCRS"]["DATUM"].is_null()) {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["DATUM"]["ELLIPSOID"];
                } else if (!jsonObj[topLevelName]["BASEGEOGCRS"]["ENSEMBLE"]["ELLIPSOID"].is_null()) {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["ENSEMBLE"]["ELLIPSOID"];
                } else {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["ELLIPSOID"];
                }
                
                if (ellipsoidObj.is_null()) {
                    throw std::runtime_error("Unable to find elliposoid information.");
                }

                // Set projection parameters
                p.type = 1;
                p.cttype = 7;
                p.a = ellipsoidObj["EQUATORIAL_RADIUS"].number_value();
                if (ellipsoidObj["INVERSE_FLATTENING"].number_value() != 0.0) {
                    p.f = (1.0/ellipsoidObj["INVERSE_FLATTENING"].number_value());
                }
                p.x0 = projcsObj["LONGITUDEOFNATURALORIGIN"];
                p.k0 = projcsObj["SCALEFACTORATNATURALORIGIN"];
                p.fe = projcsObj["FALSEEASTING"];
                p.fn = projcsObj["FALSENORTHING"];
            } else if (projTypeName== "ALBERS_CONIC_EQUAL_AREA" ||
                        projTypeName == "ALBERS" ||
                        projTypeName == "ALBERSEQUALAREA") {
                auto conversionParameters = jsonObj[topLevelName]["CONVERSION"]["PARAMETER"];
                
                // Albers conic
                std::vector<std::string> requiredParameters {
                    "LONGITUDEOFFALSEORIGIN",
                    "EASTINGATFALSEORIGIN",
                    "NORTHINGATFALSEORIGIN",
                    "LATITUDEOFFALSEORIGIN",
                    "LATITUDEOF1STSTANDARDPARALLEL",
                    "LATITUDEOF2NDSTANDARDPARALLEL",
                };

                // extract values from the JSON object and store in the map
                std::map<std::string, double> projcsObj;
                for (auto &r : requiredParameters) {
                    if (conversionParameters.is_array()) {
                        for (auto &it: conversionParameters.array_items()) {
                            if (it["NAME"] == r) {
                                projcsObj[r] = it["PARAMETER0"].number_value();
                            } else if (it["ANGLEUNIT"]["NAME"] == r) {
                                projcsObj[r] = it["ANGLEUNIT"]["PARAMETER0"].number_value();
                            }
                        }
                    } else if (conversionParameters.is_object()) {
                        // retain the case where parameter is object
                        if (conversionParameters[r].is_null()) {
                            std::stringstream err;
                            err << "Albers conic projection does not contain '" << r << "' parameter";
                            throw std::runtime_error(err.str());
                        }   
                    }
                }
                
                // check if we got all the parameters
                if (projcsObj.size() != requiredParameters.size()) {
                    std::stringstream err;
                    err << "Albers conic projection does not contain '";
                    for (auto &t: requiredParameters) {
                        if (projcsObj.find(t) == projcsObj.end()) {
                            err << t << ", ";   
                        }
                    }
                    err << "' parameter";
                    throw std::runtime_error(err.str());
                }

                // Get projection objects
                // auto &projcsObj = jsonObj[topLevelName];
                json11::Json ellipsoidObj;
                if (!jsonObj[topLevelName]["BASEGEOGCRS"]["DATUM"].is_null()) {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["DATUM"]["ELLIPSOID"];
                } else if (!jsonObj[topLevelName]["BASEGEOGCRS"]["ENSEMBLE"]["ELLIPSOID"].is_null()) {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["ENSEMBLE"]["ELLIPSOID"];
                } else {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["ELLIPSOID"];
                }
                
                if (ellipsoidObj.is_null()) {
                    throw std::runtime_error("Unable to find elliposoid information.");
                }

                // Set projection parameters
                p.type = 1;
                p.cttype = 11;
                p.a = ellipsoidObj["EQUATORIAL_RADIUS"].number_value();
                p.f = (1.0/ellipsoidObj["INVERSE_FLATTENING"].number_value());
                p.x0 = projcsObj["LONGITUDEOFFALSEORIGIN"];
                p.k0 = 0.0;
                p.fe = projcsObj["EASTINGATFALSEORIGIN"];
                p.fn = projcsObj["NORTHINGATFALSEORIGIN"];
                p.phi_0 = projcsObj["LATITUDEOFFALSEORIGIN"];
                p.phi_1 = projcsObj["LATITUDEOF1STSTANDARDPARALLEL"];
                p.phi_2 = projcsObj["LATITUDEOF2NDSTANDARDPARALLEL"];
            } else if (projTypeName == "SINUSOIDAL") {
                auto conversionParameters = jsonObj[topLevelName]["CONVERSION"]["PARAMETER"];
                // MODIS Sinusoidal
                std::vector<std::string> requiredParameters {
                    "LONGITUDEOFNATURALORIGIN",
                    "FALSEEASTING",
                    "FALSENORTHING"
                };

                // extract values from the JSON object and store in the map
                std::map<std::string, double> projcsObj;
                for (auto &r : requiredParameters) {
                    if (conversionParameters.is_array()) {
                        for (auto &it: conversionParameters.array_items()) {
                            if (it["NAME"] == r) {
                                projcsObj[r] = it["PARAMETER0"].number_value();
                            } else if (it["ANGLEUNIT"]["NAME"] == r) {
                                projcsObj[r] = it["ANGLEUNIT"]["PARAMETER0"].number_value();
                            }
                        }
                    } else if (conversionParameters.is_object()) {
                        // retain the case where parameter is object
                        if (conversionParameters[r].is_null()) {
                            std::stringstream err;
                            err << "Modis sinusoidal projection does not contain '" << r << "' parameter";
                            throw std::runtime_error(err.str());
                        }   
                    }
                }
                
                // check if we got all the parameters
                if (projcsObj.size() != requiredParameters.size()) {
                    std::stringstream err;
                    err << "Modis sinusoidal projection does not contain '";
                    for (auto &t: requiredParameters) {
                        if (projcsObj.find(t) == projcsObj.end()) {
                            err << t << ", ";   
                        }
                    }
                    err << "' parameter";
                    throw std::runtime_error(err.str());
                }

                // Get projection objects
                // auto &projcsObj = jsonObj[topLevelName];
                json11::Json ellipsoidObj;
                if (!jsonObj[topLevelName]["BASEGEOGCRS"]["DATUM"].is_null()) {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["DATUM"]["ELLIPSOID"];
                } else if (!jsonObj[topLevelName]["BASEGEOGCRS"]["ENSEMBLE"]["ELLIPSOID"].is_null()) {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["ENSEMBLE"]["ELLIPSOID"];
                } else {
                    ellipsoidObj = jsonObj[topLevelName]["BASEGEOGCRS"]["ELLIPSOID"];
                }
                
                if (ellipsoidObj.is_null()) {
                    throw std::runtime_error("Unable to find elliposoid information.");
                }

                // Set projection parameters
                p.type = 1;
                p.cttype = 24;
                p.a = ellipsoidObj["EQUATORIAL_RADIUS"].number_value();
                p.f = (1.0/ellipsoidObj["INVERSE_FLATTENING"].number_value());
                p.x0 = projcsObj["LONGITUDEOFNATURALORIGIN"];
                p.fe = projcsObj["FALSEEASTING"];
                p.fn = projcsObj["FALSENORTHING"];
            } else {
                std::stringstream err;
                err << "Not implemented " << projTypeName;
                throw std::runtime_error(err.str());
            }
        } else {
            // Return empty projection
            return ProjectionParameters<double>();
        }
        return p;
    }

    /*
    * Parse OGC WKT1 string into %Projection object
    * @param Json::object a parsed Json object
    * @return %Projection object
    */
    ProjectionParameters<double> Projection::parseWKT1(Json::object jsonObj) {
        auto p = ProjectionParameters<double>();
        std::string topLevelName = jsonObj.begin()->first;
        if (topLevelName == "GEOGCS") {

            // Check for proj4 extension
            if (!jsonObj[topLevelName]["EXTENSION"].is_null()) {

                // Get and check extension
                auto &extension = jsonObj[topLevelName]["EXTENSION"];
                if (extension["NAME"].string_value() == "PROJ4") {
                    return parsePROJ4(extension["PARAMETER0"].string_value());
                }
            }

            // Check geographic coordinate system
            if (jsonObj[topLevelName]["DATUM"].is_null()) {
                throw std::runtime_error("Geographic coordinate system has no DATUM definition");
            }

            if (jsonObj[topLevelName]["DATUM"]["SPHEROID"].is_null()) {
                throw std::runtime_error("Geographic coordinate system has no SPHEROID definition");
            }

            // Get projection objects
            auto &spheriodObj = jsonObj[topLevelName]["DATUM"]["SPHEROID"];

            // Set projection parameters
            p.type = 2;
            p.cttype = 0;
            p.a = spheriodObj["EQUATORIAL_RADIUS"].number_value();
            p.f = (1.0/spheriodObj["INVERSE_FLATTENING"].number_value());

        } else if (topLevelName == "PROJCS") {

            // Check for proj4 extension
            if (!jsonObj[topLevelName]["EXTENSION"].is_null()) {

                // Get and check extension
                auto &extension = jsonObj[topLevelName]["EXTENSION"];
                if (extension["NAME"].string_value() == "PROJ4") {
                    return parsePROJ4(extension["PARAMETER0"].string_value());
                }
            }

            // Check for geographic coordinate system
            auto GEOGCSjson = jsonObj[topLevelName]["GEOGCS"];
            if (GEOGCSjson.is_null()) {
                throw std::runtime_error("Projected system does not contain a geographic projection");
            }

            // Check projected coordinate system
            auto projTypeName = jsonObj[topLevelName]["PROJECTION"].string_value();

            // Lambert conformal conic
            if (projTypeName== "LAMBERT_CONFORMAL_CONIC_2SP" ||
                projTypeName == "LAMBERT_CONFORMAL_CONIC" ||
                projTypeName == "LAMBERT_CONFORMAL_CONIC_2SP_BELGIUM") {

                // Lambert conformal conic
                std::vector<std::string> requiredParameters {
                    "CENTRAL_MERIDIAN",
                    "FALSE_EASTING",
                    "FALSE_NORTHING",
                    "LATITUDE_OF_ORIGIN",
                    "STANDARD_PARALLEL_1",
                    "STANDARD_PARALLEL_2",
                };

                for (auto &r : requiredParameters) {
                    if (jsonObj[topLevelName][r].is_null()) {
                        std::stringstream err;
                        err << "Lambert conformal conic projection does not contain '" << r << "' parameter";
                        throw std::runtime_error(err.str());
                    }
                }

                // Get projection objects
                auto &projcsObj = jsonObj[topLevelName];
                auto &spheriodObj = projcsObj["GEOGCS"]["DATUM"]["SPHEROID"];

                // Set projection parameters
                p.type = 1;
                p.cttype = 8;
                p.a = spheriodObj["EQUATORIAL_RADIUS"].number_value();
                p.f = (1.0/spheriodObj["INVERSE_FLATTENING"].number_value());
                p.x0 = projcsObj["CENTRAL_MERIDIAN"].number_value();
                p.k0 = 0.0;
                p.fe = projcsObj["FALSE_EASTING"].number_value();
                p.fn = projcsObj["FALSE_NORTHING"].number_value();
                p.phi_0 = projcsObj["LATITUDE_OF_ORIGIN"].number_value();
                p.phi_1 = projcsObj["STANDARD_PARALLEL_1"].number_value();
                p.phi_2 = projcsObj["STANDARD_PARALLEL_2"].number_value();

            } else if (projTypeName== "TRANSVERSE_MERCATOR" ||
                        projTypeName == "TRANSVERSE_MERCATOR_SOUTH_ORIENTATED") {

                // Transverse Mercator
                std::vector<std::string> requiredParameters {
                    "CENTRAL_MERIDIAN",
                    "FALSE_EASTING",
                    "FALSE_NORTHING",
                    "LATITUDE_OF_ORIGIN",
                    "SCALE_FACTOR",
                };

                for (auto &r : requiredParameters) {
                    if (jsonObj[topLevelName][r].is_null()) {
                        std::stringstream err;
                        err << "Transverse Mercator projection does not contain '" << r << "' parameter";
                        throw std::runtime_error(err.str());
                    }
                }

                // Get projection objects
                auto &projcsObj = jsonObj[topLevelName];
                auto &spheriodObj = projcsObj["GEOGCS"]["DATUM"]["SPHEROID"];

                // Set projection parameters
                p.type = 1;
                p.cttype = 1;
                p.a = spheriodObj["EQUATORIAL_RADIUS"].number_value();
                p.f = (1.0/spheriodObj["INVERSE_FLATTENING"].number_value());
                p.x0 = projcsObj["CENTRAL_MERIDIAN"].number_value();
                p.k0 = projcsObj["SCALE_FACTOR"].number_value();
                p.fe = projcsObj["FALSE_EASTING"].number_value();
                p.fn = projcsObj["FALSE_NORTHING"].number_value();
                p.phi_0 = projcsObj["LATITUDE_OF_ORIGIN"].number_value();

            } else if (projTypeName == "MERCATOR_1SP" || projTypeName== "MERCATOR") {

                // Transverse Mercator
                std::vector<std::string> requiredParameters {
                    "CENTRAL_MERIDIAN",
                    "FALSE_EASTING",
                    "FALSE_NORTHING",
                    "SCALE_FACTOR",
                };

                for (auto &r : requiredParameters) {
                    if (jsonObj[topLevelName][r].is_null()) {
                        std::stringstream err;
                        err << "Mercator projection does not contain '" << r << "' parameter";
                        throw std::runtime_error(err.str());
                    }
                }

                // Get projection objects
                auto &projcsObj = jsonObj[topLevelName];
                auto &spheriodObj = projcsObj["GEOGCS"]["DATUM"]["SPHEROID"];

                // Set projection parameters
                p.type = 1;
                p.cttype = 7;
                p.a = spheriodObj["EQUATORIAL_RADIUS"].number_value();
                p.f = (1.0/spheriodObj["INVERSE_FLATTENING"].number_value());
                p.x0 = projcsObj["CENTRAL_MERIDIAN"].number_value();
                p.k0 = projcsObj["SCALE_FACTOR"].number_value();
                p.fe = projcsObj["FALSE_EASTING"].number_value();
                p.fn = projcsObj["FALSE_NORTHING"].number_value();

            } else if (projTypeName== "ALBERS_CONIC_EQUAL_AREA" || projTypeName == "ALBERS") {

                // Albers conic
                std::vector<std::string> requiredParameters {
                    "CENTRAL_MERIDIAN",
                    "FALSE_EASTING",
                    "FALSE_NORTHING",
                    "LATITUDE_OF_ORIGIN",
                    "STANDARD_PARALLEL_1",
                    "STANDARD_PARALLEL_2",
                };

                for (auto &r : requiredParameters) {
                    if (jsonObj[topLevelName][r].is_null()) {
                        std::stringstream err;
                        err << "Albers conic projection does not contain '" << r << "' parameter";
                        throw std::runtime_error(err.str());
                    }
                }

                // Get projection objects
                auto &projcsObj = jsonObj[topLevelName];
                auto &spheriodObj = projcsObj["GEOGCS"]["DATUM"]["SPHEROID"];

                // Set projection parameters
                p.type = 1;
                p.cttype = 11;
                p.a = spheriodObj["EQUATORIAL_RADIUS"].number_value();
                p.f = (1.0/spheriodObj["INVERSE_FLATTENING"].number_value());
                p.x0 = projcsObj["CENTRAL_MERIDIAN"].number_value();
                p.k0 = 0.0;
                p.fe = projcsObj["FALSE_EASTING"].number_value();
                p.fn = projcsObj["FALSE_NORTHING"].number_value();
                p.phi_0 = projcsObj["LATITUDE_OF_ORIGIN"].number_value();
                p.phi_1 = projcsObj["STANDARD_PARALLEL_1"].number_value();
                p.phi_2 = projcsObj["STANDARD_PARALLEL_2"].number_value();

            } else if (projTypeName == "SINUSOIDAL") {
                // Albers conic
                std::vector<std::string> requiredParameters {
                    "CENTRAL_MERIDIAN",
                    "FALSE_EASTING",
                    "FALSE_NORTHING"
                };

                for (auto &r : requiredParameters) {
                    if (jsonObj[topLevelName][r].is_null()) {
                        std::stringstream err;
                        err << "Sinusoidal projection does not contain '" << r << "' parameter";
                        throw std::runtime_error(err.str());
                    }
                }

                // Get projection objects
                auto &projcsObj = jsonObj[topLevelName];
                auto &spheriodObj = projcsObj["GEOGCS"]["DATUM"]["SPHEROID"];

                // Set projection parameters
                p.type = 1;
                p.cttype = 24;
                p.a = spheriodObj["EQUATORIAL_RADIUS"].number_value();
                p.f = (1.0/spheriodObj["INVERSE_FLATTENING"].number_value());
                p.x0 = projcsObj["CENTRAL_MERIDIAN"].number_value();
                p.k0 = 0.0;
                p.fe = projcsObj["FALSE_EASTING"].number_value();
                p.fn = projcsObj["FALSE_NORTHING"].number_value();
            } else {
                std::stringstream err;
                err << "Unknown projection type '" << projTypeName << "'";
                throw std::runtime_error(err.str());
            }
        } else {
            // Return empty projection
            return ProjectionParameters<double>();
        }

        // Return projection
        return p;
    }

    /*
    * Parse PROJ4 string into %Projection object
    * @param PROJ4 string
    * @return %Projection object
    */
    ProjectionParameters<double> Projection::parsePROJ4(const std::string PROJ4) {

        // Remove whitespace
        std::string cleanPROJ4 = Strings::removeWhitespace(PROJ4);

        // Split string into parts
        auto PROJ4params = Strings::split(cleanPROJ4, '+');
        if (PROJ4params.size() > 0) {

            // Create map
            std::map<std::string, std::string> projMap;
            for (auto param : PROJ4params) {

                auto part = Strings::split(param, '=');
                if (part.size() > 0)
                    projMap[Strings::toUpper(part[0])] = part.size() > 1 ? part[1] : "";
            }

            // Get ellipsoid parameters
            double a, f;
            auto itDatum = projMap.find("DATUM");
            if (itDatum != projMap.end()) {

                // Search for ellipse
                auto datum = itDatum->second;
                std::string ellipsoid;
                if (ProjectionTable::instance().getDatum(datum, ellipsoid)) {

                    // Get ellipsoid parameters
                    double fi;
                    if (!ProjectionTable::instance().getEllipsoid(ellipsoid, a, fi)) {
                        std::stringstream err;
                        err << "Ellipsoid '" << ellipsoid << "' not found in table";
                        throw std::runtime_error(err.str());
                    }

                    // Set flattening
                    f = fi == 0.0 ? 1.0 : 1.0/fi;

                } else {
                    throw std::runtime_error("No datum or ellipsoid found in PROJ4 string");
                }

            } else {

                // Search for ellipse
                auto itEllipse = projMap.find("ELLPS");
                if (itEllipse != projMap.end()) {

                    // Get ellipsoid parameters
                    double fi;
                    std::string ellipsoid = itEllipse->second;
                    if (!ProjectionTable::instance().getEllipsoid(ellipsoid, a, fi)) {
                        std::stringstream err;
                        err << "Ellipsoid '" << ellipsoid << "' not found in table";
                        throw std::runtime_error(err.str());
                    }

                    // Set flattening
                    f = fi == 0.0 ? 1.0 : 1.0/fi;

                } else {

                    // Get radius
                    if (ProjectionProcessing::getMapValue("R", a, projMap)) {

                        // Set flattening
                        f = 0.0;

                    } else {

                        // Get semimajor radius
                        if (!ProjectionProcessing::getMapValue("a", a, projMap)) {
                            throw std::runtime_error("No semimajor radius found in PROJ4 string");
                        }

                        // Get semiminor radius
                        double b;
                        if (ProjectionProcessing::getMapValue("b", b, projMap)) {

                            // Set flattening
                            f = (a-b)/a;

                        } else {

                            // Find flattening
                            if (!ProjectionProcessing::getMapValue("f", f, projMap)) {

                                // Find reciprocal of flattening
                                double rf = 0.0;
                                if (ProjectionProcessing::getMapValue("rf", rf, projMap)) {
                                    f = 1.0/rf;
                                } else {
                                    throw std::runtime_error("No semiminor radius or flattening found in PROJ4 string");
                                }
                            }
                        }
                    }
                }
            }

            // Parse projection types
            auto itProj = projMap.find("PROJ");
            if (itProj != projMap.end()) {

                // Get projection
                auto projName = Strings::toUpper(itProj->second);

                // Parse projection types
                if (projName == "LONGLAT") {

                    // Geographic projection
                    auto p = ProjectionParameters<double>();
                    p.type = 2;
                    p.cttype = 0;
                    p.a = a;
                    p.f = f;

                    // Return projection
                    return p;

                } else if (projName == "UTM") {

                    // Universal transverse Mercator
                    double zone;
                    if (!ProjectionProcessing::getMapValue("zone", zone, projMap)) {
                        std::stringstream err;
                        err << "No zone found for Universal transverse Mercator in PROJ4 string '" << projName << "'";
                        throw std::runtime_error(err.str());
                    }

                    // Return projection
                    auto p = ProjectionParameters<double>();
                    p.type = 1;
                    p.cttype = 1;
                    p.a = a;
                    p.f = f;
                    p.x0 = (-183.0+zone*6.0);
                    p.k0 = 0.9996;
                    p.fe = 500000;
                    p.fn = projMap.find("SOUTH") != projMap.end() ? 10000000.0 : 0.0;

                    // Return projection
                    return p;

                } else if (projName == "TMERC") {

                    // Transverse Mercator
                    double x0, y0, fe, fn, k;
                    double phi_0 = 0.0;
                    bool success = true;
                    success &= ProjectionProcessing::getMapValue("lon_0", x0, projMap);
                    success &= ProjectionProcessing::getMapValue("x_0", fe, projMap);
                    success &= ProjectionProcessing::getMapValue("y_0", fn, projMap);
                    if (!ProjectionProcessing::getMapValue("k_0", k, projMap))
                        success &= (ProjectionProcessing::getMapValue("k", k, projMap));
                    ProjectionProcessing::getMapValue("lat_0", phi_0, projMap);
                    if (!success) {
                        std::stringstream err;
                        err << "Insufficient parameters for Mercator PROJ4 type '" << projName << "'";
                        throw std::runtime_error(err.str());
                    }

                    // Return projection
                    auto p = ProjectionParameters<double>();
                    p.type = 1;
                    p.cttype = 1;
                    p.a = a;
                    p.f = f;
                    p.x0 = x0;
                    p.k0 = k;
                    p.fe = fe;
                    p.fn = fn;
                    p.phi_0 = phi_0;

                    // Return projection
                    return p;

                } else if (projName == "MERC") {

                    // Mercator
                    double x0, fe, fn, k;
                    bool success = true;
                    success &= ProjectionProcessing::getMapValue("lon_0", x0, projMap);
                    success &= ProjectionProcessing::getMapValue("x_0", fe, projMap);
                    success &= ProjectionProcessing::getMapValue("y_0", fn, projMap);
                    if (!ProjectionProcessing::getMapValue("k_0", k, projMap))
                        success &= (ProjectionProcessing::getMapValue("k", k, projMap));
                    if (!success) {
                        std::stringstream err;
                        err << "Insufficient parameters for Mercator PROJ4 type '" << projName << "'";
                        throw std::runtime_error(err.str());
                    }

                    // Return projection
                    auto p = ProjectionParameters<double>();
                    p.type = 1;
                    p.cttype = 7;
                    p.a = a;
                    p.f = f;
                    p.x0 = x0;
                    p.k0 = k;
                    p.fe = fe;
                    p.fn = fn;

                    // Return projection
                    return p;

                } else if (projName == "LCC") {

                    // Lambert conformal conic
                    double x0, fe, fn, phi_0, phi_1, phi_2;
                    bool success = true;
                    success &= ProjectionProcessing::getMapValue("lon_0", x0, projMap);
                    success &= ProjectionProcessing::getMapValue("x_0", fe, projMap);
                    success &= ProjectionProcessing::getMapValue("y_0", fn, projMap);
                    success &= ProjectionProcessing::getMapValue("lat_0", phi_0, projMap);
                    success &= ProjectionProcessing::getMapValue("lat_1", phi_1, projMap);
                    success &= ProjectionProcessing::getMapValue("lat_2", phi_2, projMap);
                    if (!success) {
                        std::stringstream err;
                        err << "Insufficient parameters for Lambert conformal conic PROJ4 type '" << projName << "'";
                        throw std::runtime_error(err.str());
                    }

                    // Return projection
                    auto p = ProjectionParameters<double>();
                    p.type = 1;
                    p.cttype = 8;
                    p.a = a;
                    p.f = f;
                    p.x0 = x0;
                    p.k0 = 0.0;
                    p.fe = fe;
                    p.fn = fn;
                    p.phi_0 = phi_0;
                    p.phi_1 = phi_1;
                    p.phi_2 = phi_2;
                    return p;

                } else if (projName == "AEA") {

                    // Albers conic
                    double x0, fe, fn, phi_0, phi_1, phi_2;
                    bool success = true;
                    success &= ProjectionProcessing::getMapValue("lon_0", x0, projMap);
                    success &= ProjectionProcessing::getMapValue("x_0", fe, projMap);
                    success &= ProjectionProcessing::getMapValue("y_0", fn, projMap);
                    success &= ProjectionProcessing::getMapValue("lat_0", phi_0, projMap);
                    success &= ProjectionProcessing::getMapValue("lat_1", phi_1, projMap);
                    success &= ProjectionProcessing::getMapValue("lat_2", phi_2, projMap);
                    if (!success) {
                        std::stringstream err;
                        err << "Insufficient parameters for Albers conic PROJ4 type '" << projName << "'";
                        throw std::runtime_error(err.str());
                    }

                    // Return projection
                    auto p = ProjectionParameters<double>();
                    p.type = 1;
                    p.cttype = 11;
                    p.a = a;
                    p.f = f;
                    p.x0 = x0;
                    p.k0 = 0.0;
                    p.fe = fe;
                    p.fn = fn;
                    p.phi_0 = phi_0;
                    p.phi_1 = phi_1;
                    p.phi_2 = phi_2;
                    return p;

                } else if (projName == "SINU") {

                    // Sinusoidal
                    double x0 = 0.0;
                    double fe = 0.0;
                    double fn = 0.0;
                    ProjectionProcessing::getMapValue("lon_0", x0, projMap);
                    ProjectionProcessing::getMapValue("x_0", fe, projMap);
                    ProjectionProcessing::getMapValue("y_0", fn, projMap);

                    // Return projection
                    auto p = ProjectionParameters<double>();
                    p.type = 1;
                    p.cttype = 24;
                    p.a = a;
                    p.f = f;
                    p.x0 = x0;
                    p.fe = fe;
                    p.fn = fn;
                    return p;

                } else {
                    std::stringstream err;
                    err << "Projection type '" << projName << "' is not currently supported";
                    throw std::runtime_error(err.str());
                }

            } else {
                std::stringstream err;
                err << "No projection name found in PROJ4 string '" << PROJ4 << "'";
                throw std::runtime_error(err.str());
            }

        } else {
            std::stringstream err;
            err << "No parameters found in PROJ4 string '" << PROJ4 << "'";
            throw std::runtime_error(err.str());
        }
    }


    /*
    * Search EPSG code and convert proj params to %Projection object
    * @param EPSG string
    * @return %Projection object
    */
    ProjectionParameters<double> Projection::fromEPSG(const std::string EPSG) {
        auto projParams = Geostack::projParamsFromEPSG(EPSG);
        auto out = Projection::parsePROJ4(projParams);

        return out;
    }

    /*
    * Convert %Projection object into PROJ4 string
    * @param %Projection object
    * @return PROJ4 string
    */
    std::string Projection::toPROJ4(const ProjectionParameters<double> proj) {

        // Create output stream
        std::ostringstream projStr;
        projStr.precision(10);

        // Add projection type
        switch (proj.type) {
            case 1:
                switch (proj.cttype) {

                    case 1: {
                        projStr << "+proj=utm";
                        int zone = (int)((proj.x0+183.0)/6.0);
                        projStr << " +zone=" << zone;
                        if (proj.fn > 0.0) projStr << " +south";
                        } break;

                    case 7:
                        projStr << "+proj=merc";
                        projStr << " +lon_0=" << proj.x0;
                        projStr << " +x_0=" << proj.fe;
                        projStr << " +y_0=" << proj.fn;
                        projStr << " +k=" << proj.k0;
                        break;

                    case 8:
                        projStr << "+proj=lcc";
                        projStr << " +lon_0=" << proj.x0;
                        projStr << " +x_0=" << proj.fe;
                        projStr << " +y_0=" << proj.fn;
                        projStr << " +lat_0=" << proj.phi_0;
                        projStr << " +lat_1=" << proj.phi_1;
                        projStr << " +lat_2=" << proj.phi_2;
                        break;

                    case 11:
                        projStr << "+proj=aea";
                        projStr << " +lon_0=" << proj.x0;
                        projStr << " +x_0=" << proj.fe;
                        projStr << " +y_0=" << proj.fn;
                        projStr << " +lat_0=" << proj.phi_0;
                        projStr << " +lat_1=" << proj.phi_1;
                        projStr << " +lat_2=" << proj.phi_2;
                        break;

                    case 24:
                        projStr << "+proj=sinu";
                        projStr << " +lon_0=" << proj.x0;
                        projStr << " +x_0=" << proj.fe;
                        projStr << " +y_0=" << proj.fn;
                        break;

                    default:
                        throw std::runtime_error("Projection type not supported for PROJ4 conversion");
                }
                break;

            case 2:
                projStr << "+proj=longlat";
                break;

            default:
                throw std::runtime_error("Projection has no type for PROJ4 conversion");
        }

        // Scan for ellipsoid
        auto ellipsoidName = ProjectionTable::instance().getEllipsoidName(proj.a, proj.f);
        if (ellipsoidName.size() != 0) {
            projStr << " +ellps=" + ellipsoidName;
        } else {
            projStr << " +a=" << proj.a;
            projStr << " +b=" << proj.a*(1-proj.f);
        }

        // Add no default
        projStr << " +no_defs";

        return projStr.str();
    }

    /*
    * Convert %Projection object into WRK string
    * @param %Projection object
    * @return WRK string
    */
    std::string Projection::toWKT(const ProjectionParameters<double> proj, int crs) {

        // Check for valid projection
        if (proj.type == 0) {
            throw std::runtime_error("Projection has no type for PROJ4 conversion");
        }

        // Create geographic string
        std::ostringstream geoStr;
        geoStr.precision(16);
        geoStr << R"(GEOGCS["Unknown",)";
        geoStr << R"(DATUM["Unknown",)";
        geoStr << R"(SPHEROID[")" << ProjectionTable::instance().getEllipsoidName(proj.a, proj.f) << R"(",)";
        geoStr << proj.a << "," << (1.0/proj.f) << "]],";
        geoStr << R"(PRIMEM["Greenwich",0],)";             // TODO handle other prime meridians
        if (proj.type == 2 && crs > 0) {
            geoStr << R"(AUTHORITY["EPSG",")" + std::to_string(crs) + R"("],)"; // TODO handle other authority codes
        }
        geoStr << R"(UNIT["Degree",0.0174532925199433]])"; // TODO handle other units

        if (proj.type == 1) {

            // Create projection string
            std::ostringstream projStr;
            projStr.precision(16);

            projStr << R"(PROJCS["Unknown",)";
            projStr << geoStr.str() << ",";

            switch (proj.cttype) {

                case 1:

                    // Transverse Mercator
                    projStr << R"(PROJECTION["TRANSVERSE_MERCATOR"],)";
                    projStr << R"(PARAMETER["CENTRAL_MERIDIAN",)" << proj.x0 << "],";
                    projStr << R"(PARAMETER["SCALE_FACTOR",)" << proj.k0 << "],";
                    projStr << R"(PARAMETER["FALSE_EASTING",)" << proj.fe << "],";
                    projStr << R"(PARAMETER["FALSE_NORTHING",)" << proj.fn << "],";
                    projStr << R"(PARAMETER["LATITUDE_OF_ORIGIN",)" << proj.phi_0 << "],";
                    projStr << R"(AXIS["Easting",EAST],)";
                    projStr << R"(AXIS["Northing",NORTH],)";

                    break;

                case 7:

                    // Mercator
                    projStr << R"(PROJECTION["MERCATOR"],)";
                    projStr << R"(PARAMETER["CENTRAL_MERIDIAN",)" << proj.x0 << "],";
                    projStr << R"(PARAMETER["SCALE_FACTOR",)" << proj.k0 << "],";
                    projStr << R"(PARAMETER["FALSE_EASTING",)" << proj.fe << "],";
                    projStr << R"(PARAMETER["FALSE_NORTHING",)" << proj.fn << "],";
                    projStr << R"(AXIS["Easting",EAST],)";
                    projStr << R"(AXIS["Northing",NORTH],)";
                    break;

                case 8:

                    // Lambert conformal conic
                    projStr << R"(PROJECTION["LAMBERT_CONFORMAL_CONIC_2SP"],)";
                    projStr << R"(PARAMETER["CENTRAL_MERIDIAN",)" << proj.x0 << "],";
                    projStr << R"(PARAMETER["FALSE_EASTING",)" << proj.fe << "],";
                    projStr << R"(PARAMETER["FALSE_NORTHING",)" << proj.fn << "],";
                    projStr << R"(PARAMETER["LATITUDE_OF_ORIGIN",)" << proj.phi_0 << "],";
                    projStr << R"(PARAMETER["STANDARD_PARALLEL_1",)" << proj.phi_1 << "],";
                    projStr << R"(PARAMETER["STANDARD_PARALLEL_2",)" << proj.phi_2 << "],";
                    projStr << R"(AXIS["Easting",EAST],)";
                    projStr << R"(AXIS["Northing",NORTH],)";
                    break;

                case 11:

                    // Albers conic
                    projStr << R"(PROJECTION["ALBERS_CONIC_EQUAL_AREA"],)";
                    projStr << R"(PARAMETER["CENTRAL_MERIDIAN",)" << proj.x0 << "],";
                    projStr << R"(PARAMETER["FALSE_EASTING",)" << proj.fe << "],";
                    projStr << R"(PARAMETER["FALSE_NORTHING",)" << proj.fn << "],";
                    projStr << R"(PARAMETER["LATITUDE_OF_ORIGIN",)" << proj.phi_0 << "],";
                    projStr << R"(PARAMETER["STANDARD_PARALLEL_1",)" << proj.phi_1 << "],";
                    projStr << R"(PARAMETER["STANDARD_PARALLEL_2",)" << proj.phi_2 << "],";
                    projStr << R"(AXIS["Easting",EAST],)";
                    projStr << R"(AXIS["Northing",NORTH],)";
                    break;

                default:
                    throw std::runtime_error("Projection type not supported for WKT conversion");
            }

            // Add authority
            if (crs > 0) {
                projStr << R"(AUTHORITY["EPSG",")" + std::to_string(crs) + R"("],)"; // TODO handle other authority codes
            }

            // Add units
            projStr << R"(UNIT["Meter",1.0]])"; // TODO handle other units

            // Return projected coodinate system
            return projStr.str();
        }

        // Return geographic coodinate system
        return geoStr.str();
    }

    /**
    * Convert single %Coordinate between projections
    * @param c @Coordinate.
    * @param to projection to convert @Coordinate to.
    * @param from projection of the @Coordinate.
    */
    template <typename CTYPE>
    void Projection::convert(Coordinate<CTYPE> &c, ProjectionParameters<double> to, ProjectionParameters<double> from) {

        // Check for same projection
        if (to == from)
            return;

        // Projected to geographical transformation
        if (from.type == 0) {

            throw std::runtime_error("No projection type set to convert from");

        } else if (from.type == 1) {

            switch (from.cttype) {

                case 1:

                    // Transverse Mercator
                    c = ProjectionCalculation::fn_Transverse_Mercator_to_Ellipsoid<CTYPE>(c, &from);
                    break;

                case 7:

                    // Mercator
                    c = ProjectionCalculation::fn_Mercator_to_Ellipsoid<CTYPE>(c, &from);
                    break;

                case 8:

                    // Lambert conformal conic (2 point)
                    c = ProjectionCalculation::fn_Lambert_to_Ellipsoid<CTYPE>(c, &from);
                    break;

                case 11:

                    // Albers equal area
                    c = ProjectionCalculation::fn_Albers_to_Ellipsoid<CTYPE>(c, &from);
                    break;

                case 24:

                    // Sinusoidal
                    c = ProjectionCalculation::fn_Sinusoidal_to_Ellipsoid<CTYPE>(c, &from);
                    break;

                default: {

                    // Unsupported transform
                    std::stringstream err;
                    err << "Unsupported projection transform '" << (uint32_t)from.cttype << "'";
                    throw std::runtime_error(err.str());
                }

            }

        } else if (from.type == 2) {

            // No operation

        } else {

            // Unsupported type
            std::stringstream err;
            err << "Unsupported projection type '" << from.type << "'";
            throw std::runtime_error(err.str());
        }

        // Geographical to projected transformation
        if (to.type == 0) {

            throw std::runtime_error("No projection type set to convert to");

        } else if (to.type == 1) {

            switch (to.cttype) {

                case 1:

                    // Transverse Mercator
                    c = ProjectionCalculation::fn_Ellipsoid_to_Transverse_Mercator<CTYPE>(c, &to);
                    break;

                case 7:

                    // Mercator
                    c = ProjectionCalculation::fn_Ellipsoid_to_Mercator<CTYPE>(c, &to);
                    break;

                case 8:

                    // Lambert conformal conic (2 point)
                    c = ProjectionCalculation::fn_Ellipsoid_to_Lambert<CTYPE>(c, &to);
                    break;

                case 11:

                    // Albers equal area
                    c = ProjectionCalculation::fn_Ellipsoid_to_Albers<CTYPE>(c, &to);
                    break;

                case 24:

                    // Sinusoidal
                    c = ProjectionCalculation::fn_Ellipsoid_to_Sinusoidal<CTYPE>(c, &to);
                    break;

                default: {

                    // Unsupported transform
                    std::stringstream err;
                    err << "Unsupported projection transform '" << (uint32_t)to.cttype << "'";
                    throw std::runtime_error(err.str());
                }

            }

        } else if (to.type == 2) {

            // No operation

        } else {

            // Unsupported type
            std::stringstream err;
            err << "Unsupported projection type '" << to.type << "'";
            throw std::runtime_error(err.str());
        }
    }

    /**
    * Convert multiple %Coordinates between projections
    * @param c @Coordinate.
    * @param to projection to convert @Coordinate to.
    * @param from projection of the @Coordinate.
    */
    template <typename CTYPE>
    void Projection::convert(std::vector<Coordinate<CTYPE> > &c, ProjectionParameters<double> to, ProjectionParameters<double> from) {

        // Create VariablesVector
        VariablesVector<Coordinate<CTYPE> > v;

        // Copy data to VariablesVector
        v.getData() = c;

        // Convert projection
        Projection::convert(v, to, from);

        // Copy data from VariablesVector
        c = v.getData();
    }

    /**
    * Convert %VariablesVector between projections
    * @param v @VariablesVector.
    * @param to projection to convert @Coordinate to.
    * @param from projection of the @Coordinate.
    */
    template <typename CTYPE>
    void Projection::convert(VariablesVector<Coordinate<CTYPE> > &v, ProjectionParameters<double> to, ProjectionParameters<double> from) {

        // Check for same projection
        if (to == from)
            return;

        // Get OpenCL solver handle
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &context = solver.getContext();
        auto &queue = solver.getQueue();
        if (solver.openCLInitialised()) {

            try {

                // Create buffer
                cl_int err = CL_SUCCESS;

                // Create local OpenCL variables
                cl::Kernel projectionToEllipseKernel, projectionFromEllipseKernel;
                size_t clWorkgroupSize, clPaddedWorkgroupSize;

                // Projected to geographical transformation
                if (from.type == 0) {

                    throw std::runtime_error("No projection type set to convert from");

                } if (from.type == 1) {

                    switch (from.cttype) {

                        case 1:

                            // Transverse Mercator
                            projectionToEllipseKernel = ProjectionOperator::instance().getKernel("convert_Transverse_Mercator_to_Ellipsoid");
                            break;

                        case 7:

                            // Mercator
                            projectionToEllipseKernel = ProjectionOperator::instance().getKernel("convert_Mercator_to_Ellipsoid");
                            break;

                        case 8:

                            // Lambert conformal conic (2 point)
                            projectionToEllipseKernel = ProjectionOperator::instance().getKernel("convert_Lambert_to_Ellipsoid");
                            break;

                        case 11:

                            // Albers equal area
                            projectionToEllipseKernel = ProjectionOperator::instance().getKernel("convert_Albers_to_Ellipsoid");
                            break;

                        case 24:

                            // Sinusoidal
                            projectionToEllipseKernel = ProjectionOperator::instance().getKernel("convert_Sinusoidal_to_Ellipsoid");
                            break;

                        default: {

                            // Unsupported transform
                            std::stringstream err;
                            err << "Unsupported projection transform '" << from.cttype << "'";
                            throw std::runtime_error(err.str());
                        }

                    }

                    // Set up kernel
                    projectionToEllipseKernel.setArg(0, v.getBuffer());
                    projectionToEllipseKernel.setArg(1, (cl_uint)v.size());
                    projectionToEllipseKernel.setArg(2, ProjectionParameters<CTYPE>(from));

                    // Execute kernel
                    clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(projectionToEllipseKernel);
                    clPaddedWorkgroupSize = (size_t)(v.size()+((clWorkgroupSize-(v.size()%clWorkgroupSize))%clWorkgroupSize));
                    queue.enqueueNDRangeKernel(projectionToEllipseKernel, cl::NullRange,
                        cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));

                } else if (from.type == 2) {

                    // No operation

                } else {

                    // Unsupported type
                    std::stringstream err;
                    err << "Unsupported projection type '" << from.type << "'";
                    throw std::runtime_error(err.str());
                }

                // Geographical to projected transformation
                if (to.type == 0) {

                    throw std::runtime_error("No projection type set to convert to");

                } else if (to.type == 1) {

                    switch (to.cttype) {

                        case 1:

                            // Transverse Mercator
                            projectionFromEllipseKernel = ProjectionOperator::instance().getKernel("convert_Ellipsoid_to_Transverse_Mercator");
                            break;

                        case 7:

                            // Mercator
                            projectionFromEllipseKernel = ProjectionOperator::instance().getKernel("convert_Ellipsoid_to_Mercator");
                            break;

                        case 8:

                            // Lambert conformal conic (2 point)
                            projectionFromEllipseKernel = ProjectionOperator::instance().getKernel("convert_Ellipsoid_to_Lambert");
                            break;

                        case 11:

                            // Albers equal area
                            projectionFromEllipseKernel = ProjectionOperator::instance().getKernel("convert_Ellipsoid_to_Albers");
                            break;

                        case 24:

                            // Sinusoidal
                            projectionFromEllipseKernel = ProjectionOperator::instance().getKernel("convert_Ellipsoid_to_Sinusoidal");
                            break;

                        default: {

                            // Unsupported transform
                            std::stringstream err;
                            err << "Unsupported projection transform '" << to.cttype << "'";
                            throw std::runtime_error(err.str());
                        }

                    }

                    // Set up kernel
                    projectionFromEllipseKernel.setArg(0, v.getBuffer());
                    projectionFromEllipseKernel.setArg(1, (cl_uint)v.size());
                    projectionFromEllipseKernel.setArg(2, ProjectionParameters<CTYPE>(to));

                    // Execute kernel
                    clWorkgroupSize = solver.getKernelPreferredWorkgroupMultiple(projectionFromEllipseKernel);
                    clPaddedWorkgroupSize = (size_t)(v.size()+((clWorkgroupSize-(v.size()%clWorkgroupSize))%clWorkgroupSize));
                    queue.enqueueNDRangeKernel(projectionFromEllipseKernel, cl::NullRange,
                        cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));

                } else if (to.type == 2) {

                    // No operation

                } else {

                    // Unsupported type
                    std::stringstream err;
                    err << "Unsupported projection type '" << to.type << "'";
                    throw std::runtime_error(err.str());
                }

            } catch (cl::Error& e) {
                std::stringstream err;
                err << "OpenCL exception '" << e.what() << "': " << e.err();
                throw std::runtime_error(err.str());
            }
        }
    }

    /**
    * Check whether projection type is supported.
    * @param proj projection to check.
    */
    bool Projection::checkProjection(const ProjectionParameters<double> &proj) {


        // Projected type
        if (proj.type == 1 && (
            proj.cttype == 1 ||
            proj.cttype == 7 ||
            proj.cttype == 8 ||
            proj.cttype == 11 ||
            proj.cttype == 24)) {
            return true;
        }

        // Geographic type
        if (proj.type == 2 && proj.cttype == 0) {
            return true;
        }

        // Invalid projection
        return false;
    }

    /**
    * %ProjectionParameters equal operator
    * @param l left-hand projection to compare.
    * @param r right-hand projection to compare.
    */
    bool operator==(const ProjectionParameters<double> &l, const ProjectionParameters<double> &r) {

        // Compare values in projection structure
        if (l.type == r.type &&
            l.cttype == r.cttype &&
            l.a == r.a &&
            l.f == r.f &&
            l.x0 == r.x0 &&
            l.k0 == r.k0 &&
            l.fe == r.fe &&
            l.fn == r.fn &&
            l.phi_0 == r.phi_0 &&
            l.phi_1 == r.phi_1 &&
            l.phi_2 == r.phi_2)
            return true;

        return false;
    }

    /**
    * %ProjectionParameters not equal operator
    * @param l left-hand projection to compare.
    * @param r right-hand projection to compare.
    */
    bool operator!=(const ProjectionParameters<double> &l, const ProjectionParameters<double> &r) {
        return !operator==(l, r);
    }

    /**
    * %ProjectionParameters output stream
    * @param os output stream.
    * @param p %ProjectionParameters.
    * @return updated ostream.
    */
    std::ostream &operator<<(std::ostream &os, const ProjectionParameters<double> &p) {

        // Write data
        return os <<
            (int)p.type << ", " <<
            (int)p.cttype << ", " <<
            p.a << ", " <<
            p.f << ", " <<
            p.x0 << ", " <<
            p.k0 << ", " <<
            p.fe << ", " <<
            p.fn << ", " <<
            p.phi_0 << ", " <<
            p.phi_1 << ", " <<
            p.phi_2;
    }

    // CTYPE float definitions
    template struct ProjectionParameters<float>;
    template void Projection::convert(Coordinate<float> &, ProjectionParameters<double>, ProjectionParameters<double>);
    template void Projection::convert(std::vector<Coordinate<float> > &, ProjectionParameters<double>, ProjectionParameters<double>);
    template void Projection::convert(VariablesVector<Coordinate<float> > &, ProjectionParameters<double>, ProjectionParameters<double>);

    // CTYPE double definitions
    template struct ProjectionParameters<double>;
    template void Projection::convert(Coordinate<double> &, ProjectionParameters<double>, ProjectionParameters<double>);
    template void Projection::convert(std::vector<Coordinate<double> > &, ProjectionParameters<double>, ProjectionParameters<double>);
    template void Projection::convert(VariablesVector<Coordinate<double> > &, ProjectionParameters<double>, ProjectionParameters<double>);

    // Mixed definitions
    template ProjectionParameters<double>::ProjectionParameters(const ProjectionParameters<float> &);
    template ProjectionParameters<float>::ProjectionParameters(const ProjectionParameters<double> &);
}
