/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include "gs_models.h"
 
namespace Geostack
{    
    /**
    * %Models static definitions
    */
    const std::string Models::cl_fill_c = std::string(R_cl_fill_c);
    const std::string Models::cl_interleave_c = std::string(R_cl_interleave_c);
    const std::string Models::cl_raster_head_c = std::string(R_cl_raster_head_c);
    const std::string Models::cl_raster_area_op2D_c = std::string(R_cl_raster_area_op2D_c);
    const std::string Models::cl_raster_block_c = std::string(R_cl_raster_block_c);
    const std::string Models::cl_map_distance_c = std::string(R_cl_map_distance_c);
    const std::string Models::cl_rasterise_c = std::string(R_cl_rasterise_c);
    const std::string Models::cl_random_c = std::string(R_cl_random_c);
    const std::string Models::cl_vectorise_c = std::string(R_cl_vectorise_c);
    const std::string Models::cl_projection_c = std::string(R_cl_projection_c);
    const std::string Models::cl_projection_head_c = std::string(R_cl_projection_head_c);
    const std::string Models::cl_index_c = std::string(R_cl_index_c);
    const std::string Models::cl_raster_indexed_block_c = std::string(R_cl_raster_indexed_block_c);
    const std::string Models::cl_raster_index_components_c = std::string(R_cl_raster_index_components_c);
    const std::string Models::cl_raster_index_reduce_c = std::string(R_cl_raster_index_reduce_c);
    const std::string Models::cl_sort_c = std::string(R_cl_sort_c);
    const std::string Models::cl_stipple_c = std::string(R_cl_stipple_c);
    const std::string Models::cl_variables_block_c = std::string(R_cl_variables_block_c);
    const std::string Models::cl_vector_block_c = std::string(R_cl_vector_block_c);

}