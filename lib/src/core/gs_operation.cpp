/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

#include <vector>
#include <iostream>
#include <chrono>
#include <regex>

#include <iomanip>
#ifdef _WIN32
#include <direct.h>
#elif  __linux__
#include <unistd.h>
#endif

#include "json11.hpp"
#include "yaml.hpp"

#include "gs_operation.h"
#include "gs_string.h"
#include "gs_series.h"
#include "gs_geojson.h"

using namespace json11;
using namespace std::chrono;

namespace Geostack {

    Json yamlToJson(Yaml::Node &yItemIn) {

        if (yItemIn.IsMap()) {

            // Parse objects
            auto jObj = Json::object();
            for (auto it = yItemIn.Begin(); it != yItemIn.End(); it++) {

                auto name = (*it).first;
                auto &yItem = (*it).second;

                if (yItem.IsScalar()) {

                    // Detect value type
                    auto str = yItem.As<std::string>();
                    if (Strings::isNumber(str)) {
                        jObj[name] = Strings::toNumber<double>(str);
                    } else {
                        jObj[name] = str;
                    }

                } else {

                    // Parse map and array objects
                    jObj[name] = yamlToJson(yItem);

                }
            }
            return jObj;

        } else if (yItemIn.IsSequence()) {

            // Parse array objects
            Json::array jArray;
            for (std::size_t i = 0; i < yItemIn.Size(); i++) {
                if (yItemIn[i].IsScalar()) {

                    // Detect value type
                    auto str = yItemIn[i].As<std::string>();
                    if (Strings::isNumber(str)) {
                        jArray.push_back(Strings::toNumber<double>(str));
                    } else {
                        jArray.push_back(str);
                    }
                } else {
                    jArray.push_back(yamlToJson(yItemIn[i]));
                }
            }
            return jArray;
        }

        return Json();
    }

    // Yaml to Json parser
    Json readYaml(std::string configFileStr) {
        // Parse yml
        Yaml::Node ymlRoot;
        Yaml::Parse(ymlRoot, configFileStr);

        // Convert to json
        auto config = yamlToJson(ymlRoot);
        return config;   
    }

    // Get values from json
    std::string getString(std::string name, jsonPair &item, bool throwError = true, std::string default_value = "") {
        auto value = item.second[name].string_value();
        if (value.length() == 0) {
            if (throwError) {
                throw std::runtime_error("Operation '" + item.first + "' must specify '" + name + "'");
            } else {
                return default_value;
            }
        }
        return value;
    }

    double getNumber(std::string name, jsonPair &item, bool throwError = true, double default_value = 0.0) {
        if (item.second[name].is_number()) {
            return item.second[name].number_value();
        } else {
            if (throwError) {
                throw std::runtime_error("Operation '" + item.first + "' must specify '" + name + "'");
            } else {
                return default_value;
            }
        }
    }

    std::vector<std::string> getStringArray(std::string name, jsonPair &item, bool throwError = true, std::vector<std::string> default_value = {}) {
        if (item.second[name].is_array()) {
            std::vector<std::string> array;
            for (auto &item_value : item.second[name].array_items()) {
                if (item_value.is_string()) {
                    array.push_back(item_value.string_value());
                }
            }
            return array;
        } else {
            if (throwError) {
                throw std::runtime_error("Operation '" + item.first + "' must specify '" + name + "'");
            } else {
                return default_value;
            }
        }
    }

    template <typename T>
    std::vector<T> getNumericArray(std::string name, jsonPair &item, bool throwError = true, std::vector<T> default_value = {}) {
        if (item.second[name].is_array()) {
            std::vector<T> array;
            for (auto &item_value : item.second[name].array_items()) {
                if (item_value.is_number()) {
                    array.push_back(item_value.number_value());
                }
            }
            return array;
        } else {
            if (throwError) {
                throw std::runtime_error("Operation '" + item.first + "' must specify '" + name + "'");
            } else {
                return default_value;
            }
        }
    }

    // Create new raster with dimensions of first in list
    template <typename T>
    RasterBasePtr<T> createRaster(std::string name, std::vector<RasterBasePtr<T> > &rasters, std::string type = "") {

        // Get dimensions
        if (rasters.size() == 0) {

            // Get type
            if (type.length() == 0) {
                type = "REAL";
            }
            type = Strings::toUpper(type);

            // Create raster
            if (type == "REAL") {
                auto rp = std::make_shared<Raster<T, T> >(name);
                rasters.push_back(rp);
                return rp;
            } else if (type == "UINT") {
                auto rp = std::make_shared<Raster<uint32_t, T> >(name);
                rasters.push_back(rp);
                return rp;
            } else if (type == "UCHAR") {
                auto rp = std::make_shared<Raster<uint8_t, T> >(name);
                rasters.push_back(rp);
                return rp;
            } else {
                throw std::runtime_error("Unknown creation type '" + type + "'");
            }

        } else {
            auto dims = rasters[0]->getRasterDimensions();

            // Get type
            if (type.length() == 0) {
                type = rasters[0]->getOpenCLTypeString();
            }
            type = Strings::toUpper(type);

            // Create raster
            if (type == "REAL") {
                auto rp = std::make_shared<Raster<T, T> >(name);
                rp->init(dims.d);
                rp->setProjectionParameters(rasters[0]->getProjectionParameters());
                rasters.push_back(rp);
                return rp;
            } else if (type == "UINT") {
                auto rp = std::make_shared<Raster<uint32_t, T> >(name);
                rp->init(dims.d);
                rp->setProjectionParameters(rasters[0]->getProjectionParameters());
                rasters.push_back(rp);
                return rp;
            } else if (type == "UCHAR") {
                auto rp = std::make_shared<Raster<uint8_t, T> >(name);
                rp->init(dims.d);
                rp->setProjectionParameters(rasters[0]->getProjectionParameters());
                rasters.push_back(rp);
                return rp;
            } else {
                throw std::runtime_error("Unknown creation type '" + type + "'");
            }
        }
        return nullptr;
    }

    template <typename T>
    void Operation<T>::runFromConfigFile(std::string configFileName) {

        // Open file
        std::ifstream fConfig(configFileName, std::ios::in | std::ios::binary);
        if (!fConfig) {
            throw std::runtime_error("Cannot open configuration file '" + configFileName + "' for operation");
        }

        // Change working directory to config directory
        auto filePath = Strings::splitPath(configFileName);
        if (filePath[0].length() > 0) {
            int rc = chdir(filePath[0].c_str());
            
            if (rc < 0) {
                throw std::runtime_error("Unable to change directory to config directory.");
            }            
        }

        // Read file into string
        std::string configFileStr;
        fConfig.seekg(0, std::ios::end);
        configFileStr.resize(fConfig.tellg());
        fConfig.seekg(0, std::ios::beg);
        fConfig.read(&configFileStr[0], configFileStr.size());
        fConfig.close();

        // Parse config
        std::string jsonConfig;
        if (Strings::toLower(filePath[2]) == "json") {
            jsonConfig = configFileStr;
        } else if (Strings::toLower(filePath[2]) == "yml" || Strings::toLower(filePath[2]) == "yaml") {

            // Parse yml and Convert to json
            auto config = readYaml(configFileStr);
            jsonConfig = config.dump();

        } else {
            throw std::runtime_error("Unrecognised config extension '" + filePath[2] + "' for operation");
        }

        // Parse json
        std::string jsonErr;
        auto config = Json::parse(jsonConfig, jsonErr);
        if (!jsonErr.empty()) {
            throw std::runtime_error(jsonErr);
        }

        // Run operation
        if (!config["operations"].is_null()) {
            std::vector<RasterBasePtr<T> > rasterLayerPtrs;
            std::vector<VectorPtr<T> > vectorLayerPtrs;
            Operation<T>::run(config["operations"].dump(), rasterLayerPtrs, vectorLayerPtrs);
        } else {
            throw std::runtime_error("Object 'operations' not found in configuration");
        }
    }

    /*
        General-purpose operation for running multiple chained scripts, commands are:

            - solver:
                type: name        # Solver type
                config: {}        # Solver configuration
                source: name      # Name of vector for source

            - variables:
                A: 1.0            # Create single variable
                B:
                    - 1.0         # Create array variable with first value of 1.0
                    - 2.0         # Create array variable with second value of 2.0
                C:
                    size: 4       # Create array variable of size 4
                script: |
                    B = A         # Run script in parallel over all variables

            - series:
                csv: name         # Name of csv file to use to generate series

            - createRaster:             # Create new raster layer, dimensions are same as first raster
                name: rA          # Name 'A'
                type: REAL        # Raster type of REAL (default), uint or uchar

            - runRasterScript:          # Run script over all rasters in list
                create: rB        # Optional name of new raster to create
                script: |
                    rB = rA       # Script to run

            - runAreaScript:      # Run area script over all rasters in list
                create: rC        # Optionally create new raster, 'output' must be specified
                width: 1          # Width of script window
                script: |
                    output = 1    # Script to run

            - mapDistance:                      # Map distance from vector
                name: vA                        # Vector to map
                create: rD                      # New raster to create
                resolution: 1                   # Resolution of raster to create
                script:                         # Optional script to use for mapping
                bounds: [-2, -2, 2, 2]          # create bounds (optional) [xmin, ymin, xmax, ymax]

            - rasterise:                        # Rasterise vector
                name: vA                        # Vector to rasterise
                create: rD                      # New raster to create
                resolution: 1                   # Resolution of raster to create
                script:                         # Optional script to use for mapping
                bounds: [-2, -2, 2, 2]          # create bounds (optional) [xmin, ymin, xmax, ymax]

            - convertVector:      # Convert vector
                name: vA          # Vector to convert
                create: rD        # Optional name of new vector to create

            - vectorise:
                name: rA          # Raster to vectorise
                create: vA        # New vector to create
                values: [1]       # values for iso vectors
                nullValue: 2      # null value (optional)

            - write:              # Write raster or vector
                name: vA          # Name of item to write
                destination:      # Path and name of file
    */

    template <typename T>
    void Operation<T>::run(std::string jsonConfig,
        std::vector<RasterBasePtr<T> > &rastersSuper,
        std::vector<VectorPtr<T> > &vectorsSuper) {

        // Parse configuration
        std::string jsonErr;
        auto config = Json::parse(jsonConfig, jsonErr);
        if (!jsonErr.empty()) {
            throw std::runtime_error(jsonErr);
        }

        if (config.is_array()) {

            // Local layers
            std::vector<RasterBasePtr<T> > rasters;
            std::vector<VectorPtr<T> > vectors;

            // Local variables
            std::shared_ptr<Variables<T, std::string> > variables;
            variables = std::make_shared<Variables<T, std::string> >();

            // Local series
            std::vector<Series<int64_t, T> > series; // TODO make shared pointer

            // Timing information
            std::vector<std::pair<std::string, double> > timings;

            // Layer name
            std::set<std::string> names;
            for (auto &r : rasters) {
                if (r->hasProperty("name")) {
                    names.insert(r->template getProperty<std::string>("name"));
                } else {
                    throw std::runtime_error("Unnamed raster cannot be used for operation");
                }
            }
            for (auto &v : vectors) {
                if (v->hasProperty("name")) {
                    names.insert(v->template getGlobalProperty<std::string>("name"));
                } else {
                    throw std::runtime_error("Unnamed vector cannot be used for operation");
                }
            }

            // Parse dependencies
            std::map<std::string, std::size_t> layerLastUsed;
            std::size_t index = 0;
            for (auto operation : config.array_items()) {
                if (operation.is_object()) {
                    for (auto &item : operation.object_items()) {
                        if (item.second.is_object()) {

                            // Parse objects
                            auto name = item.second["name"].string_value();
                            if (name.length() > 0) {
                                layerLastUsed[name] = index;
                            }

                            auto create = item.second["create"].string_value();
                            if (create.length() > 0) {
                                layerLastUsed[create] = index;
                            }

                            auto script = item.second["script"].string_value();
                            if (script.length() > 0) {
                                for (auto layer : layerLastUsed) {
                                    auto regex = std::regex("\\b" + layer.first + "\\b");
                                    if (std::regex_search(script, regex)) {
                                        layerLastUsed[layer.first] = index;
                                    }
                                }
                            }
                        } else if (item.second.is_array()) {
                            for (auto &val : item.second.array_items()) {

                                // Parse array
                                if (val.is_object()) {
                                    auto aItem = jsonPair({ item.first, val });
                                    auto name = aItem.second["name"].string_value();
                                    if (name.length() > 0) {
                                        layerLastUsed[name] = index;
                                    }

                                    auto create = aItem.second["create"].string_value();
                                    if (create.length() > 0) {
                                        layerLastUsed[create] = index;
                                    }
                                }
                            }
                        } else {
                            throw std::runtime_error("Value of JSON keys should be either JSON object or JSON array.");
                        }
                    }
                    index++;
                } else {
                    throw std::runtime_error("No operations are defined.");
                }
            }

            // Loop through operation list
            index = 0;
            Geostack::Solver &solver = Geostack::Solver::getSolver();
            uint8_t verbose = solver.getVerboseLevel();
            for (auto operation : config.array_items()) {
                if (operation.is_object()) {

                    for (auto &item : operation.object_items()) {

                        // Output operation information
                        steady_clock::time_point opTime;
                        if (verbose <= Geostack::Verbosity::Info) {
                            opTime = steady_clock::now();
                            std::cout << "Operation: " << item.first << std::endl;
                        }

                        if (item.second.is_object()) {

                            // Solver operation
                            if (item.first == "solver") {

                                // Get parameters
                                auto type = getString("type", item);

                                // TODO
                            }

                            // Variables operation
                            else if (item.first == "variables") {
                                // Read variables
                                for (auto &vItem : item.second.object_items()) {
                                    if (vItem.second.is_array()) {
                                        auto vArray = vItem.second.array_items();
                                        for (std::size_t i = 0; i < vArray.size(); i++) {
                                            variables->set(vItem.first, (T)vArray[i].number_value(), i);
                                        }
                                    } else if (vItem.second.is_object()) {

                                        std::size_t vSize = 0;
                                        for (auto &vObjItem : vItem.second.object_items()) {
                                            if (vObjItem.first == "size") {

                                                // Create new variable of given size
                                                vSize = (std::size_t)vObjItem.second.number_value();
                                                for (std::size_t i = 0; i < vSize; i++) {
                                                    variables->set(vItem.first, getNullValue<T>(), i);
                                                }
                                            }
                                        }
                                        if (vSize == 0) {
                                            throw std::runtime_error("Variable creation requires 'size' parameter for operation");
                                        }

                                    } else {
                                        variables->set(vItem.first, vItem.second.number_value());
                                    }
                                }
                            }

                            // Series operation
                            else if (item.first == "series") {

                                // Get parameters
                                auto csv = getString("csv", item);

                                // Open CSV file
                                std::ifstream seriesStream(csv);

                                // Parse CSV
                                std::string line;
                                if (std::getline(seriesStream, line)) {

                                    // Read headers
                                    auto splitLine = Strings::split(line, ',');
                                    for (int i = 1; i < splitLine.size(); i++) {
                                        series.push_back(Series<int64_t, T>(splitLine[i]));
                                    }

                                    // Read rows
                                    while (std::getline(seriesStream, line)) {

                                        // Split into list
                                        auto splitLine = Strings::split(line, ',');
                                        for (int i = 1; i < splitLine.size(); i++) {
                                            series[i-1].addValue(splitLine[0], (T)std::stod(splitLine[i]));
                                        }
                                    };
                                }
                            }

                            // Write operation
                            else if (item.first == "write") {

                                // Get parameters
                                auto name = getString("name", item);
                                auto destination = getString("destination", item);

                                // Search for name
                                bool found = false;
                                for (auto &r : rasters) {
                                    if (r->template getProperty<std::string>("name") == name) {
                                        r->write(destination);
                                        found = true;
                                        break;
                                    }
                                }
                                for (auto &v : vectors) {
                                    if (v->template getGlobalProperty<std::string>("name") == name) {
                                        v->write(destination);
                                        found = true;
                                        break;
                                    }
                                }

                                // Check if layer has been processed
                                if (!found) {
                                    throw std::runtime_error("Layer '" + name + "' not found for write operation");
                                }
                            }

                            // Raster creation operation
                            else if (item.first == "createRaster") {

                                // Get parameters
                                auto name = getString("name", item);
                                auto type = getString("type", item, false, "REAL");
                                auto source = getString("source", item, false, "");
                                auto projection = getString("projection", item, false, "");
                                auto resample = getNumber("resample", item, false);

                                // Create raster
                                auto rp = createRaster(name, rasters, type);

                                // Read from source
                                if (rp && source.length() > 0) {
                                    rp->read(source);
                                }

                                // Apply projection
                                if (rp && projection.length() > 0) {
                                    auto proj = Projection::parseProjString(projection);
                                    rp->setProjectionParameters(proj);
                                }

                                // Apply resampling
                                if (resample > 0.0) {

                                    // Create sample raster
                                    auto dims = rp->getRasterDimensions().d;
                                    auto sp = std::make_shared<Raster<T, T> >(name + "_SAMPLE");
                                    sp->init(rp->getBounds(), resample, resample, dims.nz);
                                    sp->setProjectionParameters(rp->getProjectionParameters());

                                    // Run sample script
                                    runScript<T>(name + "_SAMPLE = " + name, { *sp, *rp } );
                                    sp->template setProperty<std::string>("name", name);
                                    rasters.back() = sp;
                                }
                            }

                            // Vector creation operation
                            else if (item.first == "createVector") {

                                // Get parameters
                                auto name = getString("name", item);
                                auto source = getString("source", item, false, "");
                                auto json = getString("json", item, false, "");
                                auto projection = getString("projection", item, false, "");

                                // Create vector
                                auto vp = std::make_shared<Vector<T> >();
                                if (vp) {

                                    // Read from source
                                    if (source.length() > 0) {
                                        vp->read(source, item.second.dump());
                                    } else if (json.length() > 0) {
                                        *vp = GeoJson<T>::geoJsonToVector(json);
                                    }

                                    // Apply projection
                                    if (projection.length() > 0) {
                                        auto proj = Projection::parseProjString(projection);
                                        *vp = vp->convert(proj);
                                    }

                                    // Update name
                                    vp->template setGlobalProperty<std::string>("name", name);

                                    // Add to vector list
                                    vectors.push_back(vp);
                                }
                            }

                            // Raster script operation
                            else if (item.first == "runRasterScript") {

                                // Get parameters
                                auto script = getString("script", item);
                                auto createName = getString("create", item, false);

                                if (createName.length() > 0) {

                                    // Check name
                                    if (names.find(createName) != names.end()) {
                                        throw std::runtime_error("Cannot use duplicate layer name '" + createName + "' for script operation");
                                    }
                                    names.insert(createName);

                                    // Create new raster
                                    createRaster(createName, rasters);
                                }

                                // Create input layers
                                RasterBaseRefs<T> rastersRefs;
                                for (auto rp : rasters) {
                                    rastersRefs.push_back(*rp);
                                }
                                for (auto rp : rastersSuper) {
                                    rastersRefs.push_back(*rp);
                                }
                                runScriptNoOut<T>( script, rastersRefs);
                            }

                            // Vector script operation
                            else if (item.first == "runVectorScript") {

                                // Get parameters
                                auto name = getString("name", item);
                                auto script = getString("script", item);
                                auto properties = getStringArray("properties", item, false);

                                // Search for name in vectors
                                bool found = false;
                                for (auto &v : vectors) {
                                    if (v->template getGlobalProperty<std::string>("name") == name) {

                                        // Add new properties
                                        for (auto &property: properties) {
                                            v->addProperty(property);
                                        }

                                        // Search for target name
                                        std::vector<std::string> rasterNames;
                                        for (auto &r : rasters) {
                                            auto name = r->template getProperty<std::string>("name");
                                            if (name.length() > 0) {

                                                // Search for name in script
                                                auto regex = std::regex("\\b" + name + "\\b");
                                                if (std::regex_search(script, regex)) {

                                                    // Update name list
                                                    rasterNames.push_back(name);
                                                }
                                            }
                                        }
                                        if (rasterNames.size() == 0) {

                                            // Script with no raster references
                                            v->runScript(script);

                                        } else {

                                            // Check reduction
                                            auto reduction = getString("reduction", item);
                                            Reduction::Type reductionType = Reduction::None;
                                            if (reduction == "count") {
                                                reductionType = Reduction::Count;
                                            } else if (reduction == "max" || reduction == "maximum") {
                                                reductionType = Reduction::Maximum;
                                            } else if (reduction == "mean") {
                                                reductionType = Reduction::Mean;
                                            } else if (reduction == "min" || reduction == "minimum") {
                                                reductionType = Reduction::Minimum;
                                            } else if (reduction == "sum") {
                                                reductionType = Reduction::Sum;
                                            } else if (reduction == "sumsquares") {
                                                reductionType = Reduction::SumSquares;
                                            }

                                            // Create input layers
                                            RasterBaseRefs<T> rastersRefs;
                                            for (auto rp : rasters) {
                                                rastersRefs.push_back(*rp);
                                            }
                                            for (auto rp : rastersSuper) {
                                                rastersRefs.push_back(*rp);
                                            }

                                            // Run script with raster references
                                            runVectorScript<T, T>(script, *v, rastersRefs, reductionType);
                                        }

                                        found = true;
                                        break;
                                    }
                                }

                                // Check vector has been processed
                                if (!found) {
                                    throw std::runtime_error("Vector '" + name + "' not found for script operation");
                                }
                            }

                            // Area script operation
                            else if (item.first == "runAreaScript") {

                                // Get dimensions
                                if (rasters.size() == 0) {
                                    throw std::runtime_error("At least one raster layer required for area script operation");
                                }

                                // Get parameters
                                auto script = getString("script", item);
                                auto width = getNumber("width", item);

                                // Search for target name
                                std::size_t rasterIndex = 0;
                                std::vector<std::string> rasterNames;
                                for (std::size_t index = 0; index < rasters.size(); index++) {
                                    auto name = rasters[index]->template getProperty<std::string>("name");
                                    if (name.length() > 0) {

                                        // Search for name in script
                                        auto regex = std::regex("\\b" + name + "\\b");
                                        if (std::regex_search(script, regex)) {

                                            // Update name list
                                            rasterNames.push_back(name);
                                            if (rasterNames.size() == 1) {
                                                rasterIndex = index;
                                            }
                                        }
                                    }
                                }
                                if (rasterNames.size() == 0) {
                                    throw std::runtime_error("At least one raster layer required for area script operation");
                                } else if (rasterNames.size() > 1) {
                                    throw std::runtime_error("At least one raster layer required for area script operation");
                                }

                                // Get (optional) output
                                bool createOutput = true;
                                auto createName = item.second["create"].string_value();
                                if (createName.length() == 0) {
                                    createName = rasterNames[0];
                                    createOutput = false;
                                } else {

                                    // Check name
                                    if (names.find(createName) != names.end()) {
                                        throw std::runtime_error("Cannot use duplicate layer name '" + createName + "' for area script operation");
                                    }
                                    names.insert(createName);
                                }

                                // Run script
                                auto rasterType = rasters[rasterIndex]->getOpenCLTypeString();
                                if (rasterType == "REAL") {
                                    auto rp = std::make_shared<Raster<T, T> >(createName);
                                    *rp = runAreaScript<T, T>(script, *rasters[rasterIndex], (int)width);
                                    rp->template setProperty<std::string>("name", createName);
                                    if (createOutput) {
                                        rasters.push_back(rp);
                                    } else {
                                        rasters[rasterIndex] = rp;
                                    }
                                    // createOutput ? rasters.push_back(rp) : rasters[rasterIndex] = rp;
                                } else if (rasterType == "UINT") {
                                    auto rp = std::make_shared<Raster<uint32_t, T> >(createName);
                                    *rp = runAreaScript<uint32_t, T>(script, *rasters[rasterIndex], (int)width);
                                    rp->template setProperty<std::string>("name", createName);
                                    // createOutput ? rasters.push_back(rp) : rasters[rasterIndex] = rp;
                                    if (createOutput) {
                                        rasters.push_back(rp);
                                    } else {
                                        rasters[rasterIndex] = rp;
                                    }
                                } else if (rasterType == "UCHAR") {
                                    auto rp = std::make_shared<Raster<uint8_t, T> >(createName);
                                    *rp = runAreaScript<uint8_t, T>(script, *rasters[rasterIndex], (int)width);
                                    rp->template setProperty<std::string>("name", createName);
                                    // createOutput ? rasters.push_back(rp) : rasters[rasterIndex] = rp;
                                    if (createOutput) {
                                        rasters.push_back(rp);
                                    } else {
                                        rasters[rasterIndex] = rp;
                                    }
                                } else {
                                    throw std::runtime_error("rasterType '" + rasterType + "' is not valid!");
                                }
                            }

                            // Script operation
                            else if (item.first == "runVariablesScript") {

                                // Get parameters
                                auto script = getString("script", item);
                                
                                // Run script
                                variables->runScript(script);
                            }

                            // Vector conversion operation
                            else if (item.first == "convertVector") {

                                // Get parameters
                                auto name = getString("name", item);
                                auto type = getString("type", item);
                                auto createName = getString("create", item, false);

                                // Search for name in vectors
                                bool found = false;
                                for (std::size_t index = 0; index < vectors.size(); index++) {
                                    if (vectors[index]->template getGlobalProperty<std::string>("name") == name) {

                                        // Convert vector
                                        auto vp = std::make_shared<Vector<T> >();
                                        if (Strings::toLower(type) == "point") {
                                            *vp = vectors[index]->convert(GeometryType::Point);
                                        } else if (Strings::toLower(type) == "lines" || Strings::toLower(type) == "linestring") {
                                            *vp = vectors[index]->convert(GeometryType::LineString);
                                        } else if (Strings::toLower(type) == "polygon") {
                                            *vp = vectors[index]->convert(GeometryType::Polygon);
                                        } else {
                                            throw std::runtime_error("Unknown vector conversion type '" + type + "' for convert operation");
                                        }

                                        // Update vector list
                                        if (createName.length() > 0) {

                                            // Check name
                                            if (names.find(createName) != names.end()) {
                                                throw std::runtime_error("Cannot use duplicate layer name '" + createName + "' for convert operation");
                                            }
                                            names.insert(createName);

                                            // Create Vector
                                            vp->template setGlobalProperty<std::string>("name", createName);
                                            vectors.push_back(vp);

                                        } else {

                                            // Update Vector
                                            vp->template setGlobalProperty<std::string>("name", name);
                                            vectors[index] = vp;
                                        }
                                        found = true;
                                        break;
                                    }
                                }

                                // Check vector has been processed
                                if (!found) {
                                    throw std::runtime_error("Vector '" + name + "' not found for convert operation");
                                }
                            }

                            // Vector mapping operation
                            else if (item.first == "mapDistance" || item.first == "rasterise") {

                                // Get parameters
                                auto name = getString("name", item);
                                auto createName = getString("create", item);
                                auto script = getString("script", item, false);
                                auto levelProperty = getString("levelProperty", item, false);
                                auto bounds = getNumericArray<T>("bounds", item, false);
                                
                                auto boundingBox = BoundingBox<T>();
                                if (bounds.size() > 0) {
                                    boundingBox = BoundingBox<T>( { {bounds[0], bounds[1]}, {bounds[2], bounds[3]} } );
                                }

                                // Check name
                                if (names.find(createName) != names.end()) {
                                    throw std::runtime_error("Cannot use duplicate layer name '" + createName + "' for mapping operation");
                                }
                                names.insert(createName);

                                bool found = false;
                                for (auto &v : vectors) {
                                    if (v->template getGlobalProperty<std::string>("name") == name) {

                                        // Get levels
                                        std::size_t levels = 0;
                                        if (levelProperty.length() > 0 && v->hasProperty(levelProperty)) {
                                            auto &properties = v->getProperties();
                                            levels = properties.template reductionFromVector<std::int32_t>(levelProperty, Reduction::Maximum);
                                        }
                                        levels++;

                                        auto rp = std::make_shared<Raster<T, T> >(createName);
                                        if (item.first == "rasterise") {

                                            // Apply rasterisation
                                            if (rasters.size() > 0) {
                                                auto dims = rasters[0]->getRasterDimensions().d;
                                                dims.nz = levels;
                                                rp->init(dims);
                                                rp->setProjectionParameters(rasters[0]->getProjectionParameters());
                                                rp->rasterise(*v, script, GeometryType::All, levelProperty);
                                            } else {
                                                auto resolution = getNumber("resolution", item);
                                                *rp = v->template rasterise<T>(resolution, script, GeometryType::All, boundingBox);
                                            }
                                        } else {

                                            // Apply distance mapping
                                            if (rasters.size() > 0) {
                                                // Map distance
                                                auto dims = rasters[0]->getRasterDimensions().d;
                                                dims.nz = levels;
                                                rp->init(dims);
                                                rp->setProjectionParameters(rasters[0]->getProjectionParameters());
                                                rp->mapVector(*v, script, GeometryType::All, "", levelProperty);

                                            } else {
                                                auto resolution = getNumber("resolution", item);
                                                *rp = v->template mapDistance<T>(resolution, script, item.first == "mapDistance", boundingBox);
                                            }
                                        }
                                        rasters.push_back(rp);
                                        found = true;
                                        break;
                                    }
                                }

                                // Check vector has been processed
                                if (!found) {
                                    throw std::runtime_error("Vector '" + name + "' not found for mapping operation");
                                }
                            }

                            // Raster to vector operation
                            else if (item.first == "vectorise") {

                                // Get parameters
                                auto name = getString("name", item);
                                auto values = getNumericArray<T>("values", item);
                                auto createName = getString("create", item);
                                auto nullValue = getNumber("nullValue", item, false, getNullValue<T>());

                                // Check name
                                if (names.find(createName) != names.end()) {
                                    throw std::runtime_error("Cannot use duplicate layer name '" + createName + "' for vectorise operation");
                                }
                                names.insert(createName);

                                // Search for name in vectors
                                bool found = false;
                                for (auto &r : rasters) {
                                    if (r->template getProperty<std::string>("name") == name) {
                                        auto type = r->getOpenCLTypeString();
                                        if (type == "REAL") {

                                            // Create vectorisation
                                            auto vp = std::make_shared<Vector<T> >();
                                            *vp = dynamic_cast<Raster<T, T> *>(r.get())->vectorise(values, nullValue);
                                            vp->template setGlobalProperty<std::string>("name", createName);
                                            vectors.push_back(vp);
                                        } else {
                                            throw std::runtime_error("Invalid raster type '" + type + "' for vectorise operation");
                                        }
                                        found = true;
                                        break;
                                    }
                                }

                                // Check vector has been processed
                                if (!found) {
                                    throw std::runtime_error("Raster '" + name + "' not found for vectorise operation");
                                }
                            }

                        } else if (item.second.is_array()) {

                            // Solver operation
                            if (item.first == "operations") {
                                Operation::run(item.second.dump(), rasters, vectors);
                            }

                            // Raster creation operation
                            else if (item.first == "createRaster") {

                                for (auto &val : item.second.array_items()) {
                                    if (val.is_object()) {
                                        auto aItem = jsonPair({ item.first, val });

                                        // Get parameters
                                        auto name = getString("name", aItem);
                                        auto type = getString("type", aItem, false, "REAL");
                                        auto source = getString("source", aItem, false, "");
                                        auto projection = getString("projection", aItem, false, "");

                                        // Create raster
                                        auto rp = createRaster(name, rasters, type);

                                        // Read from source
                                        if (rp && source.length() > 0) {
                                            rp->read(source);
                                        }

                                        // Apply projection
                                        if (rp && projection.length() > 0) {
                                            auto proj = Projection::parseProjString(projection);
                                            rp->setProjectionParameters(proj);
                                        }
                                    }
                                }
                            }

                            // Vector creation operation
                            else if (item.first == "createVector") {

                                // Get array items
                                for (auto &val : item.second.array_items()) {
                                    if (val.is_object()) {
                                        auto aItem = jsonPair({ item.first, val });

                                        auto name = getString("name", aItem);
                                        auto source = getString("source", aItem, false, "");
                                        auto json = getString("json", aItem, false, "");
                                        auto projection = getString("projection", aItem, false, "");

                                        // Create vector
                                        auto vp = std::make_shared<Vector<T> >();
                                        if (vp) {

                                            // Read from source
                                            if (source.length() > 0) {
                                                vp->read(source);
                                            } else if (json.length() > 0) {
                                                *vp = GeoJson<T>::geoJsonToVector(json);
                                            }

                                            // Apply projection
                                            if (projection.length() > 0) {
                                                auto proj = Projection::parseProjString(projection);
                                                *vp = vp->convert(proj);
                                            }

                                            // Update name
                                            vp->template setGlobalProperty<std::string>("name", name);

                                            // Add to vector list
                                            vectors.push_back(vp);
                                        }
                                    }
                                }
                            }

                            // Write operation
                            else if (item.first == "write") {

                                // Get array items
                                for (auto &val : item.second.array_items()) {
                                    if (val.is_object()) {
                                        auto aItem = jsonPair({ item.first, val });
                                        auto name = getString("name", aItem);
                                        auto destination = getString("destination", aItem);

                                        // Search for name
                                        bool found = false;
                                        for (auto &r : rasters) {
                                            if (r->template getProperty<std::string>("name") == name) {
                                                r->write(destination);
                                                found = true;
                                                break;
                                            }
                                        }
                                        for (auto &v : vectors) {
                                            if (v->template getGlobalProperty<std::string>("name") == name) {
                                                v->write(destination);
                                                found = true;
                                                break;
                                            }
                                        }

                                        // Check if layer has been processed
                                        if (!found) {
                                            throw std::runtime_error("Layer '" + name + "' not found for write operation");
                                        }
                                    }
                                }
                            }
                        }

                        // Save timing information
                        if (verbose <= Geostack::Verbosity::Info) {
                            timings.push_back( { item.first, duration<double, std::micro>(steady_clock::now()-opTime).count()*1.0E-6 } );
                        }
                    }

                    // Remove any redundant layers
                    for (auto layer : layerLastUsed) {
                        if (layer.second == index) {
                            for (auto it = rasters.begin(); it != rasters.end(); it++) {
                                if ((*it)->template getProperty<std::string>("name") == layer.first) {
                                    rasters.erase(it);
                                    break;
                                }
                            }
                            for (auto it = vectors.begin(); it != vectors.end(); it++) {
                                if ((*it)->template getGlobalProperty<std::string>("name") == layer.first) {
                                    vectors.erase(it);
                                    break;
                                }
                            }
                        }
                    }
                    index++;
                } else {
                    throw std::runtime_error("No operations are defined.");
                }
            }

            // Output timing information
            if (verbose <= Geostack::Verbosity::Info) {

                // Set output decimals
                std::cout << std::fixed << std::showpoint << std::setprecision(3);

                // Write timing information
                double totalTime = 0.0;
                for (std::size_t i = 0; i < timings.size(); i++) {
                    std::cout << i << ". " << timings[i].first << ": " << timings[i].second << "s" << std::endl;
                    totalTime += timings[i].second;
                }
                std::cout << "Total: " << totalTime << "s" << std::endl;
            }
            
            for (auto &r: rasters) {
                r->closeFile();
            }
            rasters.clear();
            
        } else {
            throw std::runtime_error("Operation configuration is not correctly defined.");
        }
    }

    // CTYPE float definitions
    template std::vector<float> getNumericArray(std::string name, jsonPair &item, bool throwError,
        std::vector<float> default_value);
    template RasterBasePtr<float> createRaster(std::string name, std::vector<RasterBasePtr<float> > &rasters,
        std::string type);
    template void Operation<float>::runFromConfigFile(std::string configFileName);
    template void Operation<float>::run(std::string jsonConfig, std::vector<RasterBasePtr<float> > &rastersSuper,
        std::vector<VectorPtr<float> > &vectorSupers);

    // CTYPE double definitions
    template std::vector<double> getNumericArray(std::string name, jsonPair &item, bool throwError,
        std::vector<double> default_value);
    template RasterBasePtr<double> createRaster(std::string name, std::vector<RasterBasePtr<double> > &rasters,
        std::string type);
    template void Operation<double>::runFromConfigFile(std::string configFileName);
    template void Operation<double>::run(std::string jsonConfig, std::vector<RasterBasePtr<double> > &rastersSuper,
        std::vector<VectorPtr<double> > &vectorSupers);
}
