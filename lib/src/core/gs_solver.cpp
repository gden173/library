/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
  
#include <iostream>
#include <iomanip>
#include <functional>
#include <list>
#include <chrono>
#include <cmath>
#include <regex>
#include <sstream>

#include "gs_models.h"

// Memory management
#if defined(__APPLE__) 
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/sysctl.h>
uint64_t getMaximumMemoryBytes() {
    uint64_t mem;
    size_t len = sizeof(mem);
    sysctlbyname("hw.memsize", &mem, &len, NULL, 0);
    return mem;
}
#elif defined(linux) || defined(__linux) || defined(__linux__)
#include <unistd.h>
uint64_t getMaximumMemoryBytes() {
    return (uint64_t)sysconf(_SC_PHYS_PAGES)*sysconf(_SC_PAGESIZE);
}
#elif defined(WIN32) || defined(__WIN32__) || defined(_WIN32) || defined(_MSC_VER) || defined(__MINGW32__)
#define NOMINMAX
#include <windows.h>
uint64_t getMaximumMemoryBytes() {
    MEMORYSTATUSEX statex;
    statex.dwLength = sizeof(statex);
    GlobalMemoryStatusEx(&statex);
    return (uint64_t)statex.ullTotalPhys;
}
#endif

#include "gs_solver.h"
#include "gs_string.h"

const std::string strTestKernel = 
#ifdef REAL_DOUBLE 
" \n"
"    #define REAL double \n"
"    #pragma OPENCL EXTENSION cl_khr_fp64: enable \n"
" \n"
#else
" \n"
"    #define REAL float \n"
" \n"
#endif
" \n"
"    __kernel void test(__global REAL *f) {  \n"
"        int i = get_global_id(0);  \n"
"        *(f+i) = i == 10 ? sqrt(*(f+i)) : (*(f+i))*2.0;  \n"
"    }  \n"
" \n"
;

namespace Geostack
{
    using namespace cl;

    /**
    * Static member initialisation
    */
    std::mutex Solver::solverMtx;
    std::mutex Solver::timingsMtx;
    NullHash Solver::nullHash;

    /**
    * Specialised null checks
    */
    template<> bool isValid(const float &a) {
        return a == a;
    }

    template<> bool isValid(const double &a) {
        return a == a;
    }

    template<> bool isValid(const uint8_t &a) {
        return true;  // No null value implemented for byte types
    }

    template<> bool isValid(const uint32_t &a) {
        return a != getNullValue<uint32_t>();
    }

    template<> bool isValid(const uint64_t &a) {
        return a != getNullValue<uint64_t>();
    }

    template<> bool isValid(const int8_t &a) {
        return true;  // No null value implemented for byte types
    }

    template<> bool isValid(const int32_t &a) {
        return a != getNullValue<int32_t>();
    }

    template<> bool isValid(const int64_t &a) {
        return a != getNullValue<int64_t>();
    }

    template<> bool isValid(const std::string &a) {
    
        // Null strings have two consecutive ESC characters (127)
        return !((a.size() == 2) && (a[0] == 0x7f) && (a[1] == 0x7f));
    }

    template<> bool isInvalid(const float &a) {
        return a != a;
    }

    template<> bool isInvalid(const double &a) {
        return a != a;
    }

    template<> bool isInvalid(const uint8_t &a) {
        return false; // No null value implemented for byte types
    }

    template<> bool isInvalid(const uint32_t &a) {
        return a == getNullValue<uint32_t>();
    }

    template<> bool isInvalid(const uint64_t &a) {
        return a == getNullValue<uint64_t>();
    }

    template<> bool isInvalid(const int8_t &a) {
        return false; // No null value implemented for byte types
    }

    template<> bool isInvalid(const int32_t &a) {
        return a == getNullValue<int32_t>();
    }

    template<> bool isInvalid(const int64_t &a) {
        return a == getNullValue<int64_t>();
    }

    template<> bool isInvalid(const std::string &a) {
    
        // Null strings have two consecutive ESC characters (127)
        return (a.size() == 2) && (a[0] == 0x7f) && (a[1] == 0x7f);
    }

    /**
    * Local maximum definitions to switch between std::max and std::fmax
    */
    template<> float max(float a, float b) {
        return std::fmax(a, b);
    }

    template<> double max(double a, double b) {
        return std::fmax(a, b);
    }

    template<> uint8_t max(uint8_t a, uint8_t b) {

        // Return the maximum
        return std::max(a, b);
    }

    template<> uint32_t max(uint32_t a, uint32_t b) {

        // If either value is null return the other, else return the maximum
        if (a == getNullValue<uint32_t>())
            return b;
        if (b == getNullValue<uint32_t>())
            return a;
        return std::max(a, b);
    }

    template<> int32_t max(int32_t a, int32_t b) {

        // If either value is null return the other, else return the maximum
        if (a == getNullValue<int32_t>())
            return b;
        if (b == getNullValue<int32_t>())
            return a;
        return std::max(a, b);
    }

    template<> int64_t max(int64_t a, int64_t b) {

        // If either value is null return the other, else return the maximum
        if (a == getNullValue<int64_t>())
            return b;
        if (b == getNullValue<int64_t>())
            return a;
        return std::max(a, b);
    }

    /**
    * Local minimum definitions to switch between std::min and std::fmin
    */
    template<> float min(float a, float b) {
        return std::fmin(a, b);
    }

    template<> double min(double a, double b) {
        return std::fmin(a, b);
    }

    template<> uint8_t min(uint8_t a, uint8_t b) {

        // Return the minimum
        return std::min(a, b);
    }

    template<> uint32_t min(uint32_t a, uint32_t b) {

        // If either value is null return the other, else return the minimum
        if (a == getNullValue<uint32_t>())
            return b;
        if (b == getNullValue<uint32_t>())
            return a;
        return std::min(a, b);
    }

    template<> int32_t min(int32_t a, int32_t b) {

        // If either value is null return the other, else return the minimum
        if (a == getNullValue<int32_t>())
            return b;
        if (b == getNullValue<int32_t>())
            return a;
        return std::min(a, b);
    }

    template<> int64_t min(int64_t a, int64_t b) {

        // If either value is null return the other, else return the minimum
        if (a == getNullValue<int64_t>())
            return b;
        if (b == getNullValue<int64_t>())
            return a;
        return std::min(a, b);
    }

    
    /**
    * Singleton instance of OpenCL solver
    */
    Solver& Solver::getSolver() {

        static Solver solver;
        return solver;
    }
    
    /**
    * Null hash value
    */
    std::size_t Solver::getNullHash() {
        return nullHash.value();
    }

    /**
    * Type string for float
    */
    template <> std::string Solver::getTypeString<float>() {
        return "float";
    }

    /**
    * Type string for double
    */
    template <> std::string Solver::getTypeString<double>() {
        return "double";
    }

    /**
    * Type string for char
    */
    template <> std::string Solver::getTypeString<int8_t>() {
        return "int8_t";
    }

    /**
    * Type string for uchar
    */
    template <> std::string Solver::getTypeString<uint8_t>() {
        return "uint8_t";
    }

    /**
    * Type string for int32
    */
    template <> std::string Solver::getTypeString<int32_t>() {
        return "int32_t";
    }

    /**
    * Type string for uint32
    */
    template <> std::string Solver::getTypeString<uint32_t>() {
        return "uint32_t";
    }

    /**
    * OpenCL string type for float
    */
    template <> std::string Solver::getOpenCLTypeString<float>() {
        return "REAL";
    }

    /**
    * OpenCL string type for double
    */
    template <> std::string Solver::getOpenCLTypeString<double>() {
        return "REAL";
    }

    /**
    * OpenCL string type for char
    */
    template <> std::string Solver::getOpenCLTypeString<int8_t>() {
        return "CHAR";
    }

    /**
    * OpenCL string type for uchar
    */
    template <> std::string Solver::getOpenCLTypeString<uint8_t>() {
        return "UCHAR";
    }

    /**
    * OpenCL string type for int32
    */
    template <> std::string Solver::getOpenCLTypeString<int32_t>() {
        return "INT";
    }

    /**
    * OpenCL string type for uint32
    */
    template <> std::string Solver::getOpenCLTypeString<uint32_t>() {
        return "UINT";
    }
    
    /**
    * Null hash constructor.
    */
    NullHash::NullHash() {
        std::hash<std::string> hasher;
        nullHash = hasher("");
    }

    /**
    * %Solver constructor.
    */
    Solver::Solver():
        deviceCounter(0),
        deviceID(0),
        requestedPlatformID(0),
        hostMemoryLimit(0),
        deviceMemoryLimit(0),
        initialised(false),
        useMapping(false),
        verbose(Verbosity::Warning),
        pContext(nullptr),
        pQueue(nullptr) {

        // Default to 50% of available memory
        hostMemoryLimit = (getMaximumMemoryBytes()/2);
    }

    void Solver::clearProgramMap(){
        // Clear programs
        for (auto &program : programMap) {
           delete program.second;
        }
        programMap.clear();        
    }

    void Solver::clearKernels() {
        // Clear kernels
        for (auto &kernel : kernelMap) {
           delete kernel.second;
        }
        kernelMap.clear();
    }

    /**
    * %Solver destructor.
    */
    Solver::~Solver() {

        // Disabled due to exceptions in singleton deletion for some OpenCL drivers
        
        //// Clear queue
        //if (pQueue != nullptr) {
        //    pQueue->finish();
        //    delete pQueue;
        //    pQueue = nullptr;
        //}

        //// Clear programs
        //for (auto &program : programMap) {
        //    delete program.second;
        //}
        //programMap.clear();

        //// Clear kernels
        //for (auto &kernel : kernelMap) {
        //    delete kernel.second;
        //}
        //kernelMap.clear();

        //// Clear context
        //if (pContext != nullptr) {
        //    delete pContext;
        //    pContext = nullptr;
        //}

        #ifdef USE_PROFILER

        // Display timers
        displayTimers();

        #endif
    }
    
    /**
    * Get OpenCL context.
    * @return context
    */
    Context &Solver::getContext() { 
    
        // Lock mutex
        std::lock_guard<std::mutex> lock(solverMtx);

        // Look up context
        if (pContext == nullptr) {

            // Create new context
            if (!initialised) {

                // Initialise OpenCL
                if (!initOpenCL()) {
                    throw std::runtime_error("Cannot initialise OpenCL");
                }
            }

            // Get devices
            std::vector<cl::Platform> platforms;
            std::vector<cl::Device> platformDevices;
            Platform::get(&platforms);
            platforms[requestedPlatformID].getDevices(CL_DEVICE_TYPE_CPU | CL_DEVICE_TYPE_GPU, &platformDevices);
            
            // Create context
            try {
                pContext = new Context(platformDevices);
            } catch (cl::Error& e) {
                std::string infoString = "ERROR: OpenCL exception: " + 
                    std::string(e.what()) + " (" + std::to_string(e.err()) + ")";
                throw std::runtime_error(infoString);
            }

            // Add new context to map
            if (verbose <= Verbosity::Debug) {
                std::cout << "OpenCL: Created context" << std::endl;
            }
        }

        return *pContext;
    }
    
    /**
    * Get OpenCL queue.
    * @return queue
    */
    CommandQueue &Solver::getQueue() { 
    
        // Look up context
        Context &context = getContext();

        // Lock mutex
        std::lock_guard<std::mutex> lock(solverMtx);
        
        // Check queue
        if (pQueue == nullptr) {

            // Get device ID
            deviceID = requestedDeviceIDs[deviceCounter];

            // Increment and wrap device counter
            deviceCounter++;
            deviceCounter = deviceCounter%requestedDeviceIDs.size();

            // Get devices
            std::vector<cl::Platform> platforms;
            std::vector<cl::Device> platformDevices;
            Platform::get(&platforms);
            platforms[requestedPlatformID].getDevices(CL_DEVICE_TYPE_CPU | CL_DEVICE_TYPE_GPU, &platformDevices);
            
            // Add new context to map
            #ifdef CL_PROFILE_ENABLE
            pQueue = new CommandQueue(context, platformDevices[deviceID], CL_QUEUE_PROFILING_ENABLE);
            #else
            pQueue = new CommandQueue(context, platformDevices[deviceID]);
            #endif
            if (verbose <= Verbosity::Debug) {
                std::cout << "OpenCL: Created queue on device " + std::to_string(deviceID) << std::endl;
            }

            // Test OpenCL
            if (!testOpenCL(context, *pQueue, deviceID)) {
            
                // Output error
                throw std::runtime_error(solverError);
            }
        }

        return *pQueue;
    }
        
    /**
    * Build OpenCL program.
    * @param clProgram program OpenCL code string
    * @return hash value for program lookup if program is built successfully, null hash value otherwise
    */
    std::size_t Solver::buildProgram(std::string clProgram) {        
        
        // Look up context
        Context &context = getContext();
            
        // Hash program
        std::hash<std::string> hasher;
        std::size_t hash = hasher(clProgram);

        // Search for program
        if (programMap.find(hash) != programMap.end()) {
            return hash;
        }

        // Lock mutex
        std::lock_guard<std::mutex> lock(solverMtx); 

        // Create program
        cl::Program::Sources sources;
        sources.push_back(clHeader);
        sources.push_back(clProgram);
        cl::Program *program = new cl::Program(context, sources);

        // Check program
        if (program == nullptr) {
            std::stringstream err;
            err << "ERROR: Cannot create program" << std::endl;
            throw std::runtime_error(err.str());

            return getNullHash();
        }

        //// TEST
        //std::cout << clProgram << std::endl;

        // Get devices
        std::vector<cl::Platform> platforms;
        std::vector<cl::Device> platformDevices;
        Platform::get(&platforms);
        platforms[requestedPlatformID].getDevices(CL_DEVICE_TYPE_CPU | CL_DEVICE_TYPE_GPU, &platformDevices);

        // Parse device list
        std::vector<cl::Device> requestedDevices;
        for (auto deviceID : requestedDeviceIDs) {
            requestedDevices.push_back(platformDevices[deviceID]);
        }
                 
        // Build program 
        try {

            cl_int rBuild = program->build(requestedDevices, CL_STD);
            if (rBuild != CL_SUCCESS) {

                // Report build failure
                std::stringstream err;
                err << "ERROR: OpenCL build failure, error code: " << rBuild << std::endl;
                err << getBuildLog(requestedDevices, *program);
                
                // Throw error
                throw std::runtime_error(err.str());
            
                // Return null hash to indicate error
                return getNullHash();
            }

        } catch (cl::Error& e) {

            // Report exception
            std::stringstream err;
            err << "ERROR: OpenCL exception: " << e.what() << " : " << e.err() << std::endl;
            err << getBuildLog(requestedDevices, *program);
            
            // Throw error
            throw std::runtime_error(err.str());

            // Return null hash to indicate error
            return getNullHash();
        }

        // Add to map
        programMap[hash] = program;
        
        // Output
        if (verbose <= Verbosity::Debug) {
            std::cout << "OpenCL: Program '" << std::hex << hash << "' built" << std::endl;
            std::cout << getBuildLog(requestedDevices, *program);
        }

        // Return hash
        return hash;
    }

    /**
    * Register OpenCL program.
    * @param scriptHash user-defined script hash
    * @param programHash hash returned from buildProgram()
    */
    void Solver::registerProgram(std::size_t scriptHash, std::size_t programHash) {
        scriptMap[scriptHash] = programHash;
    }

    /**
    * Register OpenCL program.
    * @param scriptHash user-defined script hash
    * @return hash value for program lookup if program found, null hash value otherwise
    */
    std::size_t Solver::getProgram(std::size_t scriptHash) {
        
        // Search for program
        auto it = scriptMap.find(scriptHash);
        if (it != scriptMap.end()) {
            return it->second;
        } else {
            return getNullHash();
        }
    }

    /**
    * Output build log from OpenCL
    */
    std::string Solver::getBuildLog(std::vector<cl::Device> &platformDevices, cl::Program &program) {

        std::string deviceName;
        std::stringstream buildLog;
        for (auto &device : platformDevices) {
            device.getInfo(CL_DEVICE_NAME, &deviceName);
            std::string log(program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(device));
            if (log.size() > 0) {
                buildLog << "Build log for " << deviceName << ":" << std::endl;
                buildLog << log << std::endl;
            }
        }
        return buildLog.str();
    }
    
    /**
    * Get OpenCL kernel.
    * @param programHash hash value for program string
    * @param kernelName kernel name within program
    * @return kernel for function
    */
    cl::Kernel &Solver::getKernel(std::size_t programHash, std::string kernelName) {
    
        // Lock mutex
        std::lock_guard<std::mutex> lock(solverMtx);

        // Find program in map
        auto pit = programMap.find(programHash);
        if (pit == programMap.end() || pit->second == nullptr) {
            throw std::runtime_error("Cannot find program");
        }
        cl::Program &program = *pit->second;

        // Find kernel
        auto key = std::make_pair(programHash, kernelName);
        auto kit = kernelMap.find(key);
        if (kit != kernelMap.end())
            return *kit->second;
        
        // Create kernel
        cl::Kernel *kernel = new cl::Kernel(program, kernelName.c_str());

        // Check program
        if (kernel == nullptr) {
            throw std::runtime_error("Cannot create kernel '" + kernelName + "'");
        }

        // Add to map
        kernelMap[key] = kernel;

        // Add workgroup size to map
        auto maxKernelWorkgroupSize = getKernelWorkgroupSize(*kernel);
        kernelWorkgroupSizeMap[key] = maxKernelWorkgroupSize;

        // Output
        if (verbose <= Verbosity::Debug) {
            std::cout << "OpenCL: Program '" << std::hex << programHash << "' added kernel '" << kernelName << "' (WG size: " << std::dec << maxKernelWorkgroupSize << ")" << std::endl;
        }

        // Return kernel
        return *kernel;
    }
    
    /**
    * Get workgroup size for kernel.
    * @param programHash hash value for program string
    * @param kernelName kernel name within program
    * @return workgroup size for kernel
    */
    cl::size_type Solver::getKernelWorkgroupSize(std::size_t programHash, std::string kernelName) {

        // Find program and kernel in map
        auto key = std::make_pair(programHash, kernelName);
        auto kit = kernelWorkgroupSizeMap.find(key);
        if (kit == kernelWorkgroupSizeMap.end()) {
            throw std::runtime_error("Cannot find program or kernel");
        } else {
            return kit->second;
        }
        return 0;
    }

    /**
    * Get maximum workgroup size for a given kernel.
    */
    cl::size_type Solver::getKernelWorkgroupSize(cl::Kernel &kernel) {
    
        // Check for initialisation
        if (!initialised)
            initOpenCL();

        // Get devices
        std::vector<cl::Platform> platforms;
        std::vector<cl::Device> platformDevices;
        Platform::get(&platforms);
        platforms[requestedPlatformID].getDevices(CL_DEVICE_TYPE_CPU | CL_DEVICE_TYPE_GPU, &platformDevices);

        try {                
            cl::size_type maxWorkGroupSize_cl;
            kernel.getWorkGroupInfo(platformDevices[deviceID], CL_KERNEL_WORK_GROUP_SIZE, &maxWorkGroupSize_cl);
            return maxWorkGroupSize_cl;

        } catch (cl::Error e) {
            e = e; // To remove annoying compiler warning
        }

        // On failure return the device maximum workgroup size
        return maxWorkgroupSize[deviceID];
    }

    /**
    * Get preferred workgroup multiple for a given kernel.
    */
    cl::size_type Solver::getKernelPreferredWorkgroupMultiple(cl::Kernel &kernel) {

        // Check for initialisation
        if (!initialised)
            initOpenCL();

        // Get devices
        std::vector<cl::Platform> platforms;
        std::vector<cl::Device> platformDevices;
        Platform::get(&platforms);
        platforms[requestedPlatformID].getDevices(CL_DEVICE_TYPE_CPU | CL_DEVICE_TYPE_GPU, &platformDevices);

        try {                
            cl::size_type preferredWorkGroupMultiple_cl;
            kernel.getWorkGroupInfo(platformDevices[deviceID], CL_KERNEL_PREFERRED_WORK_GROUP_SIZE_MULTIPLE, &preferredWorkGroupMultiple_cl);
            return preferredWorkGroupMultiple_cl;

        } catch (cl::Error e) {
            e = e; // To remove annoying compiler warning
        }

        // On failure return 128 - this seems to work on every OpenCL device encountered
        return (cl::size_type)128;
    }

    cl::size_type Solver::getMaxWorkgroupSize() {
    
        // Check for initialisation
        if (!initialised)
            initOpenCL();

        // Return the device maximum workgroup size
        return maxWorkgroupSize[deviceID];
    }

    cl_ulong Solver::getMaxAllocSizeBytes() {
    
        // Check for initialisation
        if (!initialised)
            initOpenCL();

        // Return the device maximum workgroup size
        return maxAllocSizeBytes[deviceID];
    }

    uint64_t Solver::getHostMemoryLimit() {
    
        // Check for initialisation
        if (!initialised)
            initOpenCL();
            
        // Get user-defined memory limit
        return hostMemoryLimit;
    }

    void Solver::setHostMemoryLimit(uint64_t hostMemoryLimit_) {

        // Set user-defined memory limit        
        hostMemoryLimit = hostMemoryLimit_;
    }

    uint64_t Solver::getDeviceMemoryLimit() {

        // Check for initialisation
        if (!initialised)
            initOpenCL();
            
        // Get user-defined memory limit
        return deviceMemoryLimit;
    }

    void Solver::setDeviceMemoryLimit(uint64_t deviceMemoryLimit_) {

        // Set user-defined memory limit        
        deviceMemoryLimit = deviceMemoryLimit_;
    }

    cl_ulong Solver::getLocalMemorySizeBytes() {
    
        // Check for initialisation
        if (!initialised)
            initOpenCL();

        // Return the device maximum workgroup size
        return localMemorySizeBytes[deviceID];
    }  
    
    /**
    * Set verbosity to Debug
    */
    void Solver::setVerbose(bool verbose_) { 
        verbose = verbose_ ? Verbosity::Debug : Verbosity::Warning;    
    }    
    
    /**
    * Set verbosity level
    */
    void Solver::setVerboseLevel(uint8_t verbose_) {
        verbose = verbose_;
    }
    
    /**
    * Get verbosity level
    */
    uint8_t Solver::getVerboseLevel() { 
        return verbose; 
    }

    /**
    * Run a simple OpenCL test program and compare results against expected values from the CPU.
    */
    bool Solver::testOpenCL(Context &context, CommandQueue &queue, int deviceID) {
    
        Program::Sources sources;
        sources.push_back(strTestKernel);

        // Get devices
        std::vector<cl::Platform> platforms;
        std::vector<cl::Device> platformDevices;
        Platform::get(&platforms);
        platforms[requestedPlatformID].getDevices(CL_DEVICE_TYPE_CPU | CL_DEVICE_TYPE_GPU, &platformDevices);

        // Create program and kernel and set kernel arguments
        solverError.clear();
        Program program = Program(context, sources);
        try {

            cl_int rBuild = program.build(platformDevices, CL_STD);
            if (rBuild != CL_SUCCESS) {

                // Report build failure
                std::cout << "ERROR: OpenCL build failure, error code: " << rBuild << std::endl;
                std::string err = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(platformDevices[deviceID]);
                solverError += "OpenCL: Build log: " + std::string(err.c_str()) + '\n';
                return false;
            }

        } catch (Error& e) {

            // Report error
            std::string err = program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(platformDevices[deviceID]);
            solverError = "ERROR: OpenCL failed with exception: " + 
                std::string(e.what()) + " (" + std::to_string(e.err()) + ")" + '\n';
            solverError += "OpenCL: Build log: " + std::string(err.c_str()) + '\n';
            return false;
        }
        Kernel testKernel = Kernel(program, "test");

        // Set test size 
        int size = 32;

        // Create test buffer
        REAL *pVals = new REAL[size];
        for (int i = 0; i < size; i++) 
            pVals[i] = (REAL)(i+1);

        // Create OpenCL buffers
        cl_int err;
        Buffer testBuffer = Buffer(context, CL_MEM_READ_WRITE, size*sizeof(REAL), NULL, &err);
        if (err != CL_SUCCESS) {
            solverError = "OpenCL: Test failed; cannot create test buffer";
            return false;
        }
        queue.enqueueWriteBuffer(testBuffer, CL_FALSE, 0, size*sizeof(REAL), pVals);

        // Run test kernel
        testKernel.setArg( 0, testBuffer);
        queue.enqueueNDRangeKernel(testKernel, NullRange, size, NullRange);

        // Read results
        REAL *pResults = new REAL[size];
        for (int i = 0; i < size; i++) 
            pResults[i] = (REAL)0.0;
        queue.enqueueReadBuffer(testBuffer, CL_TRUE, 0, size*sizeof(REAL), pResults);

        // Assess results
        bool result = true;
        int fail_id = 0;
        REAL expectedResult;
        REAL delta = 0.0;
        for (int i = 0; i < size; i++) {
            expectedResult = i == 10 ? (REAL)sqrt(*(pVals+i)) : (*(pVals+i))*(REAL)2.0;
            delta = fabs(pResults[i]-expectedResult)/expectedResult;
            if (delta > 1.0E-3) {
                result = false;
                fail_id = i;
                break;
            }
        }

        // Clear up
        delete [] pVals;
        delete [] pResults;

        // Return result
        if (result) {
            if (verbose <= Verbosity::Debug) {
                std::cout << "OpenCL: Test passed for device " + std::to_string(deviceID) << std::endl;
            }
            return true;
        } else {

            solverError = "OpenCL: Test " + std::to_string(fail_id) + " failed; delta: " + std::to_string(delta) + " for device " + std::to_string(deviceID) + "\n";
            return false;
        }
    }

    /**
    * Detect OpenCL platforms and devices, create OpenCL context and run OpenCL test.
    */
    bool Solver::initOpenCL() {

        if (!initialised) {
            
            std::string infoString;
            #ifdef CL_VERSION_1_2
                infoString += "OpenCL: Version 1.2 supported\n";
            #elif CL_VERSION_1_1
                infoString += "OpenCL: Version 1.1 supported\n";
            #else
                infoString += "OpenCL: Version not supported\n";
                #error OpenCL version not supported
            #endif

            // Get platform
            std::vector<cl::Platform> platforms;
            std::vector<cl::Device> platformDevices;
            Platform::get(&platforms);
            if (platforms.size() == 0) {
                infoString += "ERROR: OpenCL: No OpenCL platforms\n";
                std::cout << infoString << std::flush;
                return false;
            }
            int pcount = 0;
            int nCPUGPU = 0;
            int defaultDeviceID = 0;
            int defaultPlatformID = 0;
            for (std::vector<Platform>::iterator it = platforms.begin(); it != platforms.end(); it++, pcount++) {
            
                std::string strPlatform;
                it->getInfo(CL_PLATFORM_NAME, &strPlatform);
                infoString += "OpenCL: Platform " + std::to_string(pcount) + ": " + strPlatform;
                std::string strVersion;
                it->getInfo(CL_PLATFORM_VERSION, &strVersion);
                infoString += "; " + strVersion + "\n";           

                try {
                    it->getDevices(CL_DEVICE_TYPE_CPU | CL_DEVICE_TYPE_GPU, &platformDevices);
                    int dcount = 0;

                    for (auto dit = platformDevices.begin(); dit != platformDevices.end(); dit++, dcount++) {
                        cl_device_type dtype;

                        infoString += "          Device " + std::to_string(dcount);
                        
                        dit->getInfo(CL_DEVICE_TYPE, &dtype);
                        if (dtype & CL_DEVICE_TYPE_CPU) {

                            // Seach for CPU device
                            infoString += ": CPU ";    
                        
                            #ifdef CL_VERSION_1_2
                            std::vector<cl_device_partition_property> dpartitions;
                            if (dit->getInfo(CL_DEVICE_PARTITION_PROPERTIES, &dpartitions) == CL_SUCCESS) {
                                if (dpartitions.size() > 0 && dpartitions[0] != 0) {
                                    infoString += " [P:";
                                    for (auto d : dpartitions)
                                        switch(d) {
                                            case CL_DEVICE_PARTITION_EQUALLY:
                                                infoString += "e";
                                                break;
                                            case CL_DEVICE_PARTITION_BY_COUNTS:
                                                infoString += "c";
                                                break;
                                            case CL_DEVICE_PARTITION_BY_AFFINITY_DOMAIN:
                                                infoString += "a";
                                                break;
                                        }
                                    infoString += "]";
                                }
                            }
                            #endif

                            infoString += "; ";  

                        } else if (dtype & CL_DEVICE_TYPE_GPU) {

                            // Seach for GPU device
                            infoString += ": GPU; ";
                            defaultPlatformID = pcount;
                            defaultDeviceID = dcount;
                        }

                        std::string strDevice;
                        dit->getInfo(CL_DEVICE_NAME, &strDevice);
                        infoString += strDevice;  

                        std::string driverVersion;
                        dit->getInfo(CL_DRIVER_VERSION, &driverVersion);
                        infoString += " (v" + driverVersion + ")"; 

                        infoString += "\n";
                    }
                    nCPUGPU++;

                } catch (cl::Error& e) {
                    e = e; // To remove annoying compiler warning
                    infoString += "no GPU/CPU on platform";
                }
            }

            // Check for any CPU or GPU devices
            if (nCPUGPU == 0) {
                infoString += "ERROR: OpenCL: No CPU or GPU devices on any platforms\n";
                std::cout << infoString;
                return false;
            }

            // Get requested OpenCL platform
            char const* envOpenCLPlatform = std::getenv("OPENCL_PLATFORM_ID");
            if (envOpenCLPlatform == NULL) {
                infoString += "OpenCL: No OPENCL_PLATFORM_ID environment variable found, defaulting to OpenCL platform: " + std::to_string(defaultPlatformID) + "\n";
                requestedPlatformID = defaultPlatformID;
            } else {
                int envPlatformID = atoi(envOpenCLPlatform);
                if (envPlatformID < 0 || envPlatformID >= (int)platforms.size()) {
                    infoString += "WARNING: OPENCL_PLATFORM_ID (" + std::to_string(envPlatformID) + ") cannot be used, defaulting to OpenCL platform: " + 
                        std::to_string(defaultPlatformID) + "\n";
                    requestedPlatformID = defaultPlatformID;
                } else {
                    requestedPlatformID = envPlatformID;
                }
            }

            // Get devices on platform
            platformDevices.clear();
            platforms[requestedPlatformID].getDevices(CL_DEVICE_TYPE_CPU | CL_DEVICE_TYPE_GPU, &platformDevices);

            // Get requested OpenCL devices
            char const* envOpenCLDevices = std::getenv("OPENCL_DEVICE_IDS");
            if (envOpenCLDevices == NULL) {

                // Get single OpenCL device
                char const* envOpenCLDevice = std::getenv("OPENCL_DEVICE_ID");
                if (envOpenCLDevice == NULL) {
                    infoString += "OpenCL: No OPENCL_DEVICE_ID(S) environment variable found, defaulting to OpenCL device: " + std::to_string(defaultDeviceID) + "\n";
                    requestedDeviceIDs.push_back(defaultDeviceID);
                } else {
                    int envDeviceID = atoi(envOpenCLDevice);
                    if (envDeviceID < 0 || envDeviceID >= (int)platformDevices.size()) {
                        infoString += "WARNING: OPENCL_DEVICE_ID (" + std::to_string(envDeviceID) + ") cannot be used, defaulting to OpenCL device: " + 
                            std::to_string(defaultDeviceID) + "\n";
                        envDeviceID = defaultDeviceID;
                        requestedDeviceIDs.push_back(defaultDeviceID);
                    }

                    // Device partioning
                    char const* envOpenCLNPartitions = std::getenv("OPENCL_DEVICE_PARTITIONS");
                    char const* envOpenCLDevicePartitions = std::getenv("OPENCL_DEVICE_PARTITION_IDS");
                    if (envOpenCLNPartitions != NULL) {
                    
                        #ifdef CL_VERSION_1_2
                        
                        // Partition device
                        std::vector<cl_device_partition_property> dpartitions;
                        if (platformDevices[envDeviceID].getInfo(CL_DEVICE_PARTITION_PROPERTIES, &dpartitions) == CL_SUCCESS && dpartitions.size() > 0 && dpartitions[0] != 0) {
                        
                            // Overwrite devices with new partition
                            cl::Device &currentDeviceList = platformDevices[envDeviceID];
                            int envDevicePartitions = atoi(envOpenCLNPartitions);
                            if (envDevicePartitions <= 0) {
                                infoString += "ERROR: OPENCL_DEVICE_PARTITIONS must be greater than zero\n";
                                std::cout << infoString;
                                return false;
                            }

                            try {

                                // Get maximum compute units
                                cl::size_type maxComputeUnits_cl;
                                platformDevices[envDeviceID].getInfo(CL_DEVICE_MAX_COMPUTE_UNITS, &maxComputeUnits_cl);
                                int maxComputeUnits = (int)maxComputeUnits_cl;

                                if (maxComputeUnits <= 0) {
                                    infoString += "ERROR: Maximum compute units on device reported as zero\n";
                                    std::cout << infoString;
                                    return false;
                                }

                                infoString += "OpenCL: Device supports partitions, using " + std::string(envOpenCLNPartitions) + 
                                    " partitions of " + std::to_string(maxComputeUnits) + " compute units\n";

                                // Create partitions
                                cl_device_partition_property subDeviceProperties [] = { CL_DEVICE_PARTITION_EQUALLY, maxComputeUnits/envDevicePartitions, 0 }; 
                                currentDeviceList.createSubDevices(subDeviceProperties, &platformDevices);
                                infoString += "OpenCL: Device split into " + std::to_string(platformDevices.size()) + " devices\n";

                            } catch (cl::Error& e) {
                                std::cout << infoString;
                                std::cout << std::endl << "ERROR: OpenCL exception: " << e.what() << " : " << e.err() << std::endl;
                                return false;
                            }
                            
                            // Get OpenCL device range
                            if (envOpenCLDevicePartitions == NULL) {

                                // Get device ID
                                infoString += "OpenCL: Using all device partitions: ";
                                for (int deviceID = 0; deviceID < platformDevices.size(); deviceID++) {
                                    requestedDeviceIDs.push_back(deviceID);
                                    infoString += std::to_string(deviceID) + ", ";
                                }
                                infoString.pop_back();
                                infoString.pop_back();
                                infoString += "\n";

                            } else {
                                std::string deviceNames(envOpenCLDevicePartitions);
                                auto deviceNamesList = Geostack::Strings::split(deviceNames, ',');
                                if (deviceNamesList.size() > 0) {

                                    // Get device ID
                                    infoString += "OpenCL: Using device partitions: ";
                                    for (auto it : deviceNamesList) {
                                        int deviceListInt = std::stoi(it);
                                        requestedDeviceIDs.push_back(deviceListInt);
                                        infoString += std::to_string(deviceListInt) + ", ";
                                    }
                                    infoString.pop_back();
                                    infoString.pop_back();
                                    infoString += "\n";

                                    // Check device
                                    for (int deviceID : requestedDeviceIDs) {
                                        if (deviceID < 0 || deviceID >= (int)platformDevices.size())
                                            infoString += "WARNING: OPENCL_DEVICE_ID " + std::to_string(deviceID) + "1) cannot be used\n";
                                    }
                                } else {
                                    infoString += "ERROR: OPENCL_DEVICE_PARTITION_IDS contains no device IDs\n";
                                    std::cout << infoString;
                                    return false;
                                }
                            }

                        } else {

                            // Device cannot be partitioned
                            infoString += "OpenCL: Device cannot be partitioned\n";
                            requestedDeviceIDs.push_back(envDeviceID);
                        }

                        #else
                    
                        // Version does not support partitioning
                        infoString += "OpenCL: Partitions not supported in OpenCL version\n";
                        requestedDeviceIDs.push_back(envDeviceID);

                        #endif

                    } else {
                        requestedDeviceIDs.push_back(envDeviceID);
                    }
                }
            } else {

                // Get OpenCL device range
                std::string deviceNames(envOpenCLDevices);
                auto deviceNamesList = Geostack::Strings::split(deviceNames, ',');
                if (deviceNamesList.size() > 0) {

                    // Get device ID
                    infoString += "OpenCL: Using devices: ";
                    int nEnvDevices = 0;
                    for (auto it = deviceNamesList.begin(); it != deviceNamesList.end(); it++, nEnvDevices++) {
                        int deviceListInt = std::stoi(*it);
                        requestedDeviceIDs.push_back(deviceListInt);
                        infoString += std::to_string(deviceListInt) + ", ";
                    }
                    infoString.pop_back();
                    infoString.pop_back();
                    infoString += "\n";

                    // Check device
                    for (int deviceID : requestedDeviceIDs) {
                        if (deviceID < 0 || deviceID >= (int)platformDevices.size())
                            infoString += "WARNING: OPENCL_DEVICE_ID " + std::to_string(deviceID) + " cannot be used\n";
                    }
                } else {
                    infoString += "ERROR: OPENCL_DEVICE_IDS contains no device IDs\n";
                    std::cout << infoString;
                    return false;
                }
            }

            // Device info
            for (int deviceID : requestedDeviceIDs) {
            
                if (deviceID < 0 || deviceID >= (int)platformDevices.size()) {
                    infoString += "ERROR: Requested device (" + std::to_string(deviceID) + ") is out of range\n";
                    std::cout << infoString;
                    return false;
                }

                std::string strDevice;
                platformDevices[deviceID].getInfo(CL_DEVICE_NAME, &strDevice);
                infoString += "OpenCL: Using platform " + std::to_string(requestedPlatformID) + ", device " + 
                    std::to_string(deviceID) + ": " + std::string(strDevice) + "\n";
            }

            // Check for double precision floating point support
            #ifdef REAL_DOUBLE 
            std::string strExt;
            for (int deviceID : requestedDeviceIDs) {
                platformDevices[deviceID].getInfo(CL_DEVICE_EXTENSIONS, &strExt);
                std::string::size_type pos = strExt.find("cl_khr_fp64");
                if (pos == std::string::npos) {
                    infoString += "ERROR: OpenCL: Device does not support double precision\n";
                    std::cout << infoString;
                    return false;
                }
            }
            infoString += "OpenCL: Using double precision\n";
            #else
            infoString += "OpenCL: Using single precision\n";
            #endif

            // Get OpenCL parameters
            for (int deviceID : requestedDeviceIDs) {
            
                // Use memory mapping for CPU devices
                cl_device_type dtype;
                platformDevices[deviceID].getInfo(CL_DEVICE_TYPE, &dtype);
                if (dtype & CL_DEVICE_TYPE_CPU) {
                    useMapping = true;
                }

                try {
                    maxWorkgroupSize[deviceID] = 0;
                    cl::size_type maxWorkGroupSize_cl;
                    platformDevices[deviceID].getInfo(CL_DEVICE_MAX_WORK_GROUP_SIZE, &maxWorkGroupSize_cl);
                    maxWorkgroupSize[deviceID] = maxWorkGroupSize_cl;
                    infoString += "OpenCL: Platform " + std::to_string(requestedPlatformID) + ", device " + 
                        std::to_string(deviceID) +" maximum workgroup size: " + std::to_string(maxWorkGroupSize_cl) + "\n";
                } catch (cl::Error e) {
                    std::cout << std::endl << "WARNING: OpenCL cannot call getInfo for CL_DEVICE_MAX_WORK_GROUP_SIZE (" << 
                        e.what() << " : " << e.err() << ")" << std::endl;
                }
            
                try {
                    maxAllocSizeBytes[deviceID] = 0;
                    cl_ulong maxAllocSizeBytes_cl;
                    platformDevices[deviceID].getInfo(CL_DEVICE_MAX_MEM_ALLOC_SIZE, &maxAllocSizeBytes_cl);
                    maxAllocSizeBytes[deviceID] = (cl_ulong)maxAllocSizeBytes_cl;
                    infoString += "OpenCL: Platform " + std::to_string(requestedPlatformID) + ", device " + 
                        std::to_string(deviceID) +" maximum allocation: " + std::to_string(maxAllocSizeBytes_cl>>20) + " Mb\n";
                } catch (cl::Error e) {
                    std::cout << std::endl << "WARNING: OpenCL cannot call getInfo for CL_DEVICE_MAX_MEM_ALLOC_SIZE (" << 
                        e.what() << " : " << e.err() << ")" << std::endl;
                }

                try {
                    globalMemorySizeBytes[deviceID] = 0;
                    cl_ulong globalMemorySizeBytes_cl;
                    platformDevices[deviceID].getInfo(CL_DEVICE_GLOBAL_MEM_SIZE, &globalMemorySizeBytes_cl);
                    globalMemorySizeBytes[deviceID] = (cl_ulong)globalMemorySizeBytes_cl;
                    infoString += "OpenCL: Platform " + std::to_string(requestedPlatformID) + ", device " + 
                        std::to_string(deviceID) +" global memory: " + std::to_string(globalMemorySizeBytes_cl>>20) + " Mb\n";
                } catch (cl::Error e) {
                    std::cout << std::endl << "WARNING: OpenCL cannot call getInfo for CL_DEVICE_GLOBAL_MEM_SIZE (" << 
                        e.what() << " : " << e.err() << ")" << std::endl;
                }

                try {
                    localMemorySizeBytes[deviceID] = 0;
                    cl_ulong localMemorySizeBytes_cl;
                    platformDevices[deviceID].getInfo(CL_DEVICE_LOCAL_MEM_SIZE, &localMemorySizeBytes_cl);
                    localMemorySizeBytes[deviceID] = (cl_ulong)localMemorySizeBytes_cl;
                    infoString += "OpenCL: Platform " + std::to_string(requestedPlatformID) + ", device " + 
                        std::to_string(deviceID) +" local memory: " + std::to_string(localMemorySizeBytes_cl>>10) + " Kb\n";
                } catch (cl::Error e) {
                    std::cout << std::endl << "WARNING: OpenCL cannot call getInfo for CL_DEVICE_LOCAL_MEM_SIZE (" << 
                        e.what() << " : " << e.err() << ")" << std::endl;
                }
            }

            // Get device ID
            deviceID = requestedDeviceIDs[deviceCounter];

            // Increment and wrap device counter
            deviceCounter++;
            deviceCounter = deviceCounter%requestedDeviceIDs.size();
            
            // Set host memory limit
            char const *hostMemoryLimitStr = std::getenv("GEOSTACK_MAX_HOST_MEMORY_GB");
            if (hostMemoryLimitStr != NULL) {
                hostMemoryLimit = (uint64_t)atol(hostMemoryLimitStr)<<30;
            }            
            if ((hostMemoryLimit >> 30) < 1 ) {
            	infoString += "OpenCL: Host memory limit set to: " + std::to_string(hostMemoryLimit>>20) + " Mb\n";
            } else {
            	infoString += "OpenCL: Host memory limit set to: " + std::to_string(hostMemoryLimit>>30) + " Gb\n";
	        }

            // Set device memory limit
            deviceMemoryLimit = (uint64_t)globalMemorySizeBytes[deviceID];
            char const *deviceMemoryLimitStr = std::getenv("GEOSTACK_MAX_DEVICE_MEMORY_GB");
            if (deviceMemoryLimitStr != NULL) {
                deviceMemoryLimit = (uint64_t)atol(deviceMemoryLimitStr)<<30;   
            }
            deviceMemoryLimit = std::min(deviceMemoryLimit, hostMemoryLimit);
            if ((deviceMemoryLimit >> 30) < 1 ) {
            	infoString += "OpenCL: Device memory limit set to: " + std::to_string(deviceMemoryLimit>>20) + " Mb\n";
            } else {
            	infoString += "OpenCL: Device memory limit set to: " + std::to_string(deviceMemoryLimit>>30) + " Gb\n";
	        }

            // Output information
            if (verbose <= Verbosity::Info) {
                std::cout << infoString;
            }

            // Set initialisation flag
            initialised = true;
        }

        return true;
    }
    
    /**
    * Switch current timer to another block
    */
    void Solver::switchTimers(std::string uid, std::string currentName) {
    #ifndef CL_PROFILE_ENABLE
    
        std::lock_guard<std::mutex> lock(timingsMtx);

        // Get handles, creating if necessary
        auto &timingMap = timingsMap[uid];
        std::string lastName = timingsMapLastName[uid];

        // Stop last timer
        if (!lastName.empty())
            timingMap[lastName].end();

        // Start current timer
        timingMap[currentName].start();

        // Store current name
        timingsMapLastName[uid] = currentName;

    #endif
    }
    
    /**
    * Increment timer
    */
    void Solver::incrementTimers(std::string uid, std::string currentName, double time) {
    
        std::lock_guard<std::mutex> lock(timingsMtx);

        // Get handles, creating if necessary
        auto &timingMap = timingsMap[uid];
        timingMap[currentName].increment(time);
    }
    
    /**
    * Increment timer using OpenCL event
    */
    void Solver::incrementTimers(std::string uid, std::string currentName, cl::Event &ev) {
    #ifdef CL_PROFILE_ENABLE

        std::lock_guard<std::mutex> lock(timingsMtx);

        auto &timingMap = timingsMap[uid];
        ev.wait();
        cl_ulong delta = 
            ev.getProfilingInfo<CL_PROFILING_COMMAND_END>()-
            ev.getProfilingInfo<CL_PROFILING_COMMAND_START>();
        timingMap[currentName].increment((double)delta/1000.0); // Convert nanoseconds to microseconds

        // Overhead
        cl_ulong delta_OV = 
            ev.getProfilingInfo<CL_PROFILING_COMMAND_START>()-
            ev.getProfilingInfo<CL_PROFILING_COMMAND_QUEUED>();
        timingMap[currentName + "_OV"].increment((double)delta_OV/1000.0); // Convert nanoseconds to microseconds
    #endif
    }
    
    /**
    * Increment timer using OpenCL events
    */
    void Solver::incrementTimers(std::string uid, std::string currentName, std::vector<cl::Event> &evs) {
    #ifdef CL_PROFILE_ENABLE

        std::lock_guard<std::mutex> lock(timingsMtx);

        auto &timingMap = timingsMap[uid];
        cl::Event::waitForEvents(evs);
        for (auto &ev : evs) {
            cl_ulong delta = 
                ev.getProfilingInfo<CL_PROFILING_COMMAND_END>()-
                ev.getProfilingInfo<CL_PROFILING_COMMAND_START>();
            timingMap[currentName].increment((double)delta/1000.0); // Convert nanoseconds to microseconds

            // Overhead
            cl_ulong delta_OV = 
                ev.getProfilingInfo<CL_PROFILING_COMMAND_START>()-
                ev.getProfilingInfo<CL_PROFILING_COMMAND_QUEUED>();
            timingMap[currentName + "_OV"].increment((double)delta_OV/1000.0); // Convert nanoseconds to microseconds
        }
    #endif
    }

    /**
    * Reset timers
    */
    void Solver::resetTimers() {

        std::lock_guard<std::mutex> lock(timingsMtx);

        timingsMap.clear();
        timingsMapLastName.clear();
    }
    
    /**
    * Display timers
    */
    void Solver::displayTimers() {
    
        for (auto &timingMap : timingsMap) {

            // Calculate total time
            double tTotal = 0;
            for (auto &item : timingMap.second)
                tTotal+=item.second.getAccTime();

            if (tTotal > 0.0) {

                // Flip map to sort by time    
                std::multimap<double, std::string> sortedTimingMap;
                for (auto &item : timingMap.second)
                    sortedTimingMap.insert(make_pair((double)item.second.getAccTime(), item.first));
    
                // Display times in seconds
                std::cout << std::fixed << std::setprecision(2);
                std::cout << "Profiling report, device " << deviceID << ":"  << std::endl;
                for (auto it = sortedTimingMap.rbegin(); it != sortedTimingMap.rend(); ++it) {
                    std::cout << std::left << std::setw(40) << it->second << it->first*1.0E-6 << " s (" << 
                        (double)it->first*100.0/(double)tTotal << "%)" << std::endl;
                }
            }
            std::cout << "Total time: " << (double)tTotal*1.0E-6 << " s" << std::endl;
        }
    }

    /**
    * Return current timer name
    */
    std::string Solver::currentTimer(std::string uid) { 

        // Return current timer
        if (timingsMapLastName.find(uid) != timingsMapLastName.end())
            return timingsMapLastName[uid];
        return std::string("<none>");
    }
    
    /**
    * Process script to replace possibly ambiguous names with OpenCL functions
    */
    std::string Solver::processScript(std::string script, bool addTerminator) {

        if (script.empty()) {
            return std::string("");
        }
    
        // Replace function names
        script = std::regex_replace(script, std::regex("\\bdegrees\\("), std::string("degrees_DEF("));
        script = std::regex_replace(script, std::regex("\\bradians\\("), std::string("radians_DEF("));
        script = std::regex_replace(script, std::regex("\\bsqrt\\("), std::string("sqrt_DEF("));
        script = std::regex_replace(script, std::regex("\\bexp\\("), std::string("exp_DEF("));
        script = std::regex_replace(script, std::regex("\\bsin\\("), std::string("sin_DEF("));
        script = std::regex_replace(script, std::regex("\\bcos\\("), std::string("cos_DEF("));
        script = std::regex_replace(script, std::regex("\\btan\\("), std::string("tan_DEF("));
        script = std::regex_replace(script, std::regex("\\basin\\("), std::string("asin_DEF("));
        script = std::regex_replace(script, std::regex("\\bacos\\("), std::string("acos_DEF("));
        script = std::regex_replace(script, std::regex("\\batan\\("), std::string("atan_DEF("));
        script = std::regex_replace(script, std::regex("\\batan2\\("), std::string("atan2_DEF("));
        script = std::regex_replace(script, std::regex("\\blog\\("), std::string("log_DEF("));
        script = std::regex_replace(script, std::regex("\\bpow\\("), std::string("pow_DEF("));
        script = std::regex_replace(script, std::regex("\\bmax\\("), std::string("max_DEF("));
        script = std::regex_replace(script, std::regex("\\bmin\\("), std::string("min_DEF("));
        script = std::regex_replace(script, std::regex("\\bfmax\\("), std::string("max_DEF("));
        script = std::regex_replace(script, std::regex("\\bfmin\\("), std::string("min_DEF("));
        script = std::regex_replace(script, std::regex("\\bclamp\\("), std::string("clamp_DEF("));
        script = std::regex_replace(script, std::regex("\\bnodata\\b"), std::string("NAN"));

        // Add terminating semicolon
        if (addTerminator && script.back() != ';') {
            script.push_back(';');
        }

        return script;
    }
    
    /**
    * Fill buffer implementation
    */
    template <typename TYPE>
    void Solver::fillBuffer(cl::Buffer &fill, TYPE v, cl_uint N) {

        // Load fill program
        std::size_t clHash;
        clHash = buildProgram(Models::cl_fill_c);
        if (clHash == getNullHash()) {
            throw std::runtime_error("Cannot build fill program");
        }

        // Get kernel
        auto fillKernel = getKernel(clHash, "fill_" + getOpenCLTypeString<TYPE>());
        
        // Execute kernel
        cl_uint arg = 0;
        fillKernel.setArg(arg++, fill);
        fillKernel.setArg(arg++, v);
        fillKernel.setArg(arg++, N);
        
        auto clWorkgroupSize = getKernelPreferredWorkgroupMultiple(fillKernel);
        auto clPaddedWorkgroupSize = (size_t)(N+((clWorkgroupSize-(N%clWorkgroupSize))%clWorkgroupSize));
        getQueue().enqueueNDRangeKernel(fillKernel, cl::NullRange, 
            cl::NDRange(clPaddedWorkgroupSize), cl::NDRange(clWorkgroupSize));
    }
    
    /**
    * Interleave buffer implementation
    */
    template <typename TYPE>
    void Solver::interleaveBuffer(cl::Buffer &in, cl::Buffer &out, TYPE noDataValue, 
        uint32_t nx, uint32_t ny, uint32_t nz) {

        // Load interleave program
        std::size_t clHash;
        clHash = buildProgram(Models::cl_interleave_c);
        if (clHash == getNullHash()) {
            throw std::runtime_error("Cannot build interleave program");
        }

        // Get kernel
        auto interleaveKernel = getKernel(clHash, "interleave_" + getOpenCLTypeString<TYPE>());
        
        // Execute kernel
        cl_uint arg = 0;
        interleaveKernel.setArg(arg++, in);
        interleaveKernel.setArg(arg++, out);
        interleaveKernel.setArg(arg++, noDataValue);

        getQueue().enqueueNDRangeKernel(interleaveKernel, 
            cl::NullRange, cl::NDRange(nx*ny, nz), cl::NDRange(nx, 1, 1));
    }
    
    /**
    * Singleton instance of KernelCache
    */
    KernelCache& KernelCache::getKernelCache() {

        static KernelCache kernelCache;
        return kernelCache;
    }

    void KernelCache::setKernelGenerator(std::size_t scriptHash, KernelGenerator kernelGenerator) {

        // Check for generator
        generatorMap[scriptHash] = kernelGenerator;
    }

    KernelGenerator KernelCache::getKernelGenerator(std::size_t scriptHash) {

        // Check for generator
        auto it = generatorMap.find(scriptHash);
        if (it != generatorMap.end()) {
            return it->second;
        }
        return KernelGenerator();
    }

    template void Solver::fillBuffer<float>(cl::Buffer &, float, cl_uint);
    template void Solver::fillBuffer<double>(cl::Buffer &, double, cl_uint);
    template void Solver::fillBuffer<int8_t>(cl::Buffer &, int8_t, cl_uint);
    template void Solver::fillBuffer<uint8_t>(cl::Buffer &, uint8_t, cl_uint);
    template void Solver::fillBuffer<int32_t>(cl::Buffer &, int32_t, cl_uint);
    template void Solver::fillBuffer<uint32_t>(cl::Buffer &, uint32_t, cl_uint);

    template void Solver::interleaveBuffer<float>(cl::Buffer &, cl::Buffer &, float, cl_uint, cl_uint, cl_uint);
    template void Solver::interleaveBuffer<double>(cl::Buffer &, cl::Buffer &, double, cl_uint, cl_uint, cl_uint);
    template void Solver::interleaveBuffer<int8_t>(cl::Buffer &, cl::Buffer &, int8_t, cl_uint, cl_uint, cl_uint);
    template void Solver::interleaveBuffer<uint8_t>(cl::Buffer &, cl::Buffer &, uint8_t, cl_uint, cl_uint, cl_uint);
    template void Solver::interleaveBuffer<int32_t>(cl::Buffer &, cl::Buffer &, int32_t, cl_uint, cl_uint, cl_uint);
    template void Solver::interleaveBuffer<uint32_t>(cl::Buffer &, cl::Buffer &, uint32_t, cl_uint, cl_uint, cl_uint);
}
