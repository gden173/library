/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <cmath>
#include <iostream>
#include <sstream>
#include <regex>

#include "json11/json11.hpp"

#include "gs_solver.h"
#include "gs_vector.h"
#include "gs_geojson.h"
#include "gs_projection.h"
#include "gs_string.h"
#include "gs_level_set.h"


// Include code for projection
#include "cl_date_head.c"

namespace Geostack
{ 
    using namespace json11;      
        
    template <typename TYPE>
    std::string LevelSet<TYPE>::buildNormal = "REALVEC2 normal_vector = normalize((REALVEC2)(0.5*(_distance_E-_distance_W)/_dim.hx, 0.5*(_distance_N-_distance_S)/_dim.hy)); \n";

    template <typename TYPE>
    std::string LevelSet<TYPE>::buildAdvectNormal = "REALVEC2 advect_normal_vector = (REALVEC2)(_dx, _dy)/hypot(_dx, _dy); \n";

    template <typename TYPE>
    std::string LevelSet<TYPE>::buildAdvectMag = "REAL advect_mag = hypot(advect_x, advect_y); \n";      

    template <typename TYPE>
    std::string LevelSet<TYPE>::buildAdvectVector = "REALVEC2 advect_vector = (REALVEC2)(advect_x, advect_y); \n";

    template <typename TYPE>
    std::string LevelSet<TYPE>::buildAdvectDotNormal = "REAL advect_dot_normal = fmax(dot(advect_vector, advect_normal_vector), (REAL)0.0); \n";        

    // Set static members
    template <typename TYPE>
    const TYPE LevelSet<TYPE>::CFL = (TYPE)0.75;

    template <typename TYPE>
    const TYPE LevelSet<TYPE>::timeStepMax = (TYPE)60.0;

    template <typename TYPE>
    const TYPE LevelSet<TYPE>::narrowBandCells = (TYPE)4.0;

    // Find dimensions from bounds
    template <typename TYPE>
    Dimensions<TYPE> LevelSet<TYPE>::boundDimensions(Vector<TYPE> &v) {
    
        const uint32_t tileSize = TileMetrics::tileSize;

        // Get distance map bounds
        auto bounds = v.getBounds();

        // Add buffer of narrow band
        bounds.extend2D((TYPE)4.0*p.bandWidth);

        // Calculate distance map cells
        auto boundsExtent = bounds.extent();
        uint32_t nx = (uint32_t)ceil(boundsExtent.p/resolution);
        uint32_t ny = (uint32_t)ceil(boundsExtent.q/resolution);

        // Calculate number of tiles
        uint32_t ntx = (nx+((tileSize-(nx%tileSize))%tileSize))/tileSize;
        uint32_t nty = (ny+((tileSize-(ny%tileSize))%tileSize))/tileSize;

        // Calculate offset
        auto boundsCentre = bounds.centroid();        
        TYPE ox = boundsCentre.p-(TYPE)0.5*resolution*ntx*tileSize;
        TYPE oy = boundsCentre.q-(TYPE)0.5*resolution*nty*tileSize;
                    
        // Snap to multiple of global resolution
        ox = floor(ox/resolution)*resolution;
        oy = floor(oy/resolution)*resolution;

        // Create raster dimensions
        Dimensions<TYPE> dim;
        dim.nx = ntx*tileSize;
        dim.ny = nty*tileSize;
        dim.nz = levels;
        dim.hx = resolution;
        dim.hy = resolution;
        dim.hz = 1.0;
        dim.ox = ox;
        dim.oy = oy;
        dim.oz = 0.0;

        return dim;
    }
    
    // Return named output layer
    template <typename TYPE>
    RasterBase<TYPE> &LevelSet<TYPE>::getOutput(std::string name) {
        auto lit = outputLayerMap.find(name);
        if (lit == outputLayerMap.end()) {
            throw std::runtime_error("Cannot find output layer '" + name + "'");
        }
        if (lit->second >= outputLayers.size())  {
            throw std::runtime_error("Invalid index for layer '" + name + "'");
        }
        // std::cout << "@C++:: Address of " << name << " is: " << outputLayers[lit->second] << std::endl;
        return *outputLayers[lit->second];
    }

    // Return LevelSetLayer
    template <typename TYPE>
    Raster<TYPE, TYPE> &LevelSet<TYPE>::getLevelSetLayer(std::uint32_t LevelSetLayer) {
        if (LevelSetLayer < LevelSetLayers::Distance || LevelSetLayer > LevelSetLayers::LevelSetLayers_END) {
            throw std::out_of_range("Cannot find level set layer '" + std::to_string(LevelSetLayer) + "'"); 
        }
        return layersReal[LevelSetLayer];
    }

    // Resize solver
    template <typename TYPE>
    void LevelSet<TYPE>::resizeTiles(uint32_t nx, uint32_t ny, uint32_t ox, uint32_t oy) {

        // Get resizing indexes
        auto resizeIndexes = classbits.resize2DIndexes(nx, ny, ox, oy);

        // Add any pending tiles, updating offset
        for (auto &t : newTileIndexes) {

            // Get indexes
            uint32_t ti = t.first;
            uint32_t tj = t.second;

            // Append offset indexes
            resizeIndexes.insert( { ti+ox, tj+oy } );
        }

        // Reseize layers
        newTileIndexes = resizeIndexes;
        if (newTileIndexes.size() > 0) {

            // Resize rasters
            classbits.resize2D(nx, ny, ox, oy);
            for (auto &l : layersReal)
                l.resize2D(nx, ny, ox, oy);
            for (auto &l : layersInt)
                l.resize2D(nx, ny, ox, oy);
            for (auto &pl : outputLayers)
                pl->resize2D(nx, ny, ox, oy);
        }
    }
    
    template <typename TYPE>
    bool LevelSet<TYPE>::init(
        std::string jsonConfig,
        Vector<TYPE> sources_,
        std::shared_ptr<Variables<TYPE, std::string> > variables_,
        std::vector<RasterBasePtr<TYPE> > inputLayers_,
        std::vector<RasterBasePtr<TYPE> > outputLayers_) {

        const uint32_t tileSize = TileMetrics::tileSize;
        
        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &context = solver.getContext();
        auto &queue = solver.getQueue();

        // Reset iterations
        iters = 0;

        // Copy start conditions and layers
        sources = sources_;
        inputLayers = inputLayers_;
        outputLayers = outputLayers_;
        if (variables_ == nullptr) {
            variables = std::make_shared<Variables<TYPE, std::string> >();
        } else {
            variables = variables_;
        }

        // Check inputs
        if (!sources.hasData() && inputLayers.size() == 0) {
            throw std::runtime_error("No sources or input layers for level set solver");
        }

        // Get input types
        uint32_t inputLayersRealCount = 0;
        uint32_t inputLayersIntCount = 0;
        for (auto &l : inputLayers) {
            auto type = l->getOpenCLTypeString();
            if (type == "UINT") {

                // Store unsigned integer types
                inputLayersIntCount++;

            } else {

                // Remap other types to float
                inputLayersRealCount++;
            }
        }

        // Parse configuration
        std::string jsonErr;
        auto config = Json::parse(jsonConfig, jsonErr);
        if (!jsonErr.empty()) {
            throw std::runtime_error(jsonErr);
        }

        // Get and create simulation projection
        auto proj = ProjectionParameters<double>();
        if (config["projection"].is_string()) {
            proj = Projection::parseProjString(config["projection"].string_value());
        } else if (config["projection"].is_number()) {
            proj = Projection::fromEPSG(std::to_string((int)config["projection"].number_value()));
        }

        // Get and check levels
        levels = 1;
        if (config["levels"].is_number()) {
            levels = (uint32_t)config["levels"].number_value();
        }
        if (levels == 0) {
            std::stringstream err;
            err << "Requested levels " << levels << " must be greater than zero.";
            throw std::runtime_error(err.str());
        }

        // Get and random seed
        randomSeed = getNullValue<uint32_t>();
        if (config["randomSeed"].is_number()) {
            randomSeed = (uint32_t)config["randomSeed"].number_value();
        }

        // Get requested output layers
        std::set<std::string> requestedOutputs;
        if (config["outputLayers"].is_array()) {
            for (auto &layer : config["outputLayers"].array_items()) {
                if (layer.is_string()) {
                    requestedOutputs.insert(layer.string_value());
                }
            }            
        } else if (config["outputLayers"].is_string()) {
            requestedOutputs.insert(config["outputLayers"].string_value());
        }
        for (auto &requestedOutput: requestedOutputs) {
            auto pOutput = std::make_shared<Raster<TYPE, TYPE> >(requestedOutput);
            outputLayers.push_back(pOutput);
        }

        // Get flat outputs, these are always 2D
        std::set<std::string> flatOutputs;
        if (config["flatOutputLayers"].is_array()) {
            for (auto &layer : config["flatOutputLayers"].array_items()) {
                if (layer.is_string()) {
                    flatOutputs.insert(layer.string_value());
                }
            }            
        } else if (config["flatOutputLayers"].is_string()) {
            flatOutputs.insert(config["flatOutputLayers"].string_value());
        }
        
        // Get and check target
        timeMultiple = 3600.0;
        if (config["timeMultiple"].is_number()) {
            timeMultiple = (TYPE)config["timeMultiple"].number_value();
        }
        if (timeMultiple < 0.0) {
            throw std::runtime_error("Time multiple must be positive");
        }

        // Get and check scripts
        std::string initScript;
        if (config["initialisationScript"].is_string()) {
            initScript = config["initialisationScript"].string_value();
        }
        if (initScript.length() == 0) {
            std::cout << "WARNING: No 'initialisationScript' value supplied." << std::endl;
        }

        std::string advectScript = "advect_x = 0.0; advect_y = 0.0; \n";
        if (config["advectionScript"].is_string()) {
            advectScript += config["advectionScript"].string_value();
        }
        if (advectScript.length() == 0) {
            std::cout << "WARNING: No 'advectionScript' value supplied, default advection vector of (0, 0) applied." << std::endl;
        }

        std::string buildScript;
        if (config["buildScript"].is_string()) {
            buildScript = config["buildScript"].string_value();
        }
        if (buildScript.length() == 0) {
            std::cout << "WARNING: No 'buildScript' value supplied." << std::endl;
        }

        std::string updateScript;
        if (config["updateScript"].is_string()) {
            updateScript = config["updateScript"].string_value();
        }
        if (updateScript.length() == 0) {
            std::cout << "WARNING: No 'updateScript' value supplied." << std::endl;
        }

        // Process start date and time
        hasStartDate = false;
        currentEpochMilliseconds = 0;
        localZoneOffsetMinutes = 0;
        startTime = date::sys_time<std::chrono::milliseconds>();
        if (config["startDateISO8601"].is_string()) {

            // Get date
            auto dateString = config["startDateISO8601"].string_value();
            if (dateString.length() > 0) {
            
                // Parse date
                std::istringstream in(dateString);
                if (dateString.back() == 'Z') {

                    // Try to parse Zulu format
                    in >> date::parse("%FT%TZ", startTime);

                } else {

                    // Try to parse time zone
                    in >> date::parse("%FT%T%Ez", startTime);

                    // Parse time zone
                    localZoneOffsetMinutes = Strings::iso8601toTimeZoneOffset(dateString)/60000;
                }

                // Get start day
                if (!in.fail()) {
                    hasStartDate = true;
                }
            }
        }
            
        // Get and check resolution
        resolution = 0.0;
        if (config["resolution"].is_number()) {
            resolution = (TYPE)config["resolution"].number_value();
        } else {
            throw std::runtime_error("No 'resolution' value supplied.");
        }
        if (resolution <= 0.0) {
            std::stringstream err;
            err << "Requested resolution " << resolution << " must be positive.";
            throw std::runtime_error(err.str());
        }

        // Initialise parameters
        p.time = 0.0;
        p.maxSpeed = 0.0;
        p.area = 0.0;
        p.bandWidth = (TYPE)narrowBandCells*resolution;
        lastTime = 0.0;

        // Set time to lower bound of source times
        if (sources.hasData()) {
            p.time = sources.getBounds().min.s;
            lastTime = p.time;
        }

        // Set minimum time step to smallest possible positive value
        TYPE timeStepMin = std::numeric_limits<TYPE>::epsilon();
        if (p.time > timeStepMin) 
            timeStepMin = std::nextafter(p.time, std::numeric_limits<TYPE>::max())-p.time;
        p.dt = timeStepMin;
        
        // Update times
        if (hasStartDate) {

            // Set epoch
            auto currentTime = startTime+std::chrono::seconds((uint64_t)((double)p.time));
            currentEpochMilliseconds = currentTime.time_since_epoch().count();
            p.JulianDate = (TYPE)DateCalculation::JulianDateFromEpoch<double>(currentEpochMilliseconds/1000.0);

            // Set date values
            auto currentTimeinZone = currentTime+std::chrono::minutes(localZoneOffsetMinutes);
            auto dmy = date::year_month_day{ date::floor<date::days>(currentTimeinZone) };
            variables->set("day", (REAL)((unsigned int)dmy.day()));
            variables->set("month", (REAL)((unsigned int)dmy.month()));
            variables->set("year", (REAL)((int)dmy.year()));
            
            // Set time values
            auto time = date::make_time(currentTimeinZone-date::floor<date::days>(currentTimeinZone));
            variables->set("hour", (REAL)time.hours().count());

        } else {
            currentEpochMilliseconds = (uint64_t)((double)p.time*1000.0);
            p.JulianDate = (TYPE)DateCalculation::JulianDateFromEpoch<double>(currentEpochMilliseconds/1000.0);
        }

        // Add parameters to variables
        variables->set("time", p.time);
        variables->set("dt", p.dt);
        variables->set("max_speed", p.maxSpeed);
        variables->set("area", p.area);
        variables->set("band_width", p.bandWidth);
        variables->set("Julian_date", p.JulianDate);

        // Re-project input layers
        if (sources.hasData() && sources.getProjectionParameters() != proj)
            sources = sources.convert(proj);
            
        // Calculate initial extent
        Dimensions<TYPE> dim;
        bool foundInitialDistanceLayer = false;
        std::size_t initialDistanceIndex = 0;
        std::string initialDistanceLayerName;

        // Find sources in timeslice
        auto bounds = sources.getBounds();
        currentSources = sources.region(BoundingBox<TYPE>( 
            { { bounds.min.p, bounds.min.q, std::numeric_limits<TYPE>::lowest(), p.time }, 
              { bounds.max.p, bounds.max.q, std::numeric_limits<TYPE>::max(), p.time } } ) );
        
        if (currentSources.hasData()) {
        
            // Create layers
            layersReal.resize(LevelSetLayers::LevelSetLayers_END+inputLayersRealCount);
            layersInt.resize(inputLayersIntCount);

            // Get bounds
            dim = boundDimensions(currentSources);

        } else {

            // Get and check resolution
            if (config["initialDistanceLayer"].is_string()) {

                // Get distance layer name
                initialDistanceLayerName = config["initialDistanceLayer"].string_value();

                // Search for distance layer
                for (initialDistanceIndex = 0; initialDistanceIndex < inputLayers.size(); initialDistanceIndex++) {

                    // Get input name and compare
                    std::string name = inputLayers[initialDistanceIndex]->template getProperty<std::string>("name");
                    if (name == initialDistanceLayerName) {

                        // Set flag
                        foundInitialDistanceLayer = true;
                        break;
                    }
                }

                if (foundInitialDistanceLayer) {
                    
                    // Create layers
                    layersReal.resize(LevelSetLayers::LevelSetLayers_END+inputLayersRealCount-1);
                    layersInt.resize(inputLayersIntCount);

                    // Dimensions from layer
                    auto rasterDimensions = inputLayers[initialDistanceIndex]->getRasterDimensions();

                    // Use only 2D values
                    dim.nx = rasterDimensions.tx*tileSize;
                    dim.ny = rasterDimensions.ty*tileSize;
                    dim.nz = levels;
                    dim.hx = rasterDimensions.d.hx;
                    dim.hy = rasterDimensions.d.hy;
                    dim.hz = 1.0;
                    dim.ox = rasterDimensions.d.ox;
                    dim.oy = rasterDimensions.d.oy;
                    dim.oz = 0.0;

                } else {
                    std::stringstream err;
                    err << "Initial distance map '" << initialDistanceLayerName << "' not found in input layer list.";
                    throw std::runtime_error(err.str());
                }

            } else {
                throw std::runtime_error("No sources or initial distance map provided.");
            }
        }

        // Create flat dimensions
        auto dim2D = dim;
        dim2D.nz = 1;

        // Populate layers
        if (isValid<uint32_t>(randomSeed)) {
            classbits.template setProperty<cl_uint>("random_seed", randomSeed);
        }
        if (!classbits.init(dim)) {
            return false;
        }
        classbits.setProjectionParameters(proj);

        for (auto &l : layersReal) {

            // Set seed
            if (isValid<uint32_t>(randomSeed)) {
                l.template setProperty<cl_uint>("random_seed", randomSeed);
            }

            // Initialise layer
            if (!l.init(dim)) {
                return false;
            }
            l.setProjectionParameters(proj);
        }

        for (auto &l : layersInt) {

            // Set seed
            if (isValid<uint32_t>(randomSeed)) {
                l.template setProperty<cl_uint>("random_seed", randomSeed);
            }

            // Initialise layer
            if (!l.init(dim)) {
                return false;
            }
            l.setInterpolationType(RasterInterpolation::Nearest);
            l.setProjectionParameters(proj);
        }

        // Build output layer map
        outputLayerMap.clear();
        for (std::size_t index = 0; index < outputLayers.size(); index++) {
            RasterBasePtr<TYPE> &pl = outputLayers[index];            
            std::string name = pl->template getProperty<std::string>("name");
            outputLayerMap[name] = index;
        }
        
        // Initialise output layers
        for (auto &pl : outputLayers) {

            // Set seed
            if (isValid<uint32_t>(randomSeed)) {
                pl->template setProperty<cl_uint>("random_seed", randomSeed);
            }
        
            // Search for flat layers
            std::string name = pl->template getProperty<std::string>("name");
            if (flatOutputs.find(name) != flatOutputs.end()) {
                if (!pl->init(dim2D)) {
                    return false;
                }
            } else {
                if (!pl->init(dim)) {
                    return false;
                }
            }
            pl->setProjectionParameters(proj);
        }

        // Get requested advective layers
        auto processAdvectiveItem = [this](json11::Json::object &advectiveLayerItems) {
        
            // Get name
            auto ait = advectiveLayerItems.find("name");
            if (ait != advectiveLayerItems.end()) {
                if (ait->second.is_string()) {
                auto name = ait->second.string_value();

                    // Check for output layer
                    auto lit = this->outputLayerMap.find(name);
                    if (lit != this->outputLayerMap.end()) {
                        auto &outputLayer = outputLayers[lit->second];
                        
                        // Get force creation
                        bool force = false;
                        auto fit = advectiveLayerItems.find("force");
                        if (fit != advectiveLayerItems.end() && fit->second.is_bool()) {
                            force = fit->second.bool_value();
                        }

                        // Get value
                        auto vit = advectiveLayerItems.find("value");
                        if (vit != advectiveLayerItems.end()) {
                            if (vit->second.is_string()) {

                                // Create new advective layer
                                this->advectiveLayers[name] = AdvectiveLayer( 
                                    vit->second.string_value(),
                                    outputLayer->getOpenCLTypeString() == "REAL" ? Reduction::Mean : Reduction::Maximum, // TEST
                                    force
                                );
                            } else {
                                throw std::runtime_error("Advective items must contain a string value");
                            }
                        } else {
                            throw std::runtime_error("Advective items must contain a value string");
                        }

                    } else {
                        std::stringstream err;
                        err << "Cannot find output layer '" << name << "' specified as advective layer.";
                        throw std::runtime_error(err.str());
                    }

                } else {
                    throw std::runtime_error("Advective items must contain a name string");
                }
            } else {
                throw std::runtime_error("Advective items must contain a name");
            }
        };

        if (config["advectiveLayers"].is_object()) {
            auto advectiveLayerItems = config["advectiveLayers"].object_items();
            processAdvectiveItem(advectiveLayerItems);
        } else if (config["advectiveLayers"].is_array()) {
            for (auto &item : config["advectiveLayers"].array_items()) {
                if (item.is_object()) {
                    auto advectiveLayerItems = item.object_items();
                    processAdvectiveItem(advectiveLayerItems);
                }
            }
        }

        // Check for advective names in script, removing any not referenced
        for (auto ait = advectiveLayers.begin(); ait != advectiveLayers.end();) {
            auto regName = std::regex("\\b" + ait->first + "\\b");
            if (!ait->second.force && !std::regex_search(buildScript, regName)) {
                advectiveLayers.erase(ait++);
            } else {
                ait++;
            }
        }

        // Set base names
        classbits.setProperty("name", "classbits");
        layersReal[LevelSetLayers::Distance].setProperty("name", "_distance");
        layersReal[LevelSetLayers::DistanceUpdate].setProperty("name", "_distance_update");
        layersReal[LevelSetLayers::Rate].setProperty("name", "_rate");
        layersReal[LevelSetLayers::Speed].setProperty("name", "_speed");
        layersReal[LevelSetLayers::Arrival].setProperty("name", "_arrival");
        layersReal[LevelSetLayers::Advect_x].setProperty("name", "advect_x");
        layersReal[LevelSetLayers::Advect_y].setProperty("name", "advect_y");

        if (foundInitialDistanceLayer) {

            // Copy data
            runScript<TYPE>("_distance = " + initialDistanceLayerName + ";", 
                { layersReal[LevelSetLayers::Distance], *inputLayers[initialDistanceIndex] } );

            // Erase input layer
            inputLayers.erase(inputLayers.begin()+initialDistanceIndex);
        }

        // Set aligned layer names
        std::string initScriptSuffix;
        inputLayersRealCount = 0;
        inputLayersIntCount = 0;
        for (auto &l : inputLayers) {

            // Get input name
            std::string name = l->template getProperty<std::string>("name");
            
            // Set aligned layer name
            auto type = l->getOpenCLTypeString();
            if (type == "UINT") {
                layersInt[inputLayersIntCount++].setProperty("name", name);
            } else {
                layersReal[LevelSetLayers::LevelSetLayers_END+inputLayersRealCount++].setProperty("name", name);
            }

            // Copy unaligned input data to aligned data in script
            initScriptSuffix += "\n" + name + "_ALIGN = " + name + ";";
        }

        // Parse initialisation script
        initScript = std::regex_replace(initScript, std::regex("\\bstate\\b"), std::string("_class_state"));
        initScript = std::regex_replace(initScript, std::regex("\\bclass\\b"), std::string("_class_lo"));
        initScript = std::regex_replace(initScript, std::regex("\\bsubclass\\b"), std::string("_class_sub_lo"));
        initScript = std::regex_replace(initScript, std::regex("\\bJulian_date\\b"), std::string("Julian_date"));
        initScript = initScript+initScriptSuffix;

        // Parse advect script
        advectScript = std::regex_replace(advectScript, std::regex("\\bstate\\b"), std::string("_class_state"));
        advectScript = std::regex_replace(advectScript, std::regex("\\bclass\\b"), std::string("_class_lo"));
        advectScript = std::regex_replace(advectScript, std::regex("\\bsubclass\\b"), std::string("_class_sub_lo"));
        advectScript = std::regex_replace(advectScript, std::regex("\\btime_step\\b"), std::string("dt"));
        
        // Parse build script
        bool needsNormal = false;
        bool needsAdvectNormal = false;
        bool needsAdvectVector = false;
        bool needsAdvectDotNormal = false;

        if (std::regex_search(buildScript, std::regex("\\bnormal_vector\\b"))) {
            needsNormal = true;
        }
        if (std::regex_search(buildScript, std::regex("\\badvect_normal_vector\\b"))) {
            needsAdvectNormal = true;
        }
        if (std::regex_search(buildScript, std::regex("\\badvect_mag\\b"))) {
            buildScript = buildAdvectMag + buildScript;
        }
        if (std::regex_search(buildScript, std::regex("\\badvect_vector\\b"))) {
            needsAdvectVector = true;
        }
        if (std::regex_search(buildScript, std::regex("\\badvect_dot_normal\\b"))) {
            needsAdvectNormal = true;
            needsAdvectVector = true;
            needsAdvectDotNormal = true;
        }
        
        if (needsAdvectDotNormal)
            buildScript = buildAdvectDotNormal + buildScript;
        if (needsAdvectVector)
            buildScript = buildAdvectVector + buildScript;
        if (needsAdvectNormal)
            buildScript = buildAdvectNormal + buildScript;
        if (needsNormal)
            buildScript = buildNormal + buildScript;

        buildScript = std::regex_replace(buildScript, std::regex("\\bstate\\b"), std::string("_class_state"));
        buildScript = std::regex_replace(buildScript, std::regex("\\bclass\\b"), std::string("_class_lo"));
        buildScript = std::regex_replace(buildScript, std::regex("\\bsubclass\\b"), std::string("_class_sub_lo"));
        buildScript = std::regex_replace(buildScript, std::regex("\\btime_step\\b"), std::string("dt"));
        buildScript = std::regex_replace(buildScript, std::regex("\\barrival\\b"), std::string("_arrival"));
        buildScript = std::regex_replace(buildScript, std::regex("\\bdistance\\b"), std::string("_distance"));
        
        // Parse update script
        needsAdvectVector = false;
        if (std::regex_search(updateScript, std::regex("\\badvect_mag\\b"))) {
            updateScript = buildAdvectMag + updateScript;
        }
        if (std::regex_search(updateScript, std::regex("\\badvect_vector\\b"))) {
            needsAdvectVector = true;
        }
        
        if (needsAdvectVector)
            updateScript = buildAdvectVector + updateScript;

        updateScript = std::regex_replace(updateScript, std::regex("\\bstate\\b"), std::string("_class_state"));
        updateScript = std::regex_replace(updateScript, std::regex("\\bclass\\b"), std::string("_class_lo"));
        updateScript = std::regex_replace(updateScript, std::regex("\\bsubclass\\b"), std::string("_class_sub_lo"));
        updateScript = std::regex_replace(updateScript, std::regex("\\btime_step\\b"), std::string("dt"));
        updateScript = std::regex_replace(updateScript, std::regex("\\barrival\\b"), std::string("_arrival"));
        
        // ---------------------------------------------------------------------
        // Create head kernel
        std::string headStr = std::string(R_cl_level_set_head_c);
        std::string headArgsList;
        advectiveLayerStructSize = 3*sizeof(cl_ushort);
        for (auto &advectiveLayer : advectiveLayers) {
            auto lit = outputLayerMap.find(advectiveLayer.first);
            if (lit != outputLayerMap.end()) {

                // Update advection struct
                auto &outputLayer = outputLayers[lit->second];
                headArgsList += outputLayer->getOpenCLTypeString() + " " + advectiveLayer.first + ";\n";
                advectiveLayerStructSize += outputLayer->getOpenCLTypeSize();

            } else {
                std::stringstream err;
                err << "Cannot find advective layer '" << advectiveLayer.first << "'.";
                throw std::runtime_error(err.str());
            }
        }
        headStr = std::regex_replace(headStr, std::regex("\\/\\*__ARGS__\\*\\/"), headArgsList);

        // ---------------------------------------------------------------------
        // Create initialisation kernel
        // Requires:
        //   classbits            - land classification layer (aligned)
        //   distance             - distance from perimeter (aligned)
        //   advect_x             - advection vector x-component (aligned)
        //   advect_y             - advection vector y-component (aligned)
        
        layersReal[LevelSetLayers::Distance].setNeedsWrite(false);
        layersReal[LevelSetLayers::Advect_x].setNeedsWrite(false);
        layersReal[LevelSetLayers::Advect_y].setNeedsWrite(false);

        initRefs = { 
            classbits, 
            layersReal[LevelSetLayers::Distance], 
            layersReal[LevelSetLayers::Advect_x], 
            layersReal[LevelSetLayers::Advect_y]
        };

        // Add aligned layers
        for (auto l = layersReal.begin()+LevelSetLayers::LevelSetLayers_END; l != layersReal.end(); l++) {
            
            // Set modified layer name
            std::string name = l->template getProperty<std::string>("name");
            name += "_ALIGN";
            l->setProperty("name", name);

            initRefs.push_back(*l);
        }
        for (auto &l: layersInt) {
            
            // Set modified layer name
            std::string name = l.template getProperty<std::string>("name");
            name += "_ALIGN";
            l.setProperty("name", name);

            initRefs.push_back(l);
        }
            
        // Add input layers
        for (std::size_t i = 0; i < inputLayers.size(); i++)  {
            initRefs.push_back(*inputLayers[i]);
        }

        // Add output layers
        for (std::size_t i = 0; i < outputLayers.size(); i++) {
            initRefs.push_back(*outputLayers[i]);
        }
        
        // Generate kernel
        initKernelGenerator = Geostack::generateKernel<TYPE>(
            initScript, std::string(R_cl_level_set_init_c), initRefs, RasterNullValue::Null, variables);

        layersReal[LevelSetLayers::Distance].setNeedsWrite(true);
        layersReal[LevelSetLayers::Advect_x].setNeedsWrite(true);
        layersReal[LevelSetLayers::Advect_y].setNeedsWrite(true);
        
        // Update aligned layers
        for (auto l = layersReal.begin()+LevelSetLayers::LevelSetLayers_END; l != layersReal.end(); l++) {
            
            // Set as read-only outside initialisation kernel
            l->setNeedsWrite(false);

            // Remove prefix from name of unaligned raster layer
            std::string name = l->template getProperty<std::string>("name");
            name.erase(name.end()-6, name.end());
            l->setProperty("name", name);
        }
        for (auto &l: layersInt) {
            
            // Set as read-only outside initialisation kernel
            l.setNeedsWrite(false);

            // Remove prefix from name of unaligned raster layer
            std::string name = l.template getProperty<std::string>("name");
            name.erase(name.end()-6, name.end());
            l.setProperty("name", name);
        }
        
        // ---------------------------------------------------------------------
        // Create advection kernel
        // Requires:
        //   classbits            - land classification layer (aligned)
        //   advect_x             - advection vector x-component (aligned)
        //   advect_y             - advection vector y-component (aligned)

        classbits.setNeedsWrite(false);
        layersReal[LevelSetLayers::Distance].setNeedsWrite(false);

        advectRefs = { 
            classbits, 
            layersReal[LevelSetLayers::Distance], 
            layersReal[LevelSetLayers::DistanceUpdate],
            layersReal[LevelSetLayers::Rate], 
            layersReal[LevelSetLayers::Speed],
            layersReal[LevelSetLayers::Advect_x], 
            layersReal[LevelSetLayers::Advect_y]
        };
        
        for (std::size_t i = 0; i < inputLayersRealCount; i++) {
            advectRefs.push_back(layersReal[i+LevelSetLayers::LevelSetLayers_END]);
        }
        for (std::size_t i = 0; i < inputLayersIntCount; i++) {
            advectRefs.push_back(layersInt[i]);
        }
        for (std::size_t i = 0; i < outputLayers.size(); i++) {
            advectRefs.push_back(*outputLayers[i]);
        }

        // Set advective variables
        std::string advectStr = std::string(R_cl_level_set_advect_c);
        std::string advectVarsList;
        for (auto &advectiveLayer : advectiveLayers) {
            auto &outputLayer = outputLayers[outputLayerMap[advectiveLayer.first]];
            outputLayer->setRequiredNeighbours(Neighbours::Queen);
            outputLayer->setNeedsWrite(false);

            // Set reduction type
            if (advectiveLayer.second.reduction == Reduction::Mean) {
                advectVarsList += "ri." + advectiveLayer.first + " = advectiveMean_" + outputLayer->getOpenCLTypeString() + "_DEF((" + 
                    advectiveLayer.second.value + "), " + advectiveLayer.first + ");\n";
            } else if (advectiveLayer.second.reduction == Reduction::Maximum) {
                advectVarsList += "ri." + advectiveLayer.first + " = advectiveMax_" + outputLayer->getOpenCLTypeString() + "_DEF((" + 
                    advectiveLayer.second.value + "), " + advectiveLayer.first + ");\n";
            } else {
                throw std::runtime_error("Invalid reduction type for advective layer");
            }
        }
        advectStr = std::regex_replace(advectStr, std::regex("\\/\\*__ADV_VARS__\\*\\/"), advectVarsList);
        
        // Generate kernel
        advectKernelGenerator = Geostack::generateKernel<TYPE>(
            advectScript, advectStr, advectRefs, RasterNullValue::Null, variables);

        classbits.setNeedsWrite(true);
        layersReal[LevelSetLayers::Distance].setNeedsWrite(true);

        for (auto &advectiveLayer : advectiveLayers) {
            auto &outputLayer = outputLayers[outputLayerMap[advectiveLayer.first]];
            outputLayer->setRequiredNeighbours(Neighbours::None);
            outputLayer->setNeedsWrite(true);
        }
        
        // ---------------------------------------------------------------------
        // Create build kernel
        // Requires:
        //   classbits            - land classification layer (aligned, neighbours)
        //   distance             - distance from perimeter (aligned, neighbours)
        //   distance update      - updated distance from perimeter (aligned)
        //   rate                 - time derivative of update (aligned)
        //   speed                - speed of perimeter (aligned)
        //   advect_x             - advection vector x-component (aligned)
        //   advect_y             - advection vector y-component (aligned)

        classbits.setNeedsWrite(false);
        layersReal[LevelSetLayers::Distance].setNeedsWrite(false);
        layersReal[LevelSetLayers::Advect_x].setNeedsWrite(false);
        layersReal[LevelSetLayers::Advect_y].setNeedsWrite(false);

        buildRefs = { 
            classbits, 
            layersReal[LevelSetLayers::Distance], 
            layersReal[LevelSetLayers::DistanceUpdate],
            layersReal[LevelSetLayers::Rate], 
            layersReal[LevelSetLayers::Speed],
            layersReal[LevelSetLayers::Advect_x], 
            layersReal[LevelSetLayers::Advect_y]
        };
        
        for (std::size_t i = 0; i < inputLayersRealCount; i++) {
            buildRefs.push_back(layersReal[i+LevelSetLayers::LevelSetLayers_END]);
        }
        for (std::size_t i = 0; i < inputLayersIntCount; i++) {
            buildRefs.push_back(layersInt[i]);
        }
        for (std::size_t i = 0; i < outputLayers.size(); i++) {
            buildRefs.push_back(*outputLayers[i]);
        }
        
        // Generate kernel
        std::string buildStr = std::string(R_cl_level_set_build_c);
        std::string buildStructVarsList;
        for (auto &advectiveLayer : advectiveLayers) {
            auto &outputLayer = outputLayers[outputLayerMap[advectiveLayer.first]];
            buildStructVarsList += advectiveLayer.first + " = (_ri+_sric+_index)->" + advectiveLayer.first + ";\n";
        }
        buildStr = std::regex_replace(buildStr, std::regex("\\/\\*__ADV_VARS__\\*\\/"), buildStructVarsList);

        buildKernelGenerator = Geostack::generateKernel<TYPE>(
            buildScript, buildStr, buildRefs, RasterNullValue::Null, variables);

        classbits.setNeedsWrite(true);
        layersReal[LevelSetLayers::Distance].setNeedsWrite(true);
        layersReal[LevelSetLayers::Advect_x].setNeedsWrite(true);
        layersReal[LevelSetLayers::Advect_y].setNeedsWrite(true);
        
        // ---------------------------------------------------------------------
        // Create update kernel
        // Requires:
        //   classbits            - land classification layer (aligned)
        //   distance             - distance from perimeter (aligned)
        //   distance update      - updated distance from perimeter (aligned, neighbours)
        //   rate                 - time derivative of update (aligned)
        //   speed                - speed of perimeter (aligned)
        //   arrival              - arrival time (aligned)
        //   advect_x             - advection vector x-component (aligned)
        //   advect_y             - advection vector y-component (aligned)

        layersReal[LevelSetLayers::DistanceUpdate].setNeedsWrite(false);
        layersReal[LevelSetLayers::Rate].setNeedsWrite(false);
        layersReal[LevelSetLayers::Speed].setNeedsWrite(false);
        layersReal[LevelSetLayers::Advect_x].setNeedsWrite(false);
        layersReal[LevelSetLayers::Advect_y].setNeedsWrite(false);

        updateRefs = {
            classbits, 
            layersReal[LevelSetLayers::Distance], 
            layersReal[LevelSetLayers::DistanceUpdate], 
            layersReal[LevelSetLayers::Rate], 
            layersReal[LevelSetLayers::Speed], 
            layersReal[LevelSetLayers::Arrival],
            layersReal[LevelSetLayers::Advect_x], 
            layersReal[LevelSetLayers::Advect_y]
        };
        
        for (std::size_t i = 0; i < inputLayersRealCount; i++) {
            updateRefs.push_back(layersReal[i+LevelSetLayers::LevelSetLayers_END]);
        }
        for (std::size_t i = 0; i < inputLayersIntCount; i++) {
            updateRefs.push_back(layersInt[i]);
        }
        for (std::size_t i = 0; i < outputLayers.size(); i++) {
            updateRefs.push_back(*outputLayers[i]);
        }
        
        // Generate kernel
        updateKernelGenerator = Geostack::generateKernel<TYPE>(
            updateScript, std::string(R_cl_level_set_update_c), updateRefs, RasterNullValue::Null, variables);

        layersReal[LevelSetLayers::DistanceUpdate].setNeedsWrite(true);
        layersReal[LevelSetLayers::Rate].setNeedsWrite(true);
        layersReal[LevelSetLayers::Speed].setNeedsWrite(true);
        layersReal[LevelSetLayers::Advect_x].setNeedsWrite(true);
        layersReal[LevelSetLayers::Advect_y].setNeedsWrite(true);
        
        // ---------------------------------------------------------------------
        // Create reinitialisation kernel
        // Requires:
        //   distance             - distance from perimeter (aligned, neighbours)
        //   distance update      - updated distance from perimeter (aligned)
        //   speed                - speed of perimeter (aligned, neighbours)

        layersReal[LevelSetLayers::Distance].setNeedsWrite(false);
        layersReal[LevelSetLayers::Speed].setNeedsWrite(false);
        
        reinitReq = RasterKernelRequirements( { Neighbours::Rook, Neighbours::None, Neighbours::Rook } );

        reinitRefs = { 
            layersReal[LevelSetLayers::Distance],
            layersReal[LevelSetLayers::DistanceUpdate], 
            layersReal[LevelSetLayers::Speed]
        };
        reinitFlipRefs = { 
            layersReal[LevelSetLayers::DistanceUpdate], 
            layersReal[LevelSetLayers::Distance], 
            layersReal[LevelSetLayers::Speed]
        };
        
        // Generate kernel
        std::string levelSetReinitScript = std::string(R_cl_level_set_reinit_c);
        if (levels > 1) {
            levelSetReinitScript = "#define LS_3D\n" + levelSetReinitScript;
        }
        reinitKernelGenerator = Geostack::generateKernel<TYPE>(
            "", levelSetReinitScript, { 
                layersReal[LevelSetLayers::Distance], 
                layersReal[LevelSetLayers::DistanceUpdate], 
                layersReal[LevelSetLayers::Speed]
            }, RasterNullValue::Null, variables);

        layersReal[LevelSetLayers::Distance].setNeedsWrite(true);
        layersReal[LevelSetLayers::Speed].setNeedsWrite(true);
        
        // ---------------------------------------------------------------------

        // Build script and get kernels
        RasterKernelGenerator levelSetGenerator;
        levelSetGenerator.script = 
            std::string(R_cl_date_head_c) +
            headStr +
            advectKernelGenerator.script + 
            initKernelGenerator.script + 
            buildKernelGenerator.script + 
            updateKernelGenerator.script + 
            reinitKernelGenerator.script;

        levelSetGenerator.reqs.usesRandom = 
            advectKernelGenerator.reqs.usesRandom ||
            initKernelGenerator.reqs.usesRandom ||
            buildKernelGenerator.reqs.usesRandom ||
            updateKernelGenerator.reqs.usesRandom;

        levelSetGenerator.projectionTypes.insert(
            advectKernelGenerator.projectionTypes.begin(), advectKernelGenerator.projectionTypes.end());
        levelSetGenerator.projectionTypes.insert(
            initKernelGenerator.projectionTypes.begin(), initKernelGenerator.projectionTypes.end());
        levelSetGenerator.projectionTypes.insert(
            buildKernelGenerator.projectionTypes.begin(), buildKernelGenerator.projectionTypes.end());
        levelSetGenerator.projectionTypes.insert(
            updateKernelGenerator.projectionTypes.begin(), updateKernelGenerator.projectionTypes.end());
        levelSetGenerator.projectionTypes.insert(
            reinitKernelGenerator.projectionTypes.begin(), reinitKernelGenerator.projectionTypes.end());

        levelSetGenerator.rasterTypes.insert(
            advectKernelGenerator.rasterTypes.begin(), advectKernelGenerator.rasterTypes.end());
        levelSetGenerator.rasterTypes.insert(
            initKernelGenerator.rasterTypes.begin(), initKernelGenerator.rasterTypes.end());
        levelSetGenerator.rasterTypes.insert(
            buildKernelGenerator.rasterTypes.begin(), buildKernelGenerator.rasterTypes.end());
        levelSetGenerator.rasterTypes.insert(
            updateKernelGenerator.rasterTypes.begin(), updateKernelGenerator.rasterTypes.end());
        levelSetGenerator.rasterTypes.insert(
            reinitKernelGenerator.rasterTypes.begin(), reinitKernelGenerator.rasterTypes.end());

        auto levelSetHash = buildRasterKernel(levelSetGenerator);
        if (levelSetHash == solver.getNullHash())
            return false;
            
        advectKernel = solver.getKernel(levelSetHash, "advect");
        advectInactiveKernel = solver.getKernel(levelSetHash, "advectInactive");
        initKernel = solver.getKernel(levelSetHash, "init");
        buildKernel = solver.getKernel(levelSetHash, "build");
        updateKernel = solver.getKernel(levelSetHash, "update");
        updateInactiveKernel = solver.getKernel(levelSetHash, "updateInactive");
        reinitKernel = solver.getKernel(levelSetHash, "reinitWeighted");
        buildKernelMultiple = solver.getKernelPreferredWorkgroupMultiple(buildKernel);
        
        // Clear internal variables
        activeTiles.clear();
        inactiveTiles.clear();
        newTileIndexes.clear();
        statusVector.clear();

        // Activate all tiles
        auto rDim = classbits.getRasterDimensions();
        for (uint32_t tj = 0; tj < rDim.ty; tj++) {
            for (uint32_t ti = 0; ti < rDim.tx; ti++) {

                // Add to active tile list
                activeTiles.insert( { ti, tj } );
                newTileIndexes.insert( { ti, tj } );
            }
        }

        // Set initialisation flag
        initialised = true;

        // Set internal rasters as non-writeable in scripts
        classbits.setNeedsWrite(false);
        layersReal[LevelSetLayers::Distance].setNeedsWrite(false);
        layersReal[LevelSetLayers::DistanceUpdate].setNeedsWrite(false);
        layersReal[LevelSetLayers::Rate].setNeedsWrite(false);
        layersReal[LevelSetLayers::Speed].setNeedsWrite(false);
        layersReal[LevelSetLayers::Arrival].setNeedsWrite(false);

        return true;
    }

    template <typename TYPE>
    bool LevelSet<TYPE>::step() {
    
        const uint32_t tileSize = TileMetrics::tileSize;

        // Check initialisation
        if (!initialised) {
            throw std::runtime_error("Solver not initialised.");
        }

        // Check for active interfaces
        if (activeTiles.size() == 0) {
            std::cout << "ERROR: No active interfaces." << std::endl;
            return false;
        }

        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &context = solver.getContext();
        auto &queue = solver.getQueue();

        // Get dimensions
        auto rDim = classbits.getRasterDimensions();

        // Update new tiles
        if (!newTileIndexes.empty()) {
            for (auto &t : newTileIndexes) {
        
                // Get indexes
                uint32_t ti = t.first;
                uint32_t tj = t.second;                

                // Set new distance tile to twice the narrow band width
                layersReal[LevelSetLayers::Distance].setAllTileCellValues(ti, tj, (TYPE)4.0*p.bandWidth);
                layersReal[LevelSetLayers::DistanceUpdate].setAllTileCellValues(ti, tj, (TYPE)4.0*p.bandWidth);

                // Run initialisation kernel
                Geostack::runTileKernel<TYPE>(ti, tj, initKernel, initRefs, initKernelGenerator.reqs, 0, variables);
            }
            newTileIndexes.clear();
        }

        // Map sources
        if (currentSources.hasData()) {
            for (auto &t : activeTiles) {

                // Map sources
                layersReal[LevelSetLayers::Distance].mapTileVector(t.first, t.second, currentSources,
                    GeometryType::Point | GeometryType::LineString | GeometryType::Polygon, "radius", "level");
            }
        }

        // Set minimum time step to smallest possible positive value
        TYPE timeStepMin = std::numeric_limits<TYPE>::epsilon();
        if (p.time > timeStepMin) 
            timeStepMin = std::nextafter(p.time, std::numeric_limits<TYPE>::max())-p.time;

        // Update time step
        p.dt = std::min(CFL*resolution/(p.maxSpeed+1.0E-6), 2.0*p.dt);
        p.dt = std::max(p.dt, timeStepMin);
        p.dt = std::min(p.dt, timeStepMax);
            
        // Modify time step to lock to target multiples
        if (timeMultiple > 0.0 && fmod(p.time, timeMultiple) != 0.0) {
            TYPE dTarget = ceil(p.time/timeMultiple)*timeMultiple;
            TYPE tGap = dTarget-p.time;
            if (p.dt > tGap) {

                // If next time is greater than target set time step to match target
                p.dt = tGap;

            } else if (p.dt > 0.9*tGap) {
                
                // If next time step is close to target, reduce time step to 'soften' approach
                p.dt *= 0.75;
            }
        }

        // Change max speed to take timestep into account
        p.maxSpeed = resolution/p.dt;        
        
        // Update times
        if (hasStartDate) {

            // Set epoch
            auto currentTime = startTime+std::chrono::seconds((uint64_t)((double)p.time));
            currentEpochMilliseconds = currentTime.time_since_epoch().count();
            p.JulianDate = (TYPE)DateCalculation::JulianDateFromEpoch<double>(currentEpochMilliseconds/1000.0);

            // Set date values
            auto currentTimeinZone = currentTime+std::chrono::minutes(localZoneOffsetMinutes);
            auto dmy = date::year_month_day{ date::floor<date::days>(currentTimeinZone) };
            variables->set("day", (REAL)((unsigned int)dmy.day()));
            variables->set("month", (REAL)((unsigned int)dmy.month()));
            variables->set("year", (REAL)((int)dmy.year()));
            
            // Set time values
            auto time = date::make_time(currentTimeinZone-date::floor<date::days>(currentTimeinZone));
            variables->set("hour", (REAL)time.hours().count());

        } else {
            currentEpochMilliseconds = (uint64_t)((double)p.time*1000.0);
            p.JulianDate = (TYPE)DateCalculation::JulianDateFromEpoch<double>(currentEpochMilliseconds/1000.0);
        }

        // Add parameters to variables
        variables->set("time", p.time);
        variables->set("dt", p.dt);
        variables->set("max_speed", p.maxSpeed);
        variables->set("area", p.area);
        variables->set("Julian_date", p.JulianDate);

        // Run reinitialisation kernel
        reinitKernel.setArg(0, (TYPE)CFL);
        for (auto &t : activeTiles) {

            // Run reinitialisation kernel
            Geostack::runTileKernel<TYPE>(t.first, t.second, reinitKernel, reinitRefs, reinitKernelGenerator.reqs, 1, variables);
        }
            
        for (auto &t : activeTiles) {
            
            // Run reinitialisation kernel
            Geostack::runTileKernel<TYPE>(t.first, t.second, reinitKernel, reinitFlipRefs, reinitKernelGenerator.reqs, 1, variables);
        }

        // Check and update index variable
        if (rasterIndexCount.size() != activeTiles.size()+1) {

            // Resize data and create buffer
            rasterIndexCount.resize(activeTiles.size()+1);
            rasterIndexCountBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY | CL_MEM_USE_HOST_PTR, 
                rasterIndexCount.size()*sizeof(cl_uint), rasterIndexCount.data());
            auto rasterCountPtr = queue.enqueueMapBuffer(rasterIndexCountBuffer, CL_TRUE, CL_MAP_READ, 0, rasterIndexCount.size()*sizeof(cl_uint));

            // Check pointer
            if (rasterCountPtr != static_cast<void *>(rasterIndexCount.data()))
                throw std::runtime_error("OpenCL mapped pointer differs from data pointer");

            // Resize index buffer
            rasterIndexBuffer = cl::Buffer(context, CL_MEM_READ_WRITE, 
                (cl::size_type)levels*TileMetrics::tileSizeSquared*advectiveLayerStructSize*activeTiles.size());
        }

        // Build tile indexes
        uint32_t  tileCount = 0;
        queue.enqueueUnmapMemObject(rasterIndexCountBuffer, static_cast<void *>(rasterIndexCount.data()));
        solver.fillBuffer<cl_uint>(rasterIndexCountBuffer, 0, rasterIndexCount.size());
        advectKernel.setArg(1, rasterIndexCountBuffer);  
        advectKernel.setArg(2, rasterIndexBuffer);  
        for (auto &t : activeTiles) {

            // Run advection kernel for active tiles
            advectKernel.setArg(0, tileCount++);  
            Geostack::runTileKernel<TYPE>(t.first, t.second, advectKernel, advectRefs, advectKernelGenerator.reqs, 3, variables);
        }

        for (auto &t : inactiveTiles) {

            // Run advection kernel for inactive tiles
            Geostack::runTileKernel<TYPE>(t.first, t.second, advectInactiveKernel, advectRefs, advectKernelGenerator.reqs, 0, variables);
        }

        // Map to host and check pointer
        auto rasterCountPtr = queue.enqueueMapBuffer(rasterIndexCountBuffer, CL_FALSE, CL_MAP_READ, 0, rasterIndexCount.size()*sizeof(cl_uint), 0, &rasterIndexMapEvent); 
        if (rasterCountPtr != static_cast<void *>(rasterIndexCount.data()))
            throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
        
        // Check and update status variable
        if (statusVector.size() != activeTiles.size()+2) {

            // Resize data and create buffer
            statusVector.clear();
            statusVector.resize(activeTiles.size()+2);
            statusBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY | CL_MEM_USE_HOST_PTR, 
                statusVector.size()*sizeof(cl_uint), statusVector.data());
            auto statusBufferPtr = queue.enqueueMapBuffer(statusBuffer, CL_TRUE, CL_MAP_READ, 0, statusVector.size()*sizeof(cl_uint));

            // Check pointer
            if (statusBufferPtr != static_cast<void *>(statusVector.data()))
                throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
        }
        queue.enqueueUnmapMemObject(statusBuffer, static_cast<void *>(statusVector.data()));
        solver.fillBuffer<cl_uint>(statusBuffer, 0, statusVector.size());

        // Build first numerical integration stage
        tileCount = 0;
        cl_uint startIndex = 0;
        rasterIndexMapEvent.wait();
        buildKernel.setArg(2, rasterIndexBuffer);  
        buildKernel.setArg(3, levels);
        for (auto &t : activeTiles) {
        
            auto size = rasterIndexCount[tileCount+1];
            if (size > 0) {

                // Set build kernel arguments
                buildKernel.setArg(0, startIndex);
                buildKernel.setArg(1, size);
                Geostack::setTileKernelArguments<TYPE>(t.first, t.second, buildKernel, buildRefs, buildKernelGenerator.reqs, 4, variables);
            
                // Run build kernel
                std::size_t paddedSize = (std::size_t)(size+((buildKernelMultiple-(size%buildKernelMultiple))%buildKernelMultiple));
                queue.enqueueNDRangeKernel(buildKernel, cl::NullRange, cl::NDRange(paddedSize), cl::NDRange(buildKernelMultiple));
            }
            tileCount++;
            startIndex += size;
        }
        
        // Run second numerical integration stage and update
        tileCount = 0;
        updateKernel.setArg(1, statusBuffer);  
        for (auto &t : activeTiles) {

            updateKernel.setArg(0, tileCount++);         
            Geostack::runTileKernel<TYPE>(t.first, t.second, updateKernel, updateRefs, updateKernelGenerator.reqs, 2, variables);
        }
 
        for (auto &t : inactiveTiles) {      
            Geostack::runTileKernel<TYPE>(t.first, t.second, updateInactiveKernel, updateRefs, updateKernelGenerator.reqs, 0, variables);
        }
        
        // Map to host and check pointer
        auto statusBufferPtr = queue.enqueueMapBuffer(statusBuffer, CL_FALSE, CL_MAP_READ, 0, statusVector.size()*sizeof(cl_uint), 0, &statusMapEvent); 
        if (statusBufferPtr != static_cast<void *>(statusVector.data()))
            throw std::runtime_error("OpenCL mapped pointer differs from data pointer");

        // Update time
        lastTime = p.time;
        p.time += p.dt;
        
        // Skip sources at first time
        auto bounds = sources.getBounds();
        if (lastTime != bounds.min.s) {

            // Decrement next time by smallest possible increment to ensure sources are not applied twice
            TYPE timeMinusDelta = std::nextafter(p.time, lastTime);

            // Find sources in current time slice
            currentSources = sources.region(BoundingBox<TYPE>( 
                { { bounds.min.p, bounds.min.q, std::numeric_limits<TYPE>::lowest(), lastTime }, 
                  { bounds.max.p, bounds.max.q, std::numeric_limits<TYPE>::max(), timeMinusDelta } } ) );

        } else {

            // Clear any sources from first time step
            currentSources = Vector<TYPE>();
        }

        // Wait for mapping to complete
        statusMapEvent.wait();

        // Get minimum distance to boundary
        p.area = (TYPE)(statusVector[0]*resolution*resolution); // First status integer is count

        // Get maximum speed
        p.maxSpeed = (TYPE)statusVector[1]/65535.0; // Second status integer is fixed-point maximum speed

        // Check maximum speed
        if (p.maxSpeed == 65535.0) {
            std::cout << "ERROR: Maximum speed exceeded." << std::endl;
            return false;
        }

        // Build active tile map
        tileCount = 0;
        cl_uint resizeBits = 0;
        for (auto &t : activeTiles) {

            // Status integers from 1 onwards are tile boundary flags
            cl_uint tileStatus = statusVector[2+tileCount++];

            if (tileStatus & 0xF) {

                // Get indexes
                uint32_t ti = t.first;
                uint32_t tj = t.second;
            
                // North boundary
                if (tileStatus & 0x01 && tj >= rDim.ty-1) {
                    resizeBits |= 0x01;
                }
                
                // East boundary
                if (tileStatus & 0x02 && ti >= rDim.tx-1) {
                    resizeBits |= 0x02;
                }
                
                // South boundary
                if (tileStatus & 0x04 && tj <= 0) {
                    resizeBits |= 0x04;
                }
                
                // West boundary
                if (tileStatus & 0x08 && ti <= 0) {
                    resizeBits |= 0x08;
                }
            }
        }

        // Expand raster if perimeter is approaching boundary
        if (resizeBits != 0) {
            
            // Set new size
            uint32_t ox = 0;
            uint32_t oy = 0;
            if (resizeBits & 0x01) {
                rDim.ty+=1;
            }
            if (resizeBits & 0x02) {
                rDim.tx+=1;
            }
            if (resizeBits & 0x04) {
                rDim.ty+=1;
                oy = 1;
            }
            if (resizeBits & 0x08) {
                rDim.tx+=1;
                ox = 1;
            }

            // Resize domain
            resizeTiles(rDim.tx, rDim.ty, ox, oy);

            // Update tile indexes
            std::set<std::pair<uint32_t, uint32_t> > updatedTiles;
            for (auto &t : activeTiles) {
                updatedTiles.insert( { t.first+ox, t.second+oy } );
            }
            activeTiles = updatedTiles;

            updatedTiles.clear();
            for (auto &t : inactiveTiles) {
                updatedTiles.insert( { t.first+ox, t.second+oy } );
            }
            inactiveTiles = updatedTiles;
        }

        // Rebuild active tile map
        tileCount = 0;
        tileIndexSet newActiveTiles;
        for (auto &t : activeTiles) {

            // Status integers from 1 onwards are tile boundary flags
            cl_uint tileStatus = statusVector[2+tileCount++];

            if (tileStatus != 0) {
            
                // Get indexes
                uint32_t ti = t.first;
                uint32_t tj = t.second;

                // Add tile
                newActiveTiles.insert( { ti, tj } );
                    
                // North boundary
                if (tileStatus & 0x01 && tj < rDim.ty-1) {
                    newActiveTiles.insert( { ti, tj+1 } );
                }
                    
                // East boundary
                if (tileStatus & 0x02 && ti < rDim.tx-1) {
                    newActiveTiles.insert( { ti+1, tj } );
                }
                    
                // South boundary
                if (tileStatus & 0x04 && tj > 0) {
                    newActiveTiles.insert( { ti, tj-1 } );
                }
                    
                // West boundary
                if (tileStatus & 0x08 && ti > 0) {
                    newActiveTiles.insert( { ti-1, tj } );
                }
            }
        }

        // Check for changes in active tiles
        if (activeTiles != newActiveTiles) {

            // Update inactive tiles
            for (auto &t : activeTiles) {
                inactiveTiles.insert(t);
            }
            for (auto &t : newActiveTiles) {
                inactiveTiles.erase(t);
            }

            // Set change flag and update active tiles
            activeTiles = newActiveTiles; 
        }
        
        if (currentSources.hasData()) {
        
            // Get distance map bounds
            auto newBounds = currentSources.getBounds();
            newBounds.extend2D((TYPE)4.0*p.bandWidth);

            // Calculate distances
            uint32_t nxp = (uint32_t)ceil(std::max((TYPE)0.0, newBounds.max.p-rDim.ex)/resolution);
            uint32_t nyp = (uint32_t)ceil(std::max((TYPE)0.0, newBounds.max.q-rDim.ey)/resolution);
            uint32_t nxm = (uint32_t)ceil(std::max((TYPE)0.0, rDim.d.ox-newBounds.min.p)/resolution);
            uint32_t nym = (uint32_t)ceil(std::max((TYPE)0.0, rDim.d.oy-newBounds.min.q)/resolution);

            // Calculate number of tiles
            uint32_t ntxp = (nxp+((tileSize-(nxp%tileSize))%tileSize))/tileSize;
            uint32_t ntyp = (nyp+((tileSize-(nyp%tileSize))%tileSize))/tileSize;
            uint32_t ntxm = (nxm+((tileSize-(nxm%tileSize))%tileSize))/tileSize;
            uint32_t ntym = (nym+((tileSize-(nym%tileSize))%tileSize))/tileSize;

            // Resize domain
            rDim.tx+=ntxp+ntxm;
            rDim.ty+=ntyp+ntym;
            resizeTiles(rDim.tx, rDim.ty, ntxm, ntym);

            // Update dimensions
            rDim = classbits.getRasterDimensions();

            // Update tile indexes
            std::set<std::pair<uint32_t, uint32_t> > updatedTiles;
            for (auto &t : activeTiles) {
                updatedTiles.insert( { t.first+ntxm, t.second+ntym } );
            }
            activeTiles = updatedTiles;

            updatedTiles.clear();
            for (auto &t : inactiveTiles) {
                updatedTiles.insert( { t.first+ntxm, t.second+ntym } );
            }
            inactiveTiles = updatedTiles;

            // Activate all tiles within new source bounds
            uint32_t si = (uint32_t)((newBounds.min.p-rDim.d.ox)/((TYPE)tileSize*resolution));
            uint32_t sj = (uint32_t)((newBounds.min.q-rDim.d.oy)/((TYPE)tileSize*resolution));
            uint32_t ei = (uint32_t)((newBounds.max.p-rDim.d.ox)/((TYPE)tileSize*resolution));
            uint32_t ej = (uint32_t)((newBounds.max.q-rDim.d.oy)/((TYPE)tileSize*resolution));

            for (uint32_t tj = sj; tj <= ej; tj++) {
                for (uint32_t ti = si; ti <= ei; ti++) {
                    std::pair<uint32_t, uint32_t> t = { ti, tj };
                    activeTiles.insert(t);
                    inactiveTiles.erase(t);
                }
            }
        }

        // Increment iteration count
        iters++;

        return true;
    }
   
    template <typename TYPE>
    void LevelSet<TYPE>::addSource(const Vector<TYPE> &v) {

        // Check projection
        auto proj = sources.getProjectionParameters();
        if (v.getProjectionParameters() != proj) {
            sources += v.convert(proj);
        } else {
            sources += v;
        }
    }
    
    template <typename TYPE>
    void LevelSet<TYPE>::resizeDomain(uint32_t nx, uint32_t ny, uint32_t ox, uint32_t oy) {

        // Check initialisation
        if (!initialised) {
            throw std::runtime_error("Solver not initialised.");
        }

        // Resize domain
        resizeTiles(nx, ny, ox, oy);

        // Update new tiles
        if (!newTileIndexes.empty()) {

            // Initialise tiles
            for (auto &t : newTileIndexes) {
        
                // Get indexes
                uint32_t ti = t.first;
                uint32_t tj = t.second; 

                // Set new distance tile to twice the narrow band width
                layersReal[LevelSetLayers::Distance].setAllTileCellValues(ti, tj, (TYPE)4.0*p.bandWidth);
                layersReal[LevelSetLayers::DistanceUpdate].setAllTileCellValues(ti, tj, (TYPE)4.0*p.bandWidth);

                // Run initialisation kernel
                Geostack::runTileKernel<TYPE>(ti, tj, initKernel, initRefs, initKernelGenerator.reqs, 0, variables);
            }
            newTileIndexes.clear();

            // Update tile indexes
            std::set<std::pair<uint32_t, uint32_t> > updatedTiles;
            for (auto &t : activeTiles) {
                updatedTiles.insert( { t.first+ox, t.second+oy } );
            }
            activeTiles = updatedTiles;

            updatedTiles.clear();
            for (auto &t : inactiveTiles) {
                updatedTiles.insert( { t.first+ox, t.second+oy } );
            }
            inactiveTiles = updatedTiles;
        }
    }

    // Float type definitions
    template class LevelSet<float>;

    // Double type definitions
    template class LevelSet<double>;
}
