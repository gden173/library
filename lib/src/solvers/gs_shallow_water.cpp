/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <cmath>
#include <iostream>
#include <iomanip>

#include "json11/json11.hpp"

#include "gs_solver.h"
#include "gs_vector.h"
#include "gs_geojson.h"
#include "gs_projection.h"
#include "gs_string.h"
#include "gs_shallow_water.h"

// Lattice stencil and indexes
//
// NW (6)  N (2)  NE (5)
//       \   |   /
//        \  |  /
//         \ | /
//          \|/
// W (3) ----0---- E (1)
//          /|\
//         / | \
//        /  |  \
//       /   |   \
// SW (7)  S (4)  SE (8)
//

namespace Geostack
{ 
    using namespace json11; 
    
    template <typename TYPE>
    bool ShallowWater<TYPE>::init(
        std::string jsonConfig,
        std::vector<RasterBasePtr<TYPE> > inputLayers_){

        // Parse configuration
        std::string jsonErr;
        auto config = Json::parse(jsonConfig, jsonErr);
        if (!jsonErr.empty()) {
            throw std::runtime_error(jsonErr);
        }
        
        // Get and check timestep
        dt = 0.0;
        if (config["dt"].is_number()) {
            dt = (TYPE)config["dt"].number_value();
        }
        if (dt <= 0.0) {
            throw std::runtime_error("Time step must be positive");
        }    
        
        // Get and check drag
        cd = 0.01;
        if (config["dragCoefficientManning"].is_number()) {
            cd = (TYPE)config["dragCoefficientManning"].number_value();
        }
        if (cd <= 0.0) {
            throw std::runtime_error("Manning drag coefficient must be positive");
        }

        // Get and check relaxation time
        tau = 0.9;
        if (config["tau"].is_number()) {
            tau = (TYPE)config["tau"].number_value();
        }
        if (tau <= 0.0) {
            throw std::runtime_error("Relaxation time must be positive");
        }

        // Copy layers
        inputLayers = inputLayers_;

        // Check inputs
        if (inputLayers.size() < 2) {
            throw std::runtime_error("Atleast two layers (height and base) are needed to run Shallow water solver.");
        }
        
        auto h_in = inputLayers[0];
        auto b_in = inputLayers[1];
        
        for (size_t i = 0; i < inputLayers.size(); i++) {
            std::string name = inputLayers[i]->template getProperty<std::string>("name");
            if (Strings::toLower(name) == "h" || Strings::toLower(name) == "height") {
                h_in = inputLayers[i];
            } else if (Strings::toLower(name) == "b" || Strings::toLower(name) == "base") {
                b_in = inputLayers[i];
            }
        }

        // get dimension from first layer
        auto dim_h = h_in->getRasterDimensions();
        
        // get projection from first layer
        auto proj = h_in->getProjectionParameters();
        
        // get name of input depth layer raster
        std::string initialDepthLayerName = h_in->template getProperty<std::string>("name");

        // check input layers
        for (auto &l: inputLayers) {
            std::string name = l->template getProperty<std::string>("name");
            // check if input layers have been initialised
            if (!l->hasData()) {
                std::stringstream err;
                err << "Shallow water " << name << " input must be initialised";
                throw std::runtime_error(err.str());
            }
            
            // check if input layers have same dimension
            auto dim = l->getRasterDimensions();
            if (!equalSpatialMetrics2D(dim_h.d, dim.d)) {
                std::stringstream err;
                err << "Raster dimensions in input layer " << name << " should be similar to " << initialDepthLayerName << ".";
                throw std::runtime_error(err.str());
            }
            
            // check if input layers have same projection
            if (l->getProjectionParameters() != proj) {
                std::stringstream err;
                err << "Projection parameters in input layer " << name << " should be similar to " << initialDepthLayerName << ".";
                throw std::runtime_error(err.str());
            }
        }

        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &context = solver.getContext();
        auto &queue = solver.getQueue();

        // Clear status
        statusVector.clear();

        // Set active tiles
        activeTiles.clear();
        for (uint32_t tj = 0; tj < dim_h.ty; tj++) {
            for (uint32_t ti = 0; ti < dim_h.tx; ti++) {
                activeTiles.insert( { ti, tj } );
            }
        }

        // resize internal raster layers
        layersReal.resize(ShallowWaterLayers::ShallowWaterLayers_END);
        
        // set names of internal raster layers
        layersReal[ShallowWaterLayers::h].setProperty("name", "h_");
        layersReal[ShallowWaterLayers::b].setProperty("name", "b_");
        layersReal[ShallowWaterLayers::uh].setProperty("name", "uh");
        layersReal[ShallowWaterLayers::vh].setProperty("name", "vh");
        layersReal[ShallowWaterLayers::fi].setProperty("name", "_fi");
        layersReal[ShallowWaterLayers::fe].setProperty("name", "_fe");
        layersReal[ShallowWaterLayers::ft].setProperty("name", "_ft");
        
        // create raster dimensions for 3D raster
        auto dim3D = dim_h;
        dim3D.d.nz = 9;

        // instantiate internal raster layers
        for (auto &l: layersReal) {
            auto layerName = l.template getProperty<std::string>("name");
            if (layerName == "_fi" || layerName == "_fe" || layerName == "_ft") {
                if (!l.init(dim3D.d)) {
                    return false;
                }
            } else {
                if (!l.init(dim_h.d)) {
                    return false;
                }
            }
            l.setProjectionParameters(proj);
        }

        // Copy input height layer to internal raster
        // h.init2D(dim_h.d.nx, dim_h.d.ny, dim_h.d.hx, dim_h.d.hy, dim_h.d.ox, dim_h.d.oy);
        // h.setProperty("name", "_h");
        // h.setProjectionParameters(proj);
        // runScript<TYPE>("_h = " + initialDepthLayerName + ";", { h, *h_in } );
        // h.setProperty("name", "h");
        
        runScript<TYPE>("h_ = " + initialDepthLayerName + ";",
            { layersReal[ShallowWaterLayers::h], *h_in } );
        layersReal[ShallowWaterLayers::h].setProperty("name", "h");

        // Copy input base layer to internal raster
        // b.init2D(dim_h.d.nx, dim_h.d.ny, dim_h.d.hx, dim_h.d.hy, dim_h.d.ox, dim_h.d.oy);
        // b.setProperty("name", "_b");
        // b.setProjectionParameters(proj);
        
        std::string initialBaseLayerName = b_in->template getProperty<std::string>("name");
        // runScript<TYPE>("_b = " + initialBaseLayerName + ";", { b, *b_in } );
        // b.setProperty("name", "b");
        
        runScript<TYPE>("b_ = " + initialBaseLayerName + ";", 
            { layersReal[ShallowWaterLayers::b], *b_in } );
        layersReal[ShallowWaterLayers::b].setProperty("name", "b");
        
        // initialise internal rasters
        // unit discharge in x direction
        // uh.init2D(dim_h.d.nx, dim_h.d.ny, dim_h.d.hx, dim_h.d.hy, dim_h.d.ox, dim_h.d.oy);
        // uh.setProperty("name", "uh");
        // uh.setProjectionParameters(proj);

        // unit discharge in y direction
        // vh.init2D(dim_h.d.nx, dim_h.d.ny, dim_h.d.hx, dim_h.d.hy, dim_h.d.ox, dim_h.d.oy);
        // vh.setProperty("name", "vh");
        // vh.setProjectionParameters(proj);

        // distribution function at current timestep
        // fi.init(dim_h.d.nx, dim_h.d.ny, 9, dim_h.d.hx, dim_h.d.hy, 1.0, dim_h.d.ox, dim_h.d.oy, dim_h.d.oz);
        // fi.setProperty("name", "_fi");
        // fi.setProjectionParameters(proj);

        // equilibrium distribution function
        // fe.init(dim_h.d.nx, dim_h.d.ny, 9, dim_h.d.hx, dim_h.d.hy, 1.0, dim_h.d.ox, dim_h.d.oy, dim_h.d.oz);
        // fe.setProperty("name", "_fe");
        // fe.setProjectionParameters(proj);

        // distribution function at next timestep
        // ft.init(dim_h.d.nx, dim_h.d.ny, 9, dim_h.d.hx, dim_h.d.hy, 1.0, dim_h.d.ox, dim_h.d.oy, dim_h.d.oz);
        // ft.setProperty("name", "_ft");
        // ft.setProjectionParameters(proj);
        
        // Create initialisation kernel
        // initRefs = { h, uh, vh, fi };
        initRefs = {
            layersReal[ShallowWaterLayers::h],
            layersReal[ShallowWaterLayers::uh],
            layersReal[ShallowWaterLayers::vh],
            layersReal[ShallowWaterLayers::fi] };
        initKernelGenerator = generateKernel<TYPE>("", std::string(R_cl_shallow_water_init_c), initRefs);

        // Create collision kernel
        // h.setReductionType(Reduction::Sum);
        // h.setNeedsWrite(false);
        // b.setNeedsWrite(false);
        // uh.setNeedsWrite(false);
        // vh.setNeedsWrite(false);
        layersReal[ShallowWaterLayers::h].setReductionType(Reduction::Sum);
        layersReal[ShallowWaterLayers::h].setNeedsWrite(false);
        layersReal[ShallowWaterLayers::b].setNeedsWrite(false);
        layersReal[ShallowWaterLayers::uh].setNeedsWrite(false);
        layersReal[ShallowWaterLayers::vh].setNeedsWrite(false);
        
        // collisionRefs = { h, b, uh, vh, fi, fe, ft };
        collisionRefs = { 
            layersReal[ShallowWaterLayers::h],
            layersReal[ShallowWaterLayers::b],
            layersReal[ShallowWaterLayers::uh],
            layersReal[ShallowWaterLayers::vh],
            layersReal[ShallowWaterLayers::fi],
            layersReal[ShallowWaterLayers::fe],
            layersReal[ShallowWaterLayers::ft] };
        collisionKernelGenerator = generateKernel<TYPE>("", std::string(R_cl_shallow_water_collision_c), collisionRefs);
        // h.setReductionType(Reduction::None);
        // h.setNeedsWrite(true);  
        // b.setNeedsWrite(true);  
        // uh.setNeedsWrite(true);
        // vh.setNeedsWrite(true);
        layersReal[ShallowWaterLayers::h].setReductionType(Reduction::None);
        layersReal[ShallowWaterLayers::h].setNeedsWrite(true);  
        layersReal[ShallowWaterLayers::b].setNeedsWrite(true);  
        layersReal[ShallowWaterLayers::uh].setNeedsWrite(true);
        layersReal[ShallowWaterLayers::vh].setNeedsWrite(true);

        // Create streaming kernel
        // b.setNeedsWrite(false);
        layersReal[ShallowWaterLayers::b].setNeedsWrite(false);
        // streamingRefs = { h, uh, vh, fi, fe, ft };
        streamingRefs = { 
            layersReal[ShallowWaterLayers::h],
            layersReal[ShallowWaterLayers::uh],
            layersReal[ShallowWaterLayers::vh],
            layersReal[ShallowWaterLayers::fi],
            layersReal[ShallowWaterLayers::fe],
            layersReal[ShallowWaterLayers::ft] };
        streamingKernelGenerator = generateKernel<TYPE>("", std::string(R_cl_shallow_water_streaming_c), streamingRefs);
        streamingKernelGenerator.reqs.rasterRequirements[4] = Neighbours::Queen;
        streamingKernelGenerator.reqs.rasterRequirements[5] = Neighbours::Queen;
        // b.setNeedsWrite(false);
        layersReal[ShallowWaterLayers::b].setNeedsWrite(false);

        // Build script and get kernels
        RasterKernelGenerator shallowWaterGenerator;
        shallowWaterGenerator.script = 
            initKernelGenerator.script + 
            collisionKernelGenerator.script +
            streamingKernelGenerator.script;

        shallowWaterGenerator.reqs.usesRandom = 
            initKernelGenerator.reqs.usesRandom;

        shallowWaterGenerator.projectionTypes.insert(
            initKernelGenerator.projectionTypes.begin(), initKernelGenerator.projectionTypes.end());
        shallowWaterGenerator.projectionTypes.insert(
            collisionKernelGenerator.projectionTypes.begin(), collisionKernelGenerator.projectionTypes.end());
        shallowWaterGenerator.projectionTypes.insert(
            streamingKernelGenerator.projectionTypes.begin(), streamingKernelGenerator.projectionTypes.end());

        shallowWaterGenerator.rasterTypes.insert(
            initKernelGenerator.rasterTypes.begin(), initKernelGenerator.rasterTypes.end());
        shallowWaterGenerator.rasterTypes.insert(
            collisionKernelGenerator.rasterTypes.begin(), collisionKernelGenerator.rasterTypes.end());
        shallowWaterGenerator.rasterTypes.insert(
            streamingKernelGenerator.rasterTypes.begin(), streamingKernelGenerator.rasterTypes.end());

        auto shallowWaterHash = buildRasterKernel(shallowWaterGenerator);
        if (shallowWaterHash == solver.getNullHash())
            return false;
            
        initKernel = solver.getKernel(shallowWaterHash, "init");
        collisionKernel = solver.getKernel(shallowWaterHash, "collision");
        streamingKernel = solver.getKernel(shallowWaterHash, "streaming");

        // Initialise
        TYPE c = (dim_h.d.hx+dim_h.d.hy)/(2.0*dt);
        TYPE ic2 = 1.0/(c*c);

        initKernel.setArg(0, ic2);
        for (uint32_t tj = 0; tj < dim_h.ty; tj++) {
            for (uint32_t ti = 0; ti < dim_h.tx; ti++) {
                runTileKernel<TYPE>(ti, tj, initKernel, initRefs, initKernelGenerator.reqs, 1);
            }
        }

        // Set initialisation flag
        initialised = true;

        return true;
    }

    template <typename TYPE>
    bool ShallowWater<TYPE>::step() {
    
        // Check initialisation
        if (!initialised) {
            throw std::runtime_error("Solver not initialised.");
        }

        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &context = solver.getContext();
        auto &queue = solver.getQueue();

        // Check and update status variable
        if (statusVector.size() != activeTiles.size()) {

            // Resize data and create buffer
            statusVector.resize(activeTiles.size());
            statusBuffer = cl::Buffer(context, CL_MEM_WRITE_ONLY | CL_MEM_HOST_READ_ONLY | CL_MEM_USE_HOST_PTR, 
                statusVector.size()*sizeof(cl_uint), statusVector.data());
            auto statusBufferPtr = queue.enqueueMapBuffer(statusBuffer, CL_TRUE, CL_MAP_READ, 0, statusVector.size()*sizeof(cl_uint));

            // Check pointer
            if (statusBufferPtr != static_cast<void *>(statusVector.data()))
                throw std::runtime_error("OpenCL mapped pointer differs from data pointer");
        }
        queue.enqueueUnmapMemObject(statusBuffer, static_cast<void *>(statusVector.data()));
        solver.fillBuffer<cl_uint>(statusBuffer, 0, statusVector.size());
        
        // Simulation variables
        // auto dim_h = h.getRasterDimensions();
        auto dim_h = layersReal[ShallowWaterLayers::h].getRasterDimensions();
        TYPE c = (dim_h.d.hx+dim_h.d.hy)/(2.0*dt);
        TYPE cs = c/sqrt(3.0); // Sound speed
        TYPE ic2 = 1.0/(c*c);
        TYPE ics2 = 1.0/(cs*cs);

        // Collision step
        cl_uint tid = 0;
        // h.setReductionType(Reduction::Sum);
        layersReal[ShallowWaterLayers::h].setReductionType(Reduction::Sum);
        collisionKernel.setArg(0, c);
        collisionKernel.setArg(1, ic2);
        collisionKernel.setArg(2, ics2);
        collisionKernel.setArg(3, (REAL)(1.0/tau));
        collisionKernel.setArg(5, statusBuffer);  
        for (auto &tile : activeTiles) {
            collisionKernel.setArg(4, tid++);  
            runTileKernel<TYPE>(tile.first, tile.second, collisionKernel, collisionRefs, collisionKernelGenerator.reqs, 6);
        }
        // h.setReductionType(Reduction::None);
        layersReal[ShallowWaterLayers::h].setReductionType(Reduction::None);

        // Map to host and check pointer
        auto statusBufferPtr = queue.enqueueMapBuffer(statusBuffer, CL_TRUE, CL_MAP_READ, 0, statusVector.size()*sizeof(cl_uint)); 
        if (statusBufferPtr != static_cast<void *>(statusVector.data()))
            throw std::runtime_error("OpenCL mapped pointer differs from data pointer"); 

        // Create new active tile set
        tid = 0;
        std::set<std::pair<uint32_t, uint32_t> > newActiveTiles;
        for (auto &tile : activeTiles) {

            auto status = statusVector[tid++];
            auto ti = tile.first;
            auto tj = tile.second;

            if (status & 0x10) {
                newActiveTiles.insert( { ti, tj } );
            }
            if (status & 0x01 && tj < dim_h.ty-1) {
                newActiveTiles.insert( { ti, tj+1 } );
                if (status & 0x02 && ti < dim_h.tx-1) {
                    newActiveTiles.insert( { ti+1, tj+1 } );
                }
                if (status & 0x08 && ti > 0) {
                    newActiveTiles.insert( { ti-1, tj+1 } );
                }
            }
            if (status & 0x04 && tj > 0) {
                newActiveTiles.insert( { ti, tj-1 } );
                if (status & 0x02 && ti < dim_h.tx-1) {
                    newActiveTiles.insert( { ti+1, tj-1 } );
                }
                if (status & 0x08 && ti > 0) {
                    newActiveTiles.insert( { ti-1, tj-1 } );
                }
            }
            if (status & 0x02 && ti < dim_h.tx-1) {
                newActiveTiles.insert( { ti+1, tj } );
            }
            if (status & 0x08 && ti > 0) {
                newActiveTiles.insert( { ti-1, tj } );
            }
        }
        activeTiles = newActiveTiles;
        
        // Check for no water
        if (activeTiles.size() == 0) {
            throw std::runtime_error("Water level is zero everywhere");
        }    

        // Streaming step
        streamingKernel.setArg(0, c);
        streamingKernel.setArg(1, tau);
        streamingKernel.setArg(2, (REAL)(9.81*cd*cd*dt));
        for (auto &tile : activeTiles) {
            runTileKernel<TYPE>(tile.first, tile.second, streamingKernel, streamingRefs, streamingKernelGenerator.reqs, 3);
        }
        
        return true;
    }
    
    template <typename TYPE>
    Raster<TYPE, TYPE> ShallowWater<TYPE>::getU() {
        // auto dim_h = h.getRasterDimensions();
        auto dim_h = layersReal[ShallowWaterLayers::h].getRasterDimensions();
        
        makeRaster(u, TYPE, TYPE);
        u.init(dim_h.d);
        // u.setProjectionParameters(h.getProjectionParameters());
        u.setProjectionParameters(layersReal[ShallowWaterLayers::h].getProjectionParameters());
        
        // runScript<TYPE>("u = uh / h;", {u, uh, h});
        runScript<TYPE>("u = uh / h;", {u, layersReal[ShallowWaterLayers::uh],
            layersReal[ShallowWaterLayers::h]});
        return u;
    }

    template <typename TYPE>
    Raster<TYPE, TYPE> ShallowWater<TYPE>::getV() {
        // auto dim_h = h.getRasterDimensions();
        auto dim_h = layersReal[ShallowWaterLayers::h].getRasterDimensions();
        
        makeRaster(v, TYPE, TYPE);
        v.init(dim_h.d);
        // v.setProjectionParameters(h.getProjectionParameters());
        v.setProjectionParameters(layersReal[ShallowWaterLayers::h].getProjectionParameters());
        
        // runScript<TYPE>("v = uh / h;", {v, vh, h});
        runScript<TYPE>("v = uh / h;", {v, layersReal[ShallowWaterLayers::vh],
            layersReal[ShallowWaterLayers::h]});
        return v;
    }

    // Float type definitions
    template class ShallowWater<float>;

    // Double type definitions
    template class ShallowWater<double>;
}