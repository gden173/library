/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#include <cmath>
#include <iostream>
#include <sstream>
#include <regex>

#include "json11/json11.hpp"

#include "gs_property.h"
#include "gs_ode.h"
#include "gs_rk_coeff.h"

namespace Geostack
{
    using namespace json11;

    template <typename TYPE>
    void ODE<TYPE>::updateSeries() {
        if (!seriesMap.empty()) {

            // Get variable data
            const auto &varIndexes = variables->getIndexes();
            const auto &varData = variables->getData();

            // Write to series
            for (auto &vars : seriesMap) {
                auto idxFirst = varIndexes.find(vars.first.first)->second;
                if (vars.first.second == "time") {

                    // Time dependent variables
                    for (std::size_t i = 0; i < variableSize; i++) {
                        vars.second[i].addValue(time, varData[idxFirst+i], true);
                    }

                } else {

                    // Non-time dependent variables
                    auto idxSecond = varIndexes.find(vars.first.second)->second;
                    for (std::size_t i = 0; i < variableSize; i++) {
                        vars.second[i].addValue(varData[idxSecond+i], varData[idxFirst+i], true);
                    }
                }
            }
        }
    }

    template <typename TYPE>
    bool ODE<TYPE>::init(
        std::string jsonConfig,
        std::shared_ptr<Variables<TYPE, std::string> > variables_) {

        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &context = solver.getContext();
        auto &queue = solver.getQueue();

        // Reset variables
        iters = 0;
        time = 0.0;

        // Copy start conditions and layers
        variables = variables_;

        // Parse configuration
        std::string jsonErr;
        auto config = Json::parse(jsonConfig, jsonErr);
        if (!jsonErr.empty()) {
            throw std::runtime_error(jsonErr);
        }

        // Get and check parameters
        dt = 0.0;
        if (config["dt"].is_number()) {
            dt = (TYPE)config["dt"].number_value();
        }
        if (dt <= 0.0) {
            throw std::runtime_error("Time step must be greater than zero.");
        }

        std::string scheme = "RK23";
        if (config["scheme"].is_string()) {
            scheme = (std::string)config["scheme"].string_value();
        }
        if (scheme != "RK23" && scheme != "RK45") {
            std::string err = std::string("Unrecognised scheme '") + scheme + "'";
            throw std::runtime_error(err);
        }

        // Define Butcher tableau for Adaptive Runge-Kutta scheme
        auto butcherTableau = getButcherTableau(scheme);

        uint32_t adaptiveMaximumSteps = 10;
        if (config["adaptiveMaximumSteps"].is_number()) {
            adaptiveMaximumSteps = (uint32_t)config["adaptiveMaximumSteps"].number_value();
        }
        if (adaptiveMaximumSteps <= 0 || adaptiveMaximumSteps > 16) {
            throw std::runtime_error("Maximum adaptive steps must be between 1 and 2^16.");
        }

        double adaptiveTolerance = 1.0E-6;
        if (config["adaptiveTolerance"].is_number()) {
            adaptiveTolerance = (double)config["adaptiveTolerance"].number_value();
        }
        if (adaptiveTolerance < 0.0) {
            throw std::runtime_error("Adaptive tolerance must be greater than zero.");
        }

        std::string initialisationScript;
        if (config["initialisationScript"].is_string()) {
            initialisationScript = config["initialisationScript"].string_value();
            initialisationScript += "\n";
        }

        std::string postUpdateScript;
        if (config["postUpdateScript"].is_string()) {
            postUpdateScript = config["postUpdateScript"].string_value();
            postUpdateScript += "\n";
        }

        // Populate variable list
        scriptVariables.clear();
        functionVariables.clear();
        auto userScripts = initialisationScript + postUpdateScript;
        const auto &variableIndexes = variables->getIndexes();
        for (auto var: variableIndexes) {
            functionVariables.insert(var.first);

            // Search for script variables
            if (std::regex_search(userScripts, std::regex("\\b" + var.first + "\\b"))) {
                scriptVariables.insert(var.first);
            }
        }

        // Check function scripts
        std::unordered_map<std::string, std::string> functionScripts;
        if (config["functionScripts"].is_array()) {
            for (auto &item : config["functionScripts"].array_items()) {
                if (item.is_object()) {
                    for (auto &obj : item.object_items()) {

                        // Get variable
                        std::string var = obj.first;

                        // Check variable list
                        if (functionVariables.find(var) == functionVariables.end()) {
                            std::string err = std::string("Script variable '") + var + "' not found";
                            throw std::runtime_error(err);
                        }

                        // Add to function list
                        functionScripts[var] = obj.second.string_value();
                    }
                } else {
                    throw std::runtime_error("Expecting object containing variable name and script");
                }
            }
        } else {
            for (auto name : { "functionScript", "functionScripts" } ) {
                if (config[name].is_object()) {
                    for (auto &obj : config[name].object_items()) {

                        // Get variable
                        std::string var = obj.first;

                        // Check variable list
                        if (functionVariables.find(var) == functionVariables.end()) {
                            std::string err = std::string("Script variable '") + var + "' not found";
                            throw std::runtime_error(err);
                        }

                        // Add to function list
                        functionScripts[var] = obj.second.string_value();
                    }
                }
            }
        }
        if (functionScripts.size() == 0) {
            throw std::runtime_error("No function scripts supplied.");
        }

        // Clean any unused function variable references
        independentVariables.clear();
        for (auto it = functionVariables.begin(); it != functionVariables.end(); ) {
            if (functionScripts.find(*it) == functionScripts.end()) {

                // Add to independent variable list
                independentVariables.insert(*it);

                // Remove from function variable list
                it = functionVariables.erase(it);

            } else {
                it++;
            }
        }

        // Clean any unused independent variable references
        for (auto it = independentVariables.begin(); it != independentVariables.end(); ) {

            // Search script for variables
            bool independentVariableUsed = false;
            for (auto &script : functionScripts) {
                if (std::regex_search(script.second, std::regex("\\b" + *it + "\\b"))) {
                    independentVariableUsed = true;
                    break;
                }
            }

            // Remove from list if not used
            if (independentVariableUsed) {
                it++;
            } else {
                it = independentVariables.erase(it);
            }
        }

        // Check variable sizes
        variableSize = 0;
        for (auto &var : functionVariables) {

            // Check size
            if (variableSize == 0) {
                variableSize = variables->getSize(var);
            } else if (variableSize != variables->getSize(var)) {
                throw std::runtime_error("Variable arrays must all be the same size.");
            }
        }
        if (variableSize == 0) {
            throw std::runtime_error("No variables found.");
        }
        for (auto &var : independentVariables) {
            auto varSize = variables->getSize(var);
            if (varSize != 1 && varSize != variableSize) {
                throw std::runtime_error("Variable '" + var + "' must be scalar, or the size of the function variables.");
            }
        }

        // Create series
        seriesMap.clear();
        if (config["outputSeries"].is_object()) {
            for (auto &obj : config["outputSeries"].object_items()) {

                // Get names
                std::string var1 = obj.first;
                if (!variables->hasVariable(var1)) {
                    throw std::runtime_error("Variable name '" + var1 + "' not found.");
                }
                if (variables->getSize(var1) != variableSize) {
                    throw std::runtime_error("Variable '" + var1 + "' must be size of the function variables for series.");
                }

                std::string var2 = obj.second.string_value();
                if (var2 != "time") {
                    if (!variables->hasVariable(var2)) {
                        throw std::runtime_error("Variable name '" + var2 + "' not found.");
                    }
                    if (variables->getSize(var2) != variableSize) {
                        throw std::runtime_error("Variable '" + var2 + "' must be size of the function variables for series.");
                    }
                }

                // Create series
                seriesMap[std::make_pair(var1, var2)] = std::vector<Series<double, TYPE> >(variableSize);
            }
        }

        // Generate scripts
        std::string updateVariableList, updatePostList;
        std::string varScript;
        for (auto &script : functionScripts) {

            // Get variable
            std::string var = script.first;

            // Get index
            std::string indexStr = std::to_string(variableIndexes.at(var));

            // Read and write variables
            varScript += var + " = " + var + "_initial;\n";
            updatePostList += "*(_vars+index+" + indexStr + ") = " + var + ";\n";

            // Create auxiliary variables
            updateVariableList += "REAL " + var + ", " + var + "_update;\n";
            updateVariableList += "REAL " + var + "_initial = *(_vars+index+" + indexStr + ");\n";
        }
        initialisationScript = varScript + initialisationScript;

        // Add independent variables to init list and variable list
        for (auto &var : independentVariables) {

            // Get index
            std::string indexStr = std::to_string(variableIndexes.at(var));

            // Add to argument list
            if (variables->getSize(var) != variableSize) {
                updateVariableList += "REAL " + var + " = *(_vars+" + indexStr + ");\n";
            } else {
                updateVariableList += "REAL " + var + " = *(_vars+index+" + indexStr + ");\n";
            }
        }

        // Add non-functional script variables
        variableArrays.clear();
        std::string initArgsList;
        for (auto &var : scriptVariables) {
            if (functionScripts.find(var) == functionScripts.end()) {

                std::string indexStr = std::to_string(variableIndexes.at(var));
                if (independentVariables.find(var) == independentVariables.end()) {

                    auto vFind = std::regex("\\b" + var + "\\s*\\[\\s*(.+?)\\s*\\]");
                    if (std::regex_search(initialisationScript, vFind)) {

                        // Use square brackets for variable array offset
                        initArgsList += ",\nconst uint " + var + "_length";
                        initArgsList += ",\nconst uint " + var + "_offset";
                        initialisationScript = std::regex_replace(initialisationScript, vFind,
                            std::string("(*(_vars+") + var + "_offset+min_uint_DEF(($1), " + var + "_length-1)))");
                        variableArrays.insert(var);

                    } else {

                        // Add variable to script
                        if (variables->getSize(var) != variableSize) {
                            updateVariableList += "const REAL " + var + " = *(_vars+" + indexStr + ");\n";
                        } else {
                            updateVariableList += "REAL " + var + " = *(_vars+index+" + indexStr + ");\n";
                            updatePostList += "*(_vars+index+" + indexStr + ") = " + var + ";\n";
                        }
                    }

                } else {
                    updatePostList += "*(_vars+index+" + indexStr + ") = " + var + ";\n";
                }
            }
        }

        updateVariableList += "\n";
        for (auto &script : functionScripts) {

            // Get variable
            std::string var = script.first;

            if (scheme == "RK23") {

                // Create Bogacki-Shampine RK rate variables
                updateVariableList += "REAL a_" + var + ", ";
                updateVariableList += "b_" + var + ", ";
                updateVariableList += "c_" + var + ", ";
                updateVariableList += "d_" + var + ";\n";

            } else if (scheme == "RK45") {

                // Create Dormand-Price RK rate variables
                updateVariableList += "REAL a_" + var + ", ";
                updateVariableList += "b_" + var + ", ";
                updateVariableList += "c_" + var + ", ";
                updateVariableList += "d_" + var + ", ";
                updateVariableList += "e_" + var + ", ";
                updateVariableList += "f_" + var + ", ";
                updateVariableList += "g_" + var + ";\n";
            }
        }

        // Patch script
        std::string functionBlock;
        std::string RKCoeffA, RKCoeffB, RKCoeffC, RKCoeffD, RKCoeffE, RKCoeffF, RKCoeffG;
        for (auto &script : functionScripts) {

            // Get variable
            std::string var = script.first;

            // Create RK code
            if (scheme == "RK23") {

                // Create Bogacki-Shampine RK code
                initialisationScript += "d_" + var + " = fn_" + var + "(t";
                RKCoeffA += "a_" + var + " = d_" + var + ";\n";
                RKCoeffB += "b_" + var + " = fn_" + var + "(t+_C2*dt";
                RKCoeffC += "c_" + var + " = fn_" + var + "(t+_C3*dt";
                RKCoeffD += "d_" + var + " = fn_" + var + "(t+_C4*dt";

            } else if (scheme == "RK45") {

                // Create Dormand-Price RK code
                initialisationScript += "g_" + var + " = fn_" + var + "(t";
                RKCoeffA += "a_" + var + " = g_" + var + ";\n";
                RKCoeffB += "b_" + var + " = fn_" + var + "(t+_C2*dt";
                RKCoeffC += "c_" + var + " = fn_" + var + "(t+_C3*dt";
                RKCoeffD += "d_" + var + " = fn_" + var + "(t+_C4*dt";
                RKCoeffE += "e_" + var + " = fn_" + var + "(t+_C5*dt";
                RKCoeffF += "f_" + var + " = fn_" + var + "(t+_C6*dt";
                RKCoeffG += "g_" + var + " = fn_" + var + "(t+_C7*dt";
            }

            // Create function code
            functionBlock += "inline REAL fn_" + var + "(const REAL t";
            for (auto &var : functionVariables) {
                functionBlock += ", const REAL " + var;
                initialisationScript += ", " + var;

                if (scheme == "RK23") {

                    // Create Bogacki-Shampine function code
                    RKCoeffB += ", " + var + "_update";
                    RKCoeffC += ", " + var + "_update";
                    RKCoeffD += ", " + var + "_update";

                } else if (scheme == "RK45") {

                    // Create Dormand-Price function code
                    RKCoeffB += ", " + var + "_update";
                    RKCoeffC += ", " + var + "_update";
                    RKCoeffD += ", " + var + "_update";
                    RKCoeffE += ", " + var + "_update";
                    RKCoeffF += ", " + var + "_update";
                    RKCoeffG += ", " + var + "_update";
                }
            }

            // Add independent variables to the function blocks
            for (auto &var : independentVariables) {

                // Add to argument list
                initialisationScript += ", " + var;
                functionBlock += ", const REAL " + var;
                if (scheme == "RK23") {
                    RKCoeffB += ", " + var;
                    RKCoeffC += ", " + var;
                    RKCoeffD += ", " + var;
                } else if (scheme == "RK45") {
                    RKCoeffB += ", " + var;
                    RKCoeffC += ", " + var;
                    RKCoeffD += ", " + var;
                    RKCoeffE += ", " + var;
                    RKCoeffF += ", " + var;
                    RKCoeffG += ", " + var;
                }
            }

            functionBlock += ") {\n" + script.second + "\n}\n";
            initialisationScript += ");\n";

            if (scheme == "RK23") {

                RKCoeffB += ");\n";
                RKCoeffC += ");\n";
                RKCoeffD += ");\n";

            } else if (scheme == "RK45") {

                RKCoeffB += ");\n";
                RKCoeffC += ");\n";
                RKCoeffD += ");\n";
                RKCoeffE += ");\n";
                RKCoeffF += ");\n";
                RKCoeffG += ");\n";

            }
        }

        std::string updateBlock;
        std::string RKUpdateA, RKUpdateB, RKUpdateC, RKUpdateD, RKUpdateE, RKUpdateF, RKUpdateG;
        for (auto &script : functionScripts) {

            // Get variable
            std::string var = script.first;

            if (scheme == "RK23") {

                // Create Bogacki-Shampine update scripts
                RKUpdateA += var + "_update = " + var + "+dt*(_A21*a_" + var + ");\n";
                RKUpdateB += var + "_update = " + var + "+dt*(_A31*a_" + var + "+_A32*b_" + var + ");\n";
                RKUpdateC += var + "_update = " + var + "+dt*(_A41*a_" + var + "+_A42*b_" + var + "+_A43*c_" + var + ");\n";
                RKUpdateD += "error = fmax(error, fabs(" + var + "_update-(" + var + "+dt*(_B1*a_" + var + "+_B2*b_" + var + "+_B3*c_" + var + "+_B4*d_" + var + "))));\n";

            } else if (scheme == "RK45") {

                // Create Dormand-Price update scripts
                RKUpdateA += var + "_update = " + var + "+dt*(_A21*a_" + var + ");\n";
                RKUpdateB += var + "_update = " + var + "+dt*(_A31*a_" + var + "+_A32*b_" + var + ");\n";
                RKUpdateC += var + "_update = " + var + "+dt*(_A41*a_" + var + "+_A42*b_" + var + "+_A43*c_" + var + ");\n";
                RKUpdateD += var + "_update = " + var + "+dt*(_A51*a_" + var + "+_A52*b_" + var + "+_A53*c_" + var + "+_A54*d_" + var + ");\n";
                RKUpdateE += var + "_update = " + var + "+dt*(_A61*a_" + var + "+_A62*b_" + var + "+_A63*c_" + var + "+_A64*d_" + var + "+_A65*e_" + var + ");\n";
                RKUpdateF += var + "_update = " + var + "+dt*(_A71*a_" + var + "+_A72*b_" + var + "+_A73*c_" + var + "+_A74*d_" + var + "+_A75*e_" + var + "+_A76*f_" + var + ");\n";
                RKUpdateG += "error = fmax(error, fabs(" + var + "_update-(" + var + "+dt*(_B1*a_" + var + "+_B2*b_" + var + "+_B3*c_" + var + "+_B4*d_" + var + "+_B5*e_" + var + "+_B6*f_" + var + "+_B7*g_" + var + "))));\n";
            }

            updateBlock += var + " = " + var + "_update;\n";
        }

        // Create function kernel
        std::string solveKernelStr = std::string(R_cl_ode_c);
        solveKernelStr = std::regex_replace(solveKernelStr, std::regex("__MAX_STEPS__"), std::to_string(adaptiveMaximumSteps));
        solveKernelStr = std::regex_replace(solveKernelStr, std::regex("__TOLERANCE__"), std::to_string(adaptiveTolerance));
        solveKernelStr = std::regex_replace(solveKernelStr, std::regex("\\/\\*__ARGS__\\*\\/"), initArgsList);
        solveKernelStr = std::regex_replace(solveKernelStr, std::regex("\\/\\*__RK_COEFF__\\*\\/"), butcherTableau);
        solveKernelStr = std::regex_replace(solveKernelStr, std::regex("\\/\\*__CODE__\\*\\/"), functionBlock);
        solveKernelStr = std::regex_replace(solveKernelStr, std::regex("\\/\\*__VARS__\\*\\/"), updateVariableList);
        solveKernelStr = std::regex_replace(solveKernelStr, std::regex("\\/\\*__INIT__\\*\\/"), initialisationScript);
        solveKernelStr = std::regex_replace(solveKernelStr, std::regex("\\/\\*__POST1__\\*\\/"), postUpdateScript);
        solveKernelStr = std::regex_replace(solveKernelStr, std::regex("\\/\\*__POST2__\\*\\/"), updatePostList);
        if (scheme == "RK23") {
            solveKernelStr = std::regex_replace(solveKernelStr, std::regex("\\/\\*__RK__\\*\\/"),
                RKCoeffA + "\n" + RKUpdateA + "\n" +
                RKCoeffB + "\n" + RKUpdateB + "\n" +
                RKCoeffC + "\n" + RKUpdateC + "\n" +
                RKCoeffD + "\n" + RKUpdateD + "\n");
        } else if (scheme == "RK45") {
            solveKernelStr = std::regex_replace(solveKernelStr, std::regex("\\/\\*__RK__\\*\\/"),
                RKCoeffA + "\n" + RKUpdateA + "\n" +
                RKCoeffB + "\n" + RKUpdateB + "\n" +
                RKCoeffC + "\n" + RKUpdateC + "\n" +
                RKCoeffD + "\n" + RKUpdateD + "\n" +
                RKCoeffE + "\n" + RKUpdateE + "\n" +
                RKCoeffF + "\n" + RKUpdateF + "\n" +
                RKCoeffG + "\n" + RKUpdateG + "\n");
        }

        solveKernelStr = std::regex_replace(solveKernelStr, std::regex("\\/\\*__UPDATE__\\*\\/"), updateBlock);

        auto functionHash = solver.buildProgram(solveKernelStr);
        if (functionHash == solver.getNullHash()) {
            throw std::runtime_error("Cannot build program");
        }
        solveKernel = solver.getKernel(functionHash, "solve");
        solveWorkitemMultiple = solver.getKernelPreferredWorkgroupMultiple(solveKernel);

        // Initialise series
        updateSeries();

        // update initialised
        initialised = true;

        return initialised;
    }

    template <typename TYPE>
    bool ODE<TYPE>::step() {

        if (!initialised) {
            throw std::runtime_error("ODE solver is not initialised");
        }

        // Get OpenCL solver handles
        Geostack::Solver &solver = Geostack::Solver::getSolver();
        auto &context = solver.getContext();
        auto &queue = solver.getQueue();

        // Populate solver kernel arguments
        cl_uint arg = 0;
        solveKernel.setArg(arg++, variables->getBuffer());
        solveKernel.setArg(arg++, time);
        solveKernel.setArg(arg++, dt);
        solveKernel.setArg(arg++, (cl_uint)functionVariables.size());
        solveKernel.setArg(arg++, (cl_uint)variableSize);

        // Add variables
        if (variableArrays.size() > 0) {
            const auto &varIndexes = variables->getIndexes();
            for (auto &vName : variableArrays) {
                solveKernel.setArg(arg++, (cl_uint)variables->getSize(vName));
                solveKernel.setArg(arg++, (cl_uint)varIndexes.at(vName));
            }
        }

        // Execute update kernel
        size_t solvePaddedWorkgroupSize =
            (size_t)(variableSize+((solveWorkitemMultiple-(variableSize%solveWorkitemMultiple))%solveWorkitemMultiple));
        queue.enqueueNDRangeKernel(solveKernel, cl::NullRange,
            cl::NDRange(solvePaddedWorkgroupSize), cl::NDRange(solveWorkitemMultiple));

        // Update time
        time += dt;

        // Update series
        updateSeries();

        return true;
    }

    template <typename TYPE>
    TYPE ODE<TYPE>::get(std::string varName2, std::string varName1, TYPE var1, uint32_t index) {

        if (!initialised) {
            throw std::runtime_error("ODE solver is not initialised");
        }

        auto it = seriesMap.find(std::make_pair(varName2, varName1));
        if (it == seriesMap.end()) {
            throw std::runtime_error("Cannot find series of " + varName2 + "(" + varName1 + ").");
        }
        auto &seriesVector = it->second;
        if (index < seriesVector.size()) {
            return seriesVector[index](var1);
        } else {
            throw std::runtime_error("Index " + std::to_string(index) + " out of range for solver.");
        }
    }

    template <typename TYPE>
    const Series<double, TYPE> &ODE<TYPE>::getSeries(std::string varName2, std::string varName1, uint32_t index) {

        if (!initialised) {
            throw std::runtime_error("ODE solver is not initialised");
        }

        auto it = seriesMap.find(std::make_pair(varName2, varName1));
        if (it == seriesMap.end()) {
            throw std::runtime_error("Cannot find series of " + varName2 + "(" + varName1 + ").");
        }
        auto &seriesVector = it->second;
        if (index < seriesVector.size()) {
            return seriesVector[index];
        } else {
            throw std::runtime_error("Index " + std::to_string(index) + " out of range for solver.");
        }
    }

    template <typename TYPE>
    void ODE<TYPE>::setTimeStep(TYPE dt_) {
        if (!initialised) {
            throw std::runtime_error("ODE solver is not initialised");
        }

        // Check time step
        if (dt_ <= 0.0) {
            throw std::runtime_error("Time step must be greater than zero.");
        }

        // Update time step
        dt = dt_;
    }

    template <typename TYPE>
    void ODE<TYPE>::setTime(TYPE time_) {
        if (!initialised) {
            throw std::runtime_error("ODE solver is not initialised");
        }
        // Update time
        time = time_;
    }

    // Float type definitions
    template class ODE<float>;

    // Double type definitions
    template class ODE<double>;
}
