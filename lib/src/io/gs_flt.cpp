/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>
#include <iomanip>

#include "json11.hpp"

#include "gs_solver.h"
#include "gs_string.h"
#include "gs_flt.h"

using namespace json11;

namespace Geostack
{
    /**
    * Read ESRI flt to raster.
    * @param fileName input file name.
    * @return true if file is read, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    void FltHandler<RTYPE, CTYPE>::read(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {
        
        // Split filename 
        auto fileNameSplit = Geostack::Strings::splitPath(fileName);
      
        // Open files
        std::string fileHeaderName = fileNameSplit[0] + fileNameSplit[1] + ".hdr";
        std::ifstream fileHeaderStream(fileHeaderName);
        if (!fileHeaderStream) {
            std::stringstream err;
            err << "Cannot open header file '" << fileHeaderName << "' for reading";
            throw std::runtime_error(err.str());
        }
        std::ifstream fileStream(fileName, std::ios::binary);
        if (!fileStream) {
            std::stringstream err;
            err << "Cannot open data file '" << fileName << "' for reading";
            throw std::runtime_error(err.str());
        }

        // Raster data
        uint32_t nx = 0;         // Number of cells in the x direction
        uint32_t ny = 0;         // Number of cells in the y direction     
        CTYPE h = 0.0;               // The spacing between each point in meters
        CTYPE ox = 0.0;              // The offset in the x direction in world co-ordinates        
        CTYPE oy = 0.0;              // The offset in the y direction in world co-ordinates
        std::string nodataValue; // The value representing unknown data   
        std::string endianness;  // String representing data endianness 

        // Read header
        std::string line;
        int nTokensFound = 0;
        try {
            while (std::getline(fileHeaderStream, line)) {

                // Split into pair
                auto splitLine = Strings::split(line, ' ');
                if (splitLine.size() == 2) {

                    // Check token
                    std::string token = splitLine[0];
                    std::string value = splitLine[1];
                    std::transform(token.begin(), token.end(), token.begin(), ::tolower);

                    if (token == "ncols") {
                        nx = std::stoi(value);
                        nTokensFound++;
                    } else if (token == "nrows") {
                        ny = std::stoi(value);
                        nTokensFound++;
                    } else if (token == "cellsize") {
                        h = (CTYPE)std::stod(value);
                        nTokensFound++;
                    } else if (token == "xllcorner") {
                        ox = (CTYPE)std::stod(value);
                        nTokensFound++;
                    } else if (token == "yllcorner") {
                        oy = (CTYPE)std::stod(value);
                        nTokensFound++;
                    } else if (token == "nodata_value" || token == "nodata") {
                        nodataValue = value;
                        nTokensFound++;
                    } else if (token == "byteorder") {
                        std::transform(value.begin(), value.end(), value.begin(), ::tolower);
                        endianness = value;
                        nTokensFound++;
                    }

                } else {
                    throw std::runtime_error("Flt incorrect header format");
                }

                if (nTokensFound == 7)
                    break;
            };
        } catch(std::invalid_argument &e) {

            // Catch conversion errors
            std::stringstream err;
            err << "Cannot convert flt header value to number: " << e.what();
            throw std::runtime_error(err.str());
        }

        // Check token count
        if (nTokensFound != 7) {
            throw std::runtime_error("Flt missing header information");
        }
        
        // Check values
        if (nx <= 0) {
            throw std::runtime_error("Flt ncols must be greater than zero");
        }
        if (ny <= 0) {
            throw std::runtime_error("Flt nrows must be greater than zero");
        }
        if (h <= (CTYPE)0.0) {
            throw std::runtime_error("Flt spacing must be greater than zero");
        }
        if (endianness != "msbfirst" && endianness != "lsbfirst") {
            std::stringstream err;
            err << "Flt endianness is not recognised '" << endianness << "'";
            throw std::runtime_error(err.str());
        }

        // TODO handle endianness
        #if defined(ENDIAN_BIG)
        if (endianness != "msbfirst") {
            throw std::runtime_error("Only msbfirst endianness is supported");
        }
        #elif defined(ENDIAN_LITTLE)
        if (endianness != "lsbfirst") {
            throw std::runtime_error("Only lsbfirst endianness is supported");
        }
        #else
        #error "Endianness must be defined as ENDIAN_BIG or ENDIAN_LITTLE"
        #endif
           
        // Create raster
        r.init2D(nx, ny, h, h, ox, oy);

        // Create line buffer
        std::vector<float> dataLine;
        dataLine.resize(nx);

        // Write data
        float nullValueFlt = std::stof(nodataValue);
        for (uint32_t j = 0; j < ny; j++) {

            // Read line
            fileStream.read(reinterpret_cast<char *>(dataLine.data()), dataLine.size()*sizeof(float));

            if (fileStream) {
                for (uint32_t i = 0; i < nx; i++) {

                    // Set value
                    r.setCellValue(dataLine[i] == nullValueFlt ? Geostack::getNullValue<RTYPE>() : (RTYPE)dataLine[i], i, (ny-1)-j);
                }

            } else {

                // Error in reading file
                throw std::runtime_error("Unexpected end of flt file");
            }
        }
    }

    /**
    * Write raster to ESRI flt format.
    * @param fileName output file name.
    * @return true if file is written, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    void FltHandler<RTYPE, CTYPE>::write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {

        // Check for data
        if (!r.hasData()) {
            throw std::runtime_error("Raster has no data to write to float file");
        }

        // Set config defaults
        uint32_t k = 0;
        std::string noDataString = "-9999.0";

        // Parse config
        if (!jsonConfig.empty()) {
        
            // Parse Json
            std::string err;
            auto config = Json::parse(jsonConfig, err);
            if (!err.empty()) {
                throw std::runtime_error(err);
            }
              
            // Set no data type
            if (config["noDataValue"].is_number()) {
                noDataString = std::to_string(config["noDataValue"].number_value());
            } else if (config["noDataValue"].is_string()) {
                noDataString = config["noDataValue"].string_value();
            }
              
            // Set slice index
            if (config["layer"].is_number()) {
                k = (uint32_t)config["layer"].number_value();
            }
        }

        // Set no data value
        RTYPE noDataValue = (RTYPE)std::stof(noDataString);

        // Get raster dimensions
        auto dim = r.getRasterDimensions();

        // Get projection
        auto proj = r.getProjectionParameters();

        // Check grid spacing is equal
        if (dim.d.hx != dim.d.hy) {
            throw std::runtime_error("Raster must have equal x and y spacing for flt conversion");
        }

        // Split filename 
        auto fileNameSplit = Geostack::Strings::splitPath(fileName);
      
        // Open files
        std::string fileHeaderName = fileNameSplit[0] + fileNameSplit[1] + ".hdr";
        std::ofstream fileHeaderStream(fileHeaderName);
        if (!fileHeaderStream) {
            std::stringstream err;
            err << "Cannot open header file '" << fileHeaderName << "' for writing";
            throw std::runtime_error(err.str());
        }

        std::ofstream fileStream(fileName, std::ios::binary);
        if (!fileStream) {
            std::stringstream err;
            err << "Cannot open '" << fileName << "' for writing";
            throw std::runtime_error(err.str());
        }
        
        // Write projection
        if (proj.type != 0) {
            std::string fileProjectionName = fileNameSplit[0] + fileNameSplit[1] + ".prj";
            std::ofstream fileProjectionStream(fileProjectionName);
            if (!fileProjectionStream) {
                std::stringstream err;
                err << "Cannot open projection file '" << fileProjectionName << "' for writing";
                throw std::runtime_error(err.str());
            }
            fileProjectionStream << Projection::toWKT(proj);
        }
        
        // Write header
        fileHeaderStream << std::setprecision(8);
        fileHeaderStream << "ncols " << dim.d.nx << '\n';
        fileHeaderStream << "nrows " << dim.d.ny << '\n';
        fileHeaderStream << "cellsize " << dim.d.hx << '\n';
        fileHeaderStream << "nodata_value " << noDataString << '\n';
        fileHeaderStream << "xllcorner " << dim.d.ox << '\n';
        fileHeaderStream << "yllcorner " << dim.d.oy << '\n';
        #if defined(ENDIAN_BIG)
        fileHeaderStream << "byteorder " << "MSBFIRST" << '\n';
        #elif defined(ENDIAN_LITTLE)
        fileHeaderStream << "byteorder " << "LSBFIRST" << '\n';
        #else
        #error "Endianness must be defined as ENDIAN_BIG or ENDIAN_LITTLE"
        #endif
        
        // Create data buffer
        std::vector<std::vector<RTYPE> > dataCache;
        dataCache.resize(dim.tx);

        // Write data
        uint64_t koff = (uint64_t)k*TileMetrics::tileSizeSquared;
        uint8_t verbose = Geostack::Solver::getSolver().getVerboseLevel();
        for (int32_t tj = 0; tj < dim.ty; tj++) {

            // Populate tiles
            for (uint32_t ti = 0; ti < dim.tx; ti++) {

                // Get data
                r.getTileData(ti, dim.ty-1-tj, dataCache[ti]);

                // Replace nodata values
                std::replace_if(dataCache[ti].begin(), dataCache[ti].end(), Geostack::isInvalid<RTYPE>, noDataValue);
            }

            uint32_t rowEnd = dim.d.nx&TileMetrics::tileSizeMask;
            if (sizeof(RTYPE) == sizeof(float)) {
                
                // Write directly
                uint32_t nj = tj == 0 ? TileMetrics::tileSize*(1-dim.ty)+dim.d.ny : TileMetrics::tileSize;
                for (uint32_t j = 0; j < nj; j++) {

                    // Calculate offset
                    uint64_t joff = ((uint64_t)(nj-j-1)<<TileMetrics::tileSizePower)+koff;

                    // Write lines
                    for (uint32_t ti = 0; ti < dim.tx-1; ti++) {
                        fileStream.write(reinterpret_cast<const char *>(dataCache[ti].data()+joff), 
                            TileMetrics::tileSize*sizeof(RTYPE));
                    }
                    fileStream.write(reinterpret_cast<const char *>(dataCache[dim.tx-1].data()+joff), 
                        rowEnd*sizeof(RTYPE));
                }
            } else {
                
                // Write with mapping to float
                uint32_t nj = tj == 0 ? TileMetrics::tileSize*(1-dim.ty)+dim.d.ny : TileMetrics::tileSize;
                std::vector<float> cache;
                for (uint32_t j = 0; j < nj; j++) {
                
                    // Calculate offset
                    uint64_t joff = ((uint64_t)(nj-j-1)<<TileMetrics::tileSizePower)+koff;
                    
                    // Write lines
                    for (uint32_t ti = 0; ti < dim.tx-1; ti++) {
                        cache = std::vector<float>(dataCache[ti].begin()+joff, dataCache[ti].begin()+joff+TileMetrics::tileSize);
                        fileStream.write(reinterpret_cast<const char *>(cache.data()), TileMetrics::tileSize*sizeof(float));
                    }
                    cache = std::vector<float>(dataCache[dim.tx-1].begin()+joff, dataCache[dim.tx-1].begin()+joff+TileMetrics::tileSize);
                    fileStream.write(reinterpret_cast<const char *>(cache.data()), 
                       (TileMetrics::tileSize*(1-dim.tx)+dim.d.nx)*sizeof(float));
                }
            }

            // Output progress
            if (verbose <= Geostack::Verbosity::Info && (tj%10) == 0) {
                std::cout << std::setprecision(2) << 100.0*(double)tj/(double)dim.ty << "%" << std::endl;
            }

            // Flush stream
            fileStream.flush();
        }
        if (verbose <= Geostack::Verbosity::Info) {
            std::cout << "100%" << std::endl;
        }
        
        if (fileStream.is_open()) {
            fileStream.close();
        }
    }    
    
    // RTYPE float, CTYPE float definitions
    template class FltHandler<float, float>;
    
    // RTYPE uchar, CTYPE float definitions
    template class FltHandler<uint8_t, float>;
    
    // RTYPE uint, CTYPE float definitions
    template class FltHandler<uint32_t, float>;
    
    // RTYPE double, CTYPE double definitions
    template class FltHandler<double, double>;

    // RTYPE uchar, CTYPE double definitions
    template class FltHandler<uint8_t, double>;
    
    // RTYPE uint, CTYPE double definitions
    template class FltHandler<uint32_t, double>;
}
