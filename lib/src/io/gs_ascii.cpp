/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <iostream>
#include <sstream>
#include <algorithm>
#include <map>
#include <iomanip>
#include <regex>

#include "json11.hpp"

#include "gs_solver.h"
#include "gs_string.h"
#include "gs_ascii.h"

using namespace json11;

namespace Geostack
{
    /**
    * Read ESRI ascii to raster.
    * @param fileName input file name.
    * @return true if file is read, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    void AsciiHandler<RTYPE, CTYPE>::read(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {
        
        // Open file
        std::ifstream fileStream(fileName);
        if (!fileStream) {
            std::stringstream err;
            err << "Cannot open '" << fileName << "' for reading";
            throw std::runtime_error(err.str());
        }

        // Raster data
        uint32_t nx = 0;             // Number of cells in the x direction
        uint32_t ny = 0;             // Number of cells in the y direction     
        CTYPE h = 0.0;               // The spacing between each point in meters
        CTYPE hx = 0.0;              // The spacing between each point in meters (in x-direction)
        CTYPE hy = 0.0;              // The spacing between each point in meters (in y-direction)
        CTYPE ox = 0.0;              // The offset in the x direction in world co-ordinates        
        CTYPE oy = 0.0;              // The offset in the y direction in world co-ordinates
        std::string nullValueString; // The value representing unknown data   

        // Read header
        std::string line;
        int nTokensFound = 0;
        try {
            while (std::getline(fileStream, line)) {

                // Split into pair
                // auto splitLine = Strings::split(line, ' ');
                const std::regex rgx("([A-z\\_]\\w*)\\s+([-+]?\\d+\\.?\\d*|\\.\\d+)");
                std::cmatch splitLine;
                auto rc = std::regex_match(line.c_str(), splitLine, rgx);
                
                // subtract 1, as response is match + captured groups
                if (splitLine.size() - 1 == 2) {

                    // Check token
                    std::string token = splitLine[1];
                    std::string value = splitLine[2];
                    std::transform(token.begin(), token.end(), token.begin(), ::tolower);

                    if (token == "ncols") {
                        nx = std::stoi(value);
                        nTokensFound++;
                    } else if (token == "nrows") {
                        ny = std::stoi(value);
                        nTokensFound++;
                    } else if (token == "cellsize") {
                        h = (CTYPE)std::stod(value);
                        nTokensFound++;
                    } else if (token == "xllcorner") {
                        ox = (CTYPE)std::stod(value);
                        nTokensFound++;
                    } else if (token == "yllcorner") {
                        oy = (CTYPE)std::stod(value);
                        nTokensFound++;
                    } else if (token == "nodata_value" || token == "nodata" || token == "NODATA_value") {
                        nullValueString = value;
                        nTokensFound++;
                    } else if (token == "dx") {
                        hx = (CTYPE)std::stod(value);
                        nTokensFound++;
                    } else if (token == "dy") {
                        hy = (CTYPE)std::stod(value);
                        nTokensFound++;
                    }
                } else {
                    throw std::runtime_error("Ascii has incorrect header format");
                }

                if (nTokensFound == 6) {
                    if (h != 0.0) {
                        break;
                    }
                } else if (nTokensFound == 7) {
                    if (hx != 0.0 && hy != 0.0) {
                        break;
                    }
                }
            };
        } catch(std::invalid_argument &e) {

            // Catch conversion errors
            std::stringstream err;
            err << "Cannot convert ascii header value to number: " << e.what();
            throw std::runtime_error(err.str());
        }

        // Check token count
        if (nTokensFound < 6) {
            throw std::runtime_error("Ascii missing header information");
        }
        
        // Check values
        if (nx <= 0) {
            throw std::runtime_error("Ascii ncols must be greater than zero");
        }
        if (ny <= 0) {
            throw std::runtime_error("Ascii nrows must be greater than zero");
        }

        if (h == (CTYPE)0.0 && hx == (CTYPE)0.0 && hy == (CTYPE)0.0) {
            throw std::runtime_error("Ascii spacing must not be zero");
        }
              
        // Create raster
        if (h != 0.0) {
            r.init2D(nx, ny, h, h, ox, oy);    
        } else if (hx != 0.0 && hy != 0.0) {
            r.init2D(nx, ny, hx, hy, ox, oy);
        }

        // Parse remaining data
        uint32_t j = 0;
        try {
            while (std::getline(fileStream, line)) {
                
                // Split line
                auto splitLine = Strings::split(line, ' ');

                // Remove whitespace from entries
                for (auto &s : splitLine) {
                    s = Strings::removeWhitespace(s);
                }

                // Remove any empty entries from end of line
                if (splitLine.back().empty()) {
                    splitLine.pop_back();
                }

                // Check size
                if (splitLine.size() != nx) {
                    throw std::runtime_error("Ascii has wrong number of entries in line");
                }

                // Set values
                for (uint32_t i = 0; i < nx; i++) {
                    RTYPE v = (splitLine[i] == nullValueString) ? 
                        Geostack::getNullValue<RTYPE>() : (RTYPE)std::stod(splitLine[i]);
                    r.setCellValue(v, i, (ny-1)-j);
                }
                
                // Increment and check vertical line counter
                if (j++ > ny) {
                    throw std::runtime_error("Unexpected data at end of ascii file");
                }
            };
        } catch(std::invalid_argument& e) {

            // Catch conversion errors
            std::stringstream err;
            err << "Cannot convert ascii value to number: " << e.what();
            throw std::runtime_error(err.str());
        }
    }

    /**
    * Write raster to ESRI ascii format.
    * @param fileName output file name.
    * @return true if file is written, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    void AsciiHandler<RTYPE, CTYPE>::write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {

        // Check for data
        if (!r.hasData()) {
            throw std::runtime_error("Raster no data to write to ascii file");
        }

        // Set config defaults
        uint32_t k = 0;
        std::string noDataString = "-9999";

        // Parse config
        if (!jsonConfig.empty()) {
        
            // Parse Json
            std::string err;
            auto config = Json::parse(jsonConfig, err);
            if (!err.empty()) {
                throw std::runtime_error(err);
            }

            // Set no data type
            if (config["noDataValue"].is_number()) {
                noDataString = std::to_string(config["noDataValue"].number_value());
            } else if (config["noDataValue"].is_string()) {
                noDataString = config["noDataValue"].string_value();
            }

            // Set no data type
            if (config["layer"].is_number()) {
                k = (uint32_t)config["layer"].number_value();
            }
        }

        // Get raster dimensions
        auto dim = r.getRasterDimensions();
        
        // // Check grid spacing is equal
        // if (dim.d.hx != dim.d.hy) {
        //     throw std::runtime_error("Raster must have equal x and y spacing for ascii conversion");
        // }      
        
        // Split filename 
        auto fileNameSplit = Geostack::Strings::splitPath(fileName);        
        
        // Get projection
        auto proj = r.getProjectionParameters();

        // Write projection
        if (proj.type != 0) {
            std::string fileProjectionName = fileNameSplit[0] + fileNameSplit[1] + ".prj";
            std::ofstream fileProjectionStream(fileProjectionName);
            if (!fileProjectionStream) {
                std::stringstream err;
                err << "Cannot open projection file '" << fileProjectionName << "' for writing";
                throw std::runtime_error(err.str());
            }
            fileProjectionStream << Projection::toWKT(proj);
        }

        // Open file
        std::ofstream fileStream(fileName);
        if (!fileStream) {
            std::stringstream err;
            err << "Cannot open '" << fileName << "' for writing";
            throw std::runtime_error(err.str());
        }
        
        // Write header  
        fileStream << std::setprecision(8);
        fileStream << "ncols " << dim.d.nx << '\n';
        fileStream << "nrows " << dim.d.ny << '\n';
        if (dim.d.hx == dim.d.hy) {
            fileStream << "cellsize " << dim.d.hx << '\n';    
        } else {
            fileStream << "dx " << dim.d.hx << '\n';
            fileStream << "dy " << dim.d.hy << '\n';
        }
        
        fileStream << "nodata_value " << noDataString << '\n';
        fileStream << "xllcorner " << dim.d.ox << '\n';
        fileStream << "yllcorner " << dim.d.oy << '\n';

        // Create data buffer
        std::vector<std::vector<RTYPE> > dataCache;
        dataCache.resize(dim.tx);

        // Write data
        uint64_t koff = (uint64_t)k*TileMetrics::tileSizeSquared;
        uint8_t verbose = Geostack::Solver::getSolver().getVerboseLevel();
        for (int32_t tj = 0; tj < dim.ty; tj++) {

            // Populate tiles
            for (uint32_t ti = 0; ti < dim.tx; ti++) {

                // Get data
                r.getTileData(ti, dim.ty-1-tj, dataCache[ti]);
            }
                            
            // Write tile data
            uint32_t rowEnd = dim.d.nx&TileMetrics::tileSizeMask;
            uint32_t nj = tj == 0 ? TileMetrics::tileSize*(1-dim.ty)+dim.d.ny : TileMetrics::tileSize;
            for (uint32_t j = 0; j < nj; j++) {
            
                // Calculate offset
                uint64_t off = ((uint64_t)(nj-j-1)<<TileMetrics::tileSizePower)+koff;

                // Write lines
                for (uint32_t ti = 0; ti < dim.tx-1; ti++) {
                
                    auto *pv = dataCache[ti].data()+off;
                    for (uint32_t i = 0; i < TileMetrics::tileSize; i++, pv++) {
                        if (Geostack::isValid(*pv))
                            fileStream << std::to_string(*pv) << " ";
                        else
                            fileStream << noDataString << " ";
                    }
                }
                auto *pv = dataCache[dim.tx-1].data()+off;
                for (uint32_t i = 0; i < rowEnd; i++, pv++) {
                    if (Geostack::isValid(*pv))
                        fileStream << std::to_string(*pv) << " ";
                    else
                        fileStream << noDataString << " ";
                }
                fileStream << '\n';
            }

            // Output progress
            if (verbose <= Geostack::Verbosity::Info && (tj%10) == 0) {
                std::cout << std::setprecision(2) << 100.0*(double)tj/(double)dim.ty << "%" << std::endl;
            }

            // Flush stream
            fileStream.flush();
        }
        if (verbose <= Geostack::Verbosity::Info) {
            std::cout << "100%" << std::endl;
        }
        
        if (fileStream.is_open()) {
            fileStream.close();
        }

    }
    
    // RTYPE float, CTYPE float definitions
    template class AsciiHandler<float, float>;
    
    // RTYPE uchar, CTYPE float definitions
    template class AsciiHandler<uint8_t, float>;
    
    // RTYPE uint, CTYPE float definitions
    template class AsciiHandler<uint32_t, float>;
    
    // RTYPE double, CTYPE double definitions
    template class AsciiHandler<double, double>;

    // RTYPE uchar, CTYPE double definitions
    template class AsciiHandler<uint8_t, double>;
    
    // RTYPE uint, CTYPE double definitions
    template class AsciiHandler<uint32_t, double>;
}
