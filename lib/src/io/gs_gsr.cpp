/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <fstream>
#include <sstream>
#include <algorithm>
#include <map>

#include "json11.hpp"
#include "miniz.h"

#include "gs_string.h"
#include "gs_gsr.h"

using namespace json11;

namespace Geostack
{
    /**
    * Geostack raster header string.
    */
    namespace GSR {
        static std::string headerString = "GSR";

        struct alignas(8) TileRecord {
            uint32_t i;
            uint32_t j;
            uint64_t dataOffset;
            uint32_t dataLength;
        };
    }

    /**
    * Read Geostack raster format to raster.
    * @param fileName input file name.
    * @return true if file is read, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    void GsrHandler<RTYPE, CTYPE>::read(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {
                      
        // Check file stream handle
        if (RasterFileHandler<RTYPE, CTYPE>::fileStream != nullptr) {
        
            // Get file stream handle
            std::fstream &fileStream = *RasterFileHandler<RTYPE, CTYPE>::fileStream;

            // Check if file is open already
            if (!fileStream.is_open()) {

                // Open file
                fileStream.open(fileName, std::ios::in | std::ios::binary);

                // Check file
                if (!fileStream) {
                    std::stringstream err;
                    err << "Cannot open '" << fileName << "' for reading";
                    throw std::runtime_error(err.str());
                }
        
                // Read header code
                std::string code;
                code.resize(GSR::headerString.size());
                fileStream.read(&code[0], code.size());
                if (fileStream.eof()) {
                    throw std::runtime_error("Unexpected end of file while reading gsr");
                }
                if (code != GSR::headerString) {
                    std::stringstream err;
                    err << "File '" << fileName << "' does not appear to be a geostack raster file";
                    throw std::runtime_error(err.str());
                }
        
                // Read endianness
                // TODO handle endianness
                std::string endianness;
                endianness.resize(1);
                fileStream.read(&endianness[0], endianness.size());     
                #if defined(ENDIAN_BIG)
                if (endianness != "B") {
                    throw std::runtime_error("Only big endianness is supported");
                }
                #elif defined(ENDIAN_LITTLE)
                if (endianness != "L") {
                    throw std::runtime_error("Only little endianness is supported");
                }
                #else
                #error "Endianness must be defined as ENDIAN_BIG or ENDIAN_LITTLE"
                #endif

                // Read header version
                uint32_t version = 0;
                fileStream.read(reinterpret_cast<char *>(&version), sizeof(version));       
                if (version != 1) {
                    std::stringstream err;
                    err << "gsr file has unsupported version '" << version << "'";
                    throw std::runtime_error(err.str());
                }

                // Read number of pages
                uint64_t pages = 1;
                fileStream.read(reinterpret_cast<char *>(&pages), sizeof(pages));  
        
                // Read raster dimensions
                uint32_t dimLen;
                fileStream.read(reinterpret_cast<char *>(&dimLen), sizeof(dimLen));      
                if (dimLen != sizeof(RasterDimensions<CTYPE>)) {
                    throw std::runtime_error("Native raster dimensions size differs from gsr file");
                }
                RasterDimensions<CTYPE> dim;
                fileStream.read(reinterpret_cast<char *>(&dim), sizeof(dim));

                // Read tile record length
                uint32_t tileRecordLen;
                fileStream.read(reinterpret_cast<char *>(&tileRecordLen), sizeof(tileRecordLen));      
                if (tileRecordLen != sizeof(GSR::TileRecord)) {
                    throw std::runtime_error("Native tile record size differs from gsr file");
                }

                // Read number of tile records
                uint32_t tileRecordCount;
                fileStream.read(reinterpret_cast<char *>(&tileRecordCount), sizeof(tileRecordCount));      
                if (tileRecordCount > dim.tx*dim.ty) {
                    throw std::runtime_error("Number of records in gsr is greater than number of tiles");
                }

                // Read records
                std::vector<GSR::TileRecord> tileRecordsIn(tileRecordCount);
                if (tileRecordCount > 0) {
                    fileStream.read(reinterpret_cast<char *>(tileRecordsIn.data()), tileRecordCount*tileRecordLen);
                    if (fileStream.eof()) {
                        throw std::runtime_error("Unexpected end of file while reading gsr records");
                    }
                }

                // Parse records into map
                tileRecordTable.clear();
                for (auto &tileRecord : tileRecordsIn)
                    tileRecordTable[ { tileRecord.i, tileRecord.j } ] = { tileRecord.dataOffset, tileRecord.dataLength };
            
                // Initialise raster
                r.init(dim.d);

                // Read data        
                if (dim.d.mx == TileMetrics::tileSize && dim.d.my == TileMetrics::tileSize) {

                    // Register data callback
                    using namespace std::placeholders;
                    RasterFileHandler<RTYPE, CTYPE>::registerReadDataHandler(
                        std::bind(&GsrHandler<RTYPE, CTYPE>::readDataFunction, this, _1, _2, _3));

                } else {

                    // Create buffers
                    std::vector<RTYPE> data;
                    data.resize((size_t)TileMetrics::tileSizeSquared*dim.d.nz);
                
                    for (uint32_t tj = 0; tj < dim.ty; tj++) {
                        for (uint32_t ti = 0; ti < dim.tx; ti++) {
                
                            // Read data
                            if (readData(ti, tj, data)) {
                    
                                // Set tile data
                                r.setTileData(ti, tj, data);
                            }
                        }
                    }
                }

            } else {

                // Register data callback
                using namespace std::placeholders;
                RasterFileHandler<RTYPE, CTYPE>::registerReadDataHandler(
                    std::bind(&GsrHandler<RTYPE, CTYPE>::readDataFunction, this, _1, _2, _3));
            }
        } else {
            // Error for invalid file stream
            throw std::runtime_error("invalid input gsr file stream");   
        }
    }

    /**
    * Write raster to Geostack raster format
    * @param fileName output file name.
    * @return true if file is written, false otherwise.
    */
    template <typename RTYPE, typename CTYPE>
    void GsrHandler<RTYPE, CTYPE>::write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig) {

        // Check for data
        if (!r.hasData()) {
            throw std::runtime_error("Raster has no data to write to gsr file");
        }

        // Check file stream handle
        if (RasterFileHandler<RTYPE, CTYPE>::fileStream != nullptr) {
        
            // Get file stream handle
            std::fstream &fileStream = *RasterFileHandler<RTYPE, CTYPE>::fileStream;

            // Check stream
            if (fileStream.is_open()) {
                fileStream.close();
            }

            // Open file
            fileStream.open(fileName, std::fstream::in | std::fstream::out | std::fstream::binary | std::fstream::trunc);

            // Check file
            if (!fileStream.is_open()) {
                std::stringstream err;
                err << "Cannot open '" << fileName << "' for writing";
                throw std::runtime_error(err.str());
            }
        
            // Write header code
            fileStream << GSR::headerString;

            // Write endianness
            #if defined(ENDIAN_BIG)
            fileStream << "B";
            #elif defined(ENDIAN_LITTLE)
            fileStream << "L";
            #else
            #error "Endianness must be defined as ENDIAN_BIG or ENDIAN_LITTLE"
            #endif

            // Write header version
            uint32_t version = 1;
            fileStream.write(reinterpret_cast<const char *>(&version), sizeof(version));

            // Write number of pages
            uint64_t pages = 1;
            fileStream.write(reinterpret_cast<const char *>(&pages), sizeof(pages));

            // Write raster dimensions
            auto dim = r.getRasterDimensions();
            dim.d.mx = TileMetrics::tileSize;
            dim.d.my = TileMetrics::tileSize;
            uint32_t dimLen = sizeof(dim);
            fileStream.write(reinterpret_cast<const char *>(&dimLen), sizeof(dimLen));
            fileStream.write(reinterpret_cast<const char *>(&dim), sizeof(dim));

            // Write tile record length
            uint32_t tileRecordLen = sizeof(GSR::TileRecord);
            fileStream.write(reinterpret_cast<const char *>(&tileRecordLen), sizeof(tileRecordLen));

            // Write number of tile records
            uint32_t tileRecordCount = dim.tx*dim.ty;
            fileStream.write(reinterpret_cast<const char *>(&tileRecordCount), sizeof(tileRecordCount));
        
            // Reserve space for tile record entries
            // These are in the form:
            //   uint32_t - Tile index in x direction
            //   uint32_t - Tile index in y direction
            //   uint64_t - Offset of tile data in file from start of file
            //   uint32_t - Length of compressed tile data in bytes            
            tileRecordPos.clear();
            GSR::TileRecord emptyTileRecord = GSR::TileRecord();
            for (uint32_t tj = 0; tj < dim.ty; tj++) {
                for (uint32_t ti = 0; ti < dim.tx; ti++) {

                    // Add placeholder record
                    tileRecordPos[ { ti, tj } ] = (uint64_t)fileStream.tellp();
                    fileStream.write(reinterpret_cast<const char *>(&emptyTileRecord), sizeof(emptyTileRecord));
                    tileRecordCount++;
                }
            }

            // Clear record table            
            tileRecordTable.clear();

            // Write data
            std::vector<RTYPE> v;
            for (uint32_t tj = 0; tj < dim.ty; tj++) {
                for (uint32_t ti = 0; ti < dim.tx; ti++) {

                    // Get tile data
                    r.getTileData(ti, tj, v);
                    writeData(ti, tj, v, r);
                }
            }
            
            if (fileStream.is_open()) {
                fileStream.close();
            }
        } else {
            // Error for invalid file stream
            throw std::runtime_error("Invalid gsr output file stream");
        }
        
    }

    /**
    * Read gsr tile.
    * @param data vector to read to.
    * @param ti tile x index.
    * @param tj tile y index.
    */
    template <typename RTYPE, typename CTYPE>
    bool GsrHandler<RTYPE, CTYPE>::readData(uint32_t ti, uint32_t tj, std::vector<RTYPE> &v) {
    
        // Check file stream handle
        if (RasterFileHandler<RTYPE, CTYPE>::fileStream != nullptr && 
            *RasterFileHandler<RTYPE, CTYPE>::fileStream) {
        
            // Get file stream handle
            std::fstream &fileStream = *RasterFileHandler<RTYPE, CTYPE>::fileStream;

            // Find data
            auto it = tileRecordTable.find( { ti, tj } );
            if (it != tileRecordTable.end()) {

                // Get offset and length from map
                auto dataOffset = it->second.first;
                auto dataLength = it->second.second;

                // Create compression buffer
                std::vector<unsigned char> compressedData(v.size()*sizeof(RTYPE));

                // Jump to data
                fileStream.seekg(dataOffset, std::ios::beg);

                // Read tile data
                mz_ulong compressedDataLen = (mz_ulong)dataLength;
                fileStream.read(reinterpret_cast<char *>(compressedData.data()), compressedDataLen);
                if (fileStream.eof()) {
                    throw std::runtime_error("Unexpected end of file while reading gsr data");
                }

                // Uncompress
                mz_ulong len = (mz_ulong)compressedData.size();
                int status = uncompress(reinterpret_cast<unsigned char *>(v.data()), &len, 
                    reinterpret_cast<const unsigned char *>(compressedData.data()), compressedDataLen);

                if (status != Z_OK) {
                    std::stringstream err;
                    err << "Uncompression failed '" << mz_error(status) << "'";
                    throw std::runtime_error(err.str());
                } 

                // Check size
                if (len != (mz_ulong)compressedData.size()) {
                    throw std::runtime_error("Unexpected data size during uncompression");
                }

                return true;
            }
        }
        return false;
    }

    /**
    * Write gsr tile.
    * @param ti tile x index.
    * @param tj tile y index.
    * @param v data vector to read to.
    * @param r %Raster to read to.
    */
    template <typename RTYPE, typename CTYPE>
    bool GsrHandler<RTYPE, CTYPE>::writeData(uint32_t ti, uint32_t tj, std::vector<RTYPE> &v, Raster<RTYPE, CTYPE> &r) {

        // Check file stream handle
        if (RasterFileHandler<RTYPE, CTYPE>::fileStream != nullptr && 
            *RasterFileHandler<RTYPE, CTYPE>::fileStream) {
        
            auto it = tileRecordPos.find( { ti, tj } );
            if (it != tileRecordPos.end()) {

                // Get file stream handle
                std::fstream &fileStream = *RasterFileHandler<RTYPE, CTYPE>::fileStream;

                // Set data sizes
                auto dim = r.getRasterDimensions();
                uint32_t dataLength = (uint32_t)TileMetrics::tileSizeSquared*dim.d.nz*sizeof(RTYPE);
                mz_ulong compressedDataLen = compressBound(dataLength);

                // Check data size
                if (v.size()*sizeof(RTYPE) != dataLength) {
                    throw std::runtime_error("Incorrect write vector size");
                }

                // Compress
                std::vector<unsigned char> compressedData(compressedDataLen);
                mz_ulong len = compressedDataLen;
                int status = compress(compressedData.data(), &len, 
                    reinterpret_cast<const unsigned char *>(v.data()), (mz_ulong)dataLength);

                if (status != Z_OK) {
                    std::stringstream err;
                    err << "Compression failed '" << mz_error(status) << "'";
                    throw std::runtime_error(err.str());
                }
                
                // Create record
                fileStream.seekp(0, std::ios::end);
                GSR::TileRecord tileRecord = { ti, tj, (uint64_t)fileStream.tellp(), (uint32_t)len };

                // Write tile data
                fileStream.write(reinterpret_cast<const char *>(compressedData.data()), len);

                // Store record
                fileStream.seekp(it->second, std::ios::beg);
                fileStream.write(reinterpret_cast<const char *>(&tileRecord), sizeof(GSR::TileRecord));

                // Store in record table
                tileRecordTable[ { ti, tj } ] = { tileRecord.dataOffset, tileRecord.dataLength };

                return true;
            }
        }

        return false;
    }

    /**
    * Supply data to %Tile through callback function
    * @param tdim %TileDimensions of tile.
    * @param v vector to fill.
    */
    template <typename RTYPE, typename CTYPE>
    void GsrHandler<RTYPE, CTYPE>::readDataFunction(TileDimensions<CTYPE> tdim, std::vector<RTYPE> &v, Raster<RTYPE, CTYPE> &r) {
    
        // Read data
        if (!readData(tdim.ti, tdim.tj, v))
            v.assign(v.size(), getNullValue<RTYPE>());
    }

    /**
    * Write data gsr through callback function
    * @param tdim %TileDimensions of tile.
    */
    template <typename RTYPE, typename CTYPE>
    void GsrHandler<RTYPE, CTYPE>::writeDataFunction(TileDimensions<CTYPE> tdim, std::vector<RTYPE> &v, Raster<RTYPE, CTYPE> &r) {
    
        // Read data
        writeData(tdim.ti, tdim.tj, v, r);
    }

    // RTYPE float, CTYPE float definitions
    template class GsrHandler<float, float>;
    
    // RTYPE uchar, CTYPE float definitions
    template class GsrHandler<uint8_t, float>;
    
    // RTYPE uint, CTYPE float definitions
    template class GsrHandler<uint32_t, float>;
    
    // RTYPE double, CTYPE double definitions
    template class GsrHandler<double, double>;

    // RTYPE uchar, CTYPE double definitions
    template class GsrHandler<uint8_t, double>;
    
    // RTYPE uint, CTYPE double definitions
    template class GsrHandler<uint32_t, double>;
}
