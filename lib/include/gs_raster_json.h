/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
*/

#ifndef GEOSTACK_RASTER_GEOJSON_H
#define GEOSTACK_RASTER_GEOJSON_H

#include <string>

#include "gs_raster.h"

namespace Geostack
{
    template <typename RTYPE, typename CTYPE>
    class JsonHandler : public RasterFileHandler<RTYPE, CTYPE> {

    public:

        /**
        * %JsonHandler constructors.
        */
        JsonHandler(): RasterFileHandler<RTYPE, CTYPE>() { }        
        ~JsonHandler(){};
        
        // Conversion to Json
        static std::string toJson(Raster<RTYPE, CTYPE> &r, bool compress = false, std::string jsonConfig="");

        // IO functions
        void read(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig); ///< Open file for reading to %Raster
        void write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig); ///< Write %Raster to file
    private:
        static RTYPE getNullValue();                     ///< Get null value for RTYPE
    };
}

#endif