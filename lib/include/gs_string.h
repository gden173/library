/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef GEOSTACK_STRING_H
#define GEOSTACK_STRING_H

#include <vector>
#include <string>

namespace Geostack
{
    namespace Strings
    {
        // String type checking
        bool isNumber(const std::string &str);
        
        template <typename T>
        T toNumber(const std::string &str);
        
        // Field path splitting
        std::vector<std::string> splitPath(const std::string &path);

        /*! Extract the filename part from the path \p path. The filename is considered to be the 
            part of the path after the last directory separator character, or the whole path if it 
            contains no directory separator character. The extension (the part of the path after 
            the last dot character, if any) is included in the result only if \p incl_ext is true.
        */
        auto extractFilename(const std::string& path, bool incl_ext = true) -> std::string;

        // String splitting
        std::vector<std::string> split(std::string str, char del, bool concatenate = false);

        // String replace
        std::string replace(std::string &str, std::string search, std::string replace);

        // String trim
        std::string removeWhitespace(std::string str);

        // Remove character
        std::string removeCharacter(std::string str, char c);

        // Change case to upper
        std::string toUpper(std::string str);

        // Change case to lower
        std::string toLower(std::string str);

        // Pad string
        std::string pad(std::string str, char padChar, std::size_t length);

        // Date time parsing
        int64_t iso8601toEpoch(std::string str);
        int64_t iso8601toTimeZoneOffset(std::string str);
    };
}

#endif