/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
  
#ifndef GEOSTACK_GEOTIFF_H
#define GEOSTACK_GEOTIFF_H

#include <vector>
#include <string>
#include <fstream>

#include "gs_raster.h"

namespace Geostack
{
    /**
    * GeoTIFF data types.
    */
    namespace GeoTIFFDataTypes {
        enum Type {
            Byte = 1,
            Ascii = 2,
            Short = 3,
            Long = 4,
            Rational = 5,
            SByte = 6,
            Undefined = 7,
            SShort = 8,
            SLong = 9,
            SRational = 10,
            Float = 11,
            Double = 12,
            IFD = 13,
            Long8 = 16,
            SLong8 = 17,
            IFD8 = 18
        };
    }

    /**
    * GeoTIFF compression types.
    */
    namespace GeoTIFFCompressionTypes {
        enum Type {
            Uncompressed = 1,
            CCITT_1D = 2,
            Group_3_Fax = 3,
            Group_4_Fax = 4,
            LZW = 5,
            OldJPEG = 6,
            NewJPEG = 7,
            Deflate = 8,
            PackBits = 32773
        };
    }

    /**
    * GeoTIFF predictor types.
    */
    namespace GeoTIFFPredictorTypes {
        enum Type {
            None = 1,
            HorizontalDifferencing = 2,
            FloatingPoint = 3
        };
    }

    /**
    * GeoTIFF sample types.
    */
    namespace GeoTIFFSampleTypes {
        enum Type {
            UnsignedInt = 1,
            SignedInt = 2,
            Float = 3
        };
    }

    /**
    * %GeoTIFFDirectory for GeoTIFF to store directory entries.
    */
    class GeoTIFFDirectory {

    public:

        // Constructor
        GeoTIFFDirectory();
        GeoTIFFDirectory(GeoTIFFDataTypes::Type type, uint64_t length, uint64_t value);

        // Read and write directory
        uint16_t read(std::fstream &fileStream, bool isBigTIFF);
        std::fstream::pos_type write(uint16_t tag, std::fstream &fileStream, bool isBigTIFF);

        // Read data from entry
        template <typename S>
        bool readData(std::fstream &fileStream, S &v, bool isBigTIFF);
        
        // Get data type
        uint16_t getType() const {
            return type;
        }

        static std::vector<uint32_t> GeoTIFFDataSizes; ///< Size in bytes of each GeoTIFF data type 

    private:

        // Directory data
        uint16_t type;   ///< Directory data type
        uint32_t size;   ///< Size in bytes of each entry
        uint64_t length; ///< Number of entries in directory
        uint64_t value;  ///< The value or offset of the entries
    };

    /**
    * %GeoTIFFHandler class for reading and writing GeoTIFF data.
    */
    template <typename RTYPE, typename CTYPE>
    class GeoTIFFHandler : public RasterFileHandler<RTYPE, CTYPE> {

    public:

        /**
        * %GeoTIFFHandler constructor.
        */
        GeoTIFFHandler(): 
            RasterFileHandler<RTYPE, CTYPE>(),
            nx(0), ny(0), bnx(0), bny(0), bcols(0), brows(0), 
            isTiled(false), isStrips(false), flip_y(true), sampleFormat(0),
            samplesPerPixel(0) {
        }
        
        ~GeoTIFFHandler(){};
 
        void read(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig); ///< Open file for reading to %Raster
        void write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig); ///< Write %Raster to file

        /*! Whether the pixel data of the file last read or written via this handler is organised in tiles.
            If called before a call to read() or write(), throws an exception.
        */ 
        auto usesDataTiles() const -> bool;

        /*! Whether the pixel data of the file last read or written via this handler is organised in strips.
            If called before a call to read() or write(), throws an exception.
        */ 
        auto usesDataStrips() const -> bool;

        /*! Get the size (in pixels) of a block (i.e. a single tile or strip) of pixel data in the file last read or 
            written via this handler. If called before a call to read() or write(), throws an exception.
            \return  0. Block size along the X axis
                     1. Block size along the Y axis 
        */
        auto getDataBlockSize() const -> std::tuple<uint32_t, uint32_t>;

    private:

        uint32_t nx;                 ///< Number of cells in x direction
        uint32_t ny;                 ///< Number of cells in y direction
        uint32_t bnx;                ///< Size of block in x
        uint32_t bny;                ///< Size of block in y
        uint32_t bcols;              ///< Number of block columns in data
        uint32_t brows;              ///< Number of block rows in data
        bool isTiled;                ///< Data formatted in tiles
        bool isStrips;               ///< Data formatted in strips
        bool flip_y;                 ///< Data is flipped in y (origin top)
        uint8_t sampleFormat;        ///< Data format of type %GeoTIFFSampleTypes
        uint32_t samplesPerPixel;    ///< Data format of type %GeoTIFFSampleTypes
        std::string nullValueString; ///< No data string value

        std::map<std::string, std::vector<uint64_t> > valuesIn;                   ///< Map of directory values
        std::vector<std::vector<RTYPE> > bCache;                                  ///< Cache of blocks read
        std::vector<uint32_t> bCacheCount;                                        ///< Count of cache access

        void readData(uint32_t ti, uint32_t tj, std::vector<RTYPE> &v);           ///< Read data from GeoTIFF at tile index
        void readDataTileFunction(TileDimensions<CTYPE> tdim, std::vector<RTYPE> &v, Raster<RTYPE, CTYPE> &r); ///< Populate %Raster tile
        void readDataStripFunction(TileDimensions<CTYPE> tdim, std::vector<RTYPE> &v, Raster<RTYPE, CTYPE> &r); ///< Populate %Raster tile

        static GeoTIFFSampleTypes::Type getSampleType(); ///< Get sample type for RTYPE
        static RTYPE getNullValue();                     ///< Get sample type for RTYPE

    };
}

#endif
