/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#include <map>
#include <string>
#include <vector>
#include <cstdint>
 
#ifndef GEOSTACK_PALETTE_H
#define GEOSTACK_PALETTE_H

namespace Geostack
{
    class Palette
    {
    private:
        static const std::map<std::string, std::vector<std::vector<double> > > Palettes;

    public:
        static std::vector<double> getRGB(const std::string name, double v);
    };
}

#endif