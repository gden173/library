/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef GEOSTACK_SERIES_H
#define GEOSTACK_SERIES_H

#include <utility>
#include <string>
#include <vector>

namespace Geostack
{
    /**
    * %Series entry
    */
    template <typename XTYPE, typename YTYPE>
    struct alignas(8) SeriesItem {

        XTYPE x; ///< Abscissa
        YTYPE y; ///< Ordinate
    };

    /**
    * %Series interpolation types.
    */
    namespace SeriesInterpolation {
        enum Type {
            Linear,         ///< Linear interpolation
            MonotoneCubic,  ///< Monotone cubic interpolation
            BoundedLinear,  ///< Linear interpolation respecting bounds (e.g. angular 0-360)
        };
    }

    /**
    * %Series capping types.
    */
    namespace SeriesCapping {
        enum Type {
            Uncapped,  ///< Returns null outside x-range
            Capped,    ///< Returns limting y-value outside x-range
        };
    }

    /**
    * %Series implementation
    */
    template <typename XTYPE, typename YTYPE>
    class Series {

    protected:

        // Data
        std::string name;                              ///< Series name
        std::vector<SeriesItem<XTYPE, YTYPE> > values; ///< Values stores as x, y pair
        std::vector<double> slopes;                    ///< Slope tangents for non-constant series
        SeriesInterpolation::Type interpolation;       ///< Interpolation type
        SeriesCapping::Type capping;                   ///< Capping type

        // Bounds
        XTYPE x_min, x_max;               ///< Abscissa limits
        YTYPE y_min, y_max;               ///< Ordinate limits
        YTYPE y_upperBound, y_lowerBound; ///< Interpolation bounds

        // Interpolated values
        YTYPE getLinear(const XTYPE);
        YTYPE getMonotoneCubic(const XTYPE);
        YTYPE getBoundedLinear(const XTYPE);

    public:
        Series();
        Series(std::string _name);
        Series(const Series &);
        Series &operator= (const Series &);
        ~Series() { };

        // Modify values
        void clear();
        void addValue(XTYPE x, YTYPE y, bool isSorted = false);
        void addValue(std::string x, YTYPE y, bool isSorted = false);
        void addValues(const std::vector<SeriesItem<XTYPE, YTYPE> > newValues);
        bool addValues(const std::vector<std::pair<std::string, YTYPE> > newValues);

        // Update series
        void update(bool isSorted = false);
        void updateLimits();
        void setBounds(YTYPE y_firstBound, YTYPE y_secondBound);

        // Get values
        YTYPE operator()(const XTYPE);
        std::vector<YTYPE> operator()(const std::vector<XTYPE>);

        // Get bounds
        XTYPE get_xMax() { return x_max; }
        XTYPE get_xMin() { return x_min; }
        YTYPE get_yMax() { return y_max; }
        YTYPE get_yMin() { return y_min; }

        // Get abscissa values
        std::vector<XTYPE> getAbscissas();

        // Get ordinate values
        std::vector<YTYPE> getOrdinates();

        // Range check
        bool inRange(const XTYPE);

        /**
        * Set name
        */
        void setName(std::string &name_) {
            name = name_;
        }

        /**
        * Get name
        */
        std::string getName() {
            return name;
        }

        /**
        * Check data
        */
        bool isInitialised() const {
            return values.size() != 0 ? true : false;
        }

        /**
        * Check for constant series
        */
        bool isConstant() {
            return values.size() == 1 ? true : false;
        }

        /**
        * Set interpolation type
        */
        void setInterpolation(SeriesInterpolation::Type interpolation_) {
            interpolation = interpolation_;
        }

        /**
        * Set capping type
        */
        void setCapping(SeriesCapping::Type capping_) {
            capping = capping_;
        }

        /**
        * Get interpolation type
        */
        SeriesInterpolation::Type getInterpolation() {
            return interpolation;
        }

        /**
        * Get capping type
        */
        SeriesCapping::Type getCapping() {
            return capping;
        }

        /**
         * @brief Get size of Series object
         *
         */
        std::size_t getSize() {
            return values.size();
        }
        
        /**
         * @brief check if abscissa are sorted
         * 
         * @return true if sorted
         * @return false otherwise
         */
        bool isSorted();

        /**
         * @brief get sum of ordinates 
         * 
         * @return YTYPE total values of ordinates
         */
        YTYPE sum();

        /**
         * @brief get mean value of ordinates
         * 
         * @return YTYPE mean value of ordinates
         */
        YTYPE mean();
    };
}

#endif
