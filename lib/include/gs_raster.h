/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef GEOSTACK_RASTER_H
#define GEOSTACK_RASTER_H

#include <atomic>
#include <memory>
#include <vector>
#include <list>
#include <set>
#include <unordered_map>
#include <iterator>
#include <functional>
#include <fstream>

#include "gs_opencl.h"
#include "gs_geometry.h"
#include "gs_variables.h"
#include "gs_property.h"
#include "gs_projection.h"
#include "gs_tile.h"
#include "gs_vector.h"

#define STRINGIFY(s) (#s)

/**
* Helper macro to create a named raster with the same name as the internal variable
*/
#define makeRaster(NAME, RTYPE, CTYPE) Geostack::Raster<RTYPE, CTYPE> NAME(STRINGIFY(NAME));

namespace Geostack
{
    // Forward declarations
    class Solver;

    template <typename CTYPE>
    class BoundingBox;

    template <typename CTYPE>
    class Vector;

    template <typename RTYPE, typename CTYPE>
    class Tile;
    
    template <typename CTYPE>
    class RasterBase;

    template <typename RTYPE, typename CTYPE>
    class Raster;
    
    // Definitions
    template <typename RTYPE, typename CTYPE>
    using TilePtr = std::shared_ptr<Tile<RTYPE, CTYPE> >;
    
    template <typename RTYPE, typename CTYPE>
    using RasterPtr = std::shared_ptr<Raster<RTYPE, CTYPE> >;

    template <typename CTYPE>
    using RasterBasePtr = std::shared_ptr<RasterBase<CTYPE> >;
    
    template <typename CTYPE>
    using RasterBaseRef = std::reference_wrapper<RasterBase<CTYPE> >;
    
    template <typename CTYPE>
    using RasterBaseRefs = std::vector<RasterBaseRef<CTYPE> >;

    using tileIndexSet = std::set<std::pair<uint32_t, uint32_t> >; 

    /**
    * Combination types
    */
    namespace RasterCombination {
        enum Type {
            Union        = 0,      ///< Combination is union of rasters
            Intersection = 1 << 0, ///< Combination is intersection of rasters
        };
    }

    /**
    * Resolution type
    */
    namespace RasterResolution {
        enum Type {
            Minimum = 0,      ///< Resolution is maximum resolution of rasters
            Maximum = 1 << 2, ///< Resolution is minimum resolution of rasters
        };
    }

    /**
    * Interpolation types.
    */
    namespace RasterInterpolation {
        enum Type {
            Nearest  = 0,      ///< Nearest neighbour interpolation
            Bilinear = 1 << 4, ///< Bilinear interpolation
            Bicubic  = 2 << 4, ///< Bicubic interpolation
        };
    }

    /**
    * Value for nodata in calculation.
    */
    namespace RasterNullValue {
        enum Type {
            Null = 0,      ///< Use null
            Zero = 1 << 6, ///< Use zero
            One  = 2 << 6, ///< Use one
        };
    }

    /**
    * Raster debug type
    */
    namespace RasterDebug {
        enum Type {
            None       = 0 << 12, ///< No debugging
            Enable     = 1 << 12, ///< Enable debugging
        };
    }

    /**
    * Raster sort type
    */
    namespace RasterSort {
        enum Type {
            None       = 0 << 14, ///< No sorting
            PreScript  = 1 << 14, ///< Sorting of anchor before script
            PostScript = 2 << 14, ///< Sorting of anchor after script
        };
    }

    /**
     * Raster data type
    */
   namespace RasterDataType {
    enum Type {
        UInt8,          // Unsigned char
        UInt32,         // Unsigned int
        Float,          // Single precision float
        Double,         // Double precision float
    };
   }

    /**
    * Raster neighbour type
    */
    namespace Neighbours {
        enum Type {
            None = 0,      ///< No neighbours
            N  = 1 << 0,   ///< North neighbour
            NE = 1 << 1,   ///< North-east neighbour
            E  = 1 << 2,   ///< East neighbour
            SE = 1 << 3,   ///< South-east neighbour
            S =  1 << 4,   ///< South neighbour
            SW = 1 << 5,   ///< South-west neighbour
            W =  1 << 6,   ///< West neighbour
            NW = 1 << 7,   ///< North-west neighbour
            Rook = 0x55,   ///< N, E, S and W neighbours
            Bishop = 0xAA, ///< NE, SW, SW and NW neighbours
            Queen = 0xFF,  ///< All neighbours
        };
    }

    class RasterKernelRequirement {
    public:
        /**
        * %RasterKernelRequirements constructors
        */
        RasterKernelRequirement(): 
            used(true), 
            requiredNeighbours(0) {
        }
        RasterKernelRequirement(uint8_t _requiredNeighbours): 
            used(true),
            requiredNeighbours(_requiredNeighbours) {
        }      
        RasterKernelRequirement(const RasterKernelRequirement &r): 
            used(r.used),
            requiredNeighbours(r.requiredNeighbours) {
        }
        RasterKernelRequirement &operator=(const RasterKernelRequirement &r) {
            if (this != &r) {
                used = r.used;
                requiredNeighbours = r.requiredNeighbours;
            }
            return *this; 
        }

        bool used;                  // Whether Raster is used in the Kernel
        uint8_t requiredNeighbours; // Bitmask of required neighbours        
    };

    class RasterKernelRequirements {    
    public:

        /**
        * %RasterKernelRequirements constructors
        */
        RasterKernelRequirements(): 
            usesRandom(false),
            usesBoundary(false) {
        }
        RasterKernelRequirements(std::vector<RasterKernelRequirement> rasterRequirements_): 
            rasterRequirements(rasterRequirements_), 
            usesRandom(false),
            usesBoundary(false) {
        }        
        RasterKernelRequirements(const RasterKernelRequirements &r): 
            usesRandom(r.usesRandom),
            rasterRequirements(r.rasterRequirements),
            usesBoundary(r.usesBoundary) {
        }
        RasterKernelRequirements &operator=(const RasterKernelRequirements &r) {
            if (this != &r) {
                usesRandom = r.usesRandom;
                rasterRequirements = r.rasterRequirements;
                usesBoundary = r.usesBoundary;
            }
            return *this; 
        }

        bool usesRandom;            // Whether Raster random values are used in the Kernel
        bool usesBoundary;          // Whether tile boundary flag is required by the Kernel
        std::vector<RasterKernelRequirement> rasterRequirements;        
    };

    /**
    * %RasterKernelGenerator class
    * Holds parsed script and flags for script building
    */
    class RasterKernelGenerator {
    public:

        /**
        * %RasterKernelGenerator constructors
        */
        RasterKernelGenerator():script() {
            programHash = Solver::getNullHash();
        }      
        RasterKernelGenerator(const RasterKernelGenerator &r): 
            script(r.script), 
            projectionTypes(r.projectionTypes),
            rasterTypes(r.rasterTypes),
            fieldNames(r.fieldNames),
            reqs(r.reqs),
            programHash(r.programHash) {
        }
        RasterKernelGenerator &operator=(const RasterKernelGenerator &r) {
            if (this != &r) {
                script = r.script; 
                projectionTypes = r.projectionTypes;
                rasterTypes = r.rasterTypes;
                fieldNames = r.fieldNames;
                reqs = r.reqs;
                programHash = r.programHash;
            }
            return *this; 
        }

        std::string script;                  ///< Parsed script
        std::set<uint32_t> projectionTypes;  ///< Projection types used in script
        std::set<std::string> rasterTypes;   ///< Raster types used in script
        std::vector<std::string> fieldNames; ///< Auxillary storage for vector field names
        std::size_t programHash;             ///< Compiled program hash
        
        /**
        * Vector of Raster requirements
        */
        RasterKernelRequirements reqs;
    };
    
    /**
    * %RasterKernelCache class
    * Contains program hashes and corresponding generators
    */
    class RasterKernelCache {  

    public:

        RasterKernelCache() { };
        ~RasterKernelCache() { };

        // Singleton instance of OpenCL solver.
        static RasterKernelCache &getKernelCache();

        // Prevent copy and assignment
        RasterKernelCache(RasterKernelCache const&) = delete;
        void operator=(RasterKernelCache const&) = delete;        

        // Member functions
        void setKernelGenerator(std::size_t, RasterKernelGenerator);
        RasterKernelGenerator getKernelGenerator(std::size_t scriptHash);

    private:
        std::map<std::size_t, RasterKernelGenerator> generatorMap; 
    };
    
    // Dimensions output stream
    template <typename CTYPE>
    std::ostream &operator<<(std::ostream &os, const Dimensions<CTYPE> &r);

    // Raster dimensions output stream
    template <typename CTYPE>
    std::ostream &operator<<(std::ostream &os, const RasterDimensions<CTYPE> &r);

    template <typename CTYPE>
    void runScriptNoOut(
        std::string script, 
        RasterBaseRefs<CTYPE> rasterBaseRefs, 
        size_t parameters = 0);
        
    template <typename CTYPE>
    std::size_t createScriptHash(
        std::string script, 
        std::string kernelBlock,
        RasterBaseRefs<CTYPE> rasterBaseRefs);

    template <typename CTYPE>
    std::size_t createScriptHash(
        std::string script, 
        std::string kernelBlock,
        RasterBase<CTYPE> &rasterBase);
        
    template <typename CTYPE>
    RasterKernelGenerator generateKernel(
        std::string script, 
        std::string kernelBlock,
        RasterBaseRefs<CTYPE> rasterBaseRefs, 
        RasterNullValue::Type rasterNullValue = RasterNullValue::Null,
        VariablesBasePtr<std::string> variables = nullptr,
        bool useConst = true,
        bool addTerminator = true);

    template <typename CTYPE>
    bool setTileKernelArguments(
        uint32_t ti, 
        uint32_t tj, 
        cl::Kernel &kernel, 
        RasterBaseRefs<CTYPE> rasterBaseRefs,
        RasterKernelRequirements reqs, 
        int startArg = 0,
        VariablesBasePtr<std::string> variables = nullptr,
        bool useDimensions = true);

    template <typename CTYPE>
    bool runTileKernel(
        uint32_t ti, 
        uint32_t tj, 
        cl::Kernel &kernel, 
        RasterBaseRefs<CTYPE> rasterBaseRefs,
        RasterKernelRequirements reqs,
        int startArg = 0,
        VariablesBasePtr<std::string> variables = nullptr,
        size_t debug = 0);

    std::size_t buildRasterKernel(RasterKernelGenerator &kernelGenerator);

    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE> runScript(
        std::string script, 
        RasterBaseRefs<CTYPE> rasterBaseRefs, 
        size_t parameters = 0);

    template <typename RTYPE, typename CTYPE>
    Raster<RTYPE, CTYPE> runAreaScript(
        std::string script, 
        RasterBase<CTYPE> &r, 
        cl_int width);    
    
    /**
    * Run script on %Raster
    * Aliased runScript function
    * @param script OpenCL script to run.
    * @param rasterBaseRefs list of input raster references.
    * @param parameters parameter flags for combination and aliasing.
    * @return true if script is successful, false otherwise
    */
    template <typename CTYPE>
    void runScript(std::string script, RasterBaseRefs<CTYPE> rasterBaseRefs, size_t parameters = 0) {
        runScriptNoOut(script, rasterBaseRefs, parameters);
    }
    
    template <typename RTYPE, typename CTYPE>
    Vector<CTYPE> stipple(std::string script, RasterBaseRefs<CTYPE> rasterBaseRefs, 
        std::vector<std::string> fields = std::vector<std::string>(), uint32_t nPerCell = 1);
    
    template <typename CTYPE>
    void sortColumns(RasterBase<CTYPE> &rasterBase);

    // Comparison operators
    template <typename RTYPE, typename CTYPE>
    bool operator==(const Raster<RTYPE, CTYPE> &l, const Raster<RTYPE, CTYPE> &r);
    
    /**
     * Get maximum possible size of a given raster tile
     * @param  rasterBaseRefs %RasterBaseRefs object.
     * @return tile size
    */
    template <typename CTYPE>
    float getMaxTileDataSize(RasterBaseRefs<CTYPE> rasterBaseRefs);
    
    /**
    * %RasterFileHandler class for %Raster file IO. 
    */
    template <typename RTYPE, typename CTYPE>
    class RasterFileHandler {

    public:
        
        // Constructor.
        RasterFileHandler();
        
        // Copy constructor.
        RasterFileHandler(const RasterFileHandler &); 
        
        // Disable assignment operator as reference cannot be initialised.
        void operator=(RasterFileHandler const &) = delete;
        
        /**
        * %RasterFileHandler destructor.
        */
        virtual ~RasterFileHandler();

        virtual void read(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig = "") = 0; ///< Open file for reading to %Raster
        virtual void write(std::string fileName, Raster<RTYPE, CTYPE> &r, std::string jsonConfig = "") = 0; ///< Write %Raster to file

        // Data handler callback function
        void registerReadDataHandler(dataHandlerReadFunction<RTYPE, CTYPE> dataHandler_);
        void registerWriteDataHandler(dataHandlerWriteFunction<RTYPE, CTYPE> dataHandler_);
        
        /**
        * Get input data handler function.
        */
        const dataHandlerReadFunction<RTYPE, CTYPE> &getReadDataHandler() const {
            return readDataHandler;
        }
        
        /**
        * Get output data handler function.
        */
        const dataHandlerWriteFunction<RTYPE, CTYPE> &getWriteDataHandler() const {
            return writeDataHandler;
        }

        std::shared_ptr<std::fstream> fileStream; ///< %Raster file handle
        
        void closeFile();

    private:

        // Callback function to read or write data
        dataHandlerReadFunction<RTYPE, CTYPE> readDataHandler;
        dataHandlerWriteFunction<RTYPE, CTYPE> writeDataHandler;

    };
    
    /**
    * %RasterBase class.
    * Base class for %Raster.
    **/
    template <typename CTYPE>
    class RasterBase : public PropertyMap {

    public:

        // Parent aliases
        using PropertyMap::properties;
    
        // Constructor
        RasterBase();

        // Destructor
        virtual ~RasterBase();

        // Copy constructor
        RasterBase(const RasterBase &rb);

        // Assignment operator
        RasterBase &operator=(const RasterBase &rb);        
        
        /**
        * Get %RasterBase dimensions.
        * @return dimensions of %RasterBase.
        */
        RasterDimensions<CTYPE> getRasterDimensions() const { 
            return dim;
        }

        /**
        * Set %RasterBase %ProjectionParameters.
        */
        void setProjectionParameters(ProjectionParameters<double> proj_);

        /**
        * Get %RasterBase %ProjectionParameters.
        */
        ProjectionParameters<double> getProjectionParameters() {
            return proj;
        }

        /**
        * Set %RasterBase interpolation type.
        */
        void setInterpolationType(std::size_t interpolation_) {
            interpolation = interpolation_;
        }

        /**
        * Get %RasterBase interpolation type
        */
        std::size_t getInterpolationType() {
            return interpolation;
        }

        /**
        * Get bounds of %RasterBase.
        * @return bounds of %RasterBase.
        */
        BoundingBox<CTYPE> getBounds() {
            return BoundingBox<CTYPE>( { { dim.d.ox, dim.d.oy, dim.d.oz }, { dim.ex, dim.ey, dim.ez } } );
        }

        /**
         * Get footprint of %RasterBase.
         * @return footprint of %RasterBase
        */
        Vector<CTYPE> getRasterFootprint();

        /**
        * Set constant flag
        */
        void setConst(bool isConst_) {
            isConst = isConst_;
        }

        /**
        * Get constant flag
        */
        bool getConst() const {
            return isConst;
        }

        // Raster initialisation from dimensions
        virtual bool init(const Dimensions<CTYPE> &dim) = 0;

        // Get type strings
        virtual std::string getDataTypeString() = 0;
        virtual std::string getOpenCLTypeString() = 0;
        virtual std::size_t getOpenCLTypeSize() = 0;

        // Raster data check
        virtual bool hasData() = 0;

        // Raster data delete
        virtual void deleteRasterData() = 0;
        
        // Get tile data
        virtual cl::Buffer &getNullBuffer() = 0;
        virtual cl::Buffer &getRandomBuffer() = 0;
        virtual void saveRandomState() = 0;
        virtual void restoreRandomState() = 0;
        virtual cl::Buffer &getTileDataBuffer(uint32_t ti, uint32_t tj) = 0;
        virtual cl::Buffer &getTileReduceBuffer(uint32_t ti, uint32_t tj) = 0;
        virtual void resetTileReduction(uint32_t ti, uint32_t tj) = 0;
        virtual cl::Buffer &getTileStatusBuffer(uint32_t ti, uint32_t tj) = 0;
        virtual cl_uint getTileStatus(uint32_t ti, uint32_t tj) = 0;
        virtual void setTileStatus(uint32_t ti, uint32_t tj, cl_uint newStatus) = 0;
        virtual TileDimensions<CTYPE> getTileDimensions(uint32_t ti, uint32_t tj) = 0;
        virtual BoundingBox<CTYPE> getTileBounds(uint32_t ti, uint32_t tj) = 0;
        virtual std::vector<RasterIndex<CTYPE> > getVectorTileIndexes(Vector<CTYPE> &v, 
            uint32_t ti, uint32_t tj, std::size_t clHash, cl_uchar parameters) = 0;
        virtual bool tileDataOnDevice(uint32_t ti , uint32_t tj) = 0;

        // 2D raster resize
        virtual bool resize2D(uint32_t nx_, uint32_t ny_, uint32_t tox_, uint32_t toy_) = 0;
        
        // Buffer from region operation
        virtual bool createRegionBuffer(
            BoundingBox<CTYPE> bounds, 
            cl::Buffer &rasterRegionBuffer, 
            RasterDimensions<CTYPE> &rasterRegionDim) = 0;
        
        // Neighbour processing options
        virtual uint8_t getRequiredNeighbours() = 0;
        virtual void setRequiredNeighbours(uint8_t requiredNeighbours_) = 0;

        // Reduction processing options
        virtual Reduction::Type getReductionType() = 0;
        virtual void setReductionType(Reduction::Type reductionType_) = 0;

        // Status processing options
        virtual bool getNeedsStatus() = 0;
        virtual void setNeedsStatus(bool needsStatus_) = 0;
        
        // Read options
        virtual bool getNeedsRead() = 0;
        virtual void setNeedsRead(bool needsRead_) = 0;

        // Write options
        virtual bool getNeedsWrite() = 0;
        virtual void setNeedsWrite(bool needsWrite_) = 0;

        virtual void read(std::string fileName, std::string jsonConfig = "") = 0; //< Read file to %Raster
        virtual void write(std::string fileName, std::string jsonConfig = "") = 0; //< Write %Raster to file

        virtual void closeFile() = 0;

        // Variable handling
        template <typename RTYPE>
        RTYPE getVariableData(std::string name); ///< Get variable data

        std::set<std::string> getVariableNames();

        template <typename RTYPE>
        RTYPE getVariableData(std::string name, std::size_t index); ///< Get variable data with index

        template <typename RTYPE>
        void setVariableData(std::string name, RTYPE value); ///< Set variable data

        template <typename RTYPE>
        void setVariableData(std::string name, RTYPE value, std::size_t index); ///< Set variable data with index
        
        void deleteVariableData();
        
        const std::map<std::string, std::size_t> &getVariablesIndexes(); ///< Get indexes of variables

        const std::size_t getVariableSize(std::string name); ///< Get size of variable array

        cl::Buffer &getVariablesBuffer(); ///< Get buffer of variables

        std::string getVariablesType(); ///< Get type of variables
        
        /**
        * Check variables in %RasterBase.
        * @return variables of %RasterBase.
        */
        bool hasVariables() {
            return vars != nullptr && vars->hasData();
        }

        // Coordinate caching
        VariablesVector<Coordinate<CTYPE> > coordsCache;
        VariablesVector<Coordinate<CTYPE> > &getCoordinateCache() {
            return coordsCache;
        }

        // Region caching
        std::size_t regionCacheBufferSize;
        cl::Buffer regionCacheBuffer;
        cl::Buffer &getRegionCacheBuffer(std::size_t dataSize);

        // Get hash string
        std::string getHashString();
        
        // Component indexing
        Raster<cl_uint, CTYPE> indexComponents(
            std::string script = std::string()); 

        Vector<CTYPE> indexComponentsVector
            (std::string indexScript = std::string(), 
            std::string reduceScript = std::string(), 
            Reduction::Type = Reduction::None,
            bool overwrite = false);

    protected:

        std::shared_ptr<Variables<CTYPE, std::string> > vars; ///% Variables data

        RTree<CTYPE> tree;                 ///< %RTree for %Tile geometry
        RasterDimensions<CTYPE> dim;       ///< %Raster dimensions
        ProjectionParameters<double> proj; ///< %Raster projection
        std::size_t interpolation;         ///< %Raster interpolation type
        bool isConst;                      ///< Flag for whether tile is cacheable
    };

    /**
    * %Raster class for one, two or three dimensional geospatial data.
    * This is the main class for gridded raster data. The raster holds a handle 
    * to a memory array as well as dimensions, cell sizes, an offset value from the 
    * origin (0, 0, 0) raster cell, projection information and a null value. 
    */
    template <typename RTYPE, typename CTYPE>
    class Raster : public RasterBase<CTYPE> {

    public:

        // Parent aliases
        using PropertyMap::properties;
        using RasterBase<CTYPE>::vars;
        using RasterBase<CTYPE>::tree;
        using RasterBase<CTYPE>::dim;
        using RasterBase<CTYPE>::proj;
        using RasterBase<CTYPE>::interpolation;

        // Constructors
        Raster();
        Raster(std::string name);

        // Destructor
        virtual ~Raster();

        // Copy constructor
        Raster(const Raster &r);

        // Assignment operator
        Raster &operator=(const Raster &r);        

        // Set origin of 2D raster layer
        void setOrigin_z(CTYPE oz_);

        // Get type strings
        std::string getDataTypeString();
        std::string getOpenCLTypeString();
        std::size_t getOpenCLTypeSize();
        
        // Raster initialisation
        bool init(
            uint32_t nx_, 
            uint32_t ny_, 
            uint32_t nz_, 
            CTYPE hx_, 
            CTYPE hy_, 
            CTYPE hz_, 
            CTYPE ox_ = 0.0, 
            CTYPE oy_ = 0.0,  
            CTYPE oz_ = 0.0);

        // Raster initialisation for 2D layer
        bool init2D(
            uint32_t nx_, 
            uint32_t ny_, 
            CTYPE hx_, 
            CTYPE hy_, 
            CTYPE ox_ = 0.0, 
            CTYPE oy_ = 0.0, 
            CTYPE oz_ = 0.0);

        // Raster initialisation from dimensions
        bool init(const Dimensions<CTYPE> &dim);

        // Raster initialisation from bounds
        bool init(const BoundingBox<CTYPE> bounds, CTYPE hx_, CTYPE hy_ = 0.0, CTYPE hz_ = 0.0);            
            
        // 2D raster resize
        bool resize2D(uint32_t nx_, uint32_t ny_, uint32_t tox_, uint32_t toy_);
        tileIndexSet resize2DIndexes(uint32_t nx_, uint32_t ny_, uint32_t tox_, uint32_t toy_);

        // Raster maximum reductions
        RTYPE max();
        RTYPE min();

        RTYPE reduce() const;                     // Raster reduction
        std::vector<RTYPE> reduceByLayer() const; // Raster reduction by layer
        
        // Read and write operations
        RTYPE getCellValue(uint32_t i, uint32_t j = 0, uint32_t k = 0);
        RTYPE getNearestValue(CTYPE x, CTYPE y = 0.0, CTYPE z = 0.0);
        RTYPE getBilinearValue(CTYPE x, CTYPE y = 0.0, CTYPE z = 0.0);

        Coordinate<CTYPE> ijk2xyz(uint32_t i, uint32_t j = 0, uint32_t k = 0);
        std::list<uint32_t> xyz2ijk(CTYPE x, CTYPE y = 0.0, CTYPE z = 0.0);

        void setAllCellValues(RTYPE val);
        void setCellValue(RTYPE val, uint32_t i, uint32_t j = 0, uint32_t k = 0);
        
        TileDimensions<CTYPE> getTileDimensions(uint32_t ti, uint32_t tj);
        BoundingBox<CTYPE> getTileBounds(uint32_t ti, uint32_t tj);
        cl::Buffer &getTileDataBuffer(uint32_t ti, uint32_t tj);
        cl::Buffer &getTileReduceBuffer(uint32_t ti, uint32_t tj);
        RTYPE getTileReduction(uint32_t ti, uint32_t tj);
        void resetTileReduction(uint32_t ti, uint32_t tj);
        cl::Buffer &getTileStatusBuffer(uint32_t ti, uint32_t tj);
        void getTileData(uint32_t ti, uint32_t tj, std::vector<RTYPE>& data);
        void setTileData(uint32_t ti, uint32_t tj, std::vector<RTYPE>& data);
        cl_uint getTileStatus(uint32_t ti, uint32_t tj);
        void setTileStatus(uint32_t ti, uint32_t tj, cl_uint newStatus);
        bool getResetTileStatus(uint32_t ti, uint32_t tj, cl_uint &status, cl_uint newStatus);
        bool setAllTileCellValues(uint32_t ti, uint32_t tj, RTYPE val);
        cl::Buffer &getNullBuffer();
        cl::Buffer &getRandomBuffer();
        void saveRandomState();
        void restoreRandomState();

        void searchTiles(BoundingBox<CTYPE> bounds,
                         std::vector<GeometryBasePtr<CTYPE> > &tileList);
        void nearestTiles(BoundingBox<CTYPE> bounds,
                          std::vector<GeometryBasePtr<CTYPE> > &tileList);

        /**
        * Check if %Raster tile data is on the OpenCL device.
        * @return true if Raster tile data is on the OpenCL device.
        */
        bool tileDataOnDevice(uint32_t ti , uint32_t tj);

        /**
        * Check if %Raster has data.
        * @return true if Raster contains data.
        */
        bool hasData() { return !tiles.empty(); }

        // Delete tile data
        void deleteRasterData();

        // Map vector onto raster
        void mapVector(Vector<CTYPE> &v, 
            std::string script = std::string(), 
            size_t parameters = GeometryType::All,
            std::string widthPropertyName = std::string(),
            std::string levelPropertyName = std::string());

        void mapVector(Vector<CTYPE> &v, 
            size_t parameters = GeometryType::All,
            std::string widthPropertyName = std::string(), 
            std::string levelPropertyName = std::string()) {
            mapVector(v, std::string(), parameters, widthPropertyName, levelPropertyName);
        }
            
        // Map vector onto raster tile
        void mapTileVector(uint32_t ti, uint32_t tj, Vector<CTYPE> &v,
            size_t parameters = GeometryType::All,
            std::string widthPropertyName = std::string(), 
            std::string levelPropertyName = std::string());

        void rasterise(Vector<CTYPE> &v, 
            std::string script = std::string(),
            size_t parameters = GeometryType::All,
            std::string levelPropertyName = std::string());

        std::vector<RasterIndex<CTYPE> > getVectorTileIndexes(Vector<CTYPE> &v, 
            uint32_t ti, uint32_t tj, std::size_t clHash, cl_uchar parameters = 0x03);

        // Vectorise raster
        Vector<CTYPE> cellCentres(bool mapValues = false);
        Vector<CTYPE> cellPolygons(bool mapValues = false);
        Vector<CTYPE> vectorise(std::vector<RTYPE> countourValues, RTYPE noDataValue = getNullValue<RTYPE>());

        // Friend functions
        friend bool operator==<RTYPE, CTYPE>(const Raster<RTYPE, CTYPE> &, const Raster<RTYPE, CTYPE> &);

        void read(std::string fileName, std::string jsonConfig = ""); //< Read file to %Raster
        void write(std::string fileName, std::string jsonConfig = ""); //< Write %Raster to file

        /**
        * Set input file handler
        * @param fileHandler to set.
        */
        void setFileInputHandler(std::shared_ptr<RasterFileHandler<RTYPE, CTYPE> > fileHandlerIn_) {
            fileHandlerIn = fileHandlerIn_;
        }

        /**
        * Get input file handler
        * @return fileHandler.
        */
        const std::shared_ptr<RasterFileHandler<RTYPE, CTYPE> > &getFileInputHandler() const {
            return fileHandlerIn;
        }

        /**
        * Directly set output file handler
        * @param fileHandler to set.
        */
        void setFileOutputHandler(std::shared_ptr<RasterFileHandler<RTYPE, CTYPE> > fileHandlerOut_) {
            fileHandlerOut = fileHandlerOut_;
        }

        /**
        * Get input file handler
        * @return fileHandler.
        */
        const std::shared_ptr<RasterFileHandler<RTYPE, CTYPE> > &getFileOutputHandler() const {
            return fileHandlerOut;
        }

        /**
        * Get whether %Raster needs continuous data at boundaries.
        * @return true if continuous data is needed, false otherwise.
        */
        uint8_t getRequiredNeighbours() { return requiredNeighbours; }

        /**
        * Set whether %Raster needs continuous data at boundaries.
        */
        void setRequiredNeighbours(uint8_t requiredNeighbours_) { requiredNeighbours = requiredNeighbours_; }

        /**
        * Get whether %Raster requires reductions.
        * @return true if reductions are needed, false otherwise.
        */
        Reduction::Type getReductionType() { return reductionType; }

        /**
        * Set whether %Raster requires reductions.
        */
        void setReductionType(Reduction::Type reductionType_) { reductionType = reductionType_; }

        /**
        * Get whether %Raster requires status.
        * @return true if status are needed, false otherwise.
        */
        bool getNeedsStatus() { return needsStatus; }

        /**
        * Set whether %Raster requires status.
        */
        void setNeedsStatus(bool needsStatus_) { needsStatus = needsStatus_; }
        
        /**
        * Get whether %Raster needs data initially read in kernel.
        * @return true if data is needed, false otherwise.
        */
        bool getNeedsRead() { return needsRead; }

        /**
        * Set whether %Raster requires data initially read in kernel.
        */
        void setNeedsRead(bool needsRead_) { needsRead = needsRead_; }

        /**
        * Get whether %Raster needs data written back in kernel.
        * @return true if write data is needed, false otherwise.
        */
        bool getNeedsWrite() { return needsWrite; }

        /**
        * Set whether %Raster requires write in kernel.
        */
        void setNeedsWrite(bool needsWrite_) { needsWrite = needsWrite_; }
        
        static RasterDataType::Type getRasterDataType();
        
        /**
         * close files under file handler
        */
        void closeFile();

    protected:

        // Tile data
        std::vector<TilePtr<RTYPE, CTYPE> > tiles; ///< %Tile array 
        uint8_t requiredNeighbours;                ///< Raster neighbours need to be taken into account during tile handling
        bool needsStatus;                          ///< Whether status bits need to be taken into account during tile handling
        bool needsRead;                            ///< Whether data needs to be initially read in kernel
        bool needsWrite;                           ///< Whether data needs to be written back to buffer in kernel
        std::shared_ptr<cl::Buffer> nullBuffer;    ///< Special buffer containing null values for boundaries
        std::shared_ptr<cl::Buffer> randomBuffer;  ///< Special buffer containing random number generator values
        cl::Buffer randomBufferSnapshot;           ///< Special buffer containing random buffer state
        Reduction::Type reductionType;             ///< Type of reduction required for %Raster
        
        // Tile access
        Tile<RTYPE, CTYPE> &getTile(uint32_t ti, uint32_t tj);

        // Raster file handlers
        std::shared_ptr<RasterFileHandler<RTYPE, CTYPE> > fileHandlerIn;
        std::shared_ptr<RasterFileHandler<RTYPE, CTYPE> > fileHandlerOut;

        // Buffer from region operation
        bool createRegionBuffer(BoundingBox<CTYPE> bounds, cl::Buffer &rasterRegionBuffer, RasterDimensions<CTYPE> &rasterRegionDim);
    };
}

#endif
