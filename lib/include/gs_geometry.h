/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
#ifndef GEOSTACK_GEOMETRY_H
#define GEOSTACK_GEOMETRY_H

#include <memory>
#include <vector>

#include "gs_opencl.h"

namespace Geostack
{
    // Forward declarations
    template <typename T>
    class Coordinate;
    
    template <typename T>
    class BoundingBox;

    template <typename T>
    class GeometryBase;

    template <typename T>
    class Geometry;

    template <typename T>
    class RTreeNode;

    template <typename T>
    class RTree;

    template <typename T>
    struct ProjectionParameters;

    template <typename T>
    class Vector;

    // Aliases
    template <typename T>
    using GeometryBasePtr = std::shared_ptr<GeometryBase<T> >;

    template <typename T>
    using RTreeNodePtr = std::shared_ptr<RTreeNode<T> >;
    
    template <typename T>
    using CoordinateList = std::vector<Coordinate<T> >;

    /**
    * Geometry types
    */
    namespace GeometryType {
        enum Type {
            None = 0,              ///< No type
            Point = 1,             ///< Points
            LineString = 1 << 1,   ///< Line strings
            Polygon = 1 << 2,      ///< Polygons
            All = 0x07,            ///< Points, Line strings and Polygons
            Tile = 1 << 3,         ///< Tiles
        };
    }

    // General functions
    template <typename T>
    T interceptValue2D( 
        const T &x0, const T &y0, 
        const T &x1, const T &y1, 
        const T &x2, const T &y2, 
        const T &x3, const T &y3);

    template <typename T>
    T projectedValue2D( 
        const T &x0, const T &y0, 
        const T &x1, const T &y1, 
        const T &x2, const T &y2, 
        const T &x3, const T &y3);

    template <typename T>
    bool intercepts2D( 
        const T &x0, const T &y0, 
        const T &x1, const T &y1, 
        const T &x2, const T &y2, 
        const T &x3, const T &y3);

    template <typename T>
    bool intercepts2D( 
        const T &x0, const T &y0, 
        const T &x1, const T &y1, 
        const BoundingBox<T> &b);
        
    template <typename T>
    T distanceSqr2D(const T p0, const T q0, const T p1, const T q1);

    /**
    * %Coordinate class for two dimensional geospatial vector data.
    */
    template <typename T>
    class alignas(8) Coordinate {

    public:

        /**
        * Default constructor
        */
        Coordinate():
            p((T)0.0), q((T)0.0), r((T)0.0), s((T)0.0) { }

        /**
        * Copy constructor
        */
        Coordinate(const Coordinate<T> &c):
            p(c.p), q(c.q), r(c.r), s(c.s) { }

        /**
        * Assignment operator
        */
        Coordinate<T> &operator=(const Coordinate<T> &c) {
            if (&c != this) {
                p = c.p;
                q = c.q;
                r = c.r;
                s = c.s;
            }
            return *this;
        }

        /**
        * Two component constructor
        */
        Coordinate(T p_, T q_, T r_ = (T)0.0, T s_ = (T)0.0):
            p(p_), q(q_), r(r_), s(s_) { }

        // Coordinates
        T p, q, r, s;
        
        // Coordinate functions
        T magnitudeSquared();
        static Coordinate<T> max(const Coordinate<T> &a, const Coordinate<T> &b);
        static Coordinate<T> min(const Coordinate<T> &a, const Coordinate<T> &b);
        static Coordinate<T> centroid(const Coordinate<T> &a, const Coordinate<T> &b);
        static T distanceSqr(const Coordinate<T> &a, const Coordinate<T> &b);
        static T distanceSqr2D(const Coordinate<T> &a, const Coordinate<T> &b);

        // Geohash functions
        const static std::string geoHashEnc32;
        std::string getGeoHash();
    };

    // Coordinate operators
    template <typename T>
    bool operator==(const Coordinate<T> &a, const Coordinate<T> &b);

    template <typename T>
    bool operator!=(const Coordinate<T> &a, const Coordinate<T> &b);

    template <typename T>
    Coordinate<T> operator+(const Coordinate<T> &a, const Coordinate<T> &b);

    template <typename T>
    Coordinate<T> operator-(const Coordinate<T> &a, const Coordinate<T> &b);
    
    // Coordinate output stream
    template <typename CTYPE>
    std::ostream &operator<<(std::ostream &os, const Coordinate<CTYPE> &c);

    /**
    * %BoundingBox class for two dimensional geospatial vector data.
    * Contains a pair of min and max coordinates.
    */
    template <typename T>
    class BoundingBox {
    
    public:

        // Constructors
        BoundingBox();
        BoundingBox(const BoundingBox<T> &A);
        BoundingBox(Coordinate<T>, Coordinate<T>);

        // Assignment operator
        BoundingBox &operator=(const BoundingBox &v);

        // Coordinate pair
        Coordinate<T> min, max;

        // Update
        void reset();
        void extend2D(T);
        void extend(Coordinate<T>);
        void extend(const BoundingBox<T> &);
        BoundingBox<T> intersect(const BoundingBox<T> &);

        // Reproject
        BoundingBox<T> convert(ProjectionParameters<double> to, ProjectionParameters<double> from);

        // Metrics
        T area2D();
        T centroidDistanceSqr(const Coordinate<T> &A);
        T centroidDistanceSqr(const BoundingBox<T> &A);
        T minimumDistanceSqr(const BoundingBox<T> &A);
        Coordinate<T> centroid() const;
        Coordinate<T> extent() const;
        uint64_t createZIndex2D(Coordinate<T> c) const;
        uint64_t createZIndex4D(Coordinate<T> c) const;
        uint64_t quadrant(Coordinate<T> c) const;

        // Intersection tests
        bool contains(const Coordinate<T> c) const {
            return boundingBoxContains(*this, c);
        }
        bool contains2D(const Coordinate<T> c) const {
            return boundingBoxContains2D(*this, c);
        }
        static bool boundingBoxContains(const BoundingBox<T> A, const  Coordinate<T> c);
        static bool boundingBoxContains2D(const BoundingBox<T> A, const  Coordinate<T> c);
        static bool boundingBoxContains(const BoundingBox<T> A, const  BoundingBox<T> B);
        static bool boundingBoxContains2D(const BoundingBox<T> A, const  BoundingBox<T> B);
        static bool boundingBoxIntersects(const BoundingBox<T> A, const  BoundingBox<T> B);
        
        // Geohash bounding box
        const static BoundingBox<T> geoHashBounds;

        // Vector bounding box
        Vector<T> toVector();
    };

    // BoundingBox operators
    template <typename T>
    bool operator==(const BoundingBox<T> &a, const BoundingBox<T> &b);

    template <typename T>
    bool operator!=(const BoundingBox<T> &a, const BoundingBox<T> &b);

    /**
    * %Geometry class for two dimensional geometry objects.
    */
    template <typename T>
    class GeometryBase {
        public:

            // Constructors
            GeometryBase() { }
            virtual ~GeometryBase() { }

            // Get bounds
            virtual BoundingBox<T> getBounds() const = 0;
            virtual Coordinate<T> getCentroid() const = 0;

            // Get type
            virtual bool isType(size_t type) const {
                return false;
            }
            
            /**
            * Check if %Geometry is a container rather than a set of objects.
            * @return true if %Geometry is a container.
            */
            virtual bool isContainer() const {
                return false;
            }
            
            /**
            * Get unique identifier of Geometry.
            * @return identifier.
            */
            virtual cl_uint getID() const {
                return 0;
            }
    };

    /**
    * %Box class to hold coordinate pair
    * Contains only a coordinate pair
    */
    template <typename T>
    class Box : public GeometryBase<T> {

        public:

            // Constructors
            Box();
            Box(BoundingBox<T>);
            virtual ~Box() { }
            
            // Get bounds and centroid
            BoundingBox<T> getBounds() const override;
            Coordinate<T> getCentroid() const override;

        protected:

            BoundingBox<T> bounds; ///< Bounding box
    };

    /**
    * %RTreeNode class to hold nodes of %RTree.
    * This class holds a hierarchical structure of nested 
    * bounding boxes for logarithmic spatial searches.
    */
    template <typename T>
    class RTreeNode : public Box<T> {

        using Box<T>::bounds;

        public:

            friend class RTree<T>;

            // Constructors
            RTreeNode() { }
            virtual ~RTreeNode() { }

            /**
            * Check if %Geometry is a container rather than a set of objects.
            * @return true for RTreeNodes.
            */
            bool isContainer() const {
                return true;
            }
            
            // Adjust bounds to fit given coordinate
            void fitBounds(const Coordinate<T> &B);

            // Adjust bounds to fit given bounds
            void fitBounds(const BoundingBox<T> &B);

            // Adjust bounds to fit items in nodes
            void fitBoundsToNodes();

        private:

            std::vector<GeometryBasePtr<T> > nodes; ///< Tree nodes

    };

    /**
    * %RTree class for spatial indexing of %Vector.
    * This class holds a hierarchical structure of nested 
    * bounding boxes for logarithmic spatial searches.
    */
    template <typename T>
    class RTree {

        public:
        
            // Constructor
            RTree();

            // Clear tree
            void clear();

            // Insert geometry into tree
            void insert(GeometryBasePtr<T> gty); 

            // Search tree
            void search(BoundingBox<T> bounds, std::vector<GeometryBasePtr<T> > &searchGeometry,
                size_t types, RTreeNodePtr<T> node = nullptr);

            // Find nearest geometry
            void nearest(BoundingBox<T> bounds, std::vector<GeometryBasePtr<T> > &nearestGeometry, size_t types);

            /**
            * Get bounds of %RTree.
            * @return bounds of %RTree.
            */
            BoundingBox<T> getBounds() const {
                return root->getBounds();
            }

        private:        

            RTreeNodePtr<T> root; ///< Root node of tree
            const static int maxItems; ///< Maximum items per tree node   
    };

}

#endif