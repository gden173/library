// Interleave 3D buffers of the form A...B...C... to ABC.........

/**
* Interleave buffer with float value
*/
__kernel void interleave_REAL(
    __global float *_in,
    __global float *_out,
    const float _noDataValue
) {
    const size_t _i = get_global_id(0);
    const size_t _k = get_global_id(1);
    const size_t _Ni = get_global_size(0);
    const size_t _Nk = get_global_size(1);
    float _v = *(_in+_i+_k*_Ni);
    *(_out+_i*_Nk+_k) = isValid_REAL(_v) ? _v : _noDataValue;
}

/**
* Interleave buffer with char value
*/
__kernel void interleave_CHAR(
    __global char *_in,
    __global char *_out,
    const char _noDataValue
) {
    const size_t _i = get_global_id(0);
    const size_t _k = get_global_id(1);
    const size_t _Ni = get_global_size(0);
    const size_t _Nk = get_global_size(1);
    *(_out+_i*_Nk+_k) = *(_in+_i+_k*_Ni);
}

/**
* Interleave buffer with unsigned char value
*/
__kernel void interleave_UCHAR(
    __global uchar *_in,
    __global uchar *_out,
    const uchar _noDataValue
) {
    const size_t _i = get_global_id(0);
    const size_t _k = get_global_id(1);
    const size_t _Ni = get_global_size(0);
    const size_t _Nk = get_global_size(1);
    *(_out+_i*_Nk+_k) = *(_in+_i+_k*_Ni);
}

/**
* Interleave buffer with int value
*/
__kernel void interleave_INT(
    __global int *_in,
    __global int *_out,
    const int _noDataValue
) {
    const size_t _i = get_global_id(0);
    const size_t _k = get_global_id(1);
    const size_t _Ni = get_global_size(0);
    const size_t _Nk = get_global_size(1);
    int _v = *(_in+_i+_k*_Ni);
    *(_out+_i*_Nk+_k) = isValid_INT(_v) ? _v : _noDataValue;
}

/**
* Interleave buffer with unsigned int value
*/
__kernel void interleave_UINT(
    __global uint *_in,
    __global uint *_out,
    const uint _noDataValue
) {
    const size_t _i = get_global_id(0);
    const size_t _k = get_global_id(1);
    const size_t _Ni = get_global_size(0);
    const size_t _Nk = get_global_size(1);
    uint _v = *(_in+_i+_k*_Ni);
    *(_out+_i*_Nk+_k) = isValid_UINT(_v) ? _v : _noDataValue;
}