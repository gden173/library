/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

 #ifdef INOPENCL

// OpenCL build specific
#define CTYPE REAL

#ifndef Coordinate_DEF
#define Coordinate_DEF
typedef struct CoordinateStruct {

    // Coordinate pair
    CTYPE p, q, r, s;
    
} __attribute__ ((aligned (8))) Coordinate;
#endif

typedef struct ProjectionParametersStruct {
    
    uchar type;    // Projection type:
                   //    1. Projection coordinate system
                   //    2. Geographic latitude-longitude system
    uchar cttype;  // Coordinate transformation type:
                   //    1. Transverse Mercator
                   //    7. Mercator
                   //    8. Lambert conformal conic (2 point)
                   //   11. Albers equal area 
                   //   24. Sinusoidal
    CTYPE a;       // Equatorial radius
    CTYPE f;       // Flattening
    CTYPE x0;      // Central meridian longitude
    CTYPE k0;      // Central meridian scale factor
    CTYPE fe;      // False easting
    CTYPE fn;      // False northing
    CTYPE phi_0;   // Origin latitude
    CTYPE phi_1;   // 1st standard latitude
    CTYPE phi_2;   // 2nd standard latitude

} __attribute__ ((aligned (8))) ProjectionParameters;

#else

// C++ build specific
#include <cmath>

#define degrees_DEF(A) degrees((A))
#define radians_DEF(A) radians((A))
#define sqrt_DEF(A) sqrt((A))
#define sin_DEF(A) sin((A))
#define cos_DEF(A) cos((A))
#define tan_DEF(A) tan((A))
#define asin_DEF(A) asin((A))
#define acos_DEF(A) acos((A))
#define atan_DEF(A) atan((A))
#define atan2_DEF(A, B) atan2((A), (B))
#define log_DEF(A) log((A))
#define pow_DEF(A, B) pow((A), (B))
#define mad_DEF(A, B, C) ((A)*(B)+(C))

#define USE_PROJ_ALL

namespace Geostack
{
namespace ProjectionCalculation
{

// Missing functions
template<typename CTYPE>
CTYPE radians(CTYPE angle) { return (CTYPE)(M_PI*angle/180.0); }

template<typename CTYPE>
CTYPE degrees(CTYPE angle) { return (CTYPE)(180.0*angle/M_PI); }

typedef unsigned int uint;

#endif

#ifdef USE_PROJ_ALL
#define USE_PROJ_TYPE_1
#define USE_PROJ_TYPE_7
#define USE_PROJ_TYPE_8
#define USE_PROJ_TYPE_11
#define USE_PROJ_TYPE_24
#endif

/**	
* Returns coefficients for fractional of distance along the meridian from the equator to latitude phi
*   d = a m
* where d is distance and a is radius and m is:
*   m = M0*phi-M2*sin(2 phi)+M4*sin(4 phi)-M6*sin(6 phi)
*/

#if defined(USE_PROJ_TYPE_1) || defined(USE_PROJ_TYPE_24)

#ifdef INOPENCL
void mc(CTYPE e2, CTYPE *M0, CTYPE *M2, CTYPE *M4, CTYPE *M6) {
#else
template<typename CTYPE>
void mc(CTYPE e2, CTYPE *M0, CTYPE *M2, CTYPE *M4, CTYPE *M6) {
#endif
    CTYPE e4 = e2*e2;
    *M0 = 1.0-e2*(0.25+e2*(0.046875+e2*0.01953125));
    *M2 = 0.375*e2*(1.0+e2*0.25+e4*0.1171875);
    *M4 = 0.05859375*e4*(1.0+0.75*e2);
    *M6 = 35.0*e2*e4/3072.0;
}

#endif

/**	
* Returns foot-point latitude
*/

#if defined(USE_PROJ_TYPE_1) || defined(USE_PROJ_TYPE_24)

#ifdef INOPENCL
CTYPE fp(CTYPE e2, CTYPE a, CTYPE m) {
#else
template<typename CTYPE>
CTYPE fp(CTYPE e2, double a, CTYPE m) {
#endif
    CTYPE es = sqrt_DEF(1.0-e2);	
    CTYPE e1 = (1.0-es)/(1.0+es);
    CTYPE e12 = e1*e1;	
    CTYPE e13 = e12*e1;	
    CTYPE e14 = e12*e12;	
    CTYPE M0 = 1.0-e2*(0.25+e2*(0.046875+e2*0.01953125));
    CTYPE mu = m/(a*M0);
    return mu+ 	
        sin_DEF(2.0*mu)*((3.0*e1/2.0)-(27.0*e13/32.0))+	
        sin_DEF(4.0*mu)*((21.0*e12/16.0)-(55*e14/32.0))+	
        sin_DEF(6.0*mu)*(151.0*e13/96.0)+	
        sin_DEF(8.0*mu)*(1097.0*e14/512.0);	
}

#endif

/**	
* Convert from Transverse Mercator projection to ellipsoid reference system 	
* Map projections - a working manual, p.48
*/
#ifdef INOPENCL
Coordinate fn_Transverse_Mercator_to_Ellipsoid(Coordinate c, const ProjectionParameters *proj) {
#else
template<typename CTYPE>
Coordinate<CTYPE> fn_Transverse_Mercator_to_Ellipsoid(Coordinate<CTYPE> c, const ProjectionParameters<double> *proj) {
#endif

#ifdef USE_PROJ_TYPE_1

    // Initialise variables
    CTYPE x = c.p-proj->fe;           // Adjust for false easting 
    CTYPE y = c.q-proj->fn;           // Adjust for false northing 
    CTYPE e2 = (2.0-proj->f)*proj->f; // Eccentricity squared	
    CTYPE e2p = e2/(1.0-e2);

    // Length of arc of meridian
    CTYPE m0 = 0.0;
    if (proj->phi_0 != 0.0) {
        CTYPE M0, M2, M4, M6;
        mc(e2, &M0, &M2, &M4, &M6);
        CTYPE phi0 = radians_DEF(proj->phi_0); // Convert origin latitude to radians 
        m0 = proj->a*(M0*phi0-M2*sin_DEF(2.0*phi0)+M4*sin_DEF(4.0*phi0)-M6*sin_DEF(6.0*phi0));
    }

    // Foot-point latitude    
    CTYPE m = m0+(y/proj->k0);
    CTYPE phid = fp(e2, proj->a, m);

    // Specify parameters
    CTYPE t_phi = tan_DEF(phid);
    CTYPE s_phi = sin_DEF(phid);
    CTYPE c_phi = cos_DEF(phid);
    CTYPE s_phi2 = s_phi*s_phi;

    CTYPE T = t_phi*t_phi;
    CTYPE C = e2p*(1.0-s_phi2);
    CTYPE N = proj->a/sqrt_DEF(1.0-e2*s_phi2);
    CTYPE R = proj->a*(1.0-e2)/pow_DEF((1.0-e2*s_phi2), 1.5);	
    CTYPE D = x/(proj->k0*N);

    // Cache pre-computed values
    CTYPE D2 = D*D;
    CTYPE D4 = D2*D2;
    CTYPE D6 = D2*D4;

    CTYPE phi1 = (D2/2.0);
    CTYPE phi2 = (D4/24.0)*(5.0+3.0*T+10.0*C-4.0*C*C-9.0*e2p);
    CTYPE phi3 = (D6/720.0)*(61.0+90.0*T+298.0*C+45.0*T*T-252.0*e2p-3.0*C*C);
	
    CTYPE lambda1 = (D2/6.0)*(1.0+2.0*T+C);
    CTYPE lambda2 = (D4/120.0)*(5.0-2.0*C+28.0*T-3.0*C*C+8.0*e2p+24.0*T*T);

    // Return projected coordinate
    c.p = proj->x0+degrees_DEF((D/c_phi)*(1.0-lambda1+lambda2));
    c.q = degrees_DEF(phid-(N*t_phi/R)*(phi1-phi2+phi3));

#endif

    return c;
}

/**	
* Convert from ellipsoid reference system to Transverse Mercator projection 
* Map projections - a working manual, p.48
*/

#ifdef INOPENCL
Coordinate fn_Ellipsoid_to_Transverse_Mercator(Coordinate c, const ProjectionParameters *proj) {
#else
template<typename CTYPE>
Coordinate<CTYPE> fn_Ellipsoid_to_Transverse_Mercator(Coordinate<CTYPE> c, const ProjectionParameters<double> *proj) {
#endif

#ifdef USE_PROJ_TYPE_1

    // Initialise variables	
    CTYPE lambda = radians_DEF(c.p-proj->x0); // Convert longitude to radians 
    CTYPE phi = radians_DEF(c.q);             // Convert latitude to radians 
    CTYPE e2 = (2.0-proj->f)*proj->f;         // Eccentricity squared	
   
    // Specify parameters
    CTYPE t_phi = tan_DEF(phi);
    CTYPE c_phi = cos_DEF(phi);
    CTYPE s_phi = sin_DEF(phi);
    CTYPE s_phi2 = s_phi*s_phi;

    CTYPE e2p = e2/(1.0-e2);
    CTYPE N = 1.0/sqrt_DEF(1.0-e2*s_phi2);
    CTYPE C = e2p*(1.0-s_phi2);
    CTYPE A = lambda*c_phi;

    // Cache pre-computed values
    CTYPE T = t_phi*t_phi;
    CTYPE A2 = A*A;
    CTYPE A4 = A2*A2;
    CTYPE A6 = A2*A4;
    
    // Length of arc of meridian
    CTYPE M0, M2, M4, M6;
    mc(e2, &M0, &M2, &M4, &M6);
    CTYPE m = M0*phi-M2*sin_DEF(2.0*phi)+M4*sin_DEF(4.0*phi)-M6*sin_DEF(6.0*phi);
    CTYPE m0 = 0.0;
    if (proj->phi_0 != 0.0) {
        CTYPE phi0 = radians_DEF(proj->phi_0);
        m0 = M0*phi0-A2*sin_DEF(2.0*phi0)+A4*sin_DEF(4.0*phi0)-M6*sin_DEF(6.0*phi0);
    }

    CTYPE E1 = (A2/6.0)*(1.0-T+C);
    CTYPE E2 = (A4/120.0)*(5.0-18.0*T+T*T+72.0*C-58.0*e2p);

    CTYPE N1 = (A2/2.0);
    CTYPE N2 = (A4/24.0)*(5.0-T+9.0*C+4.0*C*C);
    CTYPE N3 = (A6/720.0)*(61.0-58.0*T+T*T+600.0*C-330*e2p);

    // Return projected coordinate 
    c.p = mad_DEF(proj->a, proj->k0*N*A*(1.0+E1+E2), proj->fe);
    c.q = mad_DEF(proj->a, proj->k0*(m-m0+N*t_phi*(N1+N2+N3)), proj->fn); 
    
#endif

    return c;
}

/**	
*Convert from Lambert Conic projection to ellipsoid reference system 
**/	
#ifdef INOPENCL
Coordinate fn_Lambert_to_Ellipsoid(Coordinate c, const ProjectionParameters *proj) {
#else
template<typename CTYPE>
Coordinate<CTYPE> fn_Lambert_to_Ellipsoid(Coordinate<CTYPE> c, const ProjectionParameters<double> *proj) {
#endif

#ifdef USE_PROJ_TYPE_8

    // Initialise variables	
    CTYPE x = c.p-proj->fe;           // Adjust for false easting 
    CTYPE y = c.q-proj->fn;           // Adjust for false northing  
    CTYPE e2 = (2.0-proj->f)*proj->f; // Eccentricity squared	
    CTYPE e = sqrt_DEF(e2);           // Eccentricity	
    CTYPE e4 = e2*e2;                 // Eccentricity^4
    CTYPE e6 = e2*e2*e2;              // Eccentricity^6
    CTYPE e8 = e4*e4;                 // Eccentricity^8	
   
    CTYPE s_phi_0 = sin_DEF(radians_DEF(proj->phi_0));
    CTYPE s_phi_1 = sin_DEF(radians_DEF(proj->phi_1));
    CTYPE s_phi_2 = sin_DEF(radians_DEF(proj->phi_2));
   
    CTYPE es_phi_1_2 = e2*s_phi_1*s_phi_1;
    CTYPE es_phi_2_2 = e2*s_phi_2*s_phi_2;

    CTYPE m1 = cos_DEF(radians_DEF(proj->phi_1))/sqrt_DEF(1.0-es_phi_1_2);
    CTYPE m2 = cos_DEF(radians_DEF(proj->phi_2))/sqrt_DEF(1.0-es_phi_2_2);

    CTYPE t0 = sqrt_DEF(((1.0-s_phi_0)/(1.0+s_phi_0))*pow_DEF((CTYPE)((1.0+e*s_phi_0)/(1.0-e*s_phi_0)), e));
    CTYPE t1 = sqrt_DEF(((1.0-s_phi_1)/(1.0+s_phi_1))*pow_DEF((CTYPE)((1.0+e*s_phi_1)/(1.0-e*s_phi_1)), e));
    CTYPE t2 = sqrt_DEF(((1.0-s_phi_2)/(1.0+s_phi_2))*pow_DEF((CTYPE)((1.0+e*s_phi_2)/(1.0-e*s_phi_2)), e));
    
    CTYPE n = (log_DEF(m1)-log_DEF(m2))/(log_DEF(t1)-log_DEF(t2));
    CTYPE F = m1/(n*pow_DEF(t1, n));
    CTYPE rho_0 = proj->a*F*pow_DEF(t0, n);
    
    CTYPE rho_y = rho_0-y;
    CTYPE rho = sqrt_DEF(x*x+rho_y*rho_y); if (n < 0.0) rho = -rho;
    CTYPE t = pow_DEF((CTYPE)(rho/(proj->a*F)), (CTYPE)(1.0/n));
    CTYPE theta = atan_DEF(x/rho_y);
    CTYPE chi = M_PI_2-2.0*atan_DEF(t);

    CTYPE phi = chi+
        ((e2/2.0)+(5.0*e4/24.0)+(e6/12.0)+(13.0*e8/360.0))*sin_DEF(2.0*chi)+
        ((7.0*e4/48.0)+(29.0*e6/240.0)+(811.0*e8/11520.0))*sin_DEF(4.0*chi)+
        ((7.0*e6/120.0)+(81.0*e8/1120.0))*sin_DEF(6.0*chi)+
        (4279.0*e8/161280.0)*sin_DEF(8.0*chi);

    // Return projected coordinate
    c.p = proj->x0+degrees_DEF(theta/n);
    c.q = degrees_DEF(phi);	

#endif

    return c;
}

/**	
* Convert from ellipsoid reference system to Lambert Conic projection
*/	
#ifdef INOPENCL
Coordinate fn_Ellipsoid_to_Lambert(Coordinate c, const ProjectionParameters *proj) {
#else
template<typename CTYPE>
Coordinate<CTYPE> fn_Ellipsoid_to_Lambert(Coordinate<CTYPE> c, const ProjectionParameters<double> *proj) {
#endif

#ifdef USE_PROJ_TYPE_8

    // Initialise variables	
    CTYPE lambda = radians_DEF(c.p-proj->x0); // Convert longitude to radians 
    CTYPE phi = radians_DEF(c.q);             // Convert latitude to radians  
    CTYPE e2 = (2.0-proj->f)*proj->f;         // Eccentricity squared	
    CTYPE e = sqrt_DEF(e2);                   // Eccentricity		

    CTYPE s_phi = sin_DEF(phi);
    CTYPE s_phi_0 = sin_DEF(radians_DEF(proj->phi_0));
    CTYPE s_phi_1 = sin_DEF(radians_DEF(proj->phi_1));
    CTYPE s_phi_2 = sin_DEF(radians_DEF(proj->phi_2));
   
    CTYPE es_phi_1_2 = e2*s_phi_1*s_phi_1;
    CTYPE es_phi_2_2 = e2*s_phi_2*s_phi_2;
   
    CTYPE m1 = cos_DEF(radians_DEF(proj->phi_1))/sqrt_DEF(1.0-es_phi_1_2);
    CTYPE m2 = cos_DEF(radians_DEF(proj->phi_2))/sqrt_DEF(1.0-es_phi_2_2);

    CTYPE t = sqrt_DEF(((1.0-s_phi)/(1.0+s_phi))*pow_DEF((CTYPE)((1.0+e*s_phi)/(1.0-e*s_phi)), e));
    CTYPE t0 = sqrt_DEF(((1.0-s_phi_0)/(1.0+s_phi_0))*pow_DEF((CTYPE)((1.0+e*s_phi_0)/(1.0-e*s_phi_0)), e));
    CTYPE t1 = sqrt_DEF(((1.0-s_phi_1)/(1.0+s_phi_1))*pow_DEF((CTYPE)((1.0+e*s_phi_1)/(1.0-e*s_phi_1)), e));
    CTYPE t2 = sqrt_DEF(((1.0-s_phi_2)/(1.0+s_phi_2))*pow_DEF((CTYPE)((1.0+e*s_phi_2)/(1.0-e*s_phi_2)), e));
    
    CTYPE n = (log_DEF(m1)-log_DEF(m2))/(log_DEF(t1)-log_DEF(t2));
    CTYPE F = m1/(n*pow_DEF(t1, n));
    CTYPE rho_0 = pow_DEF(t0, n);
    CTYPE rho = pow_DEF(t, n);
    CTYPE theta = n*lambda;

    // Return projected coordinate
    c.p = mad_DEF(proj->a, F*rho*sin_DEF(theta), proj->fe);
    c.q = mad_DEF(proj->a, F*(rho_0-rho*cos_DEF(theta)), proj->fn);	

#endif

    return c; 
}

/**	
* Convert from Albers projection to ellipsoid reference system 
*/	
#ifdef INOPENCL
Coordinate fn_Albers_to_Ellipsoid(Coordinate c, const ProjectionParameters *proj) {
#else
template<typename CTYPE>
Coordinate<CTYPE> fn_Albers_to_Ellipsoid(Coordinate<CTYPE> c, const ProjectionParameters<double> *proj) {
#endif

#ifdef USE_PROJ_TYPE_11

    // Initialise variables	
    CTYPE x = c.p-proj->fe;           // Adjust for false easting 
    CTYPE y = c.q-proj->fn;           // Adjust for false northing  
    CTYPE e2 = (2.0-proj->f)*proj->f; // Eccentricity squared	
    CTYPE e = sqrt_DEF(e2);           // Eccentricity	
    CTYPE e6 = e2*e2*e2;              // Eccentricity squared, cubed	
   
    CTYPE s_phi_0 = sin_DEF(radians_DEF(proj->phi_0));
    CTYPE s_phi_1 = sin_DEF(radians_DEF(proj->phi_1));
    CTYPE s_phi_2 = sin_DEF(radians_DEF(proj->phi_2));
   
    CTYPE es_phi_0_2 = e2*s_phi_0*s_phi_0;
    CTYPE es_phi_1_2 = e2*s_phi_1*s_phi_1;
    CTYPE es_phi_2_2 = e2*s_phi_2*s_phi_2;

    CTYPE alpha_0 = (1.0-e2)*(s_phi_0/(1.0-es_phi_0_2)-(0.5/e)*log_DEF((1.0-e*s_phi_0)/(1.0+e*s_phi_0)));
    CTYPE alpha_1 = (1.0-e2)*(s_phi_1/(1.0-es_phi_1_2)-(0.5/e)*log_DEF((1.0-e*s_phi_1)/(1.0+e*s_phi_1)));
    CTYPE alpha_2 = (1.0-e2)*(s_phi_2/(1.0-es_phi_2_2)-(0.5/e)*log_DEF((1.0-e*s_phi_2)/(1.0+e*s_phi_2)));

    CTYPE m1 = cos_DEF(radians_DEF(proj->phi_1))/sqrt_DEF(1.0-es_phi_1_2);
    CTYPE m2 = cos_DEF(radians_DEF(proj->phi_2))/sqrt_DEF(1.0-es_phi_2_2);

    CTYPE n = (m1*m1-m2*m2)/(alpha_2-alpha_1);
    CTYPE C = m1*m1+n*alpha_1;

    CTYPE rho_0 = proj->a*sqrt_DEF(C-n*alpha_0)/n;
    CTYPE rho = sqrt_DEF(x*x+(rho_0-y)*(rho_0-y));

    CTYPE theta = atan_DEF(x/(rho_0-y));
    CTYPE alpha = (C-(rho*rho*n*n/(proj->a*proj->a)))/n;
    CTYPE beta = asin_DEF(alpha/(1.0-((1.0-e2)/(2.0*e))*log_DEF((1.0-e)/(1.0+e))));

    CTYPE phi = beta+
        ((e2/3.0)+(31.0*e2*e2/180.0)+(517.0*e6/5040.0))*sin_DEF(2.0*beta)+
        ((23.0*e2*e2/360.0)+(251.0*e6/3780.0))*sin_DEF(4.0*beta)+
        (761.0*e6/45360.0)*sin_DEF(6.0*beta);

    // Return projected coordinate
    c.q = degrees_DEF(phi);	
    c.p = proj->x0+degrees_DEF(theta/n);	

#endif

    return c; 
}

/**	
* Convert from ellipsoid reference system to Albers projection
*/	
#ifdef INOPENCL
Coordinate fn_Ellipsoid_to_Albers(Coordinate c, const ProjectionParameters *proj) {
#else
template<typename CTYPE>
Coordinate<CTYPE> fn_Ellipsoid_to_Albers(Coordinate<CTYPE> c, const ProjectionParameters<double> *proj) {
#endif

#ifdef USE_PROJ_TYPE_11

    // Initialise variables	
    CTYPE lambda = radians_DEF(c.p-proj->x0); // Convert longitude to radians 
    CTYPE phi = radians_DEF(c.q);             // Convert latitude to radians  
    CTYPE e2 = (2.0-proj->f)*proj->f;         // Eccentricity squared	
    CTYPE e = sqrt_DEF(e2);                   // Eccentricity	
   
    CTYPE s_phi = sin_DEF(phi);
    CTYPE s_phi_0 = sin_DEF(radians_DEF(proj->phi_0));
    CTYPE s_phi_1 = sin_DEF(radians_DEF(proj->phi_1));
    CTYPE s_phi_2 = sin_DEF(radians_DEF(proj->phi_2));
   
    CTYPE es_phi_2 = e2*s_phi*s_phi;
    CTYPE es_phi_0_2 = e2*s_phi_0*s_phi_0;
    CTYPE es_phi_1_2 = e2*s_phi_1*s_phi_1;
    CTYPE es_phi_2_2 = e2*s_phi_2*s_phi_2;
   
    CTYPE alpha = (1.0-e2)*(s_phi/(1.0-es_phi_2)-(0.5/e)*log_DEF((1.0-e*s_phi)/(1.0+e*s_phi)));
    CTYPE alpha_0 = (1.0-e2)*(s_phi_0/(1.0-es_phi_0_2)-(0.5/e)*log_DEF((1.0-e*s_phi_0)/(1.0+e*s_phi_0)));
    CTYPE alpha_1 = (1.0-e2)*(s_phi_1/(1.0-es_phi_1_2)-(0.5/e)*log_DEF((1.0-e*s_phi_1)/(1.0+e*s_phi_1)));
    CTYPE alpha_2 = (1.0-e2)*(s_phi_2/(1.0-es_phi_2_2)-(0.5/e)*log_DEF((1.0-e*s_phi_2)/(1.0+e*s_phi_2)));

    CTYPE m1 = cos_DEF(radians_DEF(proj->phi_1))/sqrt_DEF(1.0-es_phi_1_2);
    CTYPE m2 = cos_DEF(radians_DEF(proj->phi_2))/sqrt_DEF(1.0-es_phi_2_2);

    CTYPE n = (m1*m1-m2*m2)/(alpha_2-alpha_1);
    CTYPE C = m1*m1+n*alpha_1;

    CTYPE rho_0 = sqrt_DEF(C-n*alpha_0);
    CTYPE rho = sqrt_DEF(C-n*alpha);
    CTYPE theta = n*lambda;
    
    // Return projected coordinate
    c.p = mad_DEF(proj->a, (rho*sin_DEF(theta))/n, proj->fe);
    c.q = mad_DEF(proj->a, (rho_0-rho*cos_DEF(theta))/n, proj->fn);

#endif

    return c; 
}

/**	  
* Convert from Mercator reference system to ellipsoid reference system  
*/ 
#ifdef INOPENCL
Coordinate fn_Mercator_to_Ellipsoid(Coordinate c, const ProjectionParameters *proj) {
#else
template<typename CTYPE>
Coordinate<CTYPE> fn_Mercator_to_Ellipsoid(Coordinate<CTYPE> c, const ProjectionParameters<double> *proj) {
#endif

#ifdef USE_PROJ_TYPE_7

    CTYPE x = c.p-proj->fe;           // Adjust for false easting 
    CTYPE y = c.q-proj->fn;           // Adjust for false northing  
    CTYPE e2 = (2.0-proj->f)*proj->f; // Eccentricity squared	
    CTYPE e4 = e2*e2;                 // Eccentricity^4
    CTYPE e6 = e2*e2*e2;              // Eccentricity^6
    CTYPE e8 = e4*e4;                 // Eccentricity^8	
    
    CTYPE t = exp(-y/proj->a);    
    CTYPE chi = M_PI_2-2.0*atan_DEF(t);

    CTYPE phi = chi+
        ((e2/2.0)+(5.0*e4/24.0)+(e6/12.0)+(13.0*e8/360.0))*sin_DEF(2.0*chi)+
        ((7.0*e4/48.0)+(29.0*e6/240.0)+(811.0*e8/11520.0))*sin_DEF(4.0*chi)+
        ((7.0*e6/120.0)+(81.0*e8/1120.0))*sin_DEF(6.0*chi)+
        (4279.0*e8/161280.0)*sin_DEF(8.0*chi);

    // Return projected coordinate
    c.p = proj->x0+degrees_DEF(c.p/proj->a);
    c.q = degrees_DEF(phi);	

#endif

    return c; 
}

/**	  
* Convert from ellipsoid reference system to Mercator reference system  
* Map projections - a working manual, p.44
*/
#ifdef INOPENCL
Coordinate fn_Ellipsoid_to_Mercator(Coordinate c, const ProjectionParameters *proj) {
#else
template<typename CTYPE>
Coordinate<CTYPE> fn_Ellipsoid_to_Mercator(Coordinate<CTYPE> c, const ProjectionParameters<double> *proj) {
#endif

#ifdef USE_PROJ_TYPE_7

    CTYPE e = sqrt_DEF((2.0-proj->f)*proj->f);    // Eccentricity	
    CTYPE es = e*sin_DEF(radians_DEF(c.q));
    CTYPE e1 = (1.0-es)/(1.0+es);

    // Return projected coordinate
    c.p = mad_DEF(proj->a, radians_DEF(c.p-proj->x0), proj->fe);
    c.q = mad_DEF(proj->a, log_DEF(tan_DEF(M_PI_4+(CTYPE)0.5*radians_DEF(c.q))*pow_DEF(e1, 0.5*e)), proj->fn);

#endif

    return c; 
}

/**	  
* Convert from Sinusoidal reference system to ellipsoid reference system  
* Map projections - a working manual, p.243
*/ 
#ifdef INOPENCL
Coordinate fn_Sinusoidal_to_Ellipsoid(Coordinate c, const ProjectionParameters *proj) {
#else
template<typename CTYPE>
Coordinate<CTYPE> fn_Sinusoidal_to_Ellipsoid(Coordinate<CTYPE> c, const ProjectionParameters<double> *proj) {
#endif

#ifdef USE_PROJ_TYPE_24

    // Initialise variables	
    CTYPE x = c.p-proj->fe;           // Adjust for false easting 
    CTYPE y = c.q-proj->fn;           // Adjust for false northing  
    CTYPE e2 = (2.0-proj->f)*proj->f; // Eccentricity squared	
    
    CTYPE phi = fp(e2, proj->a, y);
    CTYPE s_phi = sin_DEF(phi);

    // Return projected coordinate
    c.p = proj->x0+degrees_DEF(x*sqrt_DEF(1.0-e2*s_phi*s_phi)/(proj->a*cos_DEF(phi)));
    c.q = degrees_DEF(phi);

#endif

    return c; 
}

/**	  
* Convert from ellipsoid reference system to Sinusoidal reference system  
* Map projections - a working manual, p.243
*/
#ifdef INOPENCL
Coordinate fn_Ellipsoid_to_Sinusoidal(Coordinate c, const ProjectionParameters *proj) {
#else
template<typename CTYPE>
Coordinate<CTYPE> fn_Ellipsoid_to_Sinusoidal(Coordinate<CTYPE> c, const ProjectionParameters<double> *proj) {
#endif

#ifdef USE_PROJ_TYPE_24

    // Initialise variables	
    CTYPE lambda = radians_DEF(c.p-proj->x0); // Convert longitude to radians 
    CTYPE phi = radians_DEF(c.q);             // Convert latitude to radians  
    CTYPE e2 = (2.0-proj->f)*proj->f;         // Eccentricity squared	

    CTYPE s_phi = sin_DEF(phi);
    
    // Length of arc of meridian
    CTYPE M0, M2, M4, M6;
    mc(e2, &M0, &M2, &M4, &M6);
    CTYPE m = M0*phi-M2*sin_DEF(2.0*phi)+M4*sin_DEF(4.0*phi)-M6*sin_DEF(6.0*phi);
    
    // Return projected coordinate
    c.p = mad_DEF(proj->a, lambda*cos_DEF(phi)/sqrt_DEF(1.0-e2*s_phi*s_phi), proj->fe);
    c.q = mad_DEF(proj->a, m, proj->fn); 

#endif

    return c; 
}

#ifdef INOPENCL

/**
* Convert coordinate
*/
bool convert(
    Coordinate *_c,
    const ProjectionParameters *_to, 
    const ProjectionParameters *_from) {

    // Ensure both projections are valid
    if (_to->type == 0 || _from->type == 0) {
        return false;
    }

    // Projected to geographical transformation
    if (_from->type == 1) {
        
        switch (_from->cttype) {
        
            case 1:

                // Transverse Mercator
                *_c = fn_Transverse_Mercator_to_Ellipsoid(*_c, _from);
                break;

            case 7:

                // Mercator
                *_c = fn_Mercator_to_Ellipsoid(*_c, _from);
                break;

            case 8:

                // Lambert conformal conic (2 point)
                *_c = fn_Lambert_to_Ellipsoid(*_c, _from);
                break;

            case 11:

                // Albers equal area 
                *_c = fn_Albers_to_Ellipsoid(*_c, _from);
                break;

            case 24:

                // Sinusoidal
                *_c = fn_Sinusoidal_to_Ellipsoid(*_c, _from);
                break;

            default:

                // Unsupported transform 
                return false;
        }

    } else if (_from->type == 2) {

        // No operation
        
    } else {
    
        // Unsupported type 
        return false;
    }
        
    // Geographical to projected transformation
    if (_to->type == 1) {
        
        switch (_to->cttype) {

            case 1:

                // Transverse Mercator
                *_c = fn_Ellipsoid_to_Transverse_Mercator(*_c, _to);
                break;

            case 7:
            
                // Mercator
                *_c = fn_Ellipsoid_to_Mercator(*_c, _to);
                break;

            case 8:

                // Lambert conformal conic (2 point)
                *_c = fn_Ellipsoid_to_Lambert(*_c, _to);
                break;

            case 11:

                // Albers equal area 
                *_c = fn_Ellipsoid_to_Albers(*_c, _to);
                break;
                
            case 24:

                // Sinusoidal
                *_c = fn_Ellipsoid_to_Sinusoidal(*_c, _to);
                break;

            default:

                // Unsupported transform 
                return false;
        }

    } else if (_to->type == 2) {

        // No operation
        
    } else {
    
        // Unsupported type 
        return false;
    }
    
    return true;    
}

#else

// C++ build specific
}
}

#endif

