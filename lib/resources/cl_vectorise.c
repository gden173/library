/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */
 
// Unaligned access
#define _val2D(R, i, j) (*((R)+(i)+(j)*_dim##R.mx))
#define _val3D(R, i, j, k) (*((R)+(i)+((j)+(k)*_dim##R.my)*_dim##R.mx))

// Aligned access
#define _val2D_a(R, i, j) (*((R)+(i)+(j)*_dim.mx))
#define _val3D_a(R, i, j, k) (*((R)+(i)+((j)+(k)*_dim.my)*_dim.mx))

typedef struct DimensionsStruct {

    uint nx; ///< Number of cells in x dimension
    uint ny; ///< Number of cells in y dimension
    uint nz; ///< Number of cells in z dimension
    REAL hx; ///< Spacing in x dimension
    REAL hy; ///< Spacing in y dimension
    REAL hz; ///< Spacing in z dimension
    REAL ox; ///< Start coordinate in x dimension
    REAL oy; ///< Start coordinate in y dimension
    REAL oz; ///< Start coordinate in z dimension
    uint mx; ///< Number of cells stored in memory for x dimension
    uint my; ///< Number of cells stored in memory for y dimension

} __attribute__ ((aligned (8))) Dimensions;

/**
* Vectorise %Tile data, returning a closed contour.
* @param _u %Tile data.
* @param _v Array of contour values.
* @param _dim_u %Dimensions structure.
* @param _noDataValue Value to substitute for nodata.
* @param _c Array of coordinates.
* @param _r Array of cell indexes.
* @param _index Atomic index of current coordinate.
*/
__kernel void vectorise(
    const REAL v,
    __global REAL *_u,
    __global REAL *_uN,
    __global REAL *_uE,
    __global REAL *_uNE,
    const Dimensions _dim,
    const REAL _noDataValue,
    __global REALVEC2 *_c,
    __global uint *_r,
    __global uint *_index) {

    const size_t i = get_global_id(0);
    const size_t j = get_global_id(1); 

    // Check limits
    if (i >= _dim.nx || j >= _dim.ny) {
        return;
    }
    
    // Cell centred position 
    const REALVEC2 p = (REALVEC2)(
        ((REAL)i+0.5)*_dim.hx+_dim.ox, 
        ((REAL)j+0.5)*_dim.hy+_dim.oy);

    // Get values from tile
    REAL v0, v1, v2, v3;
    v0 = _val2D_a(_u, i, j);
    if (i == _dim.mx-1 && j == _dim.my-1) {

        // Corner
        v1 = _val2D_a(_uE, 0, j);
        v2 = _val2D_a(_uNE, 0, 0);
        v3 = _val2D_a(_uN, i, 0);

    } else if (i == _dim.mx-1) {

        // East boundary
        v1 = _val2D_a(_uE, 0, j);
        v2 = _val2D_a(_uE, 0, j+1);
        v3 = _val2D_a(_u, i, j+1);

    } else if (j == _dim.my-1) {

        // North boundary
        v1 = _val2D_a(_u, i+1, j);
        v2 = _val2D_a(_uN, i+1, 0);
        v3 = _val2D_a(_uN, i, 0);

    } else {

        // Internal
        v1 = _val2D_a(_u, i+1, j);
        v2 = _val2D_a(_u, i+1, j+1);
        v3 = _val2D_a(_u, i, j+1);
    }
    
    // Handle nodata 
    if (isValid_REAL(_noDataValue)) {
        if (isInvalid_REAL(v0)) v0 = _noDataValue;
        if (isInvalid_REAL(v1)) v1 = _noDataValue;
        if (isInvalid_REAL(v2)) v2 = _noDataValue;
        if (isInvalid_REAL(v3)) v3 = _noDataValue;
    }
    
    // Get values
    REAL vc0 = v0-v;
    REAL vc1 = v1-v;
    REAL vc2 = v2-v;
    REAL vc3 = v3-v;

    // Edge break coordinates
    REALVEC2 p01, p12, p23, p30;

    // Find edge breaks
    unsigned char edges = 0;
    if (vc0*vc1 <= 0.0 && vc0 != vc1) {
        p01 = (REALVEC2)(p.x+_dim.hx*vc0/(v0-v1), p.y);
        if (vc0 != 0.0) {
            edges |= 0x01;
        }
    }
    if (vc1*vc2 <= 0.0 && vc1 != vc2) {
        p12 = (REALVEC2)(p.x+_dim.hx, p.y+_dim.hy*vc1/(v1-v2));
        if (vc1 != 0.0) {
            edges |= 0x02;
        }
    }
    if (vc2*vc3 <= 0.0 && vc2 != vc3) {
        p23 = (REALVEC2)(p.x+_dim.hx*(1.0-vc2/(v2-v3)), p.y+_dim.hy);
        if (vc2 != 0.0) {
            edges |= 0x04;
        }
    }
    if (vc3*vc0 <= 0.0 && vc3 != vc0) {
        p30 = (REALVEC2)(p.x, p.y+_dim.hy*(1.0-vc3/(v3-v0)));
        if (vc3 != 0.0) {
            edges |= 0x08;
        }
    }
        
    // Add lines
    switch (edges) {

        case 0x05: { 
             
            uint k = atomic_add(_index, 2);
            _c[k  ] = p01;
            _c[k+1] = p23;
            _r[k  ] = i;
            _r[k+1] = j;
            } break;

        case 0x0A: { 
            
            uint k = atomic_add(_index, 2);
            _c[k  ] = p12;
            _c[k+1] = p30;
            _r[k  ] = i;
            _r[k+1] = j;
            } break;

        case 0x03: { 
            
            uint k = atomic_add(_index, 2);
            _c[k  ] = p01;
            _c[k+1] = p12;
            _r[k  ] = i;
            _r[k+1] = j;
            } break;

        case 0x06: {    
            
            uint k = atomic_add(_index, 2);
            _c[k  ] = p12;
            _c[k+1] = p23;
            _r[k  ] = i;
            _r[k+1] = j;
            } break;

        case 0x0C: { 
            
            uint k = atomic_add(_index, 2);
            _c[k  ] = p23;
            _c[k+1] = p30;
            _r[k  ] = i;
            _r[k+1] = j;
            } break;

        case 0x09: {  
            
            uint k = atomic_add(_index, 2);
            _c[k  ] = p30;
            _c[k+1] = p01;
            _r[k  ] = i;
            _r[k+1] = j;
            } break;

        case 0x0F: {
            
            uint k = atomic_add(_index, 4);
            if (v0*v2-v1*v3 > 0.0) {
                
                _c[k  ] = p01;
                _c[k+1] = p12;
                _c[k+2] = p23;
                _c[k+3] = p30;
                                
            } else {
                
                _c[k  ] = p12;
                _c[k+1] = p23;
                _c[k+2] = p30;
                _c[k+3] = p01;
            }
            _r[k  ] = i;
            _r[k+1] = j;
            _r[k+2] = i;
            _r[k+3] = j;
        } break;
    }
}

