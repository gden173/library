/**
* Multigrid initialisation
*/
__kernel void init(
/*__ARGS__*/
) {

    // Indexes
    const size_t _i = get_global_id(0);
    const size_t _j = get_global_id(1);
    
    // Cell centred position
    const REAL x = ((REAL)_i+0.5)*_dim.hx+_dim.ox;
    const REAL y = ((REAL)_j+0.5)*_dim.hy+_dim.oy;

/*__VARS__*/

    // ---------------------------------
    // User defined code
/*__CODE__*/
    // ---------------------------------

    // Ensure forcing is valid
    if (b != b) {
        b = 0.0;
    }

    // Reset solution
    l0 = 0.0;

/*__POST__*/

}

