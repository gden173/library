#ifdef INOPENCL

// OpenCL build specific
#define CTYPE REAL

#else

// C++ build specific
namespace Geostack
{
namespace DateCalculation
{

#endif

// Julian day from year, month and day
#ifdef INOPENCL
CTYPE Julian_date_from_date(int yy, int mm, int dd) {
#else
template<typename CTYPE>
CTYPE JulianDateFromDate(int yy, int mm, int dd) {
#endif
  
  // Calculate Julian day
  if (mm <= 2) { yy--; mm+=12; }
  
  int cA = yy/100;
  int cB = cA/4;
  int cC = 2-cA+cB;
  int cE = (int)(365.25*(yy+4716));
  int cF = (int)(30.6001*(mm+1));

  return (CTYPE)(cC+dd+cE+cF)-1524.5;
}

// Julian day from year, month and day (YYYYMMDD)
#ifdef INOPENCL
CTYPE Julian_date_from_yyyymmdd(int YYYYMMDD) {
#else
template<typename CTYPE>
CTYPE JulianDateFromYYYYMMDD(int YYYYMMDD) {
#endif
  
  int dd = YYYYMMDD - (YYYYMMDD / 100) * 100;
  int mm = ((YYYYMMDD / 100) * 100 - (YYYYMMDD / 10000) * 10000) / 100;
  int yy = YYYYMMDD / 10000;

  // Calculate Julian day
  if (mm <= 2) { yy--; mm+=12; }
  
  int cA = yy/100;
  int cB = cA/4;
  int cC = 2-cA+cB;
  int cE = (int)(365.25*(yy+4716));
  int cF = (int)(30.6001*(mm+1));

  return (CTYPE)(cC+dd+cE+cF)-1524.5;
}

// Julian day from epoch seconds
#ifdef INOPENCL
CTYPE Julian_date_from_epoch(CTYPE s) {
#else
template<typename CTYPE>
CTYPE JulianDateFromEpoch(CTYPE s) {
#endif
  
  // Calculate Julian day
  return (s/86400.0)+2440587.5;
}

#ifndef INOPENCL

// C++ build specific
}
}

#endif

