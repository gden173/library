#!/usr/bin/env bash

# reference: https://github.com/inducer/pyopencl/blob/main/scripts/build-ocl.sh

set -e -x

if [[ $# -ge 1 ]];then
    INSTALL_DIR=$(realpath $1)
else
    INSTALL_DIR=`pwd`
fi

mkdir -p $(dirname ${INSTALL_DIR})/deps
cd $(dirname ${INSTALL_DIR})/deps

git clone --branch v2.3.1 https://github.com/OCL-dev/ocl-icd
cd ocl-icd
curl -L -O https://raw.githubusercontent.com/conda-forge/ocl-icd-feedstock/e2c03e3ddb1ff86630ccf80dc7b87a81640025ea/recipe/install-headers.patch

git apply install-headers.patch
curl -L -O https://github.com/isuruf/ocl-icd/commit/307f2267100a2d1383f0c4a77344b127c0857588.patch
git apply 307f2267100a2d1383f0c4a77344b127c0857588.patch
sed -i 's/pyopencl/geostack/g' ocl_icd_loader.c
sed -i 's/PYOPENCL/GEOSTACK/g' ocl_icd_loader.c

autoreconf -i
chmod +x configure
./configure --prefix=$INSTALL_DIR
make -j4
make install

# Bundle license files
echo "Geostack wheel includes ocl-icd which is licensed as below" >> ${INSTALL_DIR}/LICENSE
cat $(dirname ${INSTALL_DIR})/deps/ocl-icd/COPYING >> ${INSTALL_DIR}/LICENSE