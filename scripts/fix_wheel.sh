#!/bin/bash

function fix_wheel() {
    local wheel_source wheel_destination
    
    wheel_source=$1
    wheel_destination=$2
    
    set -x
    
    wheelname=$(find $wheel_source -type f -name '*.whl')
    echo $wheelname
    ulimit -m "$(python -c 'print(1024*1024*10)')" || true
    platform=$(auditwheel show $wheelname | grep -oE 'manylinux_([0-9]+)_([0-9]+)_([a-z0-9]+)_([0-9]+)')
    echo $platform $wheelname

    # work around to handle name and package issue in auditwheel
    temp_wheel_name=$(basename $wheelname)
    temp_wheel_name=${temp_wheel_name/_py/}
    
    mv $wheelname $(dirname $wheelname)/$temp_wheel_name
    
    auditwheel repair --plat $platform -w $wheel_destination \
        --lib-sdir=/.libs $(dirname $wheelname)/$temp_wheel_name

    if [[ -f $wheel_destination/$temp_wheel_name ]];then
        mv $wheel_destination/$temp_wheel_name $wheel_destination/$(basename $wheelname);
    else
        temp_wheel_name=$(echo $temp_wheel_name | sed "s|-linux_x86_64|-${platform}|g")
        if [[ -f $wheel_destination/$temp_wheel_name ]];then
            new_wheel_name=$(basename $wheelname)
            new_wheel_name=$(echo $new_wheel_name | sed "s|-linux_x86_64|-${platform}|g")
            mv $wheel_destination/$temp_wheel_name $wheel_destination/$new_wheel_name
        fi
    fi    
}

export -f fix_wheel

if [[ $# -eq 2 ]];then
    fix_wheel $1 $2
    exit 0
else
    echo 'Unable to fix wheel'
    exit 1
fi